﻿using FFVII.Database;
using UnityEditor;
using UnityEngine;

public class EWGraphAI : EWGraphTemplate {
    public static EWGraphAI Window;

    private void OnEnable() {
        Window = this;
    }

    private void OnDisable() {
        Window = null;
    }

    public static void Open(NodeGraphAI _aiGraph) {
        if (!Window) {
            Window = GetWindow<EWGraphAI>();
        }

        Window.selectedGraph = _aiGraph;

        Window.Initialize();
        Window.Show();

        Window.selectedGraph = _aiGraph;
        //Window.selectedGraph.blackboard.parentGraph = _aiGraph;
        Window.titleContent = new GUIContent("AI Graph" + " - " + _aiGraph.enemy.name);
        Window.styleSet = (StyleSet)Resources.Load("StyleSets/StyleSet");

        Window.pan = Window.position.size / 3;

        Window.Repaint();
    }

    protected override void Initialize() {
        styleSet = (StyleSet)Resources.Load("StyleSets/StyleSet");
        pan = position.size / 3;
    }

    public void UpdateSelection(NodeGraphAI _aiGraph) {
        if ((NodeGraphAI)selectedGraph != null) {
            ((NodeGraphAI)selectedGraph).Save();
        }

        selectedGraph = _aiGraph;
        Window.titleContent = new GUIContent("AI Graph" + " - " + _aiGraph.enemy.name);
        Window.pan = Window.position.size / 3;

        Window.Repaint();
    }

    protected override void Save() {
        Debug.Log("Saving");
        if (selectedGraph != null) {
            //EditorUtility.SetDirty(((NodeGraphAI)selectedGraph).enemy);
            EditorUtility.SetDirty(DBResources.GetEnemies);
        }
    }
}
