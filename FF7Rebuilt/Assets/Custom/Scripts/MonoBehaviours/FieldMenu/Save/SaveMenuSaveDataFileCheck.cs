using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SaveMenuSaveDataFileCheck : MonoBehaviour {
    [SerializeField] private Image imgProgressBar;
    [SerializeField] private SaveMenuSaveGameList saveGameList;
    private ScreenFieldSave screenFieldSave;

    public void Initialize(ScreenFieldSave _screenFieldSave) {
        screenFieldSave = _screenFieldSave;

        saveGameList.Initialize(screenFieldSave);

        Disable();
    }

    public void Open(int _selectedIndex) {
        Show();

        screenFieldSave.SetDescription("Checking Save Data File.");

        FieldMenu.Instance.menuControllable = null;

        StartCoroutine(OpenSaveDataFile());

        IEnumerator OpenSaveDataFile() {
            imgProgressBar.fillAmount = 0;

            yield return new WaitForSeconds(.2f);

            float t = 0;
            float duration = .8f;

            while (t < duration) {
                imgProgressBar.fillAmount = t / duration;

                t += Time.deltaTime;
                yield return null;
            }

            imgProgressBar.fillAmount = 1;

            yield return new WaitForSeconds(.2f);

            saveGameList.Open(_selectedIndex);
            Disable();
        }
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Disable() {
        gameObject.SetActive(false);
    }
}
