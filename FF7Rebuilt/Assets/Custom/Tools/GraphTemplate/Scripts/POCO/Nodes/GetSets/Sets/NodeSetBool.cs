﻿using System;
using UnityEngine;

[Serializable]
public class NodeSetBool : NodeSet {
    public NodeSetBool(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        minMaxThresholds = new Vector2(155, 175);
        size.y += 20;

        AddPortIn<PortBool>("Value", Port.Capacity.SINGLE);
        AddPortOut<PortSetBoolFunction>();
    }

    public enum Function {
        Set
    ,   Toggle
    }

    public Function function;
    private NodeGraph nodeGraph;
    private Vector2 offset;
    private Category blackboardField;

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        function = (Function)portContainerOut.ports[1].GetValue();
        portContainerIn.ports[1].IsActive = function == Function.Set;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        NodeSetBool newNode = new NodeSetBool(_nodeGraph, position + duplicationOffset, category, false);
        newNode.portContainerIn.ports[1].SetValue(portContainerIn.ports[1].GetValue());
        newNode.portContainerOut.ports[1].SetValue(portContainerOut.ports[1].GetValue());

        // do this for gets too.
        newNode.Initialize(field);

        return newNode;
    }

    public override void Run(NodeGraph _nodeGraph) {
        ((FieldBool)field).value = portContainerIn.ports[1].GetValue();
    }
}
