using CharacterTypes;
using FFVII.Database;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuCheckEnemySkillList : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private RectTransform optionsParent;

    private RectTransform scrollBarFG;
    private int selectedIndex;
    private MateriaMenuCheck check;
    private MateriaMenuCheckCommand commandWindow;
    private MateriaMenuCheckEnemySkillInfo[] arrOptions;

    // MAYBE REFACTOR
    [SerializeField] private RectTransform additionalOptionParent;
    private MateriaMenuCheckEnemySkillInfo[] additionalOptions;
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 2;
    private Ability[] arrViewItems = new Ability[0];
    //private Materia[] arrItems;

    private Player GetSelectedCharacter => check.GetSelectedCharacter;

    public void Initialize(MateriaMenuCheck _check, MateriaMenuCheckCommand _commandWindow) {
        check = _check;
        commandWindow = _commandWindow;
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        GridLayoutGroup glgOptions = GetComponentInChildren<GridLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(glgOptions.GetComponent<RectTransform>());

        arrViewItems = DBResources.GetAbilityList(AbilityType.EnemySkill).ToArray();

        arrOptions = optionsParent.GetComponentsInChildren<MateriaMenuCheckEnemySkillInfo>(true);
        foreach (MateriaMenuCheckEnemySkillInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOptions = additionalOptionParent.GetComponentsInChildren<MateriaMenuCheckEnemySkillInfo>(true);
        foreach (MateriaMenuCheckEnemySkillInfo info in additionalOptions) {
            info.Initialize();
        }

        ClearAdditional();

        Disable();

        cursor.Initialize(arrOptions[0].rct);

        listSpace = glgOptions.spacing.y;
        listHeight = arrOptions[0].rct.rect.height;
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = lowBound + i;
            arrOptions[i].Refresh(GetSelectedCharacter, arrViewItems, itemIndex);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - columns;
        MateriaMenuCheckEnemySkillInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOptionParent.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        for (int i = 0; i < additionalOptions.Length; i++) {
            Ability displayedAbility = arrViewItems[anchoredIndex + i];
            additionalOptions[i].Refresh(GetSelectedCharacter, arrViewItems, anchoredIndex + i);
        }
    }

    private void ClearAdditional() {
        foreach (var info in additionalOptions) {
            info.Clear();
        }
    }

    public void ResetSelection() {
        selectedIndex = 0;
        lowBound = 0;

        UpdateScrollBar();
        SetCursor();
    }

    public void SetCursor() {
        MateriaMenuCheckEnemySkillInfo selectedItemInfo = arrOptions[selectedIndex];

        cursor.SetCursorPosition(selectedItemInfo.rct);

        int abilityIndex = lowBound + selectedIndex;
        bool isUnlocked = selectedItemInfo.GetSkillUnlocked(GetSelectedCharacter, abilityIndex);
        Ability enemySkill = isUnlocked ? arrViewItems[abilityIndex] : null;

        check.SetDescription(enemySkill?.description);
        check.SetInfoEnemySkill(enemySkill);
    }

    public void Open() {
        lowBound = 0;

        //if (arrViewItems.Length <= 0) {
        //    return;
        //}

        //CheatUnlockRandomEnemySkills();

        Show();
        SetControllable();
        Refresh();
        ResetSelection();
        FieldMenu.Instance.SetCooldownTraverse(0);

        //void CheatUnlockRandomEnemySkills() {
        //    EnemySkillManager enemySkillManager = GetSelectedCharacter.materiaWeapon.Concat(GetSelectedCharacter.materiaArmor).FirstOrDefault(x => x.isEnemySkill).enemySkillManager;
        //    for (int i = 0; i < enemySkillManager.skillsUnlocked.Length; i++) {
        //        enemySkillManager.skillsUnlocked[i] = Random.Range(0, 2) == 0;
        //    }
        //}
    }

    public void Disable() {
        cursor.Hide();
        ClearAdditional();

        gameObject.SetActive(false);
    }

    public void Close() {
        Disable();
        commandWindow.ExitSubMenu();
        check.CloseInfoWindow();
    }

    private void Show() {
        gameObject.SetActive(true);
        SetScrollbarHeight();
        UpdateScrollBar();
        cursor.Show();

        void SetScrollbarHeight() {
            float scrollBarHeight = scrollBarBG.sizeDelta.y;
            scrollBarHeight *= arrOptions.Length / (float)arrViewItems.Length;
            scrollBarHeight = Mathf.RoundToInt(scrollBarHeight);
            scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarHeight);
        }
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        float scroll = ((float)lowBound / columns) / (arrViewItems.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

#region Override methods.
    public override void GetInputConfirmPressed() { }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex <= columns && _v > 0) || (lowBound + selectedIndex >= arrViewItems.Length - columns && _v < 0)) return;

        int v = -_v * columns;
        selectedIndex += v;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
            lowBound -= lowBound % columns;
        }

        if (selectedIndex < 0) {
            selectedIndex += columns;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= columns;
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _h < 0) || (lowBound + selectedIndex == arrViewItems.Length - columns && _h > 0)) return;

        selectedIndex += _h;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += _h * columns;
            lowBound -= lowBound % columns;

            if (isTooHigh) {
                selectedIndex -= columns;
            } else if (isTooLow) {
                selectedIndex += columns;
            }
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        SetCursor();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(_h));
        }
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;

            ClearAdditional();
        }

        int v = _pageDirection * (3 * columns);
        int lowBoundMax = arrViewItems.Length - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
    }

    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
