using UnityEngine.UIElements;

public class EditorItems : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Inventory/editor-items.uxml";
    protected override string Name { get; set; } = "editor-items";

    public EditorItems() : base() {
    }
}
