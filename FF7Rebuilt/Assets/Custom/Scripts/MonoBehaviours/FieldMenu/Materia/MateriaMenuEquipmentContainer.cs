using InventoryTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuEquipmentContainer : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtEquipmentName;
    [SerializeField] private Image[] arrImgLinks;
    private Image[] arrImgSlots;

    public void Initialize() {
        arrImgSlots = new Image[transform.childCount];

        for (int i = 0; i < transform.childCount; i++) {
            arrImgSlots[i] = transform.GetChild(i).GetComponent<Image>();
        }

        Clear();
    }

    public void Clear() {
        HideAllSlots();

        if (txtEquipmentName) {
            txtEquipmentName.text = "-";
        }

        void HideAllSlots() {
            foreach (Image img in arrImgSlots) {
                img.enabled = false;
            }

            foreach (Image img in arrImgLinks) {
                img.enabled = false;
            }
        }
    }

    public void Refresh(Equipment _target) {
        Clear();

        if (txtEquipmentName) {
            txtEquipmentName.text = _target.name;
        }

        RefreshSlots();
        RefreshLinks();

        void RefreshSlots() {
            for (int i = 0; i < _target.materiaSlotCount; i++) {
                arrImgSlots[i].enabled = true;

                bool isGrowthNone = _target.materiaGrowth == MateriaGrowthType.None;
                bool isGrowthNothing = _target.materiaGrowth == MateriaGrowthType.Nothing;
                Sprite sprSlotNoGrowth = ItemIconContainer.GetInstance().sprSlotNoGrowth;
                Sprite sprSlotNormal = ItemIconContainer.GetInstance().sprSlotNormal;

                arrImgSlots[i].sprite = isGrowthNone || isGrowthNothing ? sprSlotNoGrowth : sprSlotNormal;
            }
        }

        void RefreshLinks() {
            for (int i = 0; i < _target.links; i++) {
                arrImgLinks[i].enabled = true;
            }
        }
    }
}
