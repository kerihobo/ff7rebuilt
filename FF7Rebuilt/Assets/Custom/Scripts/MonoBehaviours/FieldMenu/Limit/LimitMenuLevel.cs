using CharacterTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LimitMenuLevel : MenuControllable {
    public TextMeshProUGUI txtTitle { get; set; }
    public UILimitInfo[] arrOptions { get; set; }
    public bool isAvailable { get; set; }

    [SerializeField] private RectTransform optionsParent;
    private int selectedIndex;
    private ScreenFieldLimit screenFieldLimit;
    private LimitMenuLevelSelect levelSelect;
    private FingerCursorRight cursor;

    //private Inventory GetInventory() => GameManager.Instance.gameData.inventory;

    public void Initialize(LimitMenuLevelSelect _levelSelect, FingerCursorRight _cursor, ScreenFieldLimit _screenFieldLimit) {
        levelSelect = _levelSelect;
        cursor = _cursor;
        screenFieldLimit = _screenFieldLimit;

        txtTitle = GetComponent<TextMeshProUGUI>();
        
        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<UILimitInfo>();
        foreach (UILimitInfo info in arrOptions) {
            info.Initialize();
        }

        Disable();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rct);

        screenFieldLimit.SetDescription(arrOptions[selectedIndex].limit?.description);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void Open() {
        cursor.Show();
        ResetSelection();
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void Disable() {
        cursor.Hide();
    }

    private void Close() {
        Disable();
        levelSelect.Open();
    }

    public void Refresh(Player _selectedCharacter, int _level) {
        isAvailable = true;
        txtTitle.color = TextColourContainer.GetInstance().white;
        
        LimitBreakManager limitManager = _selectedCharacter.limitBreakManager;
        LimitBreakManager.LimitLevel limitLevel = limitManager.limitLevels[_level];
        bool hasThisManyLevels = limitManager.availableLevels > _level;
        bool isLevelUnlocked = limitLevel.isUnlocked;
        
        if (!isLevelUnlocked || !hasThisManyLevels) {
            arrOptions[0].Refresh(null);
            arrOptions[1].Refresh(null);

            txtTitle.color = TextColourContainer.GetInstance().grey;
            isAvailable = false;

            return;
        }

        arrOptions[0].Refresh(limitLevel.limitBreakA.GetSelectedAbility(AbilityType.LimitBreak));

        if (limitLevel.usesNeeded == 0 || limitLevel.usesCount < limitLevel.usesNeeded) {
            arrOptions[1].Refresh(null);
            return;
        }

        arrOptions[1].Refresh(limitLevel.limitBreakB.GetSelectedAbility(AbilityType.LimitBreak));
    }

#region Override methods.
    public override void GetInputConfirm() { }
    public override void GetInputCancelPressed() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputSquarePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputConfirmPressed() {
    }

    public override void GetInputCancel() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (arrOptions[1].limit == null) return;

        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }
#endregion
}
