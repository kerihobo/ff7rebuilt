﻿using CharacterTypes;
using InventoryTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class IndependentManager {
    public static void AddIndependentMateriaBonuses(Player _player, EquipEffect _equipEffect) {
        IEnumerable<Materia> independentMateria = _player.Materia.Where(x => x.type == MateriaType.Independent);

        foreach (Materia m in independentMateria) {
            IndependentManager independentManager = m.independentManager;
            int boost = m.GetBonusValue();

            switch (m.subtype) {
                case 1:  // HP<->MP
                    _equipEffect.isHPSwitchMP = true;
                    break;
                case 2:  // Underwater
                    _equipEffect.isUnderWater = true;
                    break;
                case 3:  // Cover
                    _equipEffect.coverChance += boost;
                    break;
                //case 4: // Counter Attack
                //    if (equipEffectsTotal.counterAttackMateria.Count < 8) {
                //        equipEffectsTotal.counterAttackMateria.Add(m);
                //    }

                //    break;
                case 5:  // Mega All
                    Debug.Log("MEGA ALL: " + m.GetBonusValue());
                    _equipEffect.megaAll += m.GetBonusValue();
                    break;
                case 7:  // Pre-Emptive
                    _equipEffect.preEmptiveChance += boost;
                    break;
                case 8:  // Chocobo Lure
                    _equipEffect.chocoboEncounterRate += boost;
                    break;
                case 9:  // Enemy Lure
                    _equipEffect.encounterRate += boost;
                    break;
                case 10: // Enemy Away
                    _equipEffect.encounterRate -= boost;
                    break;
                case 11: // Gil Plus
                    _equipEffect.gilDropped += boost;
                    break;
                case 12: // EXP Plus
                    _equipEffect.expEarned += boost;
                    break;
                case 13: // Luck Plus
                    _equipEffect.primary.luck += _player.primary.luck * (boost / 100);
                    break;
                case 14: // Magic Plus
                    _equipEffect.primary.magic += _player.primary.magic * (boost / 100);
                    break;
                case 15: // Speed Plus
                    _equipEffect.primary.dexterity += _player.primary.dexterity * (boost / 100);
                    break;
                case 16: // HP Plus
                    _equipEffect.maxHPPercent += boost;
                    break;
                case 17: // MP Plus
                    _equipEffect.maxMPPercent += boost;
                    break;
            }
        }
    }

    public static void ApplyMegaAll(Player _player) {
        if (_player.megaAll < 1) return;

        List<Materia> magicMateria = _player.Materia.Where(x => x.type == MateriaType.Magic).ToList();
        List<Ability> lsAbilities = new List<Ability>();

        Materia master = magicMateria.FirstOrDefault(x => x.IsMaster);
        if (master != null) {
            lsAbilities = _player.lsAvailableMagic;
        } else {
            foreach (Materia m in magicMateria) {
                int level = m.GetLevel();
                int spellCount = level + 1;

                for (int i = 0; i < spellCount; i++) {
                    Ability spell = m.selectionSpells[i].GetSelectedAbility(AbilityType.Magic);

                    if (spell == null) continue;
                    lsAbilities.Add(spell);
                }
            }

            lsAbilities = lsAbilities.Distinct().ToList();
        }

        foreach (Ability spell in lsAbilities) {
            if (_player.dctAbilityAugmentations.TryGetValue(spell, out Player.AbilityAugmentation augmentation)) {
                augmentation.all += _player.megaAll;
            }
        }
    }
}
