﻿using System;
using UnityEngine;

[Serializable]
public class NodeClamp : Node {
    public NodeClamp(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(165, 95);
        name = "Clamp";

        AddPortIn<PortInt>("In", Port.Capacity.SINGLE);
        AddPortIn<PortInt>("Min", Port.Capacity.SINGLE);
        AddPortIn<PortInt>("Max", Port.Capacity.SINGLE);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(110, 175);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();
        int c = portContainerIn.ports[2].GetValue();

        return Mathf.Clamp(a, b, c);
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = new NodeClamp(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeClamp)newNode).portContainerIn.ports[0].SetValue(portContainerIn.ports[0].GetValue());
        ((NodeClamp)newNode).portContainerIn.ports[1].SetValue(portContainerIn.ports[1].GetValue());
        ((NodeClamp)newNode).portContainerIn.ports[2].SetValue(portContainerIn.ports[2].GetValue());

        return newNode;
    }
}