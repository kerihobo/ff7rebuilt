﻿using CharacterTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Ability : Data {
    public enum MagicOrder {
        NONE
    ,   RESTORE
    ,   ATTACK
    ,   INDIRECT
    }

    public Ability() : base() {
        name = "New Ability";
        mpCost = 4;
        power = 16;
        attackPercent = 95;
    }

    public Ability(string _name, AbilityType _type) {
        name = _name;
        type = _type;
    }

    public Ability(Ability _source) : base(_source) {
        description         = _source.description;
        power               = _source.power;
        attackPercent       = _source.attackPercent;
        mpCost              = _source.mpCost;
        magicOrder          = _source.magicOrder;
        remainingAllCount   = _source.remainingAllCount;
        prefabEffect        = _source.prefabEffect;
        elements = _source.elements;
        statusEffectManager       = _source.statusEffectManager;
        type                = _source.type;
        targetOptions       = _source.targetOptions;
        executionProperties = _source.executionProperties;
        accuracyCalculation          = _source.accuracyCalculation;
        damageFormula       = _source.damageFormula;
        specialEffect       = _source.specialEffect;
    }

    [Serializable]
    public class TargetOptions {
        // See target options in the Proud Clod or Scarlet tools.
        // Note that they want the scene.bin file from the FF7 installation location.

        /// <summary>
        /// Enable if a cursor should appear at all.
        /// This will be on for most abilities.
        /// Examples where it won't include commands Defend & Mime.
        /// </summary>
        public bool isEnableSelection = true;
        /// <summary>
        /// Enable to make the cursor start on enemy rows first.
        /// </summary>
        public bool isStartOnEnemies = true;
        /// <summary>
        /// Enable to force multiple target selection.
        /// </summary>
        public bool isMultipleTargetsByDefault;
        /// <summary>
        /// Enable to allow R1 to toggle between Single/Multiple targets provided "All" is available.
        /// </summary>
        public bool isToggleSingleMultipleTargets;
        /// <summary>
        /// Enable to prevent arrow-keys from switching between player & enemy rows.
        /// </summary>
        public bool isOneRowOnly;
        /// <summary>
        /// Not sure what this is used for. Couldn't find any examples in the Scarlet tool that used it.
        /// </summary>
        public bool isShortRange;
        /// <summary>
        /// Enable to include all characters, whether they're players or enemies, in the targetting.
        /// An example would be Roulette.
        /// </summary>
        public bool isAllRows;
        /// <summary>
        /// Enable to allow any of the characters in the target list (assuming isMultipleTargetsByDefault is enabled)
        /// to be hit when the damage/effect is dealt.
        /// </summary>
        public bool isRandomTarget;
        public bool isFallen;
    }

    [Serializable]
    public class ExecutionProperties {
        public bool isAffectsMP;
        public bool isDrainsSomeDamage;
        public bool isIgnoreStatusDefense;
        public bool isIgnoreDefense;
        public bool isDrainHPAndMP;
        public bool isMissIfAlive;
        public bool isNoRetargetIfDead;
        public bool isAffectedByDarkness;
        public bool isReflectable;
        public bool isAlwaysCriticalHit;
    }

    public string description;
    public int power = 16;
    public int attackPercent = 95;
    public int mpCost = 4;
    public MagicOrder magicOrder;
    // TODO: Derive from materia.
    public int remainingAllCount;
    public GameObject prefabEffect;
    [SerializeReference]
    public ElementalAffinityManager elements = new ElementalAffinityManager(true);
    [SerializeReference]
    public StatusEffectManager statusEffectManager = new StatusEffectManager();
    public AbilityType type; 
    [SerializeReference]
    public TargetOptions targetOptions = new TargetOptions();
    [SerializeReference]
    public ExecutionProperties executionProperties = new ExecutionProperties();
    public int accuracyCalculation = 0;
    public int damageFormula = 0;
    public int cursorAction = 0;
    public int restoreType = 0;
    public int specialEffect = 0;
    public bool isPreventQuadraMagic;

    // Need to sort targetting sooner than this so that Tent & Megalixer can work.
    // Not to mention Cure+All.
    public bool TryUse(Player _user, Player _target, bool _isAll, bool _isQuadraMagic) {
        StatusEffectManager statusEffectManagerCopy = new StatusEffectManager(statusEffectManager);
        List<StatusEffectManager.StatusEffect> abilityEffects = statusEffectManagerCopy.GetAsList();
        List<StatusEffectManager.StatusEffect> targetAilments = _target.statusCurrent.GetAsList();
        List<StatusEffectManager.StatusEffect> targetImmunities = _target.statusDefense.GetAsList();

        bool isPassUsabilityTest = IsPassUsabilityTest();
        Debug.Log(isPassUsabilityTest);
        if (!isPassUsabilityTest) return false;

        ProcessStatusChanges();
        
        int damage = DamageFormula.CalculateTotal(this, _user, _target);
        //Not sure which exact formulae should apply. The ones I've chosen seem probable.
        // Check Formula Modifiers (3.4.6): https://gamefaqs.gamespot.com/ps/197341-final-fantasy-vii/faqs/22395
        // Not sure if cure spells should become less effective due to Sadness though.
        switch (damageFormula) {
            case 0: // No Damage
            case 1: // (Power ÷ 16) * (Stat + ((Lv + Stat) ÷ 32) * ((Lv * Stat) ÷ 32))
            case 2: // (Power ÷ 16) * ((Lv * Stat) * 6)
            case 5: // (Power * 22) + ((Lv + Stat) * 6)
            case 6: // Power * 16
            case 7: // Power * 20
            case 8: // Power ÷ 32
                if (_isQuadraMagic) {
                    damage /= 2;
                } else if (_isAll) {
                    Debug.Log($"before: {damage}");
                    damage = (int)(damage * (2 / 3f));
                    Debug.Log($"after: {damage}");
                }

                if (_target.statusCurrent.sadness.isActive) {
                    damage -= (int)(damage * (3 / 10f));
                }
                break;
            default:
                break;
        }

        if (damageFormula == 9) { // Fully recovers HP & MP
            RecoverAllHPMP(_target);
            return true;
        //} else {
        }

        DealDamage(_target, damage);

        return true;

        bool IsPassUsabilityTest() {
            if (executionProperties.isMissIfAlive && _target.hpCurrent > 0) {
                Debug.Log("Can only use on dead targets");
                return false;
            }

            bool isHPFull = _target.hpCurrent >= _target.points.hpMaxTotal;
            bool isMPFull = _target.mpCurrent >= _target.points.mpMaxTotal;
            bool isAlive = _target.hpCurrent > 0;
            bool isAttemptRevive = statusEffectManagerCopy.IsRecover && statusEffectManagerCopy.death.isActive;

            if (elements.restorative.isActive) {
                if (damageFormula == 9) { // Fully recovers HP & MP
                    return isAlive && !(isHPFull && isMPFull);
                }

                if (executionProperties.isAffectsMP) {
                    return !(isMPFull || !isAlive);
                } else {
                    bool isAttemptReviveOnLiving = isAlive && isAttemptRevive;
                    bool isAttemptingToCureTheDead = !isAlive && !isAttemptRevive;

                    return !(isHPFull || isAttemptReviveOnLiving || isAttemptingToCureTheDead);
                }
            }

            bool canProcessStatus = false;

            if (statusEffectManagerCopy.IsRecover) {
                canProcessStatus = targetAilments.Any((x) => x.isActive && abilityEffects[targetAilments.IndexOf(x)].isActive);
                Debug.Log($"canHeal({canProcessStatus})");
            } else {
                canProcessStatus = abilityEffects.Any((x) => x.isActive && !targetAilments[abilityEffects.IndexOf(x)].isActive);
                Debug.Log($"canApply({canProcessStatus})");
            }

            return canProcessStatus;
        }

        void ProcessStatusChanges() {
            if (statusEffectManagerCopy.sadness.isActive && _target.statusCurrent.fury.isActive) {
                _target.statusCurrent.fury.isActive = false;
                statusEffectManagerCopy.sadness.isActive = false;
            } else if (statusEffectManagerCopy.fury.isActive && _target.statusCurrent.sadness.isActive) {
                _target.statusCurrent.sadness.isActive = false;
                statusEffectManagerCopy.fury.isActive = false;
            }

            if (statusEffectManagerCopy.IsRecover) {
                for (int i = 0; i < abilityEffects.Count; i++) {
                    if (abilityEffects[i].isActive && targetAilments[i].isActive) {
                        targetAilments[i].isActive = false;
                    }
                }
            } else {
                for (int i = 0; i < abilityEffects.Count; i++) {
                    if (abilityEffects[i].isActive && !targetImmunities[i].isActive) {
                        targetAilments[i].isActive = true;
                    }
                }
            }
        }
    }

    private void RecoverAllHPMP(Player _player) {
        _player.hpCurrent = _player.points.hpMaxTotal;
        _player.mpCurrent = _player.points.mpMaxTotal;
    }

    private void DealDamage(Player _target, int _total) {
        // In my imagination it works like this:
        // prioritize the lowest % from -100 (absorb) up to 200% (weakness), barring 100% (normal).
        // I need to find out what actually happens when multiple elements in 1 attack are all of the above to an enemy.
        // Try Kjata on an enemy that absorbs fire, halves ice and is weak to lightning.
        List<ElementalAffinityManager.Affinity> abilityElements = elements.GetAsList();
        List<ElementalAffinityManager.Affinity> targetAbsorption = _target.elementAbsorb .GetAsList();
        List<ElementalAffinityManager.Affinity> targetInvalid    = _target.elementInvalid.GetAsList();
        List<ElementalAffinityManager.Affinity> targetHalve      = _target.elementAbsorb .GetAsList();
        List<ElementalAffinityManager.Affinity> targetWeak       = _target.elementAbsorb .GetAsList();
        bool hasAbsorb  = abilityElements.Any((x) => x.isActive && targetAbsorption[abilityElements.IndexOf(x)].damageModifier == ElementalAffinityManager.DamageModifier.ABSORB);
        bool hasInvalid = abilityElements.Any((x) => x.isActive && targetInvalid   [abilityElements.IndexOf(x)].damageModifier == ElementalAffinityManager.DamageModifier.INVALID);
        bool hasHalve   = abilityElements.Any((x) => x.isActive && targetHalve     [abilityElements.IndexOf(x)].damageModifier == ElementalAffinityManager.DamageModifier.HALVE);
        bool hasWeak    = abilityElements.Any((x) => x.isActive && targetWeak      [abilityElements.IndexOf(x)].damageModifier == ElementalAffinityManager.DamageModifier.WEAK);

        if (hasHalve) {
            _total = (int)(_total / 2f);
        } else if (hasWeak) {
            _total *= 2;
        }
        
        if (hasInvalid) {
            _total = 0;
        }
        
        if (hasAbsorb) {
            _total = -_total;
        }
        Debug.Log($"half({hasHalve}), weak({hasWeak}), immune({hasInvalid}), absorb({hasAbsorb}),");
        Debug.Log(_total);

        if (executionProperties.isAffectsMP) {
            _target.mpCurrent = Mathf.Clamp(_target.mpCurrent - _total, 0, _target.points.mpMaxTotal);
        } else {
            _target.hpCurrent = Mathf.Clamp(_target.hpCurrent - _total, 0, _target.points.hpMaxTotal);
        }
    }
}
