﻿using UnityEditor;
using UnityEngine;

public class PortSetIntFunction : Port {
    private readonly string[] values = {
        "="
    ,   "++"
    ,   "--"
    };

    public NodeSetInt.Function value;

    protected override void RenderAsOutput(StyleSet _styleSet) {
        value = (NodeSetInt.Function)EditorGUILayout.Popup((int)value, values, GUILayout.Width(40));
    }

    public override void SetValue(dynamic _value) {
        if (_value == null) {
            _value = NodeSetInt.Function.Equal;
        }

        value = (NodeSetInt.Function)_value;
    }

    public override dynamic GetValue() {
        return (int)value;
    }
}
