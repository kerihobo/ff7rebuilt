﻿using System;
using System.Collections.Generic;
using FFVII.Database;
using UnityEngine;

[Serializable]
public class SelectionCollectible {
    public enum CollectibleType {
        Item
    ,   Weapon
    ,   Armor
    ,   Accessories
    }

    public string selectionName;
    public CollectibleType collectibleType = CollectibleType.Item;
    public SelectionWeapon selectionWeapon;
    public SelectionData selectionArmor;
    public SelectionData selectionAccessory;
    public SelectionData selectionItem;
    /// <summary>
    /// Only used for items, and typically only for specifying an amount to reward from a chest or in-game event.
    /// </summary>
    public int quantity = 1;

    public void ChangePrimarySelection() {
        switch (collectibleType) {
            case CollectibleType.Item:
                selectionItem.index = 0;
                selectionName = selectionItem.Reset(DBResources.GetItems.itemsList);
                break;
            case CollectibleType.Weapon:
                selectionWeapon.index = 0;
                selectionWeapon.Reset((List<Data>)null);
                selectionName = selectionWeapon.Reset(DBResources.GetWeaponsList(selectionWeapon.type));
                break;
            case CollectibleType.Armor:
                selectionArmor.index = 0;
                selectionName = selectionArmor.Reset(DBResources.GetArmor.armorList);
                break;
            case CollectibleType.Accessories:
                selectionAccessory.index = 0;
                selectionName = selectionAccessory.Reset(DBResources.GetAccessories.accessoryList);
                break;
            default:
                break;
        }
        Debug.Log(selectionName);
    }

    public Data GetSelectedCollectible() {
        Data item = null;

        switch (collectibleType) {
            case CollectibleType.Item:
                item = selectionItem.GetSelectedItem();
                break;
            case CollectibleType.Weapon:
                item = selectionWeapon.GetSelectedWeapon();
                break;
            case CollectibleType.Armor:
                item = selectionArmor.GetSelectedArmor();
                break;
            case CollectibleType.Accessories:
                item = selectionAccessory.GetSelectedAccessory();
                break;
        }

        return item;
    }

    public void SetDataExternally(string _selectionName) {
        selectionName = _selectionName;
    }
}