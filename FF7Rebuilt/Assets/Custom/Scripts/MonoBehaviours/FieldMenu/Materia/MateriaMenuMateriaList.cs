using CharacterTypes;
using InventoryTypes;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuMateriaList : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private UIMateriaInfo additionalOption;
    [SerializeField] private RectTransform optionsParent;

    private RectTransform scrollBarFG;
    private CanvasGroup cnvGListContainer;
    private int selectedIndex;
    private int selectedIndexArrange;
    private ScreenFieldMateria screenFieldMateria;
    private MateriaMenuExchange exchange;
    private UIMateriaInfo[] arrOptions;

    // MAYBE REFACTOR
    private int lowBound = 0;
    private int lowBoundArrange = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 1;
    private bool isArrange;

    private int LowBound {
        get { return isArrange ? lowBoundArrange : lowBound; }
        set {
            if (isArrange) {
                lowBoundArrange = value;
            } else {
                lowBound = value;
            }
        }
    }
    private int SelectedIndex {
        get {
            return isArrange ? selectedIndexArrange : selectedIndex;
        }
        set {
            if (isArrange) {
                selectedIndexArrange = value;
            } else {
                selectedIndex = value;
            }
        }
    }

    private Materia[] arrMateria               => GameManager.Instance.gameData.inventory.arrMateria;
    private Player selectedCharacter           => screenFieldMateria.GetSelectedCharacter;
    private MateriaMenuSlotsMain slotsMain     => screenFieldMateria.slotsMain;
    private MateriaMenuArrangeMenu arrangeMenu => screenFieldMateria.arrangeMenu;
    private MateriaMenuTrashMenu trashMenu     => screenFieldMateria.trashMenu;

    public void Initialize(ScreenFieldMateria _screenFieldMateria) {
        screenFieldMateria = _screenFieldMateria;
        cnvGListContainer = optionsParent.GetComponentInParent<CanvasGroup>(true);
        
        Initialize();
        Hide();
    }

    public void Initialize(MateriaMenuExchange _exchange) {
        exchange = _exchange;

        Initialize();
    }

    private void Initialize() {
        // MAYBE REFACTOR
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();
        scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarBG.sizeDelta.y * (10f / 200));

        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<UIMateriaInfo>();
        foreach (UIMateriaInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOption.Initialize();
        ClearAdditional();

        Disable();

        cursor.Initialize(arrOptions[0].rct);

        // MAYBE REFACTOR
        listSpace = vlgOptions.spacing;
        listHeight = arrOptions[0].rct.rect.height;
        //listHierarchy = arrOptions[0].rct.parent.GetComponent<RectTransform>();
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = LowBound + i;
            Materia selectedMateria = arrMateria[itemIndex];
            arrOptions[i].Refresh(selectedMateria);
        }
    }

    // MAYBE REFACTOR
    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = LowBound + arrOptions.Length - columns;
        UIMateriaInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = LowBound;
        }

        additionalOption.rct.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        Materia selectedMateria = arrMateria[anchoredIndex];
        additionalOption.Refresh(selectedMateria);
    }

    private void ClearAdditional() {
        additionalOption.Clear();
    }

    public void ResetSelection() {
        selectedIndex = 0;
        selectedIndexArrange = 0;

        lowBound = 0;
        lowBoundArrange = 0;

        UpdateScrollBar();
        SetCursor();
    }

    public void SetCursor() {
        UIMateriaInfo selectedItemInfo = arrOptions[SelectedIndex];

        cursor.SetCursorPosition(selectedItemInfo.rct);

        Materia selectedMateria = arrOptions[SelectedIndex].materia;
        screenFieldMateria.SetDescription(selectedMateria);
    }

    public void Open(bool _isArrange) {
        Show(_isArrange);
        cursor.Show();
        //ResetSelection();
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);

        SetCursor();
        screenFieldMateria.RefreshInfo(arrOptions[selectedIndex].materia);
    }

    private void Disable() {
        cursor.Hide();

        ClearAdditional();
        UpdateScrollBar();
        Refresh();
    }

    public void Hide() {
        cnvGListContainer.alpha = 0;
    }

    public void Show(bool _isArrange) {
        cnvGListContainer.alpha = 1;
        isArrange = _isArrange;

        UpdateScrollBar();
        Refresh();
    }

    public void ExitCharacterSelection() {
        cursor.SetFlickerState(false);
        SetControllable();
    }

    private void EquipMateria() {
        bool isWeapon = slotsMain.arrSelected == slotsMain.arrOptionsWeapon;
        Materia[] arrMateriaWeapon = selectedCharacter.materiaWeapon;
        Materia[] arrMateriaArmor = selectedCharacter.materiaArmor;
        Materia[] arrMateriaSelected = isWeapon ? arrMateriaWeapon : arrMateriaArmor;
        int iSlot = slotsMain.selectedIndex;
        int iMateria = LowBound + SelectedIndex;

        (arrMateria[iMateria], arrMateriaSelected[iSlot]) = (arrMateriaSelected[iSlot], arrMateria[iMateria]);

        Close();
    }

    private void Close() {
        Disable();

        if (isArrange) {
            arrangeMenu.Open();
        } else {
            Hide();
            slotsMain.ExitSubMenu();
            //screenFieldMateria.RefreshInfo(arrOptions[selectedIndex].materia);
        }
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;
        
        ClearAdditional();
        Refresh();
        SetCursor();
        screenFieldMateria.RefreshInfo(arrOptions[selectedIndex].materia);

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        float scroll = ((float)LowBound / columns) / (arrMateria.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        //Debug.Log($"progress({scroll:f3}), bgHeight({scrollBarBG.rect.height:f3}), potentialLoc({scroll})");
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

    public void ResetArrange() {
        lowBoundArrange = 0;
        selectedIndexArrange = 0;

        UpdateScrollBar();
        Refresh();
    }

    private void PromptTrash() {
        trashMenu.Open();
    }

    public void TrashSelectedMateria() {
        arrMateria[LowBound + SelectedIndex] = null;
        Refresh();
    }

#region Override methods.
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputConfirm() { }

    public override void GetInputConfirmPressed() {
        if (isArrange) {
            PromptTrash();
        } else {
            EquipMateria();
            selectedCharacter.RecalculatePlayerStats();
            screenFieldMateria.RefreshPlayerInfo();
        }
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (LowBound + SelectedIndex == 0 && _v > 0) || (LowBound + SelectedIndex == arrMateria.Length - columns && _v < 0)) return;
        //Debug.Log(LowBound + SelectedIndex + " : " + arrItems.Length);

        int v = -_v * columns;
        SelectedIndex += v;

        bool isTooLow = SelectedIndex < 0;
        bool isTooHigh = SelectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            LowBound += v;
            LowBound -= LowBound % columns;
        }

        if (SelectedIndex < 0) {
            SelectedIndex += columns;
        } else if (SelectedIndex > arrOptions.Length - 1) {
            SelectedIndex -= columns;
        }
        //SelectedIndex = Mathf.Clamp(SelectedIndex, 0, arrOptions.Length - 1);

        LowBound = Mathf.Clamp(LowBound, 0, arrMateria.Length - arrOptions.Length);
        LowBound -= LowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
            screenFieldMateria.RefreshInfo(arrOptions[selectedIndex].materia);
        }
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (isArrange) return;

        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;
            ClearAdditional();
        }

        int v = _pageDirection * (10 * columns);
        int lowBoundMax = arrMateria.Length - arrOptions.Length;
        LowBound += v;
        LowBound = Mathf.Clamp(LowBound, 0, lowBoundMax);
        LowBound -= LowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
        screenFieldMateria.RefreshInfo(arrOptions[selectedIndex].materia);
    }
#endregion
}
