using CharacterTypes;
using FFVII.Database;
using UnityEditor;
using UnityEngine.UIElements;

public class LimitBreakLevelContent : VisualElement {
    public new class UxmlFactory : UxmlFactory<LimitBreakLevelContent, UxmlTraits> { }

    public LimitBreakLevelContent() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        name = Name;
    }

    public Player target;
    public Database database;

    public int targetIndex;

    private const string UXMLPath = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/LimitBreaks/limit-break-level-content.uxml";
    private const string Name = "limit-break-level-content";
    private TemplateContainer tc;

    public LimitBreakLevelContent Initialize(Player _target, Database _database) {
        target = _target;
        database = _database;

        BindUniqueControls();
        Refresh(target);

        return this;
    }

    public void BindUniqueControls() {
        this.Q<IntegerField>("int-limit-gauge-capacity").SetValue(target.limitBreakManager.limitLevels[targetIndex].gaugeCapacity).RegisterValueChangedCallback(e => {
            target.limitBreakManager.limitLevels[targetIndex].gaugeCapacity = e.newValue;
            EditorUtility.SetDirty(database);
        });

        this.Q<IntegerField>("int-limit-uses-needed").SetValue(target.limitBreakManager.limitLevels[targetIndex].usesNeeded).RegisterValueChangedCallback(e => {
            target.limitBreakManager.limitLevels[targetIndex].usesNeeded = e.newValue;
            EditorUtility.SetDirty(database);

            this.Q<DropdownField>("ddf-limit-b").style.display = target.limitBreakManager.limitLevels[targetIndex].usesNeeded > 0 ? DisplayStyle.Flex : DisplayStyle.None;
        });
        
        this.Q<IntegerField>("int-limit-kills-needed").SetValue(target.limitBreakManager.limitLevels[targetIndex].killsNeeded).RegisterValueChangedCallback(e => {
            target.limitBreakManager.limitLevels[targetIndex].killsNeeded = e.newValue;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfLimitA = this.Q<DropdownField>("ddf-limit-a");
        ddfLimitA.Initialize(DBResources.GetAbilityList(AbilityType.LimitBreak), target.limitBreakManager.limitLevels[targetIndex].limitBreakA, database);
        ddfLimitA.RegisterValueChangedCallback(_e => {
            target.limitBreakManager.limitLevels[targetIndex].limitBreakA.SetDataExternally(ddfLimitA.index);
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfLimitB = this.Q<DropdownField>("ddf-limit-b");
        ddfLimitB.Initialize(DBResources.GetAbilityList(AbilityType.LimitBreak), target.limitBreakManager.limitLevels[targetIndex].limitBreakB, database);
        ddfLimitB.RegisterValueChangedCallback(_e => {
            target.limitBreakManager.limitLevels[targetIndex].limitBreakB.SetDataExternally(ddfLimitB.index);
            EditorUtility.SetDirty(database);
        });

        this.Q<TextField>("txt-incompatible").SetValue(target.limitBreakManager.responseSet.incompatible).RegisterValueChangedCallback(e => {
            target.limitBreakManager.responseSet.incompatible = e.newValue;
            EditorUtility.SetDirty(database);
        });

        this.Q<TextField>("txt-compatible").SetValue(target.limitBreakManager.responseSet.compatible).RegisterValueChangedCallback(e => {
            target.limitBreakManager.responseSet.compatible = e.newValue;
            EditorUtility.SetDirty(database);
        });

        this.Q<TextField>("txt-success").SetValue(target.limitBreakManager.responseSet.success).RegisterValueChangedCallback(e => {
            target.limitBreakManager.responseSet.success = e.newValue;
            EditorUtility.SetDirty(database);
        });
    }

    public void Refresh(Player _target) {
        target = _target;

        this.Q<Label>("lbl-limit-selected-index").text = (targetIndex + 1).ToString();
        this.Q<IntegerField>("int-limit-gauge-capacity").value = target.limitBreakManager.limitLevels[targetIndex].gaugeCapacity;
        this.Q<IntegerField>("int-limit-uses-needed").value = target.limitBreakManager.limitLevels[targetIndex].usesNeeded;
        this.Q<IntegerField>("int-limit-kills-needed").value = target.limitBreakManager.limitLevels[targetIndex].killsNeeded;

        this.Q<DropdownField>("ddf-limit-a").Initialize(DBResources.GetAbilityList(AbilityType.LimitBreak), target.limitBreakManager.limitLevels[targetIndex].limitBreakA, database);
        
        this.Q<DropdownField>("ddf-limit-b").Initialize(DBResources.GetAbilityList(AbilityType.LimitBreak), target.limitBreakManager.limitLevels[targetIndex].limitBreakB, database);
        this.Q<DropdownField>("ddf-limit-b").style.display = target.limitBreakManager.limitLevels[targetIndex].usesNeeded > 0 ? DisplayStyle.Flex : DisplayStyle.None;

        for (int i = 0; i < target.limitBreakManager.limitLevels.Length; i++) {
            target.limitBreakManager.limitLevels[i].limitBreakA.ForceNamesUpdate(DBResources.GetAbilityList(AbilityType.LimitBreak));
            target.limitBreakManager.limitLevels[i].limitBreakB.ForceNamesUpdate(DBResources.GetAbilityList(AbilityType.LimitBreak));
        }

        this.Q<TextField>("txt-incompatible").value = target.limitBreakManager.responseSet.incompatible;
        this.Q<TextField>("txt-compatible").value = target.limitBreakManager.responseSet.compatible;
        this.Q<TextField>("txt-success").value = target.limitBreakManager.responseSet.success;
    }
}
