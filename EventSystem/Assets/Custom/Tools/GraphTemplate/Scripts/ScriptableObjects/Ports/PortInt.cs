﻿using UnityEditor;
using UnityEngine;

public class PortInt : Port {
    public int value;

    public override void Initialize(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) {
        base.Initialize(_parentNode, _portName, _direction, _capacity, _displayMode);

        portType = NodeDataType.Int;
    }

    protected override void RenderField() {
        value = EditorGUILayout.IntField(value, GUILayout.MaxWidth(65));
    }

    public override dynamic GetValue() {
        if (direction == Direction.IN) {
            if (connectedPorts.Count > 0) {
                return connectedPorts[0].GetValue();
            }

            return value;
        }

        return parentNode.GetValue();
    }

    public override void SetValue(dynamic _value) {
        if (_value == null) {
            _value = 0;
        }

        value = _value;
    }
}
