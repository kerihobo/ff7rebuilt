using InventoryTypes;
using System;
using TMPro;
using UnityEngine;

public class MateriaMenuStatInfoAbilityListIndependent : MonoBehaviour {
    [Serializable]
    public class TextPair {
        public TextMeshProUGUI txtName;
        public TextMeshProUGUI txtValue;
    }

    public TextPair cover;
    public TextPair hpPlus;
    public TextPair mpPlus;
    public TextPair luckPlus;
    public TextPair magicPlus;
    public TextPair speedPlus;

    private TextPair[] arrTextPair;

    public void Initialize() {
        arrTextPair = new TextPair[] {
            cover
        ,   hpPlus
        ,   mpPlus
        ,   luckPlus
        ,   magicPlus
        ,   speedPlus
        };
    }


    // Use the following method to activate only the element we want, and populate its value field.
    public void Refresh(Materia _materia) {
        Clear();

        IndependentManager independent = _materia.independentManager;
        string bonus = _materia.GetBonusValue().ToString();

        switch (_materia.subtype) {
            case 3: // Cover
                SetText(cover);
                cover.txtValue.text = "0" + cover.txtValue.text;
                break;
            case 13: // LuckPlus
                SetText(luckPlus);
                break;
            case 14: // MagicPlus
                SetText(magicPlus);
                break;
            case 15: // SpeedPlus
                SetText(speedPlus);
                break;
            case 16: // HPPlus
                SetText(hpPlus);
                break;
            case 17: // MPPlus
                SetText(mpPlus);
                break;
        }

        void Clear() {
            foreach (TextPair textPair in arrTextPair) {
                textPair.txtName.gameObject.SetActive(false);
            }
        }
        
        void SetText(TextPair _textPair) {
            _textPair.txtName.gameObject.SetActive(true);
            _textPair.txtValue.text = bonus;
        }
    }
}
