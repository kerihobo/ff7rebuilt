﻿using System;
using UnityEngine;

[Serializable]
public class Character : Data {
    public Character() : base() {
        name = "New Character";
    }

    /// <summary>
    /// Copy constructor.
    /// </summary>
    /// <param name="_source"></param>
    public Character(Character _source) : base(_source) {
        hpCurrent        = _source.hpCurrent;
        mpCurrent        = _source.mpCurrent;
        points           = new Points(_source.points);
        primary          = new PrimaryStats(_source.primary);
        statusCurrent    = new StatusEffectManager(_source.statusCurrent);
        statusDefense     = new StatusEffectManager(_source.statusDefense);
    }

    [Serializable]
    public class Points {
        public Points() { }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Points(Points _source) {
            hpMaxBase           = _source.hpMaxBase;
            hpMaxTotal          = _source.hpMaxTotal;
            mpMaxBase           = _source.mpMaxBase;
            mpMaxTotal          = _source.mpMaxTotal;
            expToNextLevelTotal = _source.expToNextLevelTotal;
            level               = _source.level;
            expToNextLevelLocal = _source.expToNextLevelLocal;
            exp                 = _source.exp;
        }

        /// <summary>
        /// Natural maximum HP, without modifiers from Equipment, Materia or Sources.
        /// </summary>
        [Range(0, 9999)]       public int hpMaxBase;
        /// <summary>
        /// Maximum HP, accounting for Equipment, Materia and Sources.
        /// </summary>
        [Range(0, 9999)]       public int hpMaxTotal;
        /// <summary>
        /// Natural maximum MP, without modifiers from Equipment, Materia or Sources.
        /// </summary>
        [Range(0, 999)]        public int mpMaxBase;
        /// <summary>
        /// Maximum MP, accounting for Equipment, Materia and Sources.
        /// </summary>
        [Range(0, 999)]        public int mpMaxTotal;
        /// <summary>
        /// The total amount of EXP required to reach the next level. This includes all EXP from previous levels as well.
        /// </summary>
        [Range(0, 4294967215)] public int expToNextLevelTotal;
        [Range(0, 99)]         public int level;
        /// <summary>
        /// The amount of EXP required to reach the next level, localized to your current level. It only includes exp from this level to the next.
        /// </summary>
        public int expToNextLevelLocal;
        public int exp;
    }

    public int hpCurrent;
    public int mpCurrent;
    [SerializeReference]
    public Points points = new Points();
    [SerializeReference]
    public PrimaryStats primary = new PrimaryStats();
    [SerializeReference]
    public StatusEffectManager statusCurrent = new StatusEffectManager();
    [SerializeReference]
    public StatusEffectManager statusDefense = new StatusEffectManager();

    public ElementalAffinityManager elementAttack { get; set; }  = new ElementalAffinityManager();
    public ElementalAffinityManager elementHalve { get; set; }   = new ElementalAffinityManager();
    public ElementalAffinityManager elementInvalid { get; set; } = new ElementalAffinityManager();
    public ElementalAffinityManager elementAbsorb { get; set; }  = new ElementalAffinityManager();
    public StatusEffectManager statusAttack { get; set; } = new StatusEffectManager();

    public virtual int Attack => 0;
    public virtual int AttackPercent => 0;
    public virtual int Defense => 0;
    public virtual int DefensePercent => 0;
    public virtual int MagicAttack => 0;
    public virtual int MagicDefense => 0;
    public virtual int MagicDefensePercent => 0;

    // TODO: Elemental stuff doesn't stack. Null and absorb will override half.

    private int CalculateAttackDamage(Character _target, int _power = 1) {
        int random = UnityEngine.Random.Range(0, 256);

        int baseDamage = Attack + ((Attack + points.level) / 32) * ((Attack * points.level) / 32);
        int maxDamage = (_power * (512- _target.Defense) * baseDamage) / (16 / 512);
        return maxDamage *(3841 + random) / 4096;
    }

    private int CalculateHitPercent(Character _target) {
        return ((primary.dexterity / 4) + AttackPercent) + DefensePercent - _target.DefensePercent;
    }

    private int CalculateMagicDamage(int _power) {
        int baseDamage = 6 * (MagicAttack + points.level);

        return (_power / 16) * baseDamage;
    }
}
