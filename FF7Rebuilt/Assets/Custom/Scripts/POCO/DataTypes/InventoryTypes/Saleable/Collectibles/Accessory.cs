﻿using System;
using UnityEngine;

namespace InventoryTypes {
    [Serializable]
    public class Accessory : Collectible {
        public Accessory() : base() {
            name = "New Accessory";
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Accessory(Accessory _source) : base(_source) {
            specialAccessory   = _source.specialAccessory;
            elements = _source.elements;
            statusResistance  = _source.statusResistance;
            equipEffects       = _source.equipEffects;
            autoStatus       = _source.autoStatus;
        }
        
        public SpecialAccessoryType specialAccessory;
        [SerializeReference]
        public ElementalAffinityManager elements = new ElementalAffinityManager(true);
        [SerializeReference]
        public StatusEffectManager statusResistance = new StatusEffectManager();
        [SerializeReference]
        public EquipEffect equipEffects = new EquipEffect();
        [SerializeReference]
        public StatusEffectManager autoStatus = new StatusEffectManager();

        private float walkTimer;

        /// <summary>
        /// If a Cat's Bell is worn, recover health while walking. Simply call this method as your field player walks.
        /// </summary>
        public void CatsBell(CharacterTypes.Player _player) {
            if (specialAccessory == SpecialAccessoryType.CatsBell) {
                walkTimer += Time.deltaTime;

                if (walkTimer >= 1) {
                    walkTimer = 0;

                    int increment = (_player.hpCurrent % 2 == 0) ? 2 : 1;
                    _player.hpCurrent = Mathf.Clamp(_player.hpCurrent + increment, 0, _player.points.hpMaxTotal);
                }
            }
        }
    }
}