﻿using CharacterTypes;
using System.Linq;
using UnityEngine;

public static class DamageFormula {
    public static readonly string[] DAMAGE_FORMULA_NAMES = {
        "0: No Damage"
    ,   "1: (Power ÷ 16) * (Stat + ((Lv + Stat) ÷ 32) * ((Lv * Stat) ÷ 32))"
    ,   "2: (Power ÷ 16) * ((Lv * Stat) * 6)"
    ,   "3: HP * (Power ÷ 32)"
    ,   "4: Max HP * (Power ÷ 32)"
    ,   "5: (Power * 22) + ((Lv + Stat) * 6)"
    ,   "6: Power * 16"
    ,   "7: Power * 20"
    ,   "8: Power ÷ 32"
    ,   "9: Fully recovers HP & MP"
    ,   "10: Throw"
    ,   "11: Coin"
    ,   "12: User's HP"
    ,   "13: User's MaxHP - HP"
    ,   "14: Dice Roll * 100"
    ,   "15: Number of Escapes * 256"
    ,   "16: Target's current HP - 1"
    ,   "17: (Hours * 100) + Minutes"
    ,   "18: Target's kill count * 10"
    ,   "19: Target's number of materia held * 1111"
    ,   "20: Increased damage with status effects on user"
    ,   "21: Increased damage from Near Death and/or Death Sentence"
    ,   "22: Increased damage from decreased allies"
    ,   "23: Attack power is average level of targets"
    ,   "24: Attack power based on HP"
    ,   "25: Attack power based on MP"
    ,   "26: Attack power based on weapon AP"
    ,   "27: Attack power based on user's kill count"
    ,   "28: Attack power based on user's limit bar"
    };

    public static int CalculateTotal(Ability _ability, Player _user, Player _target) {
        bool isPhysical = _ability.accuracyCalculation < 6;
        int stat = isPhysical ? _user.Attack : _user.MagicAttack;

        int power = _ability.power;
        int lv = _user.points.level;
        int userHP = _user.hpCurrent;
        int userHPMax = _user.points.hpMaxTotal;
        int targetHPMax = _target.points.hpMaxTotal;

        Debug.Log(_ability.damageFormula);

        switch (_ability.damageFormula) {
            case 0:     // No Damage
                return 0;
            case 1:     // (Power ÷ 16) * (Stat + ((Lv + Stat) ÷ 32) * ((Lv * Stat) ÷ 32))
                return (power / 16) * (stat + ((lv + stat) / 32) * ((lv * stat) / 32));
            case 2:     // (Power ÷ 16) * ((Lv * Stat) * 6)
                return (power / 16) * (lv * stat * 6);
            case 3:     // HP * (Power ÷ 32)
                return userHP * (int)(power / 32f);
            case 4:     // Max HP * (Power ÷ 32)
                int v = (int)(targetHPMax * (power / 32f));
                Debug.Log(targetHPMax);
                Debug.Log(power/32);
                Debug.Log(v);
                return v;
            case 5:     // (Power * 22) + ((Lv + Stat) * 6)
                return (power * 22) + ((lv + stat) * 6);
            case 6:     // Power * 16
                return power * 16;
            case 7:     // Power * 20
                return power * 20; 
            case 8:     // Power ÷ 32
                return (int)(power / 32f); 
            case 9:     // Fully recovers HP & MP
                return 0;
            case 10:    // Throw
                return 0;
            case 11:    // Coin
                return 0;
            case 12:    // User's HP
                return userHP;
            case 13:    // User's MaxHP - HP
                return userHPMax - userHP;
            case 14:    // Dice Roll * 100
                return 0;
            case 15:    // Number of Escapes * 256
                return GameManager.Instance.gameData.escapes * 256;
            case 16:    // Target's current HP - 1
                return _target.hpCurrent - 1;
            case 17:    // (Hours * 100) + Minutes
                return 0;
            case 18:    // Target's kill count * 10
                return 0;
            case 19:    // Target's number of materia held * 1111
                return _target.Materia.Count() * 1111;
            case 20:    // Increased damage with status effects on user
                return 0;
            case 21:    // Increased damage from Near Death and/or Death Sentence
                return 0;
            case 22:    // Increased damage from decreased allies
                return 0;
            case 23:    // Attack power is average level of targets
                return 0;
            case 24:    // Attack power based on HP
                return 0;
            case 25:    // Attack power based on MP
                return 0;
            case 26:    // Attack power based on weapon AP
                return 0;
            case 27:    // Attack power based on user's kill count
                return 0;
            case 28:    // Attack power based on user's limit bar
                return 0;
        }

        return 0;
    }
}
