using CharacterTypes;
using InventoryTypes;
using TMPro;
using UnityEngine;

public class MagicMenuInfoWindow : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtMpRequired;

    public void Show(Player _player) {
        gameObject.SetActive(true);

        Clear();
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void Clear() {
        txtMpRequired.text = string.Empty;
    }

    public void SetInfoSummon(Materia _referenceMateria, bool _isMaster) {
        if (_referenceMateria == null) {
            Clear();
            return;
        }
        
        txtMpRequired.text = $"<mspace=12px>{_referenceMateria.selectionSpells[0].GetSelectedAbility(AbilityType.Summon).mpCost:000}";
    }

    public void SetInfoEnemySkill(Ability _enemySkill) {
        if (_enemySkill == null) {
            Clear();
            return;
        }

        txtMpRequired.text = $"<mspace=12px>{_enemySkill.mpCost:000}";
    }

    public void SetInfoMagic(Ability _magic) {
        if (_magic == null) {
            Clear();
            return;
        }

        txtMpRequired.text = $"<mspace=12px>{_magic.mpCost:000}";
    }
}
