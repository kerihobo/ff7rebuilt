﻿using System;
using UnityEngine;

[Serializable, CreateAssetMenu(fileName = "New AI Graph", menuName = "Custom/AI/AI Graph")]
public class NodeGraphAI : NodeGraph {
    protected override void InitializeBlackboard() {
        Blackboard.Create<BlackboardAI>(this);
    }

    protected override void InitializeSearchWindow() {
        searchWindow = new SearchWindowAI(this);
    }
}
