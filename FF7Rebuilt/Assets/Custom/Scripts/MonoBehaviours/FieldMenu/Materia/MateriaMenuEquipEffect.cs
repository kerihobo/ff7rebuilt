using System;
using TMPro;
using UnityEngine;

public class MateriaMenuEquipEffect : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtOperator;
    [SerializeField] private TextMeshProUGUI txtValue;
    [SerializeField] private TextMeshProUGUI txtPercent;

    private TextMeshProUGUI txtStat;
    private CanvasGroup cnvGroup;

    private TextColourContainer colourContainer => TextColourContainer.GetInstance();

    public void Initialize() {
        txtStat = GetComponent<TextMeshProUGUI>();
        cnvGroup = GetComponent<CanvasGroup>();
    }

    public void Refresh(string _label, int _value, bool _isPercent) {
        txtStat.text = _label;

        bool isIncrement = _value > 0;
        txtOperator.text = isIncrement ? "+" : "-";
        txtValue.text = Mathf.Abs(_value).ToString("00");
        txtValue.color = isIncrement ? colourContainer.yellow : colourContainer.red;
        txtPercent.enabled = _isPercent;

        Show();
    }

    public void Hide() {
        cnvGroup.alpha = 0;
    }

    private void Show() {
        cnvGroup.alpha = 1;
    }
}
