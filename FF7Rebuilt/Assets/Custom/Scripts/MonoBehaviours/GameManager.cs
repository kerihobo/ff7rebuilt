using CharacterTypes;
using FFVII.Database;
using Field;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager Instance;
    
    public GameData gameData;

    public SaveGameSlot[] saveGameSlots { get; set; } = new SaveGameSlot[10];
    public PlayerControllerNavMesh currentlyControlledPlayerCharacter { get; set; }
    public float timeInSeconds { get; set; }

    public bool IsSaveAvailable => true;
    public bool IsInNorthernCrater => true;
    
    private void Awake() {
        if (Instance && Instance != this) {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        CommandManager.Initialize();

        gameData.Initialize();

        // TODO: Later this needs to come from a save file or then be generated from scratch
        for (int i = 0; i < saveGameSlots.Length; i++) {
            saveGameSlots[i] = new();
        }

        SetScenePlayer(null);

        DontDestroyOnLoad(this);
    }

    private void Update() {
        timeInSeconds += Time.deltaTime;
    }

    private void OnDestroy() {
        if (Instance == this) {
            gameData.config.ResetWindowColour();
        }
    }

    public static void SetScenePlayer(Transform _targetPoint) {
        Player partyLeader = Instance.gameData.phs.partyLeader;
        PlayerControllerNavMesh prfPartyLeader = partyLeader.prefabField;

        PlayerControllerNavMesh scenePlayer =
            FindObjectsByType<PlayerControllerNavMesh>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                .FirstOrDefault(x => {
                    Player player = x.selectionPlayer.GetSelectedPlayer();
                    return player != null && player.idName == partyLeader.idName;
                });
        
        string s = "";
        if (scenePlayer) {
            s += "Possessing player";
        } else {
            s += "Spawning player";
            scenePlayer = Instantiate(prfPartyLeader);
        }

        Transform tPlayer = scenePlayer.transform;
        
        if (_targetPoint) {
            s += $"\nSpawning @ pos({_targetPoint.position:0.00}), fwd({_targetPoint.forward})";
            tPlayer.SetPositionAndRotation(_targetPoint.position, _targetPoint.rotation);
        } else {
            s += "\nSpawning without _targetPoint";
        }

        scenePlayer.gameObject.SetActive(true);
        scenePlayer.agent.Warp(tPlayer.position);
        scenePlayer.agent.SetDestination(tPlayer.position);

        List<Player> playerList = DBResources.GetPlayers.playerList;
        Player dbPlayer = playerList.First(x => x.idName == partyLeader.idName);
        scenePlayer.selectionPlayer.index = playerList.IndexOf(dbPlayer);

        int i = scenePlayer.selectionPlayer.index;
        s += $"\ni({i}, id({playerList[i]})";
        Debug.Log(s);

        EventManagerField.Disengage();
    }
}
