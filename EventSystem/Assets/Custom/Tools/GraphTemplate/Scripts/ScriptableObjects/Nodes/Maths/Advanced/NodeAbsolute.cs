﻿using UnityEngine;

public class NodeAbsolute : Node {
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(90, 50);
        name = "Absolute";

        AddPortIn<PortInt>("In", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_PORT);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();

        return Mathf.Abs(a);
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        return Create<NodeAbsolute>(_nodeGraph, position + duplicationOffset, category, false);
    }
}