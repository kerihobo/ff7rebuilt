using CharacterTypes;
using InventoryTypes;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class CheatManager {
    public static void UnlockAllLimitBreaks(Player _player) {
        Debug.Log("All limit breaks unlocked for " + _player.name);

        LimitBreakManager limitManager = _player.limitBreakManager;
        limitManager.currentLimitLevel = 0;
        limitManager.limitLevels[0].gaugeFill = UnityEngine.Random.Range(20, 70);

        foreach (LimitBreakManager.LimitLevel ll in limitManager.limitLevels) {
            ll.isUnlocked = true;
            ll.usesCount = ll.usesNeeded;
        }
    }

    public static void UnlockAllButLastLimitBreaks(Player _player) {
        Debug.Log("All but the last limit break unlocked for " + _player.name);

        LimitBreakManager limitManager = _player.limitBreakManager;
        limitManager.currentLimitLevel = 0;
        limitManager.limitLevels[0].gaugeFill = Random.Range(20, 70);

        int maxLevel = limitManager.availableLevels - 1;
        IEnumerable<LimitBreakManager.LimitLevel> priorLevels = limitManager.limitLevels.Take(maxLevel);

        foreach (LimitBreakManager.LimitLevel ll in priorLevels) {
            ll.isUnlocked = true;
            ll.usesCount = ll.usesNeeded;
        }
    }

    public static void PartyLevelUp(Player[] _players) {
        foreach (Player p in _players) {
            p?.levelUpManager.AddExp(p, Random.Range(10000, 1000000));
        }
    }

    public static void CheatUnlockRandomEnemySkills(Materia materia) {
        Debug.Log("RANDOM ENEMY SKILLS UNLOCKED");
        for (int i = 0; i < materia.enemySkillManager.skillsUnlocked.Length; i++) {
            materia.enemySkillManager.skillsUnlocked[i] = UnityEngine.Random.Range(0, 2) == 0;
        }
    }
}