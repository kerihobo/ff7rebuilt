using FFVII.Database;
using InventoryTypes;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

public class MateriaAbilitiesSupportEditor : MateriaAbilitiesEditor {
    public MateriaAbilitiesSupportEditor(Materia _target, Database _database) : base(_target, _database) {
    }

    protected override string UXMLPath { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/MateriaAbilities/materia-abilities-support.uxml";

    public override void BindAbilities() {
        //this.Q<EnumField>("enum-support-type").SetSupportEffectType(target.supportManager.type).RegisterValueChangedCallback(e => {
        //    target.supportManager.type = (SupportManager.Type)e.newValue;
        //    EditorUtility.SetDirty(database);
        //});

        DropdownField ddfSubtype = this.Q<DropdownField>("ddf-subtype");
        ddfSubtype.choices = SupportSubtype.SUPPORT_SUBTYPE_NAMES.ToList();
        ddfSubtype.index = target.subtype;
        ddfSubtype.RegisterValueChangedCallback(_e => {
            target.subtype = ddfSubtype.index;

            //RefreshSelectionDisplays();

            EditorUtility.SetDirty(database);
        });

        for (int i = 0; i < target.boostValues.Length; i++) {
            int j = i;
            IntegerField intF = this.Q<IntegerField>($"int-lv-{j + 1}");
            intF.SetValue(target.boostValues[j]).RegisterValueChangedCallback(e => {
                target.boostValues[j] = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }
    }

    public override void OnLevelsChanged() {
        List<IntegerField> intF = this.Query<GroupBox>("grp-effect-granted").Children<IntegerField>().ToList();
        intF.ForEach(x => x.style.display = DisplayStyle.None);

        for (int i = 0; i < target.levels; i++) {
            intF[i].style.display = DisplayStyle.Flex;
        }
    }

    public override void Refresh(Materia _target, VisualElement _ve) {
        target = _target;
        veParent = _ve;

        //this.Q<EnumField>("enum-support-type").value = target.supportManager.type;
        this.Q<DropdownField>("ddf-subtype").index = target.subtype;

        for (int i = 0; i < target.boostValues.Length; i++) {
            int j = i;
            IntegerField intF = this.Q<IntegerField>($"int-lv-{j + 1}");
            intF.value = target.boostValues[j];
        }
    }
}
