using CharacterTypes;
using FFVII.Database;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

public class LimitBreaksEditor : VisualElement {
    public new class UxmlFactory : UxmlFactory<LimitBreaksEditor, UxmlTraits> { }

    public LimitBreaksEditor() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        name = Name;

        limitBreakLevelContent = this.Q<LimitBreakLevelContent>();
    }

    public Player target;
    public Database database;
    private const string UXMLPath = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/LimitBreaks/limit-breaks-editor.uxml";
    private const string Name = "limit-breaks-editor";
    private TemplateContainer tc;
    private LimitBreakLevelContent limitBreakLevelContent;
    
    public LimitBreaksEditor Initialize(Player _target, Database _database) {
        target = _target;
        database = _database;

        BindUniqueControls();
        Refresh(target);

        return this;
    }

    public void BindUniqueControls() {
        tc.Q<SliderInt>("sld-available-limit-lvl").SetValue(target.limitBreakManager.availableLevels).RegisterValueChangedCallback(e => {
            target.limitBreakManager.availableLevels = e.newValue;
            RefreshTabButtons();

            EditorUtility.SetDirty(database);
        });

        BindLimitLevelButtons();

        limitBreakLevelContent.Initialize(target, database).BindUniqueControls();
    }


    private void BindLimitLevelButtons() {
        List<ToolbarButton> buttons = tc.Query<Toolbar>("tlb-tabs-limit-levels").Children<ToolbarButton>().ToList();

        for (int i = 0; i < buttons.Count; i++) {
            int j = i;
            buttons[j].RegisterCallback<ClickEvent>(x => {
                limitBreakLevelContent.targetIndex = j;
                limitBreakLevelContent.Refresh(target);
            });
        }
    }


    public void Refresh(Player _target) {
        target = _target;

        tc.Q<SliderInt>("sld-available-limit-lvl").value = target.limitBreakManager.availableLevels;
        
        RefreshTabButtons();

        limitBreakLevelContent.targetIndex = 0;
        limitBreakLevelContent.Refresh(target);

    }

    private void RefreshTabButtons() {
        List<ToolbarButton> buttons = tc.Query<Toolbar>("tlb-tabs-limit-levels").Children<ToolbarButton>().ToList();

        for (int i = 0; i < buttons.Count; i++) {
            buttons[i].style.display = target.limitBreakManager.availableLevels > i ? DisplayStyle.Flex : DisplayStyle.None;
        }
    }
}
