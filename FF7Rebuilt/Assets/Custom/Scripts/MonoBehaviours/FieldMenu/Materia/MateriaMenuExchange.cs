using CharacterTypes;
using UnityEngine;
using System.Collections;
using InventoryTypes;
using TMPro;
using UnityEngine.UI;
using System;

public class MateriaMenuExchange : MonoBehaviour {
    public MateriaMenuExchangeMateriaList materiaList;
    public FingerCursorRight cursorOptions;
    public FingerCursorRight cursorOptionsFlicker;
    public FingerCursorRight cursorSlots;
    public FingerCursorRight cursorSlotsFlicker;

    /// <summary>
    /// A reference to a player.
    /// It is null unless a first selection in an exchange has been made.
    /// It awaits playerB to be provided to complete the exchange.
    /// </summary>
    public Player playerA { get; set; }
    /// <summary>
    /// A reference to a piece of equipment.
    /// It is null unless a first selection in an exchange has been made.
    /// It awaits equipmentB to be provided to complete the exchange.
    /// </summary>
    public Equipment equipmentA { get; set; }
    /// <summary>
    /// A reference to either a player's materiaWeapon or materiaArmor array.
    /// It is null unless a first selection in an exchange has been made.
    /// It awaits arrMateriaB to be provided to complete the exchange.
    /// </summary>
    public Materia[] arrMateriaA { get; set; }
    /// <summary>
    /// The specific materia chosen from arrMateriaA for the purpose of exchange.
    /// This is null unless a slot has been chosen as the first selection in an exchange.
    /// The materia it holds is not for the exchange, rather it provides confirmation for single-slot exchange-checks.
    /// </summary>
    public Materia materiaA { get; set; }
    /// <summary>
    /// The index used when making the swap.
    /// It is the index of materiaA.
    /// </summary>
    public int slotIndexA;
    public int selectedSlotIndex { get; set; }
    public int selectedIndex { get; set; }
    public int lowBound { get;set; }
    public MateriaMenuExchangeCharacterContainer[] arrCharacterContainers { get; set; }
    public Player[] arrViewableCharacters { get; set; } = new Player[0];

    [SerializeField] private MateriaMenuExchangeCharacterContainer additionalCharacter;
    [SerializeField] private RectTransform charactersParent;
    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private TextMeshProUGUI txtDescription;
    [SerializeField] private MateriaMenuStarSet starSet;
    [SerializeField] private Image imgIcon;
    [SerializeField] private TextMeshProUGUI txtName;
    private ScreenFieldMateria screenFieldMateria;
    private RectTransform scrollBarFG;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;

    public void Initialize(ScreenFieldMateria _screenFieldMateria) {
        screenFieldMateria = _screenFieldMateria;
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        VerticalLayoutGroup vlgCharacters = charactersParent.GetComponent<VerticalLayoutGroup>();
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgCharacters.GetComponent<RectTransform>());

        arrCharacterContainers = charactersParent.GetComponentsInChildren<MateriaMenuExchangeCharacterContainer>(true);
        foreach (MateriaMenuExchangeCharacterContainer cc in arrCharacterContainers) {
            cc.Initialize(this);
        }

        additionalCharacter.Initialize(this);
        starSet.Initialize();
        materiaList.Initialize(this);

        //cursorOptionsFlicker.Initialize(arrOptions[0].rectTransform);
        cursorOptionsFlicker.Hide();

        TextMeshProUGUI firstOption = arrCharacterContainers[0].optionList.arrOptions[0];
        cursorOptions.Initialize(firstOption.rectTransform);
        cursorOptionsFlicker.Initialize(firstOption.rectTransform);
        cursorOptionsFlicker.Hide();
        
        UIMateriaSlotInfo firstSlot = arrCharacterContainers[0].slotsExchange.arrOptionsWeapon[0];
        cursorSlots.Initialize(firstSlot.rct);
        cursorSlotsFlicker.Initialize(firstSlot.rct);
        cursorSlotsFlicker.Hide();

        listSpace = vlgCharacters.spacing;
        listHeight = arrCharacterContainers[0].rct.rect.height;
        initialListHierarchyPosition = charactersParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrCharacterContainers.Length; i++) {
            int characterIndex = lowBound + i;
            Player characterInView = null;

            // Only show an item within the range of our viewItems.
            if (characterIndex < arrViewableCharacters.Length) {
                characterInView = arrViewableCharacters[characterIndex];
            }

            arrCharacterContainers[i].Refresh(characterInView);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrCharacterContainers.Length - 1;
        MateriaMenuExchangeCharacterContainer anchoredInfo = arrCharacterContainers[^1];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredIndex = lowBound;
            anchoredInfo = arrCharacterContainers[0];
        }

        additionalCharacter.rct.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        Player itemInView = null;

        // Only show an item within the range of our viewItems.
        if (anchoredIndex < arrViewableCharacters.Length) {
            itemInView = arrViewableCharacters[anchoredIndex];
        }

        additionalCharacter.Refresh(itemInView);
    }

    private void ClearAdditional() {
        additionalCharacter.Clear();
    }

    public void ResetSelection() {
        selectedIndex = 0;
        lowBound = 0;

        arrCharacterContainers[0].optionList.ResetSelection();
        arrCharacterContainers[0].slotsExchange.ResetSelection();

        Debug.Log("This should only ever reset on game-load, new or savefile.");
    }

    public void Open() {
        screenFieldMateria.rctMateria.gameObject.SetActive(false);
        gameObject.SetActive(true);

        // Actually this should never happen. We only expect slot0 to be the initial selection
        // on a freshly loaded game/save.
        ResetSelection();

        PopulateItemList();
        Show();

        FieldMenu.Instance.SetCooldownTraverse(0);

        arrCharacterContainers[selectedIndex].slotsExchange.Open();

        void PopulateItemList() {
            arrViewableCharacters = GameManager.Instance.gameData.phs.GetAvailablePlayersInOrder();

            Refresh();
        }
    }

    private void Show() {
        SetScrollbarVisibility();
        SetScrollbarHeight();
        UpdateScrollBar();

        materiaList.Show();

        void SetScrollbarVisibility() {
            bool isUsingScrollbar = arrViewableCharacters.Length > arrCharacterContainers.Length;
            scrollBarBG.gameObject.SetActive(isUsingScrollbar);
        }

        void SetScrollbarHeight() {
            float scrollBarHeight = scrollBarBG.sizeDelta.y;
            scrollBarHeight *= arrCharacterContainers.Length / (float)arrViewableCharacters.Length;
            scrollBarHeight = Mathf.RoundToInt(scrollBarHeight);
            scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarHeight);
        }
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void Close() {
        // Disable() PROBABLY...
        ClearAdditional();
        Hide();

        screenFieldMateria.rctMateria.gameObject.SetActive(true);
        screenFieldMateria.arrangeMenu.Open();
        screenFieldMateria.RefreshPlayerInfo();
        screenFieldMateria.slotsMain.RefreshMateria(null);

        // OPEN THE MATERIA MENU.
    }

    private IEnumerator AnimatedScroll(int _v, Action<int> _acPostScroll, int _nextEquipmentIndex) {
        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            cursorOptionsFlicker.AddPosition(newPosition - charactersParent.anchoredPosition);
            cursorSlotsFlicker.AddPosition(newPosition - charactersParent.anchoredPosition);
            charactersParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            // Maintain the cooldown during the transition to make sure we can't interact as soon as the scroll finishes.
            FieldMenu.Instance.SetCooldownTraverse(FieldMenu.COOLDOWN_SCROLL);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        // MAYBE SET A NEW COOLDOWN HERE? Really don't know about this.
        FieldMenu.Instance.SetCooldownTraverse(FieldMenu.COOLDOWN_SCROLL);

        Vector2 finalPosition = Vector2.Lerp(start, end, 1);
        cursorOptionsFlicker.AddPosition(finalPosition - charactersParent.anchoredPosition);
        cursorSlotsFlicker.AddPosition(finalPosition - charactersParent.anchoredPosition);

        charactersParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();

        _acPostScroll(_nextEquipmentIndex);

        // SET THE CURSOR OF THE NEXT CharacterContainer IN VIEW
        //SetCursor();

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        float scroll = (float)lowBound / arrViewableCharacters.Length;
        scroll = (int)(scrollBarBG.rect.height * scroll);
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

    public void RefreshMateriaInfo(Materia _materia) {
        txtName.text = _materia?.name;
        imgIcon.sprite = ItemIconContainer.GetInstance().GetIconByMateria(_materia);
        imgIcon.enabled = _materia != null;
        txtDescription.text = _materia?.description;
        starSet.Refresh(_materia);
    }

    public void ReturnFromMateriaList() {
        arrCharacterContainers[selectedIndex].ReturnFromMateriaList();

        MateriaMenuExchangeSlots selectedSlot = arrCharacterContainers[selectedIndex].slotsExchange;
        Refresh();
        RefreshMateriaInfo(selectedSlot.arrSelected[selectedSlot.selectedIndex].materia);
    }

    private void NavigateCharacters(int _v, out bool _isOutOfRange) {
        _isOutOfRange = false;
        bool isLowest = lowBound + selectedIndex == 0 && _v > 0;
        bool isHighest = lowBound + selectedIndex == arrViewableCharacters.Length - 1 && _v < 0;
        bool isAtExtreme = isLowest || isHighest;
        bool isMidScroll = corAnimatedScroll != null;
        if (isMidScroll || isAtExtreme) return;

        selectedIndex -= _v;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrCharacterContainers.Length - 1;
        _isOutOfRange = isTooLow || isTooHigh;

        if (_isOutOfRange) {
            lowBound -= _v;
        }

        selectedIndex = Mathf.Clamp(selectedIndex, 0, arrCharacterContainers.Length - 1);
        lowBound = Mathf.Clamp(lowBound, 0, arrViewableCharacters.Length - arrCharacterContainers.Length);

        UpdateScrollBar();
    }

    public void NavigateCharactersSlots(int _v) {
        NavigateCharacters(_v, out bool isOutOfRange);

        int nextEquipmentIndex = _v > 0 ? 1 : 0;
        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(-_v, PostScrollSlots, nextEquipmentIndex));
        } else {
            PostScrollSlots(nextEquipmentIndex);
        }
    }

    private void PostScrollSlots(int _nextEquipmentIndex) {
        MateriaMenuExchangeSlots slots = arrCharacterContainers[selectedIndex].slotsExchange;
        slots.SetEquipmentSelected(_nextEquipmentIndex);
        slots.Open();
    }

    public void NavigateCharactersOptions(int _v) {
        NavigateCharacters(_v, out bool isOutOfRange);

        int nextEquipmentIndex = _v > 0 ? GetHighestAllowedValue() : GetLowestAllowedValue();
        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(-_v, PostScrollOptions, nextEquipmentIndex));
        } else {
            PostScrollOptions(nextEquipmentIndex);
        }

        int GetHighestAllowedValue() {
            bool isExchangePlayer = playerA != null;
            return isExchangePlayer ? 0 : arrCharacterContainers[0].optionList.arrOptions.Length - 1;
        }

        int GetLowestAllowedValue() {
            bool isExchangeEquipment = arrMateriaA != null;
            return isExchangeEquipment ? 1 : 0;
        }
    }

    private void PostScrollOptions(int _nextOptionsIndex) {
        MateriaMenuExchangeOptionList options = arrCharacterContainers[selectedIndex].optionList;
        options.SetSelectedIndex(_nextOptionsIndex);
        options.Open();
    }

    public void SetCursorFlicker(FingerCursorRight _cursor, RectTransform _rct) {
        _cursor.Show();
        _cursor.SetCursorPosition(_rct);
        _cursor.SetFlickerState(true);
    }

    public void CloseCurrentExchange() {
        arrMateriaA = null;
        materiaA = null;
        playerA = null;
        equipmentA = null;

        cursorOptionsFlicker.Hide();
        cursorSlotsFlicker.Hide();
        cursorOptionsFlicker.SetFlickerState(false);
        cursorSlotsFlicker.SetFlickerState(false);
    }

    public void Exchange2Slots(Player _assignedPlayer, Materia[] _arrMateriaB) {
        (arrMateriaA[slotIndexA], _arrMateriaB[selectedSlotIndex]) = (_arrMateriaB[selectedSlotIndex], arrMateriaA[slotIndexA]);
        MateriaMenuExchangeSlots selectedSlot = arrCharacterContainers[selectedIndex].slotsExchange;

        playerA.RecalculatePlayerStats();
        if (playerA != _assignedPlayer) {
            _assignedPlayer.RecalculatePlayerStats();
        }

        Refresh();
        RefreshMateriaInfo(selectedSlot.arrSelected[selectedSlot.selectedIndex].materia);
        CloseCurrentExchange();
    }

    public void ExchangeSlotAndInventory(Materia[] _arrMateriaB) {
        (arrMateriaA[slotIndexA], _arrMateriaB[selectedSlotIndex]) = (_arrMateriaB[selectedSlotIndex], arrMateriaA[slotIndexA]);
        MateriaMenuExchangeSlots selectedSlot = arrCharacterContainers[selectedIndex].slotsExchange;

        playerA.RecalculatePlayerStats();

        Refresh();
        RefreshMateriaInfo(selectedSlot.arrSelected[selectedSlot.selectedIndex].materia);
        CloseCurrentExchange();
    }

    public void Exchange2Players(Player _playerB) {
        Materia[] arrMateriaWeaponA = playerA.materiaWeapon;
        Materia[] arrMateriaArmorA = playerA.materiaArmor;
        Materia[] arrMateriaWeaponB = _playerB.materiaWeapon;
        Materia[] arrMateriaArmorB = _playerB.materiaArmor;

        int min = Mathf.Min(playerA.weapon.materiaSlotCount, _playerB.weapon.materiaSlotCount);
        for (int i = 0; i < min; i++) {
            (arrMateriaWeaponA[i], arrMateriaWeaponB[i]) = (arrMateriaWeaponB[i], arrMateriaWeaponA[i]);
        }

        min = Mathf.Min(playerA.armor.materiaSlotCount, _playerB.armor.materiaSlotCount);
        for (int i = 0; i < min; i++) {
            (arrMateriaArmorA[i], arrMateriaArmorB[i]) = (arrMateriaArmorB[i], arrMateriaArmorA[i]);
        }

        playerA.RecalculatePlayerStats();
        _playerB.RecalculatePlayerStats();

        Refresh();
        CloseCurrentExchange();
    }

    public void Exchange2Equipment(Player _assignedPlayer, Equipment _equipmentB, Materia[] _arrMateriaB) {
        int min = Mathf.Min(equipmentA.materiaSlotCount, _equipmentB.materiaSlotCount);
        for (int i = 0; i < min; i++) {
            (arrMateriaA[i], _arrMateriaB[i]) = (_arrMateriaB[i], arrMateriaA[i]);
        }

        playerA.RecalculatePlayerStats();
        if (playerA != _assignedPlayer) {
            _assignedPlayer.RecalculatePlayerStats();
        }

        Refresh();
        CloseCurrentExchange();
    }
}
