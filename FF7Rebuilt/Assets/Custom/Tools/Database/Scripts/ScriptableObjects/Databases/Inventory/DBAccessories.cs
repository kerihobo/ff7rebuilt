﻿using UnityEngine;
using System;
using System.Collections.Generic;
using InventoryTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Accessories", menuName = "Kerihobo/Database/Inventory/Accessories")]
    public class DBAccessories : Database {
        public List<Accessory> accessoryList = new List<Accessory>();
    }
}