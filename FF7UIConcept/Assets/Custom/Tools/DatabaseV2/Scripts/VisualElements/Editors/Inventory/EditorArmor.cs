using UnityEngine.UIElements;

public class EditorArmor : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Inventory/editor-armor.uxml";
    protected override string Name { get; set; } = "editor-armor";

    public EditorArmor() : base() {
    }
}
