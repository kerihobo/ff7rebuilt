﻿using System;
using UnityEngine;

[Serializable]
public abstract class NodeGet : NodeGetSet {
    protected NodeGet(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
    }

    public static NodeGet CreateFromField(NodeGraph _nodeGraph, Vector2 _offset, BlackboardField _field) {
        switch (_field.dataType) {
            case NodeDataType.Bool:
                //return (NodeGetBool)Activator.CreateInstance(typeof(NodeGetBool), _nodeGraph, _offset, Category.BlackboardField, true);
                return new NodeGetBool(_nodeGraph, _offset, Category.BlackboardField);
            case NodeDataType.Int:
                return new NodeGetInt(_nodeGraph, _offset, Category.BlackboardField);
        }

        return new NodeGetBool(_nodeGraph, _offset);
    }

    public override void Initialize(BlackboardField _selectedField) {
        base.Initialize(_selectedField);

        name = "Get " + _selectedField.name;
    }
}
