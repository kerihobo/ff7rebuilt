public enum AbilityType {
    Magic
,   Summon
,   EnemySkill
,   LimitBreak
,   BattleItem
,   Command
}