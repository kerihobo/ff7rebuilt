﻿using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class FieldInt : BlackboardField {
    public int value;

    protected override void Initialize(Blackboard _blackboard) {
        base.Initialize(_blackboard);

        name = "New Int";
        dataType = NodeDataType.Int;
    }

    public override string GetValueAsString() {
        return value.ToString();
    }

    protected override void RenderValue() {
        GUILayout.BeginHorizontal();
            GUILayout.Label("Value", GUILayout.Width(40));
            value = EditorGUILayout.IntField(value);
        GUILayout.EndHorizontal();
    }
}
