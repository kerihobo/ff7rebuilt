using UnityEngine;
using UnityEngine.UIElements;

public static class ExtButton {
    public static Button ToggleAfflictionBehaviour(this Button _self, ref StatusEffectManager.StatusEffect _statusEffect, StatusEffectManager.StatusEffectAction _statusEffectAction) {
        _statusEffect.isActive = !_statusEffect.isActive;

        _self.SetAfflictionBehaviourClass(_statusEffect.isActive, _statusEffectAction);

        return _self;
    }

    public static Button ToggleAfflictionBehaviour(this Button _self, ref StatusEffectManager.StatusEffect _statusEffect) {
        _statusEffect.isActive = !_statusEffect.isActive;

        _self.SetAfflictionBehaviourClass(_statusEffect.isActive, StatusEffectManager.StatusEffectAction.AFFLICT);

        return _self;
    }

    public static Button SetAfflictionBehaviourClass(this Button _self, bool _isActive, StatusEffectManager.StatusEffectAction _statusEffectAction) {
        _self.RemoveFromClassList("status-afflict");
        _self.RemoveFromClassList("status-recover");
        _self.RemoveFromClassList("status-toggle");

        //if (_self.text.Contains("+ ") || _self.text.Contains("- ")) {
        //    _self.text = _self.text.Substring(2);
        //}

        if (_isActive) {
            //Debug.Log(_statusEffectAction);
            switch (_statusEffectAction) {
                case StatusEffectManager.StatusEffectAction.AFFLICT:
                    _self.AddToClassList("status-afflict");
                    break;
                case StatusEffectManager.StatusEffectAction.RECOVER:
                    _self.AddToClassList("status-recover");
                    break;
                case StatusEffectManager.StatusEffectAction.TOGGLE:
                    _self.AddToClassList("status-toggle");
                    break;
            }
        } 

        return _self;
    }

    public static Button ToggleElementalVulnerability(this Button _self, ref ElementalAffinityManager.Affinity _value) {
        _value.isActive = !_value.isActive;

        _self.SetElementalVulnerabilityClass(_value, true);

        return _self;
    }

    public static Button CycleElementalVulnerabilityEnemies(this Button _self, ref ElementalAffinityManager.Affinity _value) {
        switch (_value.damageModifier) {
            case ElementalAffinityManager.DamageModifier.NORMAL:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.WEAK;
                break;
            case ElementalAffinityManager.DamageModifier.WEAK:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.HALVE;
                break;
            case ElementalAffinityManager.DamageModifier.HALVE:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.INVALID;
                break;
            case ElementalAffinityManager.DamageModifier.INVALID:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.ABSORB;
                break;
            case ElementalAffinityManager.DamageModifier.ABSORB:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.NORMAL;
                break;
        }

        _self.SetElementalVulnerabilityClass(_value, false);

        return _self;
    }

    public static Button CycleElementalVulnerabilityEquipment(this Button _self, ref ElementalAffinityManager.Affinity _value) {
        switch (_value.damageModifier) {
            case ElementalAffinityManager.DamageModifier.NORMAL:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.HALVE;
                break;
            case ElementalAffinityManager.DamageModifier.HALVE:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.INVALID;
                break;
            case ElementalAffinityManager.DamageModifier.INVALID:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.ABSORB;
                break;
            case ElementalAffinityManager.DamageModifier.ABSORB:
                _value.damageModifier = ElementalAffinityManager.DamageModifier.NORMAL;
                break;
        }

        _self.SetElementalVulnerabilityClass(_value, true);

        return _self;
    }

    public static Button SetElementalVulnerabilityClass(this Button _self, ElementalAffinityManager.Affinity _value, bool _isRequireActive) {
        _self.RemoveFromClassList("elemental-weak");
        _self.RemoveFromClassList("elemental-half");
        _self.RemoveFromClassList("elemental-immune");
        _self.RemoveFromClassList("elemental-absorb");

        if (_isRequireActive && !_value.isActive) return _self;

        //if (_self.text.Contains("+ ") || _self.text.Contains("- ")) {
        //    _self.text = _self.text.Substring(2);
        //}

        switch (_value.damageModifier) {
            //case ElementalAffinities.DamageModifier.NORMAL:
            //    _self.AddToClassList("elemental-normal");
            //    break;
            case ElementalAffinityManager.DamageModifier.WEAK:
                _self.AddToClassList("elemental-weak");
                break;
            case ElementalAffinityManager.DamageModifier.HALVE:
                _self.AddToClassList("elemental-half");
                break;
            case ElementalAffinityManager.DamageModifier.INVALID:
                _self.AddToClassList("elemental-immune");
                break;
            case ElementalAffinityManager.DamageModifier.ABSORB:
                _self.AddToClassList("elemental-absorb");
                break;
        }

        return _self;
    }
}
