using FFVII.Database;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class FormationLsPlacements : VisualElement {
    public new class UxmlFactory : UxmlFactory<FormationLsPlacements, UxmlTraits> { }

    public FormationLsPlacements() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path).Instantiate();
        Add(tc);

        name = Name;
    }
    
    private const string Path = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/formation-ls-placements.uxml";
    private const string Name = "formation-ls-placements";

    private TemplateContainer tc;
    private Database database = DBResources.GetFormations;
    private Formation target;
    private ListView listView;
    private VisualElement dragArea;
    private VisualElement selectedMarker;
    private Formation.Placement selectedPlacement;
    private Vector2Field selectedV2Field;
    private bool isDragging;

    public FormationLsPlacements Initialize(Formation _target, Database _database, VisualElement _dragArea) {
        target = _target;
        database = _database;
        dragArea = _dragArea;

        dragArea.RegisterCallback<PointerDownEvent>(x => {
            if (selectedMarker != null) {
                isDragging = true;
                PositionMarkerViaDragArea(x.localPosition);
            }
        });

        dragArea.RegisterCallback<PointerMoveEvent>(x => {
            if (isDragging) {
                PositionMarkerViaDragArea(x.localPosition);
            }
        });

        dragArea.RegisterCallback<PointerUpEvent>(x => {
            if (isDragging) {
                DeselectMarker();
            }
        });

        Refresh(target);

        return this;
    }

    public void Add() {
        target.placements.Add(new Formation.Placement());

        EditorUtility.SetDirty(database);
        AssetDatabase.SaveAssets();

        listView.RefreshItems();
    }

    public void Refresh(Formation _target) {
        target = _target;
        
        DeselectMarker();
        dragArea.Clear();

        if (listView != null) {
            Remove(listView);
        }

        listView = new ListView();
        listView.AddToClassList("ls-placements");

        listView.fixedItemHeight = 24;
        listView.itemsSource = target.placements;
        listView.makeItem = MakeItem;
        listView.bindItem = BindItem;
        listView.selectionType = SelectionType.None;

        Add(listView);

        VisualElement MakeItem() => AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/formation-lsitem-placement.uxml").Instantiate();

        void BindItem(VisualElement _ve, int _i) {
            int j = _i;

            Formation.Placement p = target.placements[j];

            _ve.Q<Label>("lbl-id").text = $"{j}";

            DropdownField ddf = _ve.Q<DropdownField>("drp-placement");
            ddf.Initialize(DBResources.GetEnemies.enemyList, p.selectionEnemy, database);
            ddf.RegisterValueChangedCallback(_e => {
                p.selectionEnemy.SetDataExternally(ddf.index);
                EditorUtility.SetDirty(database);
            });

            AddMarker(out VisualElement marker);
            
            _ve.Q<Vector2Field>("v2-position").SetValue(p.initialPosition).RegisterValueChangedCallback(e => {
                p.initialPosition = e.newValue;
                p.initialPosition = p.initialPosition.Clamp(-25, 25);
                
                EditorUtility.SetDirty(database);

                PositionMarker(marker, p);

                ((Vector2Field)e.currentTarget).value = p.initialPosition;
            });

            _ve.Q<Button>("btn-select").RegisterCallback<ClickEvent>(x => {
                Select(marker, _ve.Q<Vector2Field>("v2-position"));
            });

            _ve.Q<Button>("btn-remove").RegisterCallback<ClickEvent>(x => {
                Remove(marker);
            });

            void Select(VisualElement _marker, Vector2Field vector2Field) {
                target.selectedPlacementIndex = j;
                selectedMarker = _marker;
                selectedMarker.AddToClassList("marker-selected");

                selectedPlacement = p;
                selectedV2Field = vector2Field;
            }

            void Remove(VisualElement _marker) {
                target.placements.Remove(p);
                EditorUtility.SetDirty(database);
                AssetDatabase.SaveAssets();
                
                DeselectMarker();

                dragArea.Remove(_marker);

                listView.RefreshItems();
            }

            void AddMarker(out VisualElement _marker) {
                _marker = new VisualElement();
                _marker.AddToClassList("marker");
                dragArea.Add(_marker);
                
                PositionMarker(_marker, p);

                Label markerLabel = new Label();
                markerLabel.text = j.ToString();
                _marker.Add(markerLabel);
            }
        }
    }

    void PositionMarker(VisualElement _marker, Formation.Placement _p) {
        _marker.style.left = (dragArea.contentRect.width * Mathf.InverseLerp(-25, 25, _p.initialPosition.x)) - 10;
        _marker.style.bottom = (dragArea.contentRect.height * Mathf.InverseLerp(-25, 25, _p.initialPosition.y)) - 10;
    }

    private void PositionMarkerViaDragArea(Vector2 _localPosition) {
        if (selectedMarker != null) {
            selectedPlacement.initialPosition = new Vector2(
                Mathf.Lerp(-25, 25, _localPosition.x / dragArea.contentRect.width)
            ,   Mathf.Lerp(25, -25, _localPosition.y / dragArea.contentRect.height));
        }

        selectedV2Field.SetValue(selectedPlacement.initialPosition);
        PositionMarker(selectedMarker, selectedPlacement);
    }

    private void DeselectMarker() {
        if (selectedMarker != null) {
            selectedMarker.RemoveFromClassList("marker-selected");
        }

        isDragging = false;
        selectedMarker = null;
        selectedPlacement = null;
        selectedV2Field = null;
    }
}
