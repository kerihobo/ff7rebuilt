using UnityEngine.UIElements;

public class EditorLimitBreaks : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Abilities/editor-abilities.uxml";
    protected override string Name { get; set; } = "editor-limit-breaks";

    public EditorLimitBreaks() : base() {
    }
}
