﻿using FFVII.Database;
using System.Collections.Generic;

public static class CommandManager {
    public static List<Ability> MasterCommands;
    public static List<Ability> NonAdditionalCommands;

    public static void Initialize() {
        DBAbilities.CommandList cmd = DBResources.GetAbilities.commandList;

        MasterCommands = new List<Ability> {
            cmd.cmdSteal
        ,   cmd.cmdSense
        ,   cmd.cmdCoin
        ,   cmd.cmdMorph
        ,   cmd.cmdDeathblow
        ,   cmd.cmdManipulate
        ,   cmd.cmdMime
        };

        NonAdditionalCommands = new List<Ability> {
            cmd.cmdWItem
        ,   cmd.cmdWMagic
        ,   cmd.cmdWSummon
        ,   cmd.cmd2xCut
        ,   cmd.cmd4xCut
        ,   cmd.cmdSlashAll
        ,   cmd.cmdFlash
        };
    }

    private static void Coin() {
    }

    private static void DeathBlow() {
    }

    private static void Flash() {
    }

    private static void FourXCut() {
    }

    private static void Manipulate() {
    }

    private static void Mime() {
    }

    private static void Morph() {
    }

    private static void Mug() {
    }

    private static void Sense() {
    }

    private static void SlashAll() {
    }

    /// <summary>
    /// https://finalfantasy.fandom.com/wiki/Steal#Final_Fantasy_VII
    /// </summary>
    private static void Steal(CharacterTypes.Player _player, CharacterTypes.Enemy _enemy, bool hasSneakGlove) {
        int lvPlayer = _player.points.level;
        int lvEnemy = _enemy.points.level;

        int lvDifference = 40 + lvPlayer - lvEnemy;
        if (hasSneakGlove && lvDifference < 100) {
            lvDifference = 100;
        }

        int lvFactor = (512 * lvDifference) / 100;

        for (int i = 0; i < _enemy.dropCount; i++) {
            if (_enemy.dropCount <= 0 || _enemy.hasBeenRobbed) {
                // TODO: Message "Nothing to Steal."
                return;
            }

            int itemChance = _enemy.dropItems[i].chance;
            int chance = ((itemChance * lvFactor) / 256);
            chance++;
            float chanceFraction = chance / 64;

            if (UnityEngine.Random.Range(0, 63f) < chanceFraction) {
                var stock = _enemy.dropItems[i].item;
                
                switch (stock.collectibleType) {
                    case SelectionCollectible.CollectibleType.Item:
                        //stock.selectionItem.AddSelectedItem();
                        break;
                    case SelectionCollectible.CollectibleType.Weapon:
                        //stock.selectionWeapon.AddSelectedWeapon();
                        break;
                    case SelectionCollectible.CollectibleType.Armor:
                        //stock.selectionArmor.AddSelectedArmor();
                        break;
                    case SelectionCollectible.CollectibleType.Accessories:
                        //stock.selectionAccessory.AddSelectedAccessory();
                        break;
                }

                _enemy.hasBeenRobbed = true;
                // TODO: Message "Stole + itemName"
                break;
            } else {
                // TODO: Couldn't steal anything."
            }
        }

    }

    private static void TwoXCut() {
    }

    private static void Throw() {
    }

    private static void WItem() {
    }

    private static void WMagic() {
    }

    private static void WSummon() {
    }
}