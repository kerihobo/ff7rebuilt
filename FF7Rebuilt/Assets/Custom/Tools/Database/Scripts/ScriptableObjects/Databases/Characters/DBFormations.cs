﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Formations", menuName = "Kerihobo/Database/Formations")]
    public class DBFormations : Database {
        public List<Formation> formationsNormal;
        public List<Formation> formationsSpecial;
        public List<Formation> formationsChocobo;

        public List<Formation> GetSelectedFormationList(FormationType _type) => _type switch {
            FormationType.Normal => formationsNormal,
            FormationType.Special => formationsSpecial,
            FormationType.Chocobo => formationsChocobo,
            _ => null
        };

        public FormationType selectedMenuType { get; private set; }
    }
}
