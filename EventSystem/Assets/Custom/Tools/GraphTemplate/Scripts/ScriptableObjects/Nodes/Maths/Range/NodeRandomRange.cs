﻿using UnityEngine;

public class NodeRandomRange : Node {
    private dynamic copyA;
    private dynamic copyB;
    
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(90, 75);
        name = "Random Range";

        AddPortIn<PortInt>("A", Port.Capacity.SINGLE);
        AddPortIn<PortInt>("B", Port.Capacity.SINGLE);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);

        portContainerIn.ports[0].SetValue(copyA);
        portContainerIn.ports[1].SetValue(copyB);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(90, 160);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();
        int r = Random.Range(a, b + 1);
        
        return r;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeRandomRange>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeRandomRange)newNode).copyA = portContainerIn.ports[0].GetValue();
        ((NodeRandomRange)newNode).copyB = portContainerIn.ports[1].GetValue();
        
        return newNode;
    }
}