using FFVII.Database;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class SelectionCollectibleEditor : VisualElement {
    public new class UxmlFactory : UxmlFactory<SelectionCollectibleEditor, UxmlTraits> { }

    public SelectionCollectibleEditor() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        name = Name;
    }

    public SelectionCollectible target;
    public Database database;
    
    protected virtual string UXMLPath { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/SelectionCollectibles/selection-collectible-editor.uxml";
    protected virtual string Name { get; set; } = "selection-collectible-editor";
    
    protected TemplateContainer tc;
    protected DropdownField ddf;
    protected EnumField ef;
    protected VisualElement elParent;

    public SelectionCollectibleEditor Initialize(SelectionCollectible _target, Database _database) {
        target = _target;
        database = _database;

        elParent = this.Q<VisualElement>("ve-selection-collectible");

        RebuildFields();

        BindUniqueControls();
        Refresh(target, database);

        return this;
    }

    protected virtual void BindUniqueControls() {
        this.Q<EnumField>("enum-category-a").SetCollectibleType(target.collectibleType).RegisterValueChangedCallback(e => {
            SelectionCollectible.CollectibleType newValue = (SelectionCollectible.CollectibleType)e.newValue;
            bool isPrimaryChange = newValue != target.collectibleType;

            target.collectibleType = newValue;
            EditorUtility.SetDirty(database);

            if (isPrimaryChange) {
                target.ChangePrimarySelection();
            }

            RebuildFields();
        });
    }

    private void RebuildFields() {
        switch (target.collectibleType) {
            case SelectionCollectible.CollectibleType.Item:
                MakeNewDDFItem();
                break;
            case SelectionCollectible.CollectibleType.Weapon:
                MakeNewDDFWeapon();
                break;
            case SelectionCollectible.CollectibleType.Armor:
                MakeNewDDFArmor();
                break;
            case SelectionCollectible.CollectibleType.Accessories:
                MakeNewDDFAccessory();
                break;
        }
    }

    protected virtual void Refresh(SelectionCollectible _target, Database _database) {
        target = _target;
        database = _database;

        //this.Q<EnumField>("enum-category-a").value = target.collectibleType;
    }

    private void MakeNewDDFItem() {
        ClearFields();

        target.collectibleType = SelectionCollectible.CollectibleType.Item;

        ddf = new DropdownField();
        ddf.AddToClassList("expandable");
        ddf.name = "ddf-item";

        ddf.Initialize(DBResources.GetItems.itemsList, target.selectionItem, database);
        ddf.RegisterValueChangedCallback(_e => {
            target.selectionItem.SetDataExternally(ddf.index);
            target.SetDataExternally(target.selectionItem.selectionName);
            EditorUtility.SetDirty(database);
        });

        elParent.Add(ddf);
    }

    private void MakeNewDDFWeapon() {
        ClearFields();

        target.collectibleType = SelectionCollectible.CollectibleType.Weapon;

        ef = new EnumField();
        ef.AddToClassList("expandable");
        ef.name = "enum-weapon-type";
        ef.Init(target.selectionWeapon.type);
        ef.SetWeaponType(target.selectionWeapon.type).RegisterValueChangedCallback(e => {
            target.selectionWeapon.type = (WeaponType)e.newValue;

            //target.selectionWeapon.ForceNamesUpdate(DBResources.GetWeaponsList(target.selectionWeapon.type));
            target.selectionWeapon.SetDataExternally(0);
            target.SetDataExternally(target.selectionWeapon.selectionName);

            EditorUtility.SetDirty(database);

            elParent.Remove(ddf);
            AddDropdown();
            elParent.Add(ddf);
        });

        AddDropdown();

        elParent.Add(ef);
        elParent.Add(ddf);

        void AddDropdown() {
            ddf = new DropdownField();
            ddf.AddToClassList("expandable");
            ddf.name = "ddf-weapon";
            ddf.Initialize(DBResources.GetWeaponsList(target.selectionWeapon.type), target.selectionWeapon, database);
            ddf.RegisterValueChangedCallback(_e => {
                target.selectionWeapon.SetDataExternally(ddf.index);
                target.SetDataExternally(target.selectionWeapon.selectionName);
                EditorUtility.SetDirty(database);
            });
        }
    }

    private void MakeNewDDFArmor() {
        ClearFields();

        target.collectibleType = SelectionCollectible.CollectibleType.Armor;

        ddf = new DropdownField();
        ddf.AddToClassList("expandable");
        ddf.name = "ddf-armor";
        ddf.Initialize(DBResources.GetArmor.armorList, target.selectionArmor, database);
        ddf.RegisterValueChangedCallback(_e => {
            target.selectionArmor.SetDataExternally(ddf.index);
            target.SetDataExternally(target.selectionArmor.selectionName);
            EditorUtility.SetDirty(database);
        });

        elParent.Add(ddf);
    }

    private void MakeNewDDFAccessory() {
        ClearFields();

        target.collectibleType = SelectionCollectible.CollectibleType.Accessories;

        ddf = new DropdownField();
        ddf.AddToClassList("expandable");
        ddf.name = "ddf-accessory";
        ddf.Initialize(DBResources.GetAccessories.accessoryList, target.selectionAccessory, database);
        ddf.RegisterValueChangedCallback(_e => {
            target.selectionAccessory.SetDataExternally(ddf.index);
            target.SetDataExternally(target.selectionAccessory.selectionName);
            EditorUtility.SetDirty(database);
        });

        elParent.Add(ddf);
    }

    private void ClearFields() {
        if (ddf != null) {
            ddf.parent.Remove(ddf);
            ddf = null;
        }

        if (ef != null) {
            ef.parent.Remove(ef);
            ef = null;
        }
    }

    public void Rebuild(SelectionCollectible _target) {
        target = _target;

        Remove(tc);
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        Initialize(_target, database);
    }
}
