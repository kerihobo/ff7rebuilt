using CharacterTypes;
using InventoryTypes;
using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class ScreenFieldPHS : ScreenField {
    public UICharacterInfo characterInfo;

    public override FieldMenu fieldMenu { get; set; }

    [SerializeField] private PHSMenuCharacterSelectMain menuMain;
    [SerializeField] private PHSMenuCharacterSelectReserve menuReserve;
    [SerializeField] private RectTransform rctInfoB;
    [SerializeField] private MateriaMenuSlotsMain materiaSlots;
    [SerializeField] private RectTransform equipmentTextParent;
    internal Player[] arrSelectedA;
    internal int selectedIndexA;
    private TextMeshProUGUI[] arrEquipmentTexts;

    public bool IsInfoBShown => rctInfoB.gameObject.activeSelf;

    private TextMeshProUGUI txtOptionWeapon => arrEquipmentTexts[0];
    private TextMeshProUGUI txtOptionArmor => arrEquipmentTexts[1];
    private TextMeshProUGUI txtOptionAccessory => arrEquipmentTexts[2];
    
    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        menuMain.Initialize(this);
        menuReserve.Initialize(this);
        characterInfo.Initialize(Vector2.zero, Vector2.zero);

        arrEquipmentTexts = new TextMeshProUGUI[equipmentTextParent.childCount];
        for (int i = 0; i < arrEquipmentTexts.Length; i++) {
            arrEquipmentTexts[i] = equipmentTextParent.GetChild(i).GetChild(0).GetComponent<TextMeshProUGUI>();
        }

        materiaSlots.Initialize(null);
        HideInfoB();
    }

    public override void SetDefaultDisplay() { }

    public override IEnumerator Open() {
        menuMain.ResetSelection();
        menuReserve.ResetSelection();
        
        Refresh();
        OpenMain();

        yield return null;
    }

    public void Refresh() {
        menuMain.characterInfoContainer.Refresh();
        menuReserve.characterInfoContainer.Refresh();
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public void OpenMain() {
        menuReserve.Disable();
        menuMain.Open();
    }

    public void OpenReserve() {
        menuMain.Disable();
        menuReserve.Open();
    }

    public void SwapPlayers(Player[] _arrSelectedB, int _selectedIndexB) {
        Player[] a = arrSelectedA;
        Player[] b = _arrSelectedB;
        int indexA = selectedIndexA;
        int indexB = _selectedIndexB;

        (a[indexA], b[indexB]) = (b[indexB], a[indexA]);

        Refresh();
        CancelSwap();
    }

    public void CancelSwap() {
        arrSelectedA = null;

        menuMain.cursorFlicker.Hide();
        menuReserve.cursorFlicker.Hide();
    }

    public void ShowInfoB(Player player) {
        if (player == null) return;

        Weapon weapon = player.weapon;
        Armor armor = player.armor;
        Accessory accessory = player.accessory;
        txtOptionWeapon.text = weapon?.name;
        txtOptionArmor.text = armor?.name;
        txtOptionAccessory.text = accessory?.name;

        materiaSlots.RefreshEquipment(player);
        materiaSlots.RefreshMateria(player);

        rctInfoB.gameObject.SetActive(true);
        
        CancelSwap();
    }

    public void HideInfoB() {
        rctInfoB.gameObject.SetActive(false);
    } 
}
