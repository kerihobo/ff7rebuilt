using FFVII.Database;
using System;
using System.Collections;
using UnityEditor;
using UnityEngine.UIElements;

public class EditorMagic : EditorAbilities {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Abilities/editor-magic.uxml";
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetAbilityList(AbilityType.Magic)[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetAbilityList(AbilityType.Magic);

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        ve.Q<EnumField>("enum-magic-order").SetMagicOrder(((Ability)target).magicOrder).RegisterValueChangedCallback(e => {
            ((Ability)target).magicOrder = (Ability.MagicOrder)e.newValue;
            EditorUtility.SetDirty(database);
        });
    }

    protected override void Refresh() {
        base.Refresh();

        ve.Q<EnumField>("enum-magic-order").value = ((Ability)target).magicOrder;
    }
}
