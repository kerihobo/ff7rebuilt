using CharacterTypes;
using InventoryTypes;
using FFVII.Database;
using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class PHS {
    public List<Player> lsFullCast { get; set; } = new List<Player>();
    public Player[] arrCurrent { get; set; } = new Player[3];
    public Player[] arrReserve { get; set; } = new Player[9];
    public Player partyLeader { get; set; }

    /// <summary>
    /// Only to be used for New Games. It is meant to populate starting data.
    /// </summary>
    public void Initialize() {
        foreach (Player player in DBResources.GetPlayers.playerList) {
            Player newPlayer = new Player(player);
            Weapon sourceWeapon = player.selectionStartingWeapon.GetSelectedWeapon();
            Armor sourceArmor = player.selectionStartingArmor.GetSelectedArmor();
            Accessory sourceAccessory = player.selectionStartingAccessory.GetSelectedAccessory();

            if (sourceWeapon != null) {
                newPlayer.weapon = new Weapon(sourceWeapon);

                SetEquipmentMateria(sourceWeapon, player.defaultMateriaWeapon, newPlayer.materiaWeapon);
            }

            if (sourceArmor != null) {
                newPlayer.armor = new Armor(sourceArmor);

                SetEquipmentMateria(sourceArmor, player.defaultMateriaArmor, newPlayer.materiaArmor);
            }

            if (sourceAccessory != null) {
                newPlayer.accessory = new Accessory(sourceAccessory);
            }

            newPlayer.levelUpManager.RunInitialLevelUp(newPlayer);
            newPlayer.limitBreakManager.limitLevels[0].isUnlocked = true;

            //CheatManager.UnlockAllLimitAbilities(newPlayer);
            CheatManager.UnlockAllButLastLimitBreaks(newPlayer);

            newPlayer.RecalculatePlayerStats();

            lsFullCast.Add(newPlayer);
        }

        SelectionData[] lsSDCurrent = DBResources.GetInitial.currentPlayers;
        for (int i = 0; i < arrCurrent.Length; i++) {
            arrCurrent[i] = lsSDCurrent[i].GetSelectedPlayer(lsFullCast);
            if (arrCurrent[i] != null && partyLeader == null) {
                partyLeader = arrCurrent[i];
            }
        }

        SelectionData[] lsSDReserve = DBResources.GetInitial.reservePlayers;
        for (int i = 0; i < arrReserve.Length; i++) {
            arrReserve[i] = lsSDReserve[i].GetSelectedPlayer(lsFullCast);
        }

        // Test
        CheatManager.PartyLevelUp(arrCurrent);

        //arrCurrent[0].hpCurrent = 0;
        //arrCurrent[0].statusCurrent.death.isActive = true;
        //arrCurrent[1].hpCurrent = 0;
        //arrCurrent[1].statusCurrent.death.isActive = true;
        //arrCurrent[2].hpCurrent = 0;
        //arrCurrent[2].statusCurrent.death.isActive = true;

        foreach (Player player in arrCurrent) {
            player?.Log();
        }

        void SetEquipmentMateria(Equipment _equipment, SelectionMateria[] _arrSelectionMateria, Materia[] _arrMateria) {
            for (int i = 0; i < _equipment.materiaSlotCount; i++) {
                Materia source = _arrSelectionMateria[i].GetSelectedMateria();

                if (source != null) {
                    _arrMateria[i] = new Materia(source);
                }
            }
        }
    }

    public Player[] GetAvailablePlayersInOrder() {
        Player[] arrPlayers = arrCurrent
            .Concat(arrReserve)
            .Where(x => x != null)
            .OrderBy(x => lsFullCast.IndexOf(x))
            .ToArray()
        ;

        return arrPlayers;
    }
}