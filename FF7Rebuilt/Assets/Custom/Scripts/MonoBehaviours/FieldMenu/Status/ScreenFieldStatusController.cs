public class ScreenFieldStatusController : MenuControllable {
    private ScreenFieldStatus screenFieldStatus;

    public void Initialize(ScreenFieldStatus _screenFieldStatus) {
        screenFieldStatus = _screenFieldStatus;
    }

    public void Open() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputVertical(int _v, bool isVerticalPressed) { }
    public override void SetControllable() { }

    public override void GetInputPage(int pageDirection, bool isPageXPressed) {
        screenFieldStatus.CycleCharacter(pageDirection);
    }

    public override void GetInputConfirmPressed() {
        screenFieldStatus.GoToNext();
    }

    public override void GetInputCancelPressed() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }
}
