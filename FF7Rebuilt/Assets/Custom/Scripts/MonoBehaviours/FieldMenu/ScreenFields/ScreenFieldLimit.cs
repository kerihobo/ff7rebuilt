using CharacterTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;


// JUST HAVE TO TAKE SOME NICE VIDS OF IT WORKING!
// MAYBE ADD CHEAT TO PHS FOR RANDOM LIMIT UNLOCKS
public class ScreenFieldLimit : ScreenField {
    public override FieldMenu fieldMenu { get; set; }
    public bool isSet { get; set; }

    [SerializeField] private TextMeshProUGUI txtDescription;
    [SerializeField] private UICharacterInfo playerInfo;
    [SerializeField] private LimitMenuOptionList optionList;
    [SerializeField] private RectTransform rctLimitSet;
    private Vector2 sizeLimitSet;
    private List<Player> lsActivePlayers = new List<Player>();
    private int selectedCharacterIndex;

    public Player GetSelectedCharacter => lsActivePlayers.Count > selectedCharacterIndex ? lsActivePlayers[selectedCharacterIndex] : null;

    private Player GetInitiallySelectedCharacter => GameManager.Instance.gameData.phs.arrCurrent[fieldMenu.selectedCharacterIndex];
    
    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        sizeLimitSet = rctLimitSet.sizeDelta;
        rctLimitSet.gameObject.SetActive(false);

        playerInfo.Initialize(Vector2.zero, Vector2.zero);
        optionList.Initialize(this);
    }

    public override void SetDefaultDisplay() { }

    public override IEnumerator Open() {
        lsActivePlayers = GameManager.Instance.gameData.phs.arrCurrent.Where(x => x != null).ToList();
        selectedCharacterIndex = lsActivePlayers.IndexOf(GetInitiallySelectedCharacter);

        Refresh();

        isSet = false;

        optionList.ResetSelection();
        optionList.Open();

        yield return null;
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public void CycleCharacter(int _direction) {
        selectedCharacterIndex = (selectedCharacterIndex + _direction) % lsActivePlayers.Count;
        if (selectedCharacterIndex < 0) {
            selectedCharacterIndex = lsActivePlayers.Count - 1;
        }

        Refresh();
    }

    private void Refresh() {
        playerInfo.Refresh(GetSelectedCharacter);
        optionList.RefreshLevels(GetSelectedCharacter);
    }

    public void SetDescription(string _description) {
        txtDescription.text = _description;
    }

    public IEnumerator SetCharacterLimitLevel(int _selectedLevel) {
        optionList.cursor.SetFlickerState(false);

        LimitBreakManager limitBreakManager = GetSelectedCharacter.limitBreakManager;
        limitBreakManager.currentLimitLevel = _selectedLevel;
        limitBreakManager.limitLevels[_selectedLevel].gaugeFill = 0;

        Refresh();

        rctLimitSet.gameObject.SetActive(true);
        rctLimitSet.sizeDelta = sizeLimitSet;

        yield return new WaitForSeconds(.33f);

        float t = 0;
        float duration = .125f;

        while (t < duration) {
            rctLimitSet.sizeDelta = Vector2.Lerp(sizeLimitSet, Vector2.zero, t / duration);

            t += Time.deltaTime;
            yield return null;
        }

        rctLimitSet.gameObject.SetActive(false);

        optionList.Open();
    }
}
