using CharacterTypes;
using FFVII.Database;
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class EnemyLsUniqueAbilities : VisualElement {
    public new class UxmlFactory : UxmlFactory<EnemyLsUniqueAbilities, UxmlTraits> { }

    public EnemyLsUniqueAbilities() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path).Instantiate();
        Add(tc);

        name = Name;
    }
    
    private const string Path = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/enemy-ls-unique-abilities.uxml";
    private const string Name = "enemy-ls-unique-abilities";

    private TemplateContainer tc;
    private Database database = DBResources.GetEnemies;
    private Enemy target;
    private ListView listView;

    public EnemyLsUniqueAbilities Initialize(Enemy _target, Database _database) {
        target = _target;
        database = _database;

        Refresh(target);

        return this;
    }

    public void Add() {
        target.uniqueAbilities.Add(new Enemy.UniqueAbiity());

        EditorUtility.SetDirty(database);
        AssetDatabase.SaveAssets();

        listView.RefreshItems();
    }

    public void Refresh(Enemy _target) {
        target = _target;

        if (listView != null) {
            Remove(listView);
        }

        listView = new ListView();
        listView.AddToClassList("scr-abilities");

        listView.fixedItemHeight = 24;
        listView.itemsSource = target.uniqueAbilities;
        listView.makeItem = MakeItem;
        listView.bindItem = BindItem;
        listView.selectionType = SelectionType.None;

        Add(listView);

        VisualElement MakeItem() => AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/enemy-lsitem-unique-ability.uxml").Instantiate();

        void BindItem(VisualElement _ve, int _i) {
            int j = _i;
            
            Enemy.UniqueAbiity ua = target.uniqueAbilities[j];

            _ve.Q<Label>().text = ua.ability.name;

            _ve.Q<Button>("btn-edit").clicked += Edit;
            _ve.Q<Button>("btn-remove").clicked += Remove;

            void Remove() {
                target.uniqueAbilities.Remove(ua);
                EditorUtility.SetDirty(database);
                AssetDatabase.SaveAssets();

                listView.RefreshItems();
            }

            void Edit() {
                //FFVII.Database.EWUniqueAbility.Initialize(ua.ability);
                EWUniqueAbility.OpenWindow();
                EWUniqueAbility.Instance.Initialize(ua, _ve.Q<Label>(), database);
            }
        }
    }

}
