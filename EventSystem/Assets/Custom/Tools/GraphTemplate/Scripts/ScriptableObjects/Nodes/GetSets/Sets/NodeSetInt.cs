﻿using UnityEngine;

public class NodeSetInt : NodeSet {
    public enum Function {
        Equal
    ,   Increment
    ,   Decrement
    }

    public Function function;

    private dynamic copyA;
    private dynamic copyB;

    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        minMaxThresholds = new Vector2(130, 190);
        size = new Vector2(minMaxThresholds.y, 74);

        AddPortIn<PortInt>("Value", Port.Capacity.SINGLE);
        AddPortOut<PortSetIntFunction>();

        portContainerIn.ports[1].SetValue(copyA);
        portContainerOut.ports[1].SetValue(copyB);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        function = (Function)portContainerOut.ports[1].GetValue();
        portContainerIn.ports[1].IsActive = function == Function.Equal;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeSetInt>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeSetInt)newNode).copyA = portContainerIn.ports[1].GetValue();
        ((NodeSetInt)newNode).copyB = portContainerOut.ports[1].GetValue();
        
        ((NodeSetInt)newNode).field = field;

        field.nodes.Add((NodeSetInt)newNode);

        return newNode;
    }

    public override void Run(NodeGraph _nodeGraph) {
        FieldInt targetField = (FieldInt)field;

        switch (function) {
            case Function.Equal:
                targetField.value = portContainerIn.ports[1].GetValue();
                break;
            case Function.Increment:
                targetField.value++;
                break;
            case Function.Decrement:
                targetField.value--;
                break;
        }
    }
}
