using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ConfigMenuMain : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform optionsParent;
    [SerializeField] private RectTransform optionsParentController;
    [SerializeField] private RectTransform optionsParentCursor;
    [SerializeField] private RectTransform optionsParentATB;
    [SerializeField] private RectTransform optionsParentCameraAngle;
    [SerializeField] private RectTransform textParentMagicOrder;
    [SerializeField] private ConfigWindowColorSelect menuWindowColorSelect;
    [SerializeField] private ConfigSound menuSound;
    [SerializeField] private ConfigMenuSliderPair sliderSpeedBattle;
    [SerializeField] private ConfigMenuSliderPair sliderSpeedBattleMessage;
    [SerializeField] private ConfigMenuSliderPair sliderSpeedFieldMessage;
    /// <summary>
    /// Window color: 0<br/>
    /// Sound: 1<br/>
    /// Controller: 2<br/>
    /// Cursor: 3<br/>
    /// ATB: 4<br/>
    /// Battle speed: 5<br/>
    /// Battle message: 6<br/>
    /// Field message: 7<br/>
    /// Camera angle: 8<br/>
    /// Magic order: 9<br/>
    /// </summary>
    private UIConfigInfo[] arrOptions;
    private int selectedIndex;
    private ScreenFieldConfig screenFieldConfig;
    private Color grey;
    private Color white;
    private TextMeshProUGUI[] arrControllerOptions;
    private TextMeshProUGUI[] arrCursorOptions;
    private TextMeshProUGUI[] arrATBOptions;
    private TextMeshProUGUI[] arrCameraAngleOptions;
    private TextMeshProUGUI[] arrTextMagicOrder;
    private float speedGaugeMaxX;

    private Config config => GameManager.Instance.gameData.config;

    public void Initialize(ScreenFieldConfig _screenFieldConfig) {
        screenFieldConfig = _screenFieldConfig;

        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());
        
        arrControllerOptions  = optionsParentController.GetComponentsInChildren<TextMeshProUGUI>();
        arrCursorOptions      = optionsParentCursor.GetComponentsInChildren<TextMeshProUGUI>();
        arrATBOptions         = optionsParentATB.GetComponentsInChildren<TextMeshProUGUI>();
        arrCameraAngleOptions = optionsParentCameraAngle.GetComponentsInChildren<TextMeshProUGUI>();
        arrTextMagicOrder     = textParentMagicOrder.GetComponentsInChildren<TextMeshProUGUI>();
        arrOptions            = optionsParent.GetComponentsInChildren<UIConfigInfo>();

        foreach (UIConfigInfo info in arrOptions) {
            info.Initialize();
        }

        //Disable();

        cursor.Initialize(arrOptions[0].rct);

        grey = TextColourContainer.GetInstance().grey;
        white = TextColourContainer.GetInstance().white;

        menuWindowColorSelect.Initialize(this);
        menuSound.Initialize(this);
        sliderSpeedBattle.Initialize();
        sliderSpeedBattleMessage.Initialize();
        sliderSpeedFieldMessage.Initialize();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rct);
    }

    public void ResetSelection() {
        canDelayOnPress = true;
        selectedIndex = 0;

        SetCursor();
    }

    public void Disable() {
        gameObject.SetActive(false);
    }

    private void Close() {
        canDelayOnPress = true;
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }

    public void Open() {
        gameObject.SetActive(true);

        cursor.Show();
        cursor.SetFlickerState(false);
        SetControllable();

        Refresh();

        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void RefreshControllerType() {
        int i = (int)config.controller;

        foreach (TextMeshProUGUI txt in arrControllerOptions) {
            txt.color = grey;
        }

        arrControllerOptions[i].color = white;
    }

    private void RefreshCursorType() {
        int i = (int)config.cursor;

        foreach (TextMeshProUGUI txt in arrCursorOptions) {
            txt.color = grey;
        }

        arrCursorOptions[i].color = white;
    }

    private void RefreshATBType() {
        int i = (int)config.atb;

        foreach (TextMeshProUGUI txt in arrATBOptions) {
            txt.color = grey;
        }

        arrATBOptions[i].color = white;
    }

    private void RefreshSpeedBattle() {
        sliderSpeedBattle.Refresh(config.speedBattle);
    }

    private void RefreshBattleMessage() {
        sliderSpeedBattleMessage.Refresh(config.speedMessageBattle);
    }

    private void RefreshFieldMessage() {
        sliderSpeedFieldMessage.Refresh(config.speedMessageField);
    }

    private void RefreshCameraAngle() {
        int i = (int)config.cameraAngle;

        foreach (TextMeshProUGUI txt in arrCameraAngleOptions) {
            txt.color = grey;
        }

        arrCameraAngleOptions[i].color = white;
    }

    private void RefreshMagicOrder() {
        int magicOrderIndex = config.magicOrderIndex;
        arrTextMagicOrder[0].text = $"<mspace=12>{magicOrderIndex + 1}";

        //Debug.Log($"txts({arrTextMagicOrder.Length}), " +
        //    $"magicOrders({Config.MagicOrders.Length}), " +
        //    $"magicOrderIndex({magicOrderIndex}), " +
        //    $"orders({Config.MagicOrders[magicOrderIndex].order.Length})"
        //);

        for (int i = 1; i < arrTextMagicOrder.Length; i++) {
            arrTextMagicOrder[i].text = Config.MagicOrders[magicOrderIndex].order[i - 1].ToString().ToLower();
        }
    }

    private void Refresh() {
        RefreshControllerType();
        RefreshCursorType();
        RefreshATBType();
        RefreshSpeedBattle();
        RefreshBattleMessage();
        RefreshFieldMessage();
        RefreshCameraAngle();
        RefreshMagicOrder();
    }

#region Override methods.
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputSquarePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        canDelayOnPress = true;

        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        screenFieldConfig.SetDescription(arrOptions[selectedIndex].description);

        SetCursor();
    }

    public void SetDescription(string _description) {
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        switch (selectedIndex) {
            case 3:
                config.IncrementCursorType(_h);
                RefreshCursorType();
                break;
            case 4:
                config.IncrementATBType(_h);
                RefreshATBType();
                break;
            case 5:
                canDelayOnPress = false;
                config.IncrementBattleSpeed(_h);
                RefreshSpeedBattle();
                break;
            case 6:
                canDelayOnPress = false;
                config.IncrementBattleMessage(_h);
                RefreshBattleMessage();
                break;
            case 7:
                canDelayOnPress = false;
                config.IncrementFieldMessage(_h);
                RefreshFieldMessage();
                break;
            case 8:
                config.IncrementCameraAngle(_h);
                RefreshCameraAngle();
                break;
            case 9:
                config.IncrementMagicOrder(_h);
                RefreshMagicOrder();
                break;
        }
    }

    public override void GetInputConfirmPressed() {
        //switch (selectedIndex) {
        //    case 0:
        //    case 1:
        //    case 2:
        //        Disable();
        //        break;
        //}

        switch (selectedIndex) {
            case 0:
                menuWindowColorSelect.Open();
                cursor.SetFlickerState(true);
                break;
            case 1:
                menuSound.Open();
                cursor.SetFlickerState(true);
                break;
            case 2:
                screenFieldConfig.OpenController();
                break;
        }

    }
#endregion
}
