using CharacterTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UISaveGameInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public GameData gameData { get; set; }

    [SerializeField] private RectTransform charactersParent;
    [SerializeField] private TextMeshProUGUI txtFirstCharacterName;
    [SerializeField] private TextMeshProUGUI txtFirstCharacterLevel;
    [SerializeField] private TextMeshProUGUI txtHours;
    [SerializeField] private TextMeshProUGUI txtMinutes;
    [SerializeField] private TextMeshProUGUI txtLocation;
    [SerializeField] private TextMeshProUGUI txtGil;
    [SerializeField] private RectTransform rctData;
    [SerializeField] private RectTransform rctEmpty;
    private Image[] imgCharacters;
    private Color white;
    private Color grey;

    public void Initialize() {
        rct = GetComponent<RectTransform>();

        imgCharacters = charactersParent.GetComponentsInChildren<Image>(true);

        white = TextColourContainer.GetInstance().white;
        grey = TextColourContainer.GetInstance().grey;
    }

    public void Refresh(GameData _gameData) {
        gameData = _gameData;
        //gameData = GameManager.Instance.gameData;

        Clear();

        if (gameData == null) {
            HideData();
            return;
        } else {
            ShowData();
        }

        string sFirstCharacterName = string.Empty;
        int iFirstCharacterLevel = 0;

        for (int i = 0; i < imgCharacters.Length; i++) {
            Player player = gameData.phs.arrCurrent[i];
            
            if (player != null) {
                imgCharacters[i].sprite = player.avatar;
                imgCharacters[i].color = Color.white;

                if (sFirstCharacterName == string.Empty) {
                    sFirstCharacterName = player.name;
                    iFirstCharacterLevel = player.points.level;

                    txtFirstCharacterName.text = sFirstCharacterName;
                    txtFirstCharacterLevel.text = iFirstCharacterLevel.ToString();
                }
            }
        }

        int secondsTotal = gameData.playTime;
        int minutes = ((int)(secondsTotal / 60f)) % 60;
        int hours = ((int)(secondsTotal / (60f * 12)));
        txtMinutes.text = "<mspace=12>" + minutes.ToString("00");
        txtHours.text = "<mspace=12>" + hours.ToString("00");

        txtLocation.text = gameData.location;
        txtGil.text = gameData.gil.ToString();
    }

    public void Clear() {
        for (int i = 0; i < imgCharacters.Length; i++) {
            imgCharacters[i].sprite = null;
            imgCharacters[i].color = Color.clear;
        }

        txtFirstCharacterName.text = string.Empty;
        txtFirstCharacterLevel.text = string.Empty;
        txtHours.text = string.Empty;
        txtMinutes.text = string.Empty;
        txtLocation.text = string.Empty;
        txtGil.text = string.Empty;
    }

    private void HideData() {
        rctData.gameObject.SetActive(false);
        rctEmpty.gameObject.SetActive(true);
    }

    private void ShowData() {
        rctData.gameObject.SetActive(true);
        rctEmpty.gameObject.SetActive(false);
    }
}
