using CharacterTypes;
using InventoryTypes;
using UnityEngine;

public class MateriaMenuExchangeSlots : MenuControllable {
    public UIMateriaSlotInfo[] arrSelected { get; set; }
    public ScreenFieldMateria screenFieldMateria { get; set; }
    public UIMateriaSlotInfo[] arrOptionsWeapon { get; set; }
    public UIMateriaSlotInfo[] arrOptionsArmor { get; set; }
    public int selectedIndex {
        get {
            return exchange.selectedSlotIndex;
        } set {
            exchange.selectedSlotIndex = value;
        }
    }

    [SerializeField] private RectTransform slotsParentWeapon;
    [SerializeField] private RectTransform slotsParentArmor;
    [SerializeField] private MateriaMenuEquipmentContainer equipmentContainerWeapon;
    [SerializeField] private MateriaMenuEquipmentContainer equipmentContainerArmor;
    private Player assignedPlayer;
    private MateriaMenuExchange exchange;
    private MateriaMenuExchangeCharacterContainer characterContainer;
    private float shortcutTimer;

    public bool IsWeapon => arrSelected == arrOptionsWeapon;

    private MateriaMenuExchangeMateriaList materiaList => exchange.materiaList;
    private FingerCursorRight cursor                   => exchange.cursorSlots;
    private FingerCursorRight cursorFlicker            => exchange.cursorSlotsFlicker;
    //private Materia[] arrTarget                        => exchange.arrMateriaA;
    //private int selectedSlotIndex                      => exchange.selectedSlotIndex;

    // Hack shortcut I discovered where pressing a diagonal...
    // ... or left and then up/down within a small window...
    // ... will auto navigate to Arm or Wpn.
    private void Update() {
        Shortcut();
        
        void Shortcut() {
            if (FieldMenu.Instance.menuControllable != this) {
                shortcutTimer = -1f;
                return;
            }

            float v = Input.GetAxisRaw("Vertical");
            if (Input.GetButtonDown("Vertical")) {
                if (shortcutTimer > 0) {
                    if ((IsWeapon && v < 0) || (!IsWeapon && v > 0)) {
                        Disable();

                        int optionsSelectedIndex = IsWeapon ? 2 : 1;
                        characterContainer.optionList.SetSelectedIndex(optionsSelectedIndex);
                        characterContainer.optionList.Open();

                        selectedIndex = 0;
                    }
                }
            }

            shortcutTimer -= Time.deltaTime;
        }
    }

    public void Initialize(MateriaMenuExchange _exchange, MateriaMenuExchangeCharacterContainer _characterContainer) {
        exchange = _exchange;
        characterContainer = _characterContainer;

        arrOptionsWeapon = slotsParentWeapon.GetComponentsInChildren<UIMateriaSlotInfo>(true);
        arrOptionsArmor = slotsParentArmor.GetComponentsInChildren<UIMateriaSlotInfo>(true);

        foreach (UIMateriaSlotInfo msi in arrOptionsWeapon) {
            msi.Initialize();
        }

        foreach (UIMateriaSlotInfo msi in arrOptionsArmor) {
            msi.Initialize();
        }

        equipmentContainerWeapon.Initialize();
        equipmentContainerArmor.Initialize();

        arrSelected = arrOptionsWeapon;
    }

    public void SetPlayer(Player _player) {
        assignedPlayer = _player;
    }

    public void SetDefaultDisplay() {
        //ShowWeapons();
        // Hide item list
        ResetSelection();
    }

    // Actually this should never happen. We only expect slot0 to be the initial selection
    // on a freshly loaded game/save.
    public void ResetSelection() {
        selectedIndex = 0;
        arrSelected = arrOptionsWeapon;

        //SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrSelected[selectedIndex].rct);
        RefreshMateriaInfo();
    }

    public void ExitSubMenu() {
        Open();
        FieldMenu.Instance.SetCooldownNavigate(FieldMenu.COOLDOWN_MAIN);
    }

    public void RefreshEquipment() {
        equipmentContainerWeapon.Refresh(assignedPlayer.weapon);
        equipmentContainerArmor.Refresh(assignedPlayer.armor);
    }

    public void RefreshMateria() {
        if (assignedPlayer == null) return;

        Weapon weapon = assignedPlayer.weapon;
        Armor armor = assignedPlayer.armor;

        for (int i = 0; i < arrOptionsWeapon.Length; i++) {
            arrOptionsWeapon[i].Clear();
            arrOptionsArmor[i].Clear();

            arrOptionsWeapon[i].Refresh(assignedPlayer.materiaWeapon[i]);
            arrOptionsArmor[i].Refresh(assignedPlayer.materiaArmor[i]);
        }
    }

    // This should be trying to show icon, name, stars and description at the top of the screen.
    public void RefreshMateriaInfo() {
        exchange.RefreshMateriaInfo(arrSelected[selectedIndex].materia);
    }

    public void Open() {
        //RefreshEquipment();
        //RefreshMateria();

        SetControllable();
        SetCursor();
        cursor.Show();
        //cursor.SetFlickerState(false);
    }

    public void Disable() {
        cursor.Hide();
        //screenFieldMateria.RefreshInfo(null);
    }

    public void SetEquipmentSelected(int verticalIndex) {
        arrSelected = verticalIndex == 0 ? arrOptionsWeapon : arrOptionsArmor;
    }

    public void RemoveMateriaFromSlot(int _slotIndex, Materia[] _arrMateria, UIMateriaSlotInfo[] _arrInfo) {
        UIMateriaSlotInfo slotInfo = _arrInfo[_slotIndex];

        GameManager.Instance.gameData.inventory.AddMateria(_arrMateria[_slotIndex]);
        _arrMateria[_slotIndex] = null;

        slotInfo.Refresh(null);
    }

#region Override methods.
    public override void GetInputSquarePressed() { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
    
    public override void GetInputConfirmPressed() {
        if (selectedIndex >= (IsWeapon ? assignedPlayer.weapon.materiaSlotCount : assignedPlayer.armor.materiaSlotCount)) return;

        if (exchange.arrMateriaA != null) {
            Materia[] arrTargetB = IsWeapon ? assignedPlayer.materiaWeapon : assignedPlayer.materiaArmor;
            exchange.Exchange2Slots(assignedPlayer, arrTargetB);
            // swap

            return;
        }

        exchange.SetCursorFlicker(cursorFlicker, arrSelected[selectedIndex].rct);

        Materia[] arrTarget = IsWeapon ? assignedPlayer.materiaWeapon : assignedPlayer.materiaArmor;
        exchange.playerA = assignedPlayer;
        exchange.arrMateriaA = arrTarget;
        exchange.materiaA = arrTarget[selectedIndex];
        exchange.slotIndexA = selectedIndex;
    }

    public override void GetInputCancelPressed() {
        if (exchange.arrMateriaA != null) {
            exchange.CloseCurrentExchange();
        } else {
            exchange.Close();
            // Close the Exchange menu and return to the Arrange menu.
            //FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
        }
    }
    
    // Can we make assignedPlayer just part of the base-class?
    public override void GetInputTrianglePressed() {
        Materia[] arrMateria = IsWeapon
        ? assignedPlayer.materiaWeapon
        : assignedPlayer.materiaArmor
        ;

        RemoveMateriaFromSlot(selectedIndex, arrMateria, arrSelected);
        assignedPlayer.RecalculatePlayerStats();
        RefreshMateriaInfo();

        FieldMenu.Instance.SetCooldownTraverse(2 / 60f);
    }

    // We need to be able to navigate right, to the list IF a selection A has been made, while our cursor flickers.
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (isHorizontalPressed && _h < 0) {
            shortcutTimer = FieldMenu.COOLDOWN_SCROLL;
        }

        bool isMidExchange = exchange.arrMateriaA != null;
        bool isExtremeLeft = selectedIndex + _h == -1;
        bool isExtremeRight = selectedIndex + _h == arrOptionsWeapon.Length;

        if (isExtremeLeft && !isMidExchange) {
            Disable();

            int optionsSelectedIndex = IsWeapon ? 1 : 2;
            characterContainer.optionList.SetSelectedIndex(optionsSelectedIndex);
            characterContainer.optionList.Open();

            return;
        } else if (isExtremeRight && isMidExchange) {
            Disable();
            materiaList.Open();

            return;
        }

        selectedIndex = Mathf.Clamp(selectedIndex + _h, 0, arrSelected.Length - 1);

        SetCursor();
        //RefreshMateria();
        exchange.RefreshMateriaInfo(arrSelected[selectedIndex].materia);
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        bool isPressUp = _v > 0;
        bool isPressDown = _v < 0;
        bool hasRoomAbove = exchange.lowBound + exchange.selectedIndex > 0;
        bool hasRoomBelow = exchange.lowBound + exchange.selectedIndex < exchange.arrViewableCharacters.Length - 1;
        bool isScrollUp = arrSelected == arrOptionsWeapon && isPressUp && hasRoomAbove;
        bool isScrollDown = arrSelected == arrOptionsArmor && isPressDown && hasRoomBelow;
        if (isScrollUp || isScrollDown) {
            exchange.NavigateCharactersSlots(_v);

            return;
        }

        arrSelected = _v < 0 ? arrOptionsArmor : arrOptionsWeapon;

        SetCursor();
        //RefreshMateria();
        exchange.RefreshMateriaInfo(arrSelected[selectedIndex].materia);
    }
#endregion
}