﻿using UnityEngine;
using System;
using System.Collections.Generic;
using CharacterTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Players", menuName = "Kerihobo/Database/Characters/Players")]
    public class DBPlayers : Database {
        public List<Player> playerList;
    }
}