﻿using UnityEngine;

public class NodeEntry : Node {
    public NodeEntry(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(65, 50);
        name = "Entry";
     
        AddPortOut<Port>();
    }

    public override void Run(NodeGraph _nodeGraph) {
        Port port = portContainerOut.ports[0];

        if (port.connectedPorts.Count > 0) {
            port.connectedPorts[0].parentNode.Run(_nodeGraph);
        }
    }
}
