using TMPro;
using UnityEngine;

public class ItemMenuUse : MonoBehaviour {
    public ItemMenuItemList itemList;
    
    [SerializeField] private TextMeshProUGUI txtDescription;
    private ItemMenuOptionList itemMenuOptionList;

    public void Initialize(ScreenFieldItem _screenFieldItem, ItemMenuOptionList _itemMenuOptionList) {
        itemMenuOptionList = _itemMenuOptionList;

        SetDescription(null);
        itemList.Initialize(_screenFieldItem, this);
    }

    public void Open() {
        itemList.Open();
    }

    public void Close() {
        itemMenuOptionList.ExitSubMenu();
        SetDescription(null);
    }

    public void SetDescription(string _description) {
        txtDescription.text = _description;
    }

}
