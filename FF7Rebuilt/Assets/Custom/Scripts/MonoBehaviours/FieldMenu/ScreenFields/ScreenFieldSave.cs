using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class ScreenFieldSave : ScreenField {
    public override FieldMenu fieldMenu { get; set; }
    public SaveMenuSaveGameList saveGameList { get; set; }

    [SerializeField] private SaveMenuDataFileSelect dataFileSelect;
    [SerializeField] private TextMeshProUGUI txtDescription;
    [SerializeField] private RectTransform rctSaveWait;
    [SerializeField] private RectTransform rctSaveComplete;

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        dataFileSelect.Initialize(this);
        rctSaveWait.gameObject.SetActive(false);
        rctSaveComplete.gameObject.SetActive(false);
    }

    public override void SetDefaultDisplay() { }

    public override IEnumerator Open() {
        dataFileSelect.ResetSelection();
        dataFileSelect.Open();

        yield return null;
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public void SetDescription(string _description) {
        txtDescription.text = _description;
    }

    public void OpenDataFileSelect() {
        dataFileSelect.Open();
    }

    public void Save(int _selectedSlot, int _selectedSave) {
        saveGameList.Disable();
        SetDescription("");
        rctSaveWait.gameObject.SetActive(true);

        StartCoroutine(Save());
        
        IEnumerator Save() {
            GameData gameData = GameManager.Instance.gameData;
            SaveGameSlot saveGameSlot = GameManager.Instance.saveGameSlots[_selectedSlot];
            GameData[] arrSaveGameFiles = saveGameSlot.saveGameFiles;
            arrSaveGameFiles[_selectedSave] = gameData;
            
            yield return new WaitForSeconds(.5f);
            
            saveGameList.Show();
            rctSaveWait.gameObject.SetActive(false);
            rctSaveComplete.gameObject.SetActive(true);

            yield return new WaitForSeconds(.5f);

            rctSaveComplete.gameObject.SetActive(false);
            saveGameList.Open();
        }
    }

}
