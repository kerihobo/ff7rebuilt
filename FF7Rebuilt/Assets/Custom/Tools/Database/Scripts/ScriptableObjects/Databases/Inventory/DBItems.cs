﻿using UnityEngine;
using System;
using System.Collections.Generic;
using InventoryTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Items", menuName = "Kerihobo/Database/Inventory/Items")]
    public class DBItems : Database {
        public List<Item> itemsList = new List<Item>();
    }
}