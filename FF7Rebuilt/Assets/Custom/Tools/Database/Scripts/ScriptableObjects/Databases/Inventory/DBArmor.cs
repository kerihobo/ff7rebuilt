﻿using UnityEngine;
using System;
using System.Collections.Generic;
using InventoryTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Armor", menuName = "Kerihobo/Database/Inventory/Armor")]
    public class DBArmor : Database {
        public List<Armor> armorList = new List<Armor>();
    }
}