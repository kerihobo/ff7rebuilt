using CharacterTypes;
using InventoryTypes;
using System.Linq;
using TMPro;
using UnityEngine;

public class MagicMenuEnemySkillInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public Ability abilityReference { get; set; }

    private TextMeshProUGUI txtName;

    public void Initialize() {
        txtName = GetComponent<TextMeshProUGUI>();
        rct = GetComponent<RectTransform>();
    }

    public void Refresh(Player _player, Ability[] _arrViewItems, int _abilityIndex) {
        bool foundEnemySkill = GetSkillUnlocked(_player, _abilityIndex);
        
        if (foundEnemySkill) {
            abilityReference = _arrViewItems[_abilityIndex];
            txtName.text = abilityReference.name;
        } else {
            Clear();
        }
    }
    
    public bool GetSkillUnlocked(Player _player, int _abilityIndex) {
        Materia materia = _player.Materia.FirstOrDefault(x => x.IsEnemySkill);

        if (materia == null) return false;

        bool[] skillsUnlocked = materia.enemySkillManager.skillsUnlocked;

        return skillsUnlocked[_abilityIndex];
    }

    public void Clear() {
        abilityReference = null;
        txtName.text = "";
    }
}
