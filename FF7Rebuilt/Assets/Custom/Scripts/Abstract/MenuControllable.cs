using UnityEngine;

public abstract class MenuControllable : MonoBehaviour {
    public virtual bool canDelayOnPress { get; set; } = true;

    public abstract void SetControllable();
    public abstract void GetInputConfirmPressed();
    public abstract void GetInputSquarePressed();
    public abstract void GetInputTrianglePressed();
    public abstract void GetInputConfirm();
    public abstract void GetInputCancel();
    public abstract void GetInputCancelPressed();
    public abstract void GetInputVertical(int _v, bool isVerticalPressed);
    public abstract void GetInputHorizontal(int _h, bool isHorizontalPressed);
    public abstract void GetInputPage(int pageDirection, bool isPageXPressed);
}
