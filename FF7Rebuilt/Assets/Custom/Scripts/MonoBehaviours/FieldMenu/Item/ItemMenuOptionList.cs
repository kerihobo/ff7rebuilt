using TMPro;
using UnityEngine;

public class ItemMenuOptionList : MenuControllable {
    public FingerCursorRight cursor;
    public ItemMenuUse itemMenuUse;
    public ItemMenuArrange itemMenuArrange;
    public ItemMenuKeyItems itemMenuKeyItems;

    public RectTransform rct { get; private set; }

    [SerializeField] private RectTransform optionsParent;
    /// <summary>
    /// Use: 0<br/>
    /// Arrange: 1<br/>
    /// Key Items: 2<br/>
    /// </summary>
    private TextMeshProUGUI[] arrOptions;
    private int selectedIndex;
    private ScreenFieldItem screenFieldItem;

    public TextMeshProUGUI txtOptionUse => arrOptions[0];

    public void Initialize(ScreenFieldItem _screenFieldItem) {
        screenFieldItem = _screenFieldItem;
        
        rct = GetComponent<RectTransform>();

        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        cursor.Initialize(txtOptionUse.rectTransform);
        itemMenuUse.Initialize(screenFieldItem, this);
        itemMenuArrange.Initialize(this);
        itemMenuKeyItems.Initialize(this);
    }

    public void SetDefaultDisplay() {
        ShowItems();
        ResetSelection();
    }

    private void ShowItems() {
        itemMenuUse.gameObject.SetActive(true);
        itemMenuKeyItems.gameObject.SetActive(false);

        itemMenuUse.itemList.Refresh();
    }

    private void ShowKeyItems() {
        itemMenuUse.gameObject.SetActive(false);
        itemMenuKeyItems.gameObject.SetActive(true);
        
        itemMenuKeyItems.Refresh();
    }

    private void ResetSelection() {
        selectedIndex = 0;

        SetCursor();

        itemMenuUse.itemList.ResetSelection();
        //itemMenuKeyItems.itemList.ResetSelection();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void EnterItemSelection() {
        itemMenuUse.Open();
        //characterSelect.Open(selectedIndex);
    }

    private void EnterArrangeSelection() {
        itemMenuArrange.Open();
    }

    private void EnterKeyItemSelection() {
        itemMenuKeyItems.Open();
    }

    public void ExitSubMenu() {
        cursor.SetFlickerState(false);
        SetControllable();
        FieldMenu.Instance.SetCooldownNavigate(FieldMenu.COOLDOWN_MAIN);
    }

    public override void GetInputConfirmPressed() {
        cursor.SetFlickerState(true);

        switch (selectedIndex) {
            case 0: // Use
                EnterItemSelection();
                break;
            case 1: // Arrange
                EnterArrangeSelection();
                break;
            case 2: // Key Items
                EnterKeyItemSelection();
                break;
            default:
                break;
        }
    }

    public override void GetInputCancelPressed() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
        // Exit to Field Menu.
    }

    public override void GetInputCancel() {
        GetInputCancelPressed();
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        selectedIndex = (selectedIndex + _h) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();

        switch (selectedIndex) {
            case 2: // Arrange
                ShowKeyItems();
                break;
            default:
                ShowItems();
                break;
        }
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) { }
    public override void GetInputPage(int _pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
}
