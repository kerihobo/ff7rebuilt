using System.Collections;
using UnityEngine;

public class ScreenFieldQuit : ScreenField {
    public override FieldMenu fieldMenu { get; set; }

    [SerializeField] private QuitMenuOptionList optionList;

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        optionList.Initialize(this);
    }

    public override void SetDefaultDisplay() { }

    public override IEnumerator Open() {
        optionList.Open();

        yield return null;
    }

    public override IEnumerator Close() {
        yield return null;
    }
}
