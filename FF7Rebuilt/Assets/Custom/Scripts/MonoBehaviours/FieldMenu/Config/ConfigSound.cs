﻿using TMPro;
using UnityEngine;

public class ConfigSound : MenuControllable {
    [SerializeField] private FingerCursorRight cursor;
    [SerializeField] private RectTransform optionParent;
    [SerializeField] private ConfigMenuSliderPair sliderVolumeMusic;
    [SerializeField] private ConfigMenuSliderPair sliderVolumeSFX;
    // See about manipulating canDelayOnPress and TraverseCooldown.
    [SerializeField] private TextMeshProUGUI txtVolumeMusic;
    [SerializeField] private TextMeshProUGUI txtVolumeSFX;
    private int selectedIndex;
    private RectTransform[] arrOptions;
    private ConfigMenuMain configOptionList;

    private Config config => GameManager.Instance.gameData.config;

    public void Initialize(ConfigMenuMain _configOptionList) {
        configOptionList = _configOptionList;

        arrOptions = new RectTransform[optionParent.childCount];
        for (int i = 0; i < arrOptions.Length; i++) {
            arrOptions[i] = optionParent.GetChild(i).GetComponent<RectTransform>();
        }

        cursor.Initialize(arrOptions[0]);
        sliderVolumeMusic.Initialize();
        sliderVolumeSFX.Initialize();

        Disable();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex]);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    private void Disable() {
        gameObject.SetActive(false);
    }

    private void Close() {
        Disable();
        configOptionList.Open();
    }

    public void Open() {
        gameObject.SetActive(true);

        ResetSelection();

        cursor.Show();
        RefreshVolumeMusic();
        RefreshVolumeSFX();

        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void RefreshVolumeMusic() {
        FieldMenu.Instance.SetCooldownTraverse(.05f);
        sliderVolumeMusic.Refresh(config.volumeMusic);
        txtVolumeMusic.text = $"<mspace=12>{config.volumeMusic:000}";
    }

    private void RefreshVolumeSFX() {
        FieldMenu.Instance.SetCooldownTraverse(.05f);
        sliderVolumeSFX.Refresh(config.volumeSFX);
        txtVolumeSFX.text = $"<mspace=12>{config.volumeSFX:000}";
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputConfirmPressed() { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        //canDelay = true;

        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();

    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        switch (selectedIndex) {
            case 0:
                config.IncrementVolumeMusic(_h);
                RefreshVolumeMusic();
                break;
            case 1:
                config.IncrementVolumeSFX(_h);
                RefreshVolumeSFX();
                break;
        }
    }
#endregion
}