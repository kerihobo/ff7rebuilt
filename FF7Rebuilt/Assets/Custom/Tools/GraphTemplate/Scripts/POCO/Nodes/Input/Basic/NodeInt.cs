﻿using System;
using UnityEngine;

[Serializable]
public class NodeInt : Node {
    public NodeInt(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(165, 55);
        name = "Int";

        AddPortIn<PortInt>("In", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_FIELD);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        return portContainerIn.ports[0].GetValue();
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = new NodeInt(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeInt)newNode).portContainerIn.ports[0].SetValue(portContainerIn.ports[0].GetValue());

        return newNode;
    }
}