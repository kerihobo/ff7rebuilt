﻿using System;
using UnityEngine;

[Serializable]
public class NodeGetInt : NodeGet {
    public NodeGetInt(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        minMaxThresholds = new Vector2(140, 140);
        size = new Vector2(minMaxThresholds.y, 50);

        AddPortIn<PortFieldLabel>();
        AddPortOut<PortInt>("Value", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        int value = ((FieldInt)field).value;
        return value;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        NodeGetInt newNode = new NodeGetInt(_nodeGraph, position + duplicationOffset, category, false);

        newNode.Initialize(field);

        return newNode;
    }
}
