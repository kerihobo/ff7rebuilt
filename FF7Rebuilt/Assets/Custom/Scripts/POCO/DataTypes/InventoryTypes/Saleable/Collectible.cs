﻿using System;

namespace InventoryTypes {
    [Serializable]
    public class Collectible : Saleable {
        public Collectible() : base() {
            fieldApplication = ItemFieldApplication.NONE;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Collectible(Collectible _source) : base(_source) {
            quantity         = _source.quantity;
            fieldApplication = _source.fieldApplication;
            isBattleItem     = _source.isBattleItem;
        }

        public int quantity;
        public ItemFieldApplication fieldApplication = ItemFieldApplication.NONE;
        public bool isBattleItem;
        public bool isThrowable;

        public bool IsUsableInField() {
            switch (fieldApplication) {
                case ItemFieldApplication.NONE:
                    return false;
                case ItemFieldApplication.TENT:
                    return GameManager.Instance.IsSaveAvailable;
                case ItemFieldApplication.SAVE_CRYSTAL:
                    return GameManager.Instance.IsInNorthernCrater;
                default:
                    return true;
            }
        }

        public ItemSortTag GetSortTag() {
            Weapon weaponSelf = this as Weapon;
            if (isBattleItem && fieldApplication != ItemFieldApplication.NONE) {
                return ItemSortTag.FIELD | ItemSortTag.BATTLE;
            } else if (isBattleItem) {
                return ItemSortTag.BATTLE;
            } else if (fieldApplication != ItemFieldApplication.NONE) {
                return ItemSortTag.FIELD;
            } else if (weaponSelf != null && weaponSelf.isThrowable) {
                return ItemSortTag.THROWABLE;
            }

            return ItemSortTag.NONE;
        }
    }
}
