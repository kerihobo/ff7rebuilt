using UnityEngine.UIElements;

public class EditorMateria : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Inventory/editor-materia.uxml";
    protected override string Name { get; set; } = "editor-materia";

    public EditorMateria() {
    }
}
