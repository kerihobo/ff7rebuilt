using UnityEngine.UIElements;

public static class ExtSliderInt {
    public static SliderInt SetValue(this SliderInt _self, int _value) {
        _self.value = _value;
        return _self;
    }
}
