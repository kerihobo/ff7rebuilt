using FFVII.Database;
using InventoryTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorMateria : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Inventory/editor-materia.uxml";
    protected override Database database { get; set; } = DBResources.GetMateria;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetMateriaList(MateriaType.Magic)[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetMateriaList(MateriaType.Magic);

    private MateriaAbilitiesEditor abilityEditor;
    private (Button, StatusEffectManager.StatusEffect)[] arrPairStatusResistanceButtons;

    public EditorMateria() : base() {
        SetSelected(ve.Q<Button>("btn-type-select-magic"));

        OnSelectionChange(DBResources.GetMateriaList(MateriaType.Magic));
        //ChangeCategory(MateriaType.Magic, new MateriaAbilitiesMagicEditor((Materia)target, database), e);
    }

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Materia)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        BindTypeSelectionButtons();

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<TextField>("txt-description").SetValue(((Materia)target).description).RegisterValueChangedCallback(e => {
            ((Materia)target).description = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<IntegerField>("int-sale-cost").SetValue(((Materia)target).saleCost).RegisterValueChangedCallback(e => {
            ((Materia)target).saleCost = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<IntegerField>("int-sell-value-master").SetValue(((Materia)target).sellValueMaster).RegisterValueChangedCallback(e => {
            ((Materia)target).sellValueMaster = e.newValue;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfElement = ve.Q<DropdownField>("ddf-element");
        ddfElement.choices = ElementSelection.ELEMENT_SELECTION_NAMES.ToList();
        ddfElement.index = ((Materia)target).elementIndex;
        ddfElement.RegisterValueChangedCallback(_e => {
            ((Materia)target).elementIndex = ddfElement.index;

            List<ElementalAffinityManager.Affinity> lsElements = ((Materia)target).elements.GetAsList();
            lsElements.ForEach(x => x.isActive = false);

            int i = ElementSelection.GetIndex(((Materia)target).elementIndex);
            Debug.Log(i);
            if (i > -1) {
                lsElements[i].isActive = true;
            }

            EditorUtility.SetDirty(database);
        });

        BindStatEnhancements();
        BindLevelParameters();
        BindStatuses();

        abilityEditor = new MateriaAbilitiesMagicEditor((Materia)target, database);
        ve.Q<VisualElement>("ve-abilities").Add(abilityEditor);
        abilityEditor.BindAbilities();

        void BindTypeSelectionButtons() {
            ve.Q<Button>("btn-type-select-magic").RegisterCallback<ClickEvent>((e) => {
                ChangeCategory(MateriaType.Magic, new MateriaAbilitiesMagicEditor((Materia)target, database), e);
            });

            ve.Q<Button>("btn-type-select-summon").RegisterCallback<ClickEvent>((e) =>
                ChangeCategory(MateriaType.Summon, new MateriaAbilitiesSummonEditor((Materia)target, database), e)
            );

            ve.Q<Button>("btn-type-select-command").RegisterCallback<ClickEvent>((e) =>
                ChangeCategory(MateriaType.Command, new MateriaAbilitiesCommandEditor((Materia)target, database, this), e)
            );

            ve.Q<Button>("btn-type-select-independent").RegisterCallback<ClickEvent>((e) =>
                ChangeCategory(MateriaType.Independent, new MateriaAbilitiesIndependentEditor((Materia)target, database), e)
            );

            ve.Q<Button>("btn-type-select-support").RegisterCallback<ClickEvent>((e) =>
                ChangeCategory(MateriaType.Support, new MateriaAbilitiesSupportEditor((Materia)target, database), e)
            );

            void ChangeCategory(MateriaType _materiaType, MateriaAbilitiesEditor _abilityEditor, ClickEvent e) {
                ReplaceAbilityEditor();
                RenewList();
                SetSelected((Button)e.currentTarget);
                OnSelectionChange(DBResources.GetMateriaList(_materiaType));

                Refresh();

                void ReplaceAbilityEditor() {
                    VisualElement veAbilities = ve.Q<VisualElement>("ve-abilities");
                    veAbilities.Remove(abilityEditor);
                    abilityEditor = _abilityEditor;
                    veAbilities.Add(abilityEditor);
                    abilityEditor.BindAbilities();
                }

                void RenewList() {
                    itemSource = DBResources.GetMateriaList(_materiaType);
                    bindItem = (e, i) => e.Q<Label>().text = DBResources.GetMateriaList(_materiaType)[i].name;

                    listView.bindItem = null;
                    listView.itemsSource = itemSource;
                    listView.bindItem = bindItem;
                    listView.SetSelection(0);
                }
            }

        }

        void BindStatEnhancements() {
            ve.Q<IntegerField>("int-strength").SetValue(((Materia)target).equipEffects.primary.strength).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.primary.strength = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-vitality").SetValue(((Materia)target).equipEffects.primary.vitality).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.primary.vitality = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-magic").SetValue(((Materia)target).equipEffects.primary.magic).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.primary.magic = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-spirit").SetValue(((Materia)target).equipEffects.primary.spirit).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.primary.spirit = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-luck").SetValue(((Materia)target).equipEffects.primary.luck).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.primary.luck = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-dexterity").SetValue(((Materia)target).equipEffects.primary.dexterity).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.primary.dexterity = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-hp-percent").SetValue(((Materia)target).equipEffects.maxHPPercent).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.maxHPPercent = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-mp-percent").SetValue(((Materia)target).equipEffects.maxMPPercent).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.maxMPPercent = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-magic-defense").SetValue(((Materia)target).equipEffects.derived.magicDefense).RegisterValueChangedCallback(e => {
                ((Materia)target).equipEffects.derived.magicDefense = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }
        
        void BindLevelParameters() {
            List<IntegerField> integerFields = ve.Query<GroupBox>("grp-levels").Children<IntegerField>().ToList();
            
            ve.Q<SliderInt>("sld-levels").SetValue(((Materia)target).levels).RegisterValueChangedCallback(e => {
                ((Materia)target).levels = e.newValue;
                EditorUtility.SetDirty(database);

                List<IntegerField> integerFields = ve.Query<GroupBox>("grp-levels").Children<IntegerField>().ToList();
                integerFields.ForEach(x => x.style.display = DisplayStyle.None);
                for (int i = 0; i < e.newValue - 1; i++) {
                    integerFields[i].style.display = DisplayStyle.Flex;
                }

                abilityEditor.OnLevelsChanged();
            });

            for (int i = 0; i < integerFields.Count; i++) {
                int j = i;
                ve.Q<IntegerField>($"int-lv-{j + 2}").SetValue(((Materia)target).apThresholds[j + 1]).RegisterValueChangedCallback(e => {
                    ((Materia)target).apThresholds[j + 1] = e.newValue;
                    EditorUtility.SetDirty(database);
                });
            }
        }

        void BindStatuses() {
            RestockButtonArrayStatusResistance();

            for (int i = 0; i < arrPairStatusResistanceButtons.Length; i++) {
                int j = i;
                arrPairStatusResistanceButtons[j].Item1.RegisterCallback<ClickEvent>(e => {
                    arrPairStatusResistanceButtons[j].Item1.ToggleAfflictionBehaviour(ref arrPairStatusResistanceButtons[j].Item2, ((Materia)target).statusEffectManager.statusEffectAction);
                    EditorUtility.SetDirty(database);
                });
            }
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = ((Materia)target).name;
        ve.Q<TextField>("txt-description").value = ((Materia)target).description;
        ve.Q<IntegerField>("int-sale-cost").value = ((Materia)target).saleCost;
        ve.Q<IntegerField>("int-sell-value-master").value = ((Materia)target).sellValueMaster;
        ve.Q<DropdownField>("ddf-element").index = ((Materia)target).elementIndex;

        int i = ElementSelection.GetIndex(((Materia)target).elementIndex);
        List<ElementalAffinityManager.Affinity> lsElements = ((Materia)target).elements.GetAsList();
        if (i > -1) {
            lsElements[i].isActive = true;
        }

        RefreshStatEnhancements();
        RefreshLevelParameters();
        RefreshStatuses();

        abilityEditor?.Refresh((Materia)target, ve);
        //ve.Q<VisualElement>("ve-abilities").style.display = ((Materia)target).IsMaster ? DisplayStyle.None : DisplayStyle.Flex;
        //ve.Q<VisualElement>("ve-levels").style.display = ((Materia)target).IsMaster ? DisplayStyle.None : DisplayStyle.Flex;

        void RefreshLevelParameters() {
            ve.Q<SliderInt>("sld-levels").value = ((Materia)target).levels;

            List<IntegerField> integerFields = ve.Query<GroupBox>("grp-levels").Children<IntegerField>().ToList();
            integerFields.ForEach(x => x.style.display = DisplayStyle.None);
            for (int i = 0; i < ((Materia)target).levels - 1; i++) {
                integerFields[i].style.display = DisplayStyle.Flex;
            }

            for (int i = 0; i < integerFields.Count; i++) {
                integerFields[i].value = ((Materia)target).apThresholds[i+1];
            }
        }

        void RefreshStatEnhancements() {
            GroupBox statEnhancements = ve.Q<GroupBox>("grp-stat-enhancements");

            statEnhancements.Q<IntegerField>("int-strength").value = ((Materia)target).equipEffects.primary.strength;
            statEnhancements.Q<IntegerField>("int-vitality").value = ((Materia)target).equipEffects.primary.vitality;
            statEnhancements.Q<IntegerField>("int-magic").value = ((Materia)target).equipEffects.primary.magic;
            statEnhancements.Q<IntegerField>("int-spirit").value = ((Materia)target).equipEffects.primary.spirit;
            statEnhancements.Q<IntegerField>("int-dexterity").value = ((Materia)target).equipEffects.primary.dexterity;
            statEnhancements.Q<IntegerField>("int-luck").value = ((Materia)target).equipEffects.primary.luck;
            statEnhancements.Q<IntegerField>("int-hp-percent").value = ((Materia)target).equipEffects.maxHPPercent;
            statEnhancements.Q<IntegerField>("int-mp-percent").value = ((Materia)target).equipEffects.maxMPPercent;
            statEnhancements.Q<IntegerField>("int-magic-defense").value = ((Materia)target).equipEffects.derived.magicDefense;
        }

    }

    private void RefreshStatuses() {
        VisualElement statusEffects = ve.Q<VisualElement>("ve-afflictions");

        RestockButtonArrayStatusResistance();

        StatusEffectManager statusEffectManager = ((Materia)target).statusEffectManager;
        for (int i = 0; i < arrPairStatusResistanceButtons.Length; i++) {
            arrPairStatusResistanceButtons[i].Item1.SetAfflictionBehaviourClass(arrPairStatusResistanceButtons[i].Item2.isActive, statusEffectManager.statusEffectAction);
        }
    }

    private void RestockButtonArrayStatusResistance() {
        GroupBox statusEffects = ve.Q<GroupBox>("grp-afflictions");
        List<Button> lsButtons = statusEffects.Query<Button>().ToList();

        StatusEffectManager statusEffectManager = ((Materia)target).statusEffectManager;

        arrPairStatusResistanceButtons = new (Button, StatusEffectManager.StatusEffect)[] {
                (lsButtons[0],  statusEffectManager.death)
            ,   (lsButtons[1],  statusEffectManager.sleep)
            ,   (lsButtons[2],  statusEffectManager.poison)
            ,   (lsButtons[3],  statusEffectManager.sadness)
            ,   (lsButtons[4],  statusEffectManager.fury)
            ,   (lsButtons[5],  statusEffectManager.confusion)
            ,   (lsButtons[6],  statusEffectManager.silence)
            ,   (lsButtons[7],  statusEffectManager.haste)
            ,   (lsButtons[8],  statusEffectManager.slow)
            ,   (lsButtons[9],  statusEffectManager.stop)
            ,   (lsButtons[10], statusEffectManager.frog)
            ,   (lsButtons[11], statusEffectManager.small)
            ,   (lsButtons[12], statusEffectManager.slowNumb)
            ,   (lsButtons[13], statusEffectManager.petrify)
            ,   (lsButtons[14], statusEffectManager.regen)
            ,   (lsButtons[15], statusEffectManager.barrier)
            ,   (lsButtons[16], statusEffectManager.mBarrier)
            ,   (lsButtons[17], statusEffectManager.reflect)
            ,   (lsButtons[18], statusEffectManager.shield)
            ,   (lsButtons[19], statusEffectManager.deathSentence)
            ,   (lsButtons[20], statusEffectManager.berserk)
            ,   (lsButtons[21], statusEffectManager.paralyzed)
            ,   (lsButtons[22], statusEffectManager.darkness)
            ,   (lsButtons[23], statusEffectManager.deathForce)
            ,   (lsButtons[24], statusEffectManager.resist)
            ,   (lsButtons[25], statusEffectManager.manipulate)
            ,   (lsButtons[26], statusEffectManager.sense)
            ,   (lsButtons[27], statusEffectManager.seizure)
            ,   (lsButtons[28], statusEffectManager.imprisoned)
            ,   (lsButtons[29], statusEffectManager.peerless)
            };
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Materia() { type = GetSelectedMateriaType() });

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new Materia() { type = GetSelectedMateriaType() });

        base.Add();
    }

    private MateriaType GetSelectedMateriaType() {
        if (listView.itemsSource == DBResources.GetMateriaList(MateriaType.Magic)) return MateriaType.Magic;
        if (listView.itemsSource == DBResources.GetMateriaList(MateriaType.Summon)) return MateriaType.Summon;
        if (listView.itemsSource == DBResources.GetMateriaList(MateriaType.Command)) return MateriaType.Command;
        if (listView.itemsSource == DBResources.GetMateriaList(MateriaType.Independent)) return MateriaType.Independent;
        if (listView.itemsSource == DBResources.GetMateriaList(MateriaType.Support)) return MateriaType.Support;

        return MateriaType.Magic;
    }

    void SetSelected(VisualElement _ve) {
        VisualElement typeList = ve.Q<VisualElement>("type-list");
        typeList.Query<Button>().ForEach(x => x.RemoveFromClassList("selected"));
        _ve.AddToClassList("selected");
    }
}