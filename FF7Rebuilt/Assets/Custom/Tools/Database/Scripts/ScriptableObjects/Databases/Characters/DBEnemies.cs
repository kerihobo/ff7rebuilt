﻿using UnityEngine;
using System;
using System.Collections.Generic;
using CharacterTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Enemies", menuName = "Kerihobo/Database/Characters/Enemies")]
    public class DBEnemies : Database {
        public List<Enemy> enemyList;
    }
}