using FFVII.Database;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public static class ExtDropdownField {
    public static void Initialize<T>(this DropdownField _self, List<T> _boundList, SelectionData _boundData, Database _database) where T : Data {
        _self.RefreshNames(_boundList, _boundData);
        _self.ClampEnemyIndex(_boundList, _boundData, _database);

        _self.index = _boundData.index + 1;
    }

    public static void RefreshNames<T>(this DropdownField _self, List<T> _list, SelectionData _boundData) where T : Data {
        _boundData.ForceNamesUpdate(_list);
        
        List<string> names = _list.Select(x => x.GetNonPathName()).ToList();
        names.Insert(0, "-");
        for (int i = 0; i < names.Count; i++) {
            if (string.IsNullOrEmpty(names[i])) {
                names[i] = "UNNAMED";
            }
        }

        _self.choices = names;
        _self.index = _boundData.index + 1;
    }

    public static void ClampEnemyIndex<T>(this DropdownField _self, List<T> _boundList, SelectionData _boundData, Database _database) {
        if (_boundData.index >= _boundList.Count) {
            Debug.Log($"(>=) {_boundData.index}");
            _boundData.index = _boundList.Count - 1;
            EditorUtility.SetDirty(_database);
        }

        if (_boundData.index < -1) {
            Debug.Log($"(<0) {_boundData.index}");
            _boundData.index = -1;
            EditorUtility.SetDirty(_database);
        }
    }
}