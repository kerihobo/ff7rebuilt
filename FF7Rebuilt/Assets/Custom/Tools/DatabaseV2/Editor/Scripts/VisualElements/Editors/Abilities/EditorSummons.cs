using FFVII.Database;
using System;
using System.Collections;
using UnityEngine.UIElements;

public class EditorSummons : EditorAbilities {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Abilities/editor-abilities.uxml";
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetAbilityList(AbilityType.Summon)[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetAbilityList(AbilityType.Summon);
}
