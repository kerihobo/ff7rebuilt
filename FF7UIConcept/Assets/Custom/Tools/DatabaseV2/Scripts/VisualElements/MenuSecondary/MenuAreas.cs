using System;
using UnityEngine.UIElements;

public class MenuAreas : MenuSecondary {
    public new class UxmlFactory : UxmlFactory<MenuAreas, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/MenuSecondary/menu-areas.uxml";
    protected override string Name { get; set; } = "menu-areas";

    public MenuAreas() : base() {
        ve.Q<Button>("btn-field").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorField()); };
        ve.Q<Button>("btn-world").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorWorld()); };

        Show(ve.Q<Button>("btn-field"), new EditorField());
    }
}
