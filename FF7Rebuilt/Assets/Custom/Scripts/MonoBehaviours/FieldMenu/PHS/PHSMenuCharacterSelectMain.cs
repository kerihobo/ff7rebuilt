using CharacterTypes;
using UnityEngine;

public class PHSMenuCharacterSelectMain : MenuControllable {
    public FingerCursorRight cursor;
    public FingerCursorRight cursorFlicker;
    public UICharacterInfoContainer characterInfoContainer;

    private int selectedIndex;
    private ScreenFieldPHS screenFieldPHS;

    private PHS phs => GameManager.Instance.gameData.phs;

    public void Initialize(ScreenFieldPHS _screenFieldPHS) {
        screenFieldPHS = _screenFieldPHS;

        characterInfoContainer.Initialize();
        
        RectTransform firstRectTransform = characterInfoContainer.arrPlayerInfos[0].rct;
        cursor.Initialize(firstRectTransform);
        cursorFlicker.Initialize(firstRectTransform);
        cursorFlicker.Hide();

        Disable();
    }

    public void ResetSelection() {
        selectedIndex = 0;

        cursor.Show();

        SetCursor(cursor);
    }

    public void SetCursor(FingerCursorRight _cursor) {
        _cursor.SetCursorPosition(characterInfoContainer.arrPlayerInfos[selectedIndex].rct);
        
        Player player = phs.arrReserve[selectedIndex];
        screenFieldPHS.characterInfo.Refresh(player);
    }

    public void Open() {
        cursor.Show();
        SetControllable();
        SetCursor(cursor);
    }

    public void Disable() {
        cursor.Hide();
    }

    private void Close() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }

#region Override methods.
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    
    public override void GetInputConfirmPressed() {
        if (screenFieldPHS.IsInfoBShown) {
            screenFieldPHS.HideInfoB();
            return;
        }

        if (phs.partyLeader == phs.arrCurrent[selectedIndex]) return;

        if (screenFieldPHS.arrSelectedA == null) {
            cursorFlicker.Show();
            SetCursor(cursorFlicker);
            //cursorFlicker.SetCursorPosition(characterInfoContainer.arrPlayerInfos[selectedIndex].imgAvatar.rectTransform);
            cursorFlicker.SetFlickerState(true);

            screenFieldPHS.arrSelectedA = phs.arrCurrent;
            screenFieldPHS.selectedIndexA = selectedIndex;
        } else {
            Player player = phs.arrCurrent[selectedIndex];
            
            if (screenFieldPHS.arrSelectedA[screenFieldPHS.selectedIndexA] == player) {
                screenFieldPHS.ShowInfoB(player);
            } else {
                screenFieldPHS.SwapPlayers(phs.arrCurrent, selectedIndex);
            }
        }
    }

    public override void GetInputCancelPressed() {
        if (screenFieldPHS.IsInfoBShown) {
            screenFieldPHS.HideInfoB();
            return;
        }

        if (screenFieldPHS.arrSelectedA == null) {
            Close();
        } else {
            screenFieldPHS.CancelSwap();
        }
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (screenFieldPHS.IsInfoBShown) return;

        if (_h > 0) {
            Disable();
            screenFieldPHS.OpenReserve();
        }
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (screenFieldPHS.IsInfoBShown) return;

        int v = -_v;
        selectedIndex = (selectedIndex + v) % characterInfoContainer.arrPlayerInfos.Length;

        if (selectedIndex < 0) {
            selectedIndex = characterInfoContainer.arrPlayerInfos.Length - 1;
        }

        SetCursor(cursor);
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
