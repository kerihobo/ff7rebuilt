﻿using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class PortSetBoolFunction : Port {
    public PortSetBoolFunction(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) : base(_parentNode, _portName, _direction, _capacity, _displayMode) {
    }

    public NodeSetBool.Function value;

    protected override void RenderAsOutput(StyleSet _styleSet) {
        value = (NodeSetBool.Function)EditorGUILayout.EnumPopup(value, GUILayout.Width(70));
    }

    public override void SetValue(dynamic _value) {
        value = _value is int ? (NodeSetBool.Function)_value : NodeSetBool.Function.Set;
    }

    public override dynamic GetValue() {
        return (int)value;
    }
}
