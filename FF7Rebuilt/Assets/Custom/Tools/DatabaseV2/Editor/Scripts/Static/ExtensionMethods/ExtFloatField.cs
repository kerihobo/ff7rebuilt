using UnityEngine.UIElements;

public static class ExtFloatField {
    public static FloatField SetValue(this FloatField _self, float _value) {
        _self.value = _value;
        return _self;
    }
}