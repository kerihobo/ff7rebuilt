using FFVII.Database;
using InventoryTypes;
using System.Linq;
using TMPro;
using UnityEngine;

public class MateriaMenuStatInfoAbilityList : MonoBehaviour {
    [SerializeField] private RectTransform listParentMagic;
    [SerializeField] private MateriaMenuStatInfoAbilityListIndependent listParentIndependent;
    [SerializeField] private TextMeshProUGUI txtSummon;
    [SerializeField] private TextMeshProUGUI txtSupport;

    private TextMeshProUGUI[] arrTxtMagic;
    private TextMeshProUGUI[] arrTxtIndependent;

    private DBAbilities.CommandList cmd => DBResources.GetAbilities.commandList;

    public void Initialize() {
        arrTxtMagic = listParentMagic.GetComponentsInChildren<TextMeshProUGUI>();
     
        listParentIndependent.Initialize();

        arrTxtIndependent = new TextMeshProUGUI[listParentIndependent.transform.childCount];
        for (int i = 0; i < arrTxtIndependent.Length; i++) {
            arrTxtIndependent[i] = listParentIndependent.transform.GetChild(i).GetComponent<TextMeshProUGUI>();
        }
    }

    public void Refresh(Materia _materia) {
        switch (_materia.type) {
            case MateriaType.Magic:
            case MateriaType.Command:
                SetActiveText(listParentMagic.gameObject);
                break;
            case MateriaType.Summon:
                SetActiveText(txtSummon.gameObject);
                break;
            case MateriaType.Independent:
            case MateriaType.Support:
                SetActiveText(txtSupport.gameObject);
                break;
            default:
                break;
        }

        switch (_materia.type) {
            case MateriaType.Magic:
                SetMagicList();
                break;
            case MateriaType.Command:
                SetCommandList();
                break;
            case MateriaType.Summon:
                SetSummon();
                break;
            case MateriaType.Independent:
                SetIndependent();
                break;
            case MateriaType.Support:
                SetSupport();
                break;
            default:
                break;
        }

        void SetActiveText(GameObject _goKeep) {
            listParentMagic.gameObject.SetActive(false);
            listParentIndependent.gameObject.SetActive(false);
            txtSummon.gameObject.SetActive(false);
            txtSupport.gameObject.SetActive(false);

            _goKeep.SetActive(true);
        }

        void SetMagicList() {
            DisableArrTxtMagic();

            SelectionAbility[] spells = _materia.selectionSpells;
            int level = _materia.GetLevel();

            for (int i = 0; i < _materia.levels - 1; i++) {
                Ability ability = spells[i].GetSelectedAbility(AbilityType.Magic);

                if (ability != null) {
                    arrTxtMagic[i].gameObject.SetActive(true);
                    arrTxtMagic[i].text = ability.name;

                    if (level >= i) {
                        arrTxtMagic[i].color = TextColourContainer.GetInstance().white;
                    }
                }
            }
        }

        void SetSupport() {
            txtSupport.text = _materia.name;
        }

        void SetIndependent() {
            switch (_materia.subtype) {
                case 3:  // Cover
                case 13: // Luck Plus
                case 14: // Magic Plus
                case 15: // Speed Plus
                case 16: // HP Plus
                case 17: // MP Plus
                    SetActiveText(listParentIndependent.gameObject);
                    listParentIndependent.Refresh(_materia);
                    
                    break;
                default:
                    txtSupport.text = IndependentSubtype.GetSubTypeLabel(_materia.subtype);
                    break;
            }

            //if (_materia.subtype == 3  // Cover
            //||  _materia.subtype == 15 // Speed Plus
            //||  _materia.subtype == 16 // HP Plus
            //||  _materia.subtype == 17 // MP Plus
            //||  _materia.subtype == 13 // Luck Plus
            //||  _materia.subtype == 14 // Magic Plus
            //) {
            //    SetActiveText(listParentIndependent.gameObject);
            //    listParentIndependent.Refresh(_materia);
            //} else {
            //    // Need 
            //    txtSupport.text = IndependentManager.GetAbilityName(_materia);
            //}
        }

        void SetSummon() {
            txtSummon.text = _materia.name;
        }

        void SetCommandList() {
            DisableArrTxtMagic();

            Ability command = _materia.selectionSpells[0].GetSelectedAbility(AbilityType.Command);
            if (command == cmd.cmdWItem
            ||  command == cmd.cmdWMagic
            ||  command == cmd.cmdWSummon
            ) {
                SetActiveText(txtSupport.gameObject);
                txtSupport.text = command.name;

                return;
            }

            string[] commands = _materia.selectionSpells.Where((x, i) => i < _materia.levels - 1).Select(x => x.GetSelectedAbility(AbilityType.Command).name).ToArray();
            int level = _materia.GetLevel();

            for (int i = 0; i < commands.Length; i++) {
                arrTxtMagic[i].gameObject.SetActive(true);
                arrTxtMagic[i].text = commands[i];

                if (level >= i) {
                    arrTxtMagic[i].color = TextColourContainer.GetInstance().white;
                }
            }
        }
    }

    private void DisableArrTxtMagic() {
        foreach (TextMeshProUGUI txt in arrTxtMagic) {
            txt.gameObject.SetActive(false);
            txt.color = TextColourContainer.GetInstance().grey;
        }
    }
}
