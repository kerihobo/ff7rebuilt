﻿using System;
using UnityEngine;

namespace InventoryTypes {
    [Serializable]
    public class Weapon : Equipment {
        public Weapon() : base() {
            name = "New Weapon";
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Weapon(Weapon _source) : base(_source) {
            type                    = _source.type;
            prefabWeapon            = _source.prefabWeapon;
            prefabEffectOverride    = _source.prefabEffectOverride;
            attackAnimationOverride = _source.attackAnimationOverride;
            equipEffects            = _source.equipEffects;
            elements          = _source.elements;
            criticalPercent         = _source.criticalPercent;
            isRanged                = _source.isRanged;
        }

        public WeaponType type;
        public Transform prefabWeapon;
        public GameObject prefabEffectOverride;
        public string attackAnimationOverride;
        [SerializeReference]
        public EquipEffect equipEffects = new EquipEffect();
        [SerializeReference]
        public ElementalAffinityManager elements = new ElementalAffinityManager(true);
        public int criticalPercent;
        public bool isRanged;
        // TODO: What weapons have unique effects? Ultimate weapons? How do they all work? Shall I just make unique scripts and build those into the prefabs?
    }
}