﻿using UnityEditor;
using UnityEngine;

public class BlackboardAI : Blackboard {
    public BlackboardAI(NodeGraph _parentGraph) : base(_parentGraph) {
    }

    protected override void PopulateContextMenuNewField(GenericMenu menu) {
        menu.AddItem(new GUIContent("Bool"), false, () => new FieldBool(this));
        menu.AddItem(new GUIContent("Int"), false, () => new FieldInt(this));
    }
}
