using UnityEngine;
using UnityEngine.Rendering;

public class VertexColorRaycast : MonoBehaviour {
    public float rayDistance = 2;
    private int layerMaskWalkMesh;
    private SortingGroup sortingGroup;

    private void Awake() {
        layerMaskWalkMesh = LayerMask.GetMask("WalkMesh");
        sortingGroup = GetComponent<SortingGroup>();
    }

    void Update() {
        Ray ray = new Ray(transform.position, Vector3.down);

        //Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.red);

        if (Physics.Raycast(ray, out RaycastHit hit, rayDistance, layerMaskWalkMesh)) {
            MeshCollider meshCollider = hit.collider as MeshCollider;
            if (meshCollider != null && meshCollider.sharedMesh != null) {
                Mesh mesh = meshCollider.sharedMesh;
                int[] triangles = mesh.triangles;
                Color[] colors = mesh.colors;

                int triangleIndex = hit.triangleIndex * 3;
                int vertexIndex = triangles[triangleIndex];
                Color color = colors[vertexIndex];
                
                Color32 color32 = color;
                byte rValue = color32.r;

                if (sortingGroup != null) {
                    sortingGroup.sortingOrder = rValue;
                }
            }
        }
    }
}
