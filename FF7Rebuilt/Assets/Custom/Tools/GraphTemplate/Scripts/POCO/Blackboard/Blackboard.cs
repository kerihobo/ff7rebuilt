﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[Serializable]
public class Blackboard {
    public Blackboard(NodeGraph _parentGraph) {
        name = "Blackboard";
        parentGraph = _parentGraph;

        _parentGraph.blackboard = this;
    }

    public string name;
    [SerializeReference]
    public NodeGraph parentGraph;
    [SerializeReference]
    public List<BlackboardField> fields = new List<BlackboardField>();
    
    public bool isHovered { get; set; }
    
    private Vector2 scrollPosition;
    [SerializeField]
    private Rect rect = new Rect(0, 0, 250, 400);
    private BlackboardField selectedField;
    private bool isDraggingField;

    public void Render(Event _e, StyleSet _styleSet, Vector2 _pan) {
        GetIsHovered();

        GUILayout.BeginVertical(_styleSet.panel, GUILayout.Width(rect.width), GUILayout.Height(rect.height));
            RenderTitle();

            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
                RenderFieldHeading();
                RenderFieldList(_styleSet);
            GUILayout.EndScrollView();
        GUILayout.EndVertical();
    }

    private void RenderFieldList(StyleSet _styleSet) {
        for (int i = 0; i < fields.Count; i++) {
            if (fields[i] != null) {
                fields[i].Render(_styleSet);
            }
        }
    }

    private void RenderFieldHeading() {
        GUILayout.BeginHorizontal();
            GUILayout.Label("Generic Fields", GUILayout.Width(150));
            
            if (GUILayout.Button("+")) {
                ProcessContextMenuNewField();
            }
        GUILayout.EndHorizontal();
    }

    private static void RenderTitle() {
        GUILayout.BeginHorizontal();
            GUILayout.Label("Variables");
        GUILayout.EndHorizontal();
    }

    public void PreventDuplicateName(BlackboardField _blackboardField) {
        string candidateName = _blackboardField.name;

        int i = 0;
        while (fields.Any(x => x.name == candidateName && x != _blackboardField)) {
            candidateName = $"{_blackboardField.name}({++i})";
        }

        _blackboardField.name = candidateName;
    }

    public void GetHoveredField(Event _e) {
        if (_e.modifiers == EventModifiers.Alt) {
            return;
        }

        foreach (BlackboardField bf in fields) {
            if (bf.isHovered) {
                Debug.Log(bf.name);
                selectedField = bf;
            }
        }
    }

    public void DeleteField(BlackboardField _field) {
        for (int i = _field.nodes.Count - 1; i >= 0; i--) {
            _field.nodes[i].DisconnectPorts();
            parentGraph.DeleteNode(_field.nodes[i]);
        }

        fields.Remove(_field);
    }

    public void ProcessFieldContext() {
        if (selectedField == null) return;

        BlackboardField field = selectedField;
        Event e = Event.current;

        GenericMenu menu = new GenericMenu();
        menu.AddItem(new GUIContent("Delete"), false, () => DeleteField(field));

        menu.ShowAsContext();
        e.Use();
    }

    public void DragField(Event _e) {
        if (isDraggingField) {
            Rect dragFieldRect = new Rect(_e.mousePosition, new Vector2(100, 20));
            dragFieldRect.position -= dragFieldRect.size / 2;
            GUI.Box(dragFieldRect, selectedField.name);
        }
    }

    public void OnRelease(Event _e, Vector2 _pan) {
        if (selectedField != null && !isHovered) {
            ProcessContextFieldNewNode(_e, _pan, selectedField);
        }

        selectedField = null;
        isDraggingField = false;
    }

    private void ProcessContextMenuNewField() {
        Event e = Event.current;

        GenericMenu menu = new GenericMenu();
        PopulateContextMenuNewField(menu);

        menu.ShowAsContext();
        e.Use();
    }

    protected virtual void PopulateContextMenuNewField(GenericMenu menu) {
        menu.AddItem(new GUIContent("Bool"), false, () => new FieldBool(this));
        menu.AddItem(new GUIContent("Int"), false, () => new FieldInt(this));
    }

    private void ProcessContextFieldNewNode(Event _e, Vector2 _pan, BlackboardField _selectedField) {
        //Event e = Event.current;
        //Debug.Log(_e.mousePosition);
        Vector2 mousePosition = _e.mousePosition;

        GenericMenu menu = new GenericMenu();
        menu.AddItem(new GUIContent("Get"), false, () => SpawnGet(_e, _pan, _selectedField, mousePosition));
        menu.AddItem(new GUIContent("Set"), false, () => SpawnSet(_e, _pan, _selectedField, mousePosition));

        menu.ShowAsContext();
        _e.Use();
    }

    private NodeSet SpawnSet(Event _e, Vector2 _pan, BlackboardField _selectedField, Vector2 _reservedMousePosition) {
        Vector2 offset = _reservedMousePosition - _pan;
        NodeSet newNode = NodeSet.CreateFromField(parentGraph, offset, _selectedField);
        newNode.Initialize(_selectedField);

        return newNode;
    }

    private NodeGet SpawnGet(Event _e, Vector2 _pan, BlackboardField _selectedField, Vector2 _reservedMousePosition) {
        Vector2 offset = _reservedMousePosition - _pan;
        NodeGet newNode = NodeGet.CreateFromField(parentGraph, offset, _selectedField);
        newNode.Initialize(_selectedField);

        return newNode;
    }

    private void GetIsHovered() {
        Event e = Event.current;

        if (e.type == EventType.Repaint) {
            isHovered = rect.Contains(Event.current.mousePosition);
        }
    }

    public bool OnDrag() {
        isDraggingField = selectedField != null;
        return isDraggingField;
    }
}
