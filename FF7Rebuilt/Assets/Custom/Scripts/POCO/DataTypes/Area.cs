﻿using System;
using UnityEngine;

[Serializable]
public class Area : Data {
    public Area() : base() {
        name = "New Area";
        encounterValue = 2;
    }

    /// <summary>
    /// Copy constructor.
    /// </summary>
    /// <param name="_source"></param>
    public Area(Area _source) : base(_source) {
        battleScenePrefab = _source.battleScenePrefab;
        encounterValue    = _source.encounterValue;
        isWorldMap        = _source.isWorldMap;
        formationsNormal  = _source.formationsNormal;
        formationsSpecial = _source.formationsSpecial;
        formationsChocobo = _source.formationsChocobo;
    }
    
    public static int DANGER_COUNTER { get; set; }

    public GameObject battleScenePrefab;
    [Range(2, 32)]
    public int encounterValue;
    public bool isWorldMap;
    [SerializeReference]
    public FormationManager formationsNormal = new FormationManager(FormationType.Normal);
    [SerializeReference]
    public FormationManager formationsSpecial = new FormationManager(FormationType.Special);
    [SerializeReference]
    public FormationManager formationsChocobo = new FormationManager(FormationType.Chocobo);

    // TODO: This should be called from the player controller 3x/sec for fields, and 1 per second for world map.
    private void BattleCheck(GameData _gm, bool _isRunning) {
        int numerator = _isRunning ? 4096 : 1024; ;
        int threshold = (int)(DANGER_COUNTER * _gm.enemyLureValue) / 256;

        if (isWorldMap) {
            float enemyLureValue = 0;
            if (_gm.enemyLureValue > (16 / 16f)) {
                enemyLureValue = _gm.enemyLureValue * 2;
            }

            numerator = (int)(enemyLureValue * 16384) / encounterValue;
            threshold = DANGER_COUNTER / 256;
        }

        DANGER_COUNTER +=  numerator / encounterValue;

        if (UnityEngine.Random.Range(0, 256) < threshold) {
            DetermineBattle(_gm);
        }
    }

    private void DetermineBattle(GameData _gm) {
        Formation formation = null;

        // TODO: Evaluate Mystery Ninja.

        EvaluateForChocoboBattle(ref formation, _gm);

        // TODO: Evaluate pre-Emptive.

        // TODO: Evaluate special formations.
        // TODO: Evaluate normal battle.
    }

    private void EvaluateForChocoboBattle(ref Formation _formation, GameData _gm) {
        if (_formation != null) {
            return;
        }

        int chance = UnityEngine.Random.Range(0, 256) * 4096 / _gm.chocoboLureRating;
        for (int i = 0; i < formationsChocobo.formations.Length; i++) {
            Formation selectedFormation = formationsChocobo.formations[i].GetSelectedFormation();
            int chocoboBattleChance = selectedFormation.probability * 1024 * selectedFormation.id;

            if (chance < chocoboBattleChance) {
                _formation = selectedFormation;
                break;
            } else {
                chance -= chocoboBattleChance;
            }
        }
    }
}
