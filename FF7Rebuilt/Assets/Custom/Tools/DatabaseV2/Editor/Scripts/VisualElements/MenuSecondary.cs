using UnityEditor;
using UnityEngine.UIElements;

/// <summary>
/// Intended as an abstract type.
/// </summary>
public class MenuSecondary : VisualElement {
    protected virtual string Path { get; set; } = null;
    protected virtual string Name { get; set; } = null;
    
    protected VisualTreeAsset vta;
    protected VisualElement ve;

    public MenuSecondary() {
        vta = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path);
        ve = vta.Instantiate();
        name = Name;
        Add(ve);

        EWDatabaseEditor.Instance.ShowSecondary(this);
    }

    protected void Show(EventBase _e, VisualElement _menu, EWDatabaseEditor.ColumnWidths _width = EWDatabaseEditor.ColumnWidths.ONE) {
        Show((VisualElement)_e.currentTarget, _menu, _width);
    }

    protected void Show(VisualElement _ve, VisualElement _menu, EWDatabaseEditor.ColumnWidths _width = EWDatabaseEditor.ColumnWidths.ONE) {
        SetSelected(_ve);
        EWDatabaseEditor.Instance.ShowEditor(_menu, _width);
    }

    protected void SetSelected(VisualElement _ve) {
        ve.Query<Button>().ForEach(x => x.RemoveFromClassList("selected-secondary"));
        _ve.AddToClassList("selected-secondary");
    }
}
