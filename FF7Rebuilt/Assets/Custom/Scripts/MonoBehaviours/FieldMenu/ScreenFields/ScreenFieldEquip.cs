using CharacterTypes;
using InventoryTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ScreenFieldEquip : ScreenField {
    public override FieldMenu fieldMenu { get; set; }

    [SerializeField] private EquipMenuOptionList optionList;
    [SerializeField] private TextMeshProUGUI txtDescription;
    [SerializeField] private UICharacterInfo playerInfo;
    [SerializeField] private EquipMenuStatInfo statInfo;
    [SerializeField] private EquipMenuEquipmentSlotsInfo materiaInfo;
    private List<Player> lsActivePlayers = new List<Player>();
    private int selectedCharacterIndex;
    public bool isChanged;

    public Player GetSelectedCharacter => lsActivePlayers.Count > selectedCharacterIndex ? lsActivePlayers[selectedCharacterIndex] : null;
    
    private Player GetInitiallySelectedCharacter => GameManager.Instance.gameData.phs.arrCurrent[fieldMenu.selectedCharacterIndex];

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        statInfo.Initialize();
        materiaInfo.Initialize();
        optionList.Initialize(this);
        playerInfo.Initialize(Vector2.zero, Vector2.zero);
    }

    public override void SetDefaultDisplay() {
        optionList.SetDefaultDisplay();
    }

    public override IEnumerator Open() {
        lsActivePlayers = GameManager.Instance.gameData.phs.arrCurrent.Where(x => x != null).ToList();
        selectedCharacterIndex = lsActivePlayers.IndexOf(GetInitiallySelectedCharacter);

        RefreshPlayerInfo();
        optionList.Open();

        isChanged = false;

        yield return null;
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public void ToggleStatInfoChangedText(bool _isActive) {
        statInfo.ToggleStatInfoChangedText(_isActive);
    }

    public void CycleCharacter(int _direction) {
        selectedCharacterIndex = (selectedCharacterIndex + _direction) % lsActivePlayers.Count;
        if (selectedCharacterIndex < 0) {
            selectedCharacterIndex = lsActivePlayers.Count - 1;
        }

        RefreshPlayerInfo();
        optionList.Refresh();
    }

    public void RefreshPlayerInfo() {
        playerInfo.Refresh(GetSelectedCharacter);
    }

    public void RefreshInfoWeapon(Weapon _weapon) {
        Debug.Log($"Weapon: name({_weapon?.name}), attack({_weapon?.equipEffects.derived.attack})");
        SetDescription(_weapon);
        materiaInfo.Refresh(_weapon);
        materiaInfo.ToggleDisplay(true);
        statInfo.RefreshWeapon(GetSelectedCharacter, _weapon);
    }

    public void RefreshInfoArmor(Armor _armor) {
        Debug.Log("Armor: " + _armor?.name);
        SetDescription(_armor);
        materiaInfo.Refresh(_armor);
        materiaInfo.ToggleDisplay(true);
        statInfo.RefreshArmor(GetSelectedCharacter, _armor);
    }

    public void RefreshInfoAccessory(Accessory _accessory) {
        Debug.Log("Accessory: " + _accessory?.name);
        SetDescription(_accessory);
        materiaInfo.ToggleDisplay(false);

        statInfo.RefreshBaseInfo(
            GetSelectedCharacter
        ,   out int attackCurrent
        ,   out int attackPercentCurrent
        //,   out int magicAttackCurrent
        ,   out int defenseCurrent
        ,   out int defensePercentCurrent
        ,   out int magicDefenseCurrent
        ,   out int magicDefensePercentCurrent
        );
    }

    void SetDescription(Collectible _collectible) {
        txtDescription.text = _collectible?.description;
    }
}
