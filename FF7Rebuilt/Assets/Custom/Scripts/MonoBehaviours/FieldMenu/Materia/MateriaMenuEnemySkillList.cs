using FFVII.Database;
using InventoryTypes;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuEnemySkillList : MonoBehaviour {
    [SerializeField] private float scrollSpeed = 1;
    private RectTransform rct;
    private Vector2 initialPosition;
    private Vector2 targetPosition;
    private float gridHeight;
    private float gridSpacing;
    private TextMeshProUGUI[] arrTxtAbilities;
    private string[] names;
    private int lowBound;
    private Materia source;
    private float scroll;

    public void Initialize() {
        rct = GetComponent<RectTransform>();
        initialPosition = rct.anchoredPosition;
        
        GridLayoutGroup grid = GetComponent<GridLayoutGroup>();
        gridHeight = grid.cellSize.y;
        gridSpacing = grid.spacing.y;

        arrTxtAbilities = GetComponentsInChildren<TextMeshProUGUI>();
        
        targetPosition = Vector2.up * (gridHeight + gridSpacing);

        names = DBResources.GetAbilityList(AbilityType.EnemySkill).Select(x => x.name).ToArray();
    }

    public void Refresh(Materia _materia) {
        source = _materia;

        //CheatUnlockAllSkills();

        Refresh();
    }

    /// <summary>
    /// Unlocks all Enemy Skills on the source materia. This is a cheat and should not be called in the full game.
    /// </summary>
    private void CheatUnlockAllSkills() {
        bool[] arrSkillUnlocks = source.enemySkillManager.skillsUnlocked;
        for (int i = 0; i < arrSkillUnlocks.Length; i++) {
            arrSkillUnlocks[i] = true;
        }
    }

    private void Refresh() {
        int countIndex = lowBound;
        foreach (TextMeshProUGUI txt in arrTxtAbilities) {
            int abilityIndex = countIndex % EnemySkillManager.MAX_INDEX;
            bool isUnlocked = source.enemySkillManager.skillsUnlocked[abilityIndex];
            txt.text = isUnlocked ? names[abilityIndex] : "";
            
            countIndex++;
        }
    }

    void Update() {
        if (source == null) return;

        scroll += Time.deltaTime * scrollSpeed;

        if (scroll > 1) {
            scroll %= 1;

            lowBound += 2;
            lowBound = lowBound % EnemySkillManager.MAX_INDEX;

            Refresh();
        }
        rct.anchoredPosition = initialPosition + (targetPosition * scroll);
    }
}
