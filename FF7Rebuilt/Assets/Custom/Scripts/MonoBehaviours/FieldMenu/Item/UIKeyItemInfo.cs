using InventoryTypes;
using TMPro;
using UnityEngine;

public class UIKeyItemInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public KeyItem keyItem { get; set; }

    private CanvasGroup cnvGrp;

    private TextMeshProUGUI txtName;

    public void Initialize() {
        rct = GetComponent<RectTransform>();
        cnvGrp = GetComponent<CanvasGroup>();
        txtName = GetComponent<TextMeshProUGUI>();
    }

    public void Refresh(KeyItem _keyItem) {
        keyItem = _keyItem;
        cnvGrp.alpha = keyItem != null ? 1 : 0;

        if (keyItem == null) {
            Clear();
            return;
        }

        string keyItemName = keyItem.name;
        
        int charLocation = keyItem.name.IndexOf('|');
        if (charLocation >= 0) {
            keyItemName = keyItem.name.Substring(0, charLocation);
        }

        txtName.text = keyItemName;
    }

    public void Clear() {
        txtName.text = "";
        cnvGrp.alpha = 0;
    }
}
