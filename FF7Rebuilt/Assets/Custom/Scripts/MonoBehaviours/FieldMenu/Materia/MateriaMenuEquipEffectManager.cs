using InventoryTypes;
using UnityEngine;

public class MateriaMenuEquipEffectManager :MonoBehaviour {
    [SerializeField] private RectTransform equipEffectsParent;
    private MateriaMenuEquipEffect[] equipEffects;

    public void Initialize() {
        equipEffects = equipEffectsParent.GetComponentsInChildren<MateriaMenuEquipEffect>();
        
        foreach (MateriaMenuEquipEffect ee in equipEffects) {
            ee.Initialize();
        }
    }

    public void Refresh(Materia _materia) {
        Clear();

        int i = 0;

        if (_materia.equipEffects.primary.strength != 0) {
            equipEffects[i++].Refresh("Strength", _materia.equipEffects.primary.strength, false);
        }

        if (_materia.equipEffects.primary.vitality != 0) {
            equipEffects[i++].Refresh("Vitality", _materia.equipEffects.primary.vitality, false);
        }

        if (_materia.equipEffects.primary.magic != 0) {
            equipEffects[i++].Refresh("Magic", _materia.equipEffects.primary.magic, false);
        }

        if (_materia.equipEffects.primary.spirit != 0) {
            equipEffects[i++].Refresh("Spirit", _materia.equipEffects.primary.spirit, false);
        }

        if (_materia.equipEffects.primary.luck != 0) {
            equipEffects[i++].Refresh("Luck", _materia.equipEffects.primary.luck, false);
        }

        if (_materia.equipEffects.primary.dexterity != 0) {
            equipEffects[i++].Refresh("Dexterity", _materia.equipEffects.primary.dexterity, false);
        }

        if (i >= 6) {
            Debug.Log("Maximum stats active for " + _materia.name + " materia. The UI can't fit anymore");
            return;
        }

        if (_materia.equipEffects.maxHPPercent != 0) {
            equipEffects[i++].Refresh("MaxHP", _materia.equipEffects.maxHPPercent, true);
        }

        if (i >= 6) {
            Debug.Log("Maximum stats active for " + _materia.name + " materia. The UI can't fit anymore");
            return;
        }

        if (_materia.equipEffects.maxMPPercent != 0) {
            equipEffects[i++].Refresh("MaxMP", _materia.equipEffects.maxMPPercent, true);
        }

        if (i >= 6) {
            Debug.Log("Maximum stats active for " + _materia.name + " materia. The UI can't fit anymore");
            return;
        }
        
        if (_materia.equipEffects.derived.magicDefense != 0) {
            equipEffects[i++].Refresh("Magic def", _materia.equipEffects.derived.magicDefense, false);
        }
    }

    private void Clear() {
        foreach (MateriaMenuEquipEffect equipEffect in equipEffects) {
            equipEffect.Hide();
        }
    }
}
