using UnityEngine.UIElements;

public static class ExtVisualElement {
    public static VisualElement FlipBool(this VisualElement _self, ref bool _value) {
        _value = !_value;
        _self.SetBoolClass(_value);

        return _self;
    }

    public static VisualElement SetBoolClass(this VisualElement _self, bool _value) {
        _self.RemoveFromClassList("on");
        if (_value) {
            _self.AddToClassList("on");
        }

        return _self;
    }
}
