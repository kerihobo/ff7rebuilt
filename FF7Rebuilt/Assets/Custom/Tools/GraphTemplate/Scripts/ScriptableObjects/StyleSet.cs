﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Style Set", menuName = "Custom/Style Set")]
public class StyleSet : ScriptableObject {
    public GUIStyle bg;
    public GUIStyle buttonContainer;
    public GUIStyle panel;
    public GUIStyle node;
    public GUIStyle nodeSelected;
    public GUIStyle debugText;
    public GUIStyle nodeTitle;
    public GUIStyle nodeContent;
    public GUIStyle inLabel;
    public GUIStyle outLabel;
    public GUIStyle portContainerIn;
    public GUIStyle portContainerOut;
    public GUIStyle portInactive;
    public GUIStyle portInExecution;
    public GUIStyle portInDynamic;
    public GUIStyle portInBool;
    public GUIStyle portInInt;
    public GUIStyle portOutExecution;
    public GUIStyle portOutDynamic;
    public GUIStyle portOutInt;
    public GUIStyle portOutBool;
    public GUIStyle blackboardFieldBody;
    public GUIStyle rectSelection;
    public GUIStyle searchWindowBG;
    public GUIStyle searchEntry;
    public GUIStyle searchEntryButton;
    public GUIStyle searchGroupIndicator;
    public GUIStyle searchWindowList;
    public GUIStyle searchWindowListFrame;
    public GUIStyle searchBackButton;
    public GUIStyle searchTitle;
    public GUIStyle searchIcon;
}