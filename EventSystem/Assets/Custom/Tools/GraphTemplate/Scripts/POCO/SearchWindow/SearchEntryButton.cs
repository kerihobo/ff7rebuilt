﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SearchEntryButton {
    public SearchEntryButton(SearchWindow _searchWindow, string _name) {
        searchWindow = _searchWindow;
        name = _name;
    }

    public string name;

    private SearchWindow searchWindow;

    public List<SearchEntryButton> Render(StyleSet _styleSet) {
        GUILayout.BeginHorizontal(GUILayout.Height(20));
            if (searchWindow.currentSearchPath.Length > 0) {
                GUILayout.Space(10);
            }
            GUILayout.BeginHorizontal(_styleSet.searchEntry);
                if (GUILayout.Button(name, _styleSet.searchEntryButton)) {
                    return Respond();
                }

                bool isLast = searchWindow.dictSearchEntries.Any(x => x.Value.path == searchWindow.currentSearchPath + "/" + name || x.Value.path == name);
                if (!isLast) {
                    if (GUILayout.Button(">", _styleSet.searchGroupIndicator, GUILayout.Width(20))) {
                        return Respond();
                    }
                }
            GUILayout.EndHorizontal();
        GUILayout.EndHorizontal();

        return null;
    }

    private List<SearchEntryButton> Respond() {
        if (searchWindow.currentSearchPath.Length > 0) {
            searchWindow.currentSearchPath += "/";
        }
        searchWindow.currentSearchPath += name;

        if (IsSelectNode()) {
            searchWindow.Close();
        } else {
            List<SearchEntryButton> newSearchEntryButtons = searchWindow.GetNewChoiceList();
            return new List<SearchEntryButton>(newSearchEntryButtons);
        }

        return null;
    }

    private bool IsSelectNode() {
        bool isLast = searchWindow.dictSearchEntries.Any(x => x.Value.path == searchWindow.currentSearchPath);
        if (isLast) {
            searchWindow.dictSearchEntries[name].action();
        }

        return isLast;
    }
}
