using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MagicMenuOptions : MenuControllable {
    [SerializeField] public RectTransform optionsParent;
    [SerializeField] public FingerCursorRight cursor;
    /// <summary>
    /// Magic: 0<br/>
    /// Summon: 1<br/>
    /// Enemy-Skill: 2<br/>
    /// </summary>
    private TextMeshProUGUI[] arrOptions;
    private int selectedIndex;
    private ScreenFieldMagic screenFieldMagic;

    private Inventory GetInventory() => GameManager.Instance.gameData.inventory;
    
    public void Open() {
        gameObject.SetActive(true);
        SetControllable();
        SetCursor();
    }

    public void Initialize(ScreenFieldMagic _screenFieldMagic) {
        screenFieldMagic = _screenFieldMagic;
        
        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        //Close();

        cursor.Initialize(arrOptions[0].rectTransform);
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void Disable() {
        gameObject.SetActive(false);
    }

    private void Close() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }

#region Override methods.
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputSquarePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputConfirmPressed() {
        switch (selectedIndex) {
            case 0:
            case 1:
            case 2:
                Disable();
                break;
        }

        switch (selectedIndex) {
            case 0:
                screenFieldMagic.OpenWindowMagic();
                break;
            case 1:
                screenFieldMagic.OpenWindowSummon();
                break;
            case 2:
                screenFieldMagic.OpenWindowEnemySkill();
                break;
        }
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;

        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }

    public override void GetInputPage(int pageDirection, bool isPageXPressed) {
        screenFieldMagic.CycleCharacter(pageDirection);
        SetCursor();
    }
#endregion
}
