using UnityEngine.UIElements;

public class EditorField : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Areas/editor-field.uxml";
    protected override string Name { get; set; } = "editor-field";

    public EditorField() : base() {
    }
}
