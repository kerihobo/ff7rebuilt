using FFVII.Database;
using Field;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[CustomEditor(typeof(PlayerControllerNavMesh))]
public class EditorPlayerControllerNavMesh : Editor {
    private PlayerControllerNavMesh self;

    private void OnEnable() {
        self = (PlayerControllerNavMesh)target;
    }

    public override VisualElement CreateInspectorGUI() {
        VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Test/Resources/UXML/EditorTest.uxml");
        TemplateContainer ui = visualTree.CloneTree();

        DropdownField ddf = ui.Q<DropdownField>("drp-selection");
        ddf.Initialize(DBResources.GetPlayers.playerList, self.selectionPlayer, DBResources.GetPlayers);
        ddf.RegisterValueChangedCallback(_e => {
            self.selectionPlayer.SetDataExternally(ddf.index);
            EditorUtility.SetDirty(target);
        });
        Debug.Log(self.selectionPlayer);

        return ui;
    }
}