using FFVII.Database;
using InventoryTypes;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

public class MateriaAbilitiesIndependentEditor : MateriaAbilitiesEditor {
    public MateriaAbilitiesIndependentEditor(Materia _target, Database _database) : base(_target, _database) {
        OnIndependentTypeChanged();
    }

    protected override string UXMLPath { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/MateriaAbilities/materia-abilities-independent.uxml";

    public override void BindAbilities() {
        DropdownField ddfSubtype = this.Q<DropdownField>("ddf-subtype");
        ddfSubtype.choices = IndependentSubtype.INDEPENDENT_SUBTYPE_NAMES.ToList();
        ddfSubtype.index = target.subtype;
        ddfSubtype.RegisterValueChangedCallback(_e => {
            target.subtype = ddfSubtype.index;

            //RefreshSelectionDisplays();
            OnIndependentTypeChanged();

            EditorUtility.SetDirty(database);
        });

        //this.Q<EnumField>("enum-independent-type").SetIndependentEffectType(target.independentManager.type).RegisterValueChangedCallback(e => {
        //    target.independentManager.type = (IndependentManager.Type)e.newValue;
        //    EditorUtility.SetDirty(database);

        //    OnIndependentTypeChanged();
        //});

        for (int i = 0; i < target.boostValues.Length; i++) {
            int j = i;
            IntegerField intF = this.Q<IntegerField>($"int-lv-{j+1}");
            intF.SetValue(target.boostValues[j]).RegisterValueChangedCallback(e => {
                target.boostValues[j] = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }
    }

    public override void OnLevelsChanged() {
        List<IntegerField> intF = this.Query<GroupBox>("grp-effect-granted").Children<IntegerField>().ToList();
        intF.ForEach(x => x.style.display = DisplayStyle.None);

        for (int i = 0; i < target.levels; i++) {
            intF[i].style.display = DisplayStyle.Flex;
        }
    }

    public void OnIndependentTypeChanged() {
        bool isLevelless = false;
        switch (target.subtype) {
            case 2:
            case 1:
            case 6:
                isLevelless = true;
                break;
        }

        this.Q<GroupBox>("grp-effect-granted").style.display = isLevelless ? DisplayStyle.None : DisplayStyle.Flex;
    }

    public override void Refresh(Materia _target, VisualElement _ve) {
        target = _target;
        veParent = _ve;

        //this.Q<EnumField>("enum-independent-type").value = target.independentManager.type;
        this.Q<DropdownField>("ddf-subtype").index = target.subtype;

        for (int i = 0; i < target.boostValues.Length; i++) {
            int j = i;
            IntegerField intF = this.Q<IntegerField>($"int-lv-{j + 1}");
            intF.value = target.boostValues[j];
        }
    }
}
