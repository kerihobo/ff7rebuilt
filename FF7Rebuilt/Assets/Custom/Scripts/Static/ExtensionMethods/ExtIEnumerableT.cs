using System.Collections.Generic;
using System.Linq;

public static class ExtIEnumerableT {
    public static bool IsNullOrEmpty<T>(this IEnumerable<T> obj) => !obj?.Any() ?? true;
}
