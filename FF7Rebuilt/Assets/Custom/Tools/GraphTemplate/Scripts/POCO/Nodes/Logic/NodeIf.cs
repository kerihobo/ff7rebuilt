﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NodeIf : Node {
    public NodeIf(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(160, 75);
        name = "If";

        AddPortIn<Port>();
        AddPortIn<PortBool>("Predicate");
        AddPortOut<Port>("True");
        AddPortOut<Port>("False");
    }

    public bool value = false;

    public override void Run(NodeGraph _nodeGraph) {
        List<Port> predicatePorts = portContainerIn.ports[1].connectedPorts;
        bool predicate = predicatePorts.Count > 0 ? predicatePorts[0].GetValue() : value;

        if (predicate) {
            portContainerOut.ports[0].connectedPorts[0].parentNode.Run(_nodeGraph);
        } else {
            portContainerOut.ports[1].connectedPorts[0].parentNode.Run(_nodeGraph);
        }
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = new NodeIf(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeIf)newNode).portContainerIn.ports[1].SetValue(portContainerIn.ports[1].GetValue());

        return newNode;
    }
}
