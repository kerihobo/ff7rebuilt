using Field;
using InventoryTypes;
using System.Collections;
using TMPro;
using UnityEngine;

public class PromptSaveCrystal : MenuControllable {
    public static PromptSaveCrystal Instance;
    
    public FingerCursorRight cursor;
    
    [SerializeField] private RectTransform optionsParent;
    [SerializeField] private GameObject prfSaveCrystal;
    private int selectedIndex;

    private TextMeshProUGUI[] arrOptions;
    private Collectible saveCrystal;
    private RectTransform rct;
    private Vector2 initialSize;

    public void Initialize() {
        Instance = this;

        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        rct = GetComponent<RectTransform>();
        initialSize = rct.sizeDelta;

        cursor.Initialize(arrOptions[0].rectTransform);

        Disable();
    }

    public void Open() {
        EventManagerField.Engage();
        
        gameObject.SetActive(true);
        
        ResetSelection();
        StartCoroutine(PopUp());
        SetControllable();
    }

    public IEnumerator PopUp() {
        gameObject.SetActive(true);
        rct.sizeDelta = Vector2.zero;

        float t = 0;
        float duration = .1f;

        while (t < duration) {
            rct.sizeDelta = Vector2.Lerp(Vector2.zero, initialSize, t / duration);

            t += Time.deltaTime;
            yield return null;
        }

        rct.sizeDelta = initialSize;
        t = 0;

        SetControllable();
    }

    public IEnumerator PopDown() {
        FieldMenu.Instance.menuControllable = null;

        float t = 0;
        float duration = .05f;

        while (t < duration) {
            rct.sizeDelta = Vector2.Lerp(initialSize, Vector2.zero, t / duration);

            t += Time.deltaTime;
            yield return null;
        }

        Disable();
        EventManagerField.Disengage();
    }

    private void Disable() {
        saveCrystal = null;
        gameObject.SetActive(false);
        rct.sizeDelta = Vector2.zero;
    }
    
    private void Close() {
        StartCoroutine(PopDown());
    }

    public void ResetSelection() {
        selectedIndex = 1;
        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputConfirmPressed() {
        if (selectedIndex == 0) {
            // Spawn Save Crystal
            // Remove item from Inventory.
            PlayerControllerNavMesh playerController = GameManager.Instance.currentlyControlledPlayerCharacter;
            Instantiate(prfSaveCrystal, playerController.transform.position, Quaternion.identity);

            GameManager.Instance.gameData.inventory.RemoveItem(saveCrystal);
        }

        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }

    public void SetCrystal(Collectible _item) {
        saveCrystal = _item;
    }
#endregion
}
