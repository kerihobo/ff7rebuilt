using System;
using TMPro;
using UnityEngine;

public class MateriaMenuCheckCommandInfo : MonoBehaviour {
    public Ability command;
    
    public RectTransform rct { get; set; }
    
    public Action action;
    private string actionName;
    private MateriaMenuCheckCommand commandWindow;
    private TextMeshProUGUI txtLabel;

    public void Initialize(MateriaMenuCheckCommand _commandWindow) {
        commandWindow = _commandWindow;
        txtLabel = GetComponent<TextMeshProUGUI>();
        rct = GetComponent<RectTransform>();
    }

    public void Refresh(Ability _command, Action _action) {
        command = _command;
        txtLabel.text = _command?.name;
        action = _action;
    }

    public void Clear() {
        command = null;
        txtLabel.text = "";
    }

    public void OnHover() {
        commandWindow.SetDescription(command.description);
    }

    public void OnSelect() {
        action();
    }
}
