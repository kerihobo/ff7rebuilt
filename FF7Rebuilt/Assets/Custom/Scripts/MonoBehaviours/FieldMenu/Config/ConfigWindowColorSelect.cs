using UnityEngine;

public class ConfigWindowColorSelect : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform optionsParent;
    [SerializeField] private ConfigWindowColorAdjust configWindowColorAdjust;
    private ConfigMenuMain configMenuMain;
    private int selectedIndex;
    private RectTransform[] arrOptions;

    public void Initialize(ConfigMenuMain _configMenuMain) {
        configMenuMain = _configMenuMain;

        int childCount = optionsParent.childCount;
        arrOptions = new RectTransform[childCount];
        for (int i = 0; i < childCount; i++) {
            arrOptions[i] = optionsParent.GetChild(i).GetComponent<RectTransform>();
            arrOptions[i].gameObject.SetActive(false);
        }

        // Probably should take responsibility for initializing the Adjustment window.
        configWindowColorAdjust.Initialize(this);

        Disable();

        cursor.Initialize(arrOptions[0]);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex]);
    }

    public void Open() {
        cursor.Show();
        cursor.SetFlickerState(false);
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void Disable() {
        cursor.Hide();
    }

    private void Close() {
        Disable();
        configMenuMain.Open();
    }

    #region Override methods.
    public override void GetInputPage(int _pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        int step = 2;
        selectedIndex += step * _h;

        int lowEnd = selectedIndex - (selectedIndex % step);
        int difference = selectedIndex - lowEnd;
        int highEnd = lowEnd + 1;

        LoopWithinRow();

        SetCursor();

        void LoopWithinRow() {
            if (selectedIndex < 0) {
                selectedIndex = (arrOptions.Length - step) + -difference;
            } else if (selectedIndex >= arrOptions.Length) {
                selectedIndex = difference;
            }
        }
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int nextSelectedIndex = selectedIndex - _v;

        int lowEnd = selectedIndex - (selectedIndex % 2);
        int highEnd = lowEnd + 1;

        LoopWithinBracket();

        selectedIndex = nextSelectedIndex;

        SetCursor();

        void LoopWithinBracket() {
            if (nextSelectedIndex < lowEnd) {
                nextSelectedIndex = highEnd;
            } else if (nextSelectedIndex > highEnd) {
                nextSelectedIndex = lowEnd;
            }
        }
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputConfirmPressed() {
        cursor.SetFlickerState(true);
        configWindowColorAdjust.Open(selectedIndex);
    }
#endregion
}
