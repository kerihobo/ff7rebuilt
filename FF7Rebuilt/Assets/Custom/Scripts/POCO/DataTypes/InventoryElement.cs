﻿using System;

[Serializable]
public class InventoryElement : Data {
    public InventoryElement() : base() {
        name = "New Inventory";
    }

    /// <summary>
    /// Copy constructor.
    /// </summary>
    /// <param name="_source"></param>
    public InventoryElement(InventoryElement _source) : base(_source) {
        description = _source.description;
    }

    public string description;
}
