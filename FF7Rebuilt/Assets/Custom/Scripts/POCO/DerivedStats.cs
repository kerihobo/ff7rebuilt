using System;
using UnityEngine;

[Serializable]
public class DerivedStats {
    public DerivedStats() {
    }

    public DerivedStats(DerivedStats _source) {
        attack = _source.attack;
        attackPercent = _source.attackPercent;
        defense = _source.defense;
        defensePercent = _source.defensePercent;
        magicAttack = _source.magicAttack;
        magicDefense = _source.magicDefense;
        magicDefensePercent = _source.magicDefensePercent;
    }

    [Range(0, 255)] public int attack;
    [Range(0, 255)] public int attackPercent;
    [Range(0, 255)] public int defense;
    [Range(0, 255)] public int defensePercent;
    [Range(0, 255)] public int magicAttack;
    [Range(0, 255)] public int magicDefense;
    [Range(0, 255)] public int magicDefensePercent;

    public static DerivedStats operator +(DerivedStats a, DerivedStats b) {
        DerivedStats temp = new DerivedStats();

        if (b == null) return a;

        temp.attack              = a.attack              + b.attack;
        temp.attackPercent       = a.attackPercent       + b.attackPercent;
        temp.defense             = a.defense             + b.defense;
        temp.defensePercent      = a.defensePercent      + b.defensePercent;
        temp.magicDefense        = a.magicDefense        + b.magicDefense;
        temp.magicDefensePercent = a.magicDefensePercent + b.magicDefensePercent;

        return temp;
    }

    public void Log() {
        Debug.Log($"Atk({attack}), Atk%({attackPercent}), Def({defense}), Def%({defensePercent}), MDef({magicDefense}), MDef%({magicDefensePercent})");
    }

    public void Clamp() {
        attack              = Mathf.Clamp(attack             , 0, 255);
        attackPercent       = Mathf.Clamp(attackPercent      , 0, 255);
        defense             = Mathf.Clamp(defense            , 0, 255);
        defensePercent      = Mathf.Clamp(defensePercent     , 0, 255);
        magicDefense        = Mathf.Clamp(magicDefense       , 0, 255);
        magicDefensePercent = Mathf.Clamp(magicDefensePercent, 0, 255);
    }
}