using UnityEngine;
using UnityEngine.UI;

public class UICharacterInfoContainer : MonoBehaviour {
    public RectTransform rct { get; private set; }

    public UICharacterInfo playerInfo0;
    public UICharacterInfo playerInfo1;
    public UICharacterInfo playerInfo2;

    public UICharacterInfo[] arrPlayerInfos { get; set; }

    public void Initialize() {
        rct = GetComponent<RectTransform>();

        LayoutRebuilder.ForceRebuildLayoutImmediate(rct);

        arrPlayerInfos = new UICharacterInfo[] {
            playerInfo0
        ,   playerInfo1
        ,   playerInfo2
        };
        
        Vector2 frontRowPosition = arrPlayerInfos[0].imgAvatar.rectTransform.anchoredPosition;
        Vector2 backRowPosition = arrPlayerInfos[1].imgAvatar.rectTransform.anchoredPosition;

        foreach (UICharacterInfo playerInfo in arrPlayerInfos) {
            playerInfo.Initialize(frontRowPosition, backRowPosition);
        }
    }

    /// <summary>
    /// An easy way to call Refresh for all characters in your party.
    /// </summary>
    public void Refresh() {
        for (int i = 0; i < arrPlayerInfos.Length; i++) {
            arrPlayerInfos[i].Refresh(GameManager.Instance.gameData.phs.arrCurrent[i]);
        }
    }
}
