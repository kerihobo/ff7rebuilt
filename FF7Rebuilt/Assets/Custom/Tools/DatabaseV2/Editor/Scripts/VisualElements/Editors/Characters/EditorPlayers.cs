using CharacterTypes;
using FFVII.Database;
using Field;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorPlayers : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Characters/editor-players.uxml";
    protected override Database database { get; set; } = DBResources.GetPlayers;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetPlayers.playerList[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetPlayers.playerList;

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Player)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        if (target == null) return;

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            ((Player)target).idName = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<ObjectField>("spr-avatar").SetSprite(((Player)target).avatar).RegisterValueChangedCallback(e => {
            ((Player)target).avatar = (Sprite)e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<EnumField>("enum-gender").SetGender(((Player)target).gender).RegisterValueChangedCallback(e => {
            ((Player)target).gender = (Gender)e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<EnumField>("enum-weapon-type").SetWeaponType(((Player)target).selectionStartingWeapon.type).RegisterValueChangedCallback(e => {
            ((Player)target).selectionStartingWeapon.type = (WeaponType)e.newValue;
            EditorUtility.SetDirty(database);

            ve.Q<PlayerDefaultWeapon>().Initialize((Player)target, database);
        });

        ve.Q<ObjectField>("field-prefab").objectType = typeof(PlayerControllerNavMesh);
        ve.Q<ObjectField>("field-prefab").SetPlayerControllerNavMesh(((Player)target).prefabField).RegisterValueChangedCallback(e => {
            ((Player)target).prefabField = (PlayerControllerNavMesh)e.newValue;
            EditorUtility.SetDirty(database);
        });
        
        ve.Q<ObjectField>("anim-battle-prefab").SetAnimator(((Player)target).prefabBattle).RegisterValueChangedCallback(e => {
            ((Player)target).prefabBattle = (Animator)e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<ObjectField>("go-attack-effect-prefab").SetGameObject(((Player)target).prefabAttackEffect).RegisterValueChangedCallback(e => {
            ((Player)target).prefabAttackEffect = (GameObject)e.newValue;
            EditorUtility.SetDirty(database);
        });

        BindStartingStats();
        ve.Q<PlayerDefaultWeapon>().Initialize((Player)target, database);
        ve.Q<PlayerDefaultArmor>().Initialize((Player)target, database);

        DropdownField ddf = ve.Q<DropdownField>("ddf-default-accessory");
        ddf.Initialize(DBResources.GetAccessories.accessoryList, ((Player)target).selectionStartingAccessory, database);
        ddf.RegisterValueChangedCallback(_e => {
            ((Player)target).selectionStartingAccessory.SetDataExternally(ddf.index);
            EditorUtility.SetDirty(database);
        });

        BindStatGrades();
        BindRankCurves();

        ve.Q<LimitBreaksEditor>().Initialize((Player)target, database).BindUniqueControls();

        void BindStartingStats() {
            GroupBox statGrades = ve.Q<GroupBox>("grp-starting-stats");

            statGrades.Q<IntegerField>("int-starting-stats-level").SetValue(((Player)target).points.level).RegisterValueChangedCallback(e => {
                ((Player)target).points.level = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-starting-stats-experience").SetValue(((Player)target).points.exp).RegisterValueChangedCallback(e => {
                ((Player)target).points.exp = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-starting-stats-hp").SetValue(((Player)target).points.hpMaxBase).RegisterValueChangedCallback(e => {
                ((Player)target).points.hpMaxBase = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-starting-stats-mp").SetValue(((Player)target).points.mpMaxBase).RegisterValueChangedCallback(e => {
                ((Player)target).points.mpMaxBase = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-starting-stats-strength").SetValue(((Player)target).primary.strength).RegisterValueChangedCallback(e => {
                ((Player)target).primary.strength = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-starting-stats-vitality").SetValue(((Player)target).primary.vitality).RegisterValueChangedCallback(e => {
                ((Player)target).primary.vitality = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-starting-stats-magic").SetValue(((Player)target).primary.magic).RegisterValueChangedCallback(e => {
                ((Player)target).primary.magic = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-starting-stats-spirit").SetValue(((Player)target).primary.spirit).RegisterValueChangedCallback(e => {
                ((Player)target).primary.spirit = e.newValue;
                EditorUtility.SetDirty(database);
            });
            
            statGrades.Q<IntegerField>("int-starting-stats-dexterity").SetValue(((Player)target).primary.dexterity).RegisterValueChangedCallback(e => {
                ((Player)target).primary.dexterity = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-starting-stats-luck").SetValue(((Player)target).primary.luck).RegisterValueChangedCallback(e => {
                ((Player)target).primary.luck = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindStatGrades() {
            GroupBox statGrades = ve.Q<GroupBox>("grp-stat-grades");

            statGrades.Q<IntegerField>("int-stat-grades-strength").SetValue(((Player)target).levelUpManager.statGrades.strength).RegisterValueChangedCallback(e => {
                ((Player)target).levelUpManager.statGrades.strength = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-stat-grades-vitality").SetValue(((Player)target).levelUpManager.statGrades.vitality).RegisterValueChangedCallback(e => {
                ((Player)target).levelUpManager.statGrades.vitality = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-stat-grades-magic").SetValue(((Player)target).levelUpManager.statGrades.magic).RegisterValueChangedCallback(e => {
                ((Player)target).levelUpManager.statGrades.magic = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-stat-grades-spirit").SetValue(((Player)target).levelUpManager.statGrades.spirit).RegisterValueChangedCallback(e => {
                ((Player)target).levelUpManager.statGrades.spirit = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statGrades.Q<IntegerField>("int-stat-grades-dexterity").SetValue(((Player)target).levelUpManager.statGrades.dexterity).RegisterValueChangedCallback(e => {
                ((Player)target).levelUpManager.statGrades.dexterity = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindRankCurves() {
            List<IntegerField> lsIntFEXPMod = this.Q<VisualElement>("ve-modifier-column-exp-mod").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFHPGrd = this.Q<VisualElement>("ve-modifier-column-hp-grd").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFHPBs = this.Q<VisualElement>("ve-modifier-column-hp-bs").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFMPGrd = this.Q<VisualElement>("ve-modifier-column-mp-grd").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFMPBs = this.Q<VisualElement>("ve-modifier-column-mp-bs").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFLCKGrd = this.Q<VisualElement>("ve-modifier-column-lck-grd").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFLCKBs = this.Q<VisualElement>("ve-modifier-column-lck-bs").Query<VisualElement>("container").Children<IntegerField>().ToList();

            for (int i = 0; i < ((Player)target).levelUpManager.expModifiers.Length; i++) {
                int j = i;

                lsIntFEXPMod[j].SetValue(((Player)target).levelUpManager.expModifiers[j]).RegisterValueChangedCallback(e => {
                    ((Player)target).levelUpManager.expModifiers[j] = e.newValue;
                    EditorUtility.SetDirty(database);
                });

                lsIntFHPGrd[j].SetValue(((Player)target).levelUpManager.rankCurvesHP[j].valueGradient).RegisterValueChangedCallback(e => {
                    ((Player)target).levelUpManager.rankCurvesHP[j].valueGradient = e.newValue;
                    EditorUtility.SetDirty(database);
                });

                lsIntFHPBs[j].SetValue(((Player)target).levelUpManager.rankCurvesHP[j].valueBase).RegisterValueChangedCallback(e => {
                    ((Player)target).levelUpManager.rankCurvesHP[j].valueBase = e.newValue;
                    EditorUtility.SetDirty(database);
                });

                lsIntFMPGrd[j].SetValue(((Player)target).levelUpManager.rankCurvesMP[j].valueGradient).RegisterValueChangedCallback(e => {
                    ((Player)target).levelUpManager.rankCurvesMP[j].valueGradient = e.newValue;
                    EditorUtility.SetDirty(database);
                });

                lsIntFMPBs[j].SetValue(((Player)target).levelUpManager.rankCurvesMP[j].valueBase).RegisterValueChangedCallback(e => {
                    ((Player)target).levelUpManager.rankCurvesMP[j].valueBase = e.newValue;
                    EditorUtility.SetDirty(database);
                });

                lsIntFLCKGrd[j].SetValue(((Player)target).levelUpManager.rankCurvesLuck[j].valueGradient).RegisterValueChangedCallback(e => {
                    ((Player)target).levelUpManager.rankCurvesLuck[j].valueGradient = e.newValue;
                    EditorUtility.SetDirty(database);
                });

                lsIntFLCKBs[j].SetValue(((Player)target).levelUpManager.rankCurvesLuck[j].valueBase).RegisterValueChangedCallback(e => {
                    ((Player)target).levelUpManager.rankCurvesLuck[j].valueBase = e.newValue;
                    EditorUtility.SetDirty(database);
                });
            }
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = target.name;
        ((Player)target).idName = target.name;
        ve.Q<ObjectField>("spr-avatar").value = ((Player)target).avatar;
        ve.Q<EnumField>("enum-gender").value = ((Player)target).gender;
        ve.Q<EnumField>("enum-weapon-type").value = ((Player)target).selectionStartingWeapon.type;
        ve.Q<ObjectField>("field-prefab").value = ((Player)target).prefabField;
        ve.Q<ObjectField>("anim-battle-prefab").value = ((Player)target).prefabBattle;
        ve.Q<ObjectField>("go-attack-effect-prefab").value = ((Player)target).prefabAttackEffect;

        RefreshStartingStats();
        ve.Q<PlayerDefaultWeapon>().Refresh((Player)target, database);
        ve.Q<PlayerDefaultArmor>().Refresh((Player)target, database);

        ve.Q<DropdownField>("ddf-default-accessory").Initialize(DBResources.GetAccessories.accessoryList, ((Player)target).selectionStartingAccessory, database);

        RefreshStatGrades();
        RefreshRankCurves();
        
        ve.Q<LimitBreaksEditor>().Refresh((Player)target);

        void RefreshStartingStats() {
            GroupBox statGrades = ve.Q<GroupBox>("grp-starting-stats");

            statGrades.Q<IntegerField>("int-starting-stats-level").value = ((Player)target).points.level;
            statGrades.Q<IntegerField>("int-starting-stats-experience").value = ((Player)target).points.exp;
            statGrades.Q<IntegerField>("int-starting-stats-hp").value = ((Player)target).points.hpMaxBase;
            statGrades.Q<IntegerField>("int-starting-stats-mp").value = ((Player)target).points.mpMaxBase;
            statGrades.Q<IntegerField>("int-starting-stats-strength").value = ((Player)target).primary.strength;
            statGrades.Q<IntegerField>("int-starting-stats-vitality").value = ((Player)target).primary.vitality;
            statGrades.Q<IntegerField>("int-starting-stats-magic").value = ((Player)target).primary.magic;
            statGrades.Q<IntegerField>("int-starting-stats-spirit").value = ((Player)target).primary.spirit;
            statGrades.Q<IntegerField>("int-starting-stats-dexterity").value = ((Player)target).primary.dexterity;
            statGrades.Q<IntegerField>("int-starting-stats-luck").value = ((Player)target).primary.luck;
        }

        void RefreshStatGrades() {
            GroupBox statGrades = ve.Q<GroupBox>("grp-stat-grades");

            statGrades.Q<IntegerField>("int-stat-grades-strength").value = ((Player)target).levelUpManager.statGrades.strength;
            statGrades.Q<IntegerField>("int-stat-grades-vitality").value = ((Player)target).levelUpManager.statGrades.vitality;
            statGrades.Q<IntegerField>("int-stat-grades-magic").value = ((Player)target).levelUpManager.statGrades.magic;
            statGrades.Q<IntegerField>("int-stat-grades-spirit").value = ((Player)target).levelUpManager.statGrades.spirit;
            statGrades.Q<IntegerField>("int-stat-grades-dexterity").value = ((Player)target).levelUpManager.statGrades.dexterity;
        }

        void RefreshRankCurves() {
            List<IntegerField> lsIntFEXPMod = this.Q<VisualElement>("ve-modifier-column-exp-mod").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFHPGrd = this.Q<VisualElement>("ve-modifier-column-hp-grd").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFHPBs = this.Q<VisualElement>("ve-modifier-column-hp-bs").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFMPGrd = this.Q<VisualElement>("ve-modifier-column-mp-grd").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFMPBs = this.Q<VisualElement>("ve-modifier-column-mp-bs").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFLCKGrd = this.Q<VisualElement>("ve-modifier-column-lck-grd").Query<VisualElement>("container").Children<IntegerField>().ToList();
            List<IntegerField> lsIntFLCKBs = this.Q<VisualElement>("ve-modifier-column-lck-bs").Query<VisualElement>("container").Children<IntegerField>().ToList();

            for (int i = 0; i < ((Player)target).levelUpManager.expModifiers.Length; i++) {
                lsIntFEXPMod[i].value = ((Player)target).levelUpManager.expModifiers[i];
                lsIntFHPGrd[i].value = ((Player)target).levelUpManager.rankCurvesHP[i].valueGradient;
                lsIntFHPBs[i].value = ((Player)target).levelUpManager.rankCurvesHP[i].valueBase;
                lsIntFMPGrd[i].value = ((Player)target).levelUpManager.rankCurvesMP[i].valueGradient;
                lsIntFMPBs[i].value = ((Player)target).levelUpManager.rankCurvesMP[i].valueBase;
                lsIntFLCKGrd[i].value = ((Player)target).levelUpManager.rankCurvesLuck[i].valueGradient;
                lsIntFLCKBs[i].value = ((Player)target).levelUpManager.rankCurvesLuck[i].valueBase;
            }
        }
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Player());

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new Player());

        base.Add();
    }
}
