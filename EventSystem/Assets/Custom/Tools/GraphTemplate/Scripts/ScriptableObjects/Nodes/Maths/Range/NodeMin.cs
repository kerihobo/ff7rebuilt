﻿using UnityEngine;

public class NodeMin : Node {
    private dynamic copyA;
    private dynamic copyB;

    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(90, 75);
        name = "Min";

        AddPortIn<PortInt>("A", Port.Capacity.SINGLE);
        AddPortIn<PortInt>("B", Port.Capacity.SINGLE);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);

        portContainerIn.ports[0].SetValue(copyA);
        portContainerIn.ports[1].SetValue(copyB);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(90, 160);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();

        return Mathf.Min(a, b);
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeMin>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeMin)newNode).copyA = portContainerIn.ports[0].GetValue();
        ((NodeMin)newNode).copyB = portContainerIn.ports[1].GetValue();

        return newNode;
    }
}