using CharacterTypes;
using FFVII.Database;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MagicMenuEnemySkillList : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private RectTransform optionsParent;
    private MagicMenuOptions magicMenuOptions;
    private ScreenFieldMagic screenFieldMagic;
    private RectTransform scrollBarFG;
    private int selectedIndex;
    private MagicMenuEnemySkillInfo[] arrOptions;

    [SerializeField] private RectTransform additionalOptionParent;
    private MagicMenuEnemySkillInfo[] additionalOptions;
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 2;
    private Ability[] arrViewItems = new Ability[0];

    private Player GetSelectedCharacter => screenFieldMagic.GetSelectedCharacter;

    public void Initialize(ScreenFieldMagic _screenFieldMagic, MagicMenuOptions _magicMenuOptions) {
        magicMenuOptions = _magicMenuOptions;
        screenFieldMagic = _screenFieldMagic;

        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        GridLayoutGroup glgOptions = GetComponentInChildren<GridLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(glgOptions.GetComponent<RectTransform>());

        arrViewItems = DBResources.GetAbilityList(AbilityType.EnemySkill).ToArray();

        arrOptions = optionsParent.GetComponentsInChildren<MagicMenuEnemySkillInfo>(true);
        foreach (MagicMenuEnemySkillInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOptions = additionalOptionParent.GetComponentsInChildren<MagicMenuEnemySkillInfo>(true);
        foreach (MagicMenuEnemySkillInfo info in additionalOptions) {
            info.Initialize();
        }

        ClearAdditional();

        Disable();

        cursor.Initialize(arrOptions[0].rct);

        listSpace = glgOptions.spacing.y;
        listHeight = arrOptions[0].rct.rect.height;
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = lowBound + i;
            arrOptions[i].Refresh(GetSelectedCharacter, arrViewItems, itemIndex);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - columns;
        MagicMenuEnemySkillInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOptionParent.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        for (int i = 0; i < additionalOptions.Length; i++) {
            Ability displayedAbility = arrViewItems[anchoredIndex + i];
            additionalOptions[i].Refresh(GetSelectedCharacter, arrViewItems, anchoredIndex + i);
        }
    }

    private void ClearAdditional() {
        foreach (var info in additionalOptions) {
            info.Clear();
        }
    }

    public void ResetSelection() {
        selectedIndex = 0;
        lowBound = 0;

        UpdateScrollBar();
        SetCursor();
    }

    public void SetCursor() {
        MagicMenuEnemySkillInfo selectedItemInfo = arrOptions[selectedIndex];

        cursor.SetCursorPosition(selectedItemInfo.rct);

        int abilityIndex = lowBound + selectedIndex;
        bool isUnlocked = selectedItemInfo.GetSkillUnlocked(GetSelectedCharacter, abilityIndex);
        Ability enemySkill = isUnlocked ? arrViewItems[abilityIndex] : null;

        screenFieldMagic.SetDescription(enemySkill?.description);
        screenFieldMagic.SetInfoEnemySkill(enemySkill);
    }

    public void Open() {
        lowBound = 0;

        Show();
        SetControllable();
        Refresh();
        SetCursor();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    public void Disable() {
        cursor.Hide();
        ClearAdditional();

        gameObject.SetActive(false);
    }

    public void Close() {
        Disable();
        screenFieldMagic.OpenOptions();
    }

    private void Show() {
        gameObject.SetActive(true);
        SetScrollbarHeight();
        UpdateScrollBar();
        cursor.Show();

        void SetScrollbarHeight() {
            float scrollBarHeight = scrollBarBG.sizeDelta.y;
            scrollBarHeight *= arrOptions.Length / (float)arrViewItems.Length;
            scrollBarHeight = Mathf.RoundToInt(scrollBarHeight);
            scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarHeight);
        }
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        if (arrViewItems == null || arrViewItems.Length == 0) return;

        float scroll = ((float)lowBound / columns) / (arrViewItems.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

#region Override methods.
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputConfirmPressed() { }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex <= columns - 1 && _v > 0) || (lowBound + selectedIndex >= arrViewItems.Length - columns && _v < 0)) return;

        int v = -_v * columns;
        selectedIndex += v;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
            lowBound -= lowBound % columns;
        }

        if (selectedIndex < 0) {
            selectedIndex += columns;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= columns;
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _h < 0) || (lowBound + selectedIndex == arrViewItems.Length - 1 && _h > 0)) return;

        selectedIndex += _h;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += _h * columns;
            lowBound -= lowBound % columns;

            if (isTooHigh) {
                selectedIndex -= columns;
            } else if (isTooLow) {
                selectedIndex += columns;
            }
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        SetCursor();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(_h));
        }
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;

            ClearAdditional();
        }

        int v = _pageDirection * (3 * columns);
        int lowBoundMax = arrViewItems.Length - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
