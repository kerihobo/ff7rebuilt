using UnityEngine;

public static class ExtVector2 {
    public static Vector2 Clamp(this Vector2 _self, float _min, float _max) {
        if (_self.x < _min) _self.x = _min;
        if (_self.x > _max) _self.x = _max;

        if (_self.y < _min) _self.y = _min;
        if (_self.y > _max) _self.y = _max;

        return _self;
    }

    public static Vector2 Clamp(this Vector2 _self, Vector2 _min, Vector2 _max) {
        if (_self.x < _min.x) _self.x = _min.x;
        if (_self.x > _max.x) _self.x = _max.x;

        if (_self.y < _min.y) _self.y = _min.y;
        if (_self.y > _max.y) _self.y = _max.y;

        return _self;
    }
}
