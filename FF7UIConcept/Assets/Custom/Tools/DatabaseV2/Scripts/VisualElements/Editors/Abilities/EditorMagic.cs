using UnityEngine.UIElements;

public class EditorMagic : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Abilities/editor-abilities.uxml";
    protected override string Name { get; set; } = "editor-magic";

    public EditorMagic() : base() {
    }
}
