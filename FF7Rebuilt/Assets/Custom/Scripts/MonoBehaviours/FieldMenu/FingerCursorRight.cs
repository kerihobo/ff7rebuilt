using System.Collections.Generic;
using UnityEngine;

public class FingerCursorRight : MonoBehaviour {
    public Animator anim { get; set; }
    public Vector3 offset { get; set; } = new Vector2(-14, -3);

    private const float allCycleDelay = 2 / 60f;

    private RectTransform rct;
    private List<RectTransform> targets = new List<RectTransform>();
    private int targetIndex = 0;
    private float t;

    public bool IsAll => targets.Count > 1;

    private void Update() {
        if (IsAll) {
            if (t > allCycleDelay) {
                t = 0;
                targetIndex = (targetIndex + 1) % targets.Count;
                SetCursorPosition(targets[targetIndex]);
            }

            t += Time.deltaTime;
        }
    }

    public void Initialize(RectTransform _initialTargetRectTransform) {
        anim = GetComponent<Animator>();
        rct = GetComponent<RectTransform>();

        offset = rct.position - _initialTargetRectTransform.position;

        SetCursorPosition(_initialTargetRectTransform);
    }

    public void SetCursorPosition(RectTransform _targetRectTransform) {
        Vector2 position = _targetRectTransform.position;
        position += new Vector2(offset.x, offset.y);
        //position = new Vector3(
        //    Mathf.Ceil(position.x),
        //    Mathf.Ceil(position.y),
        //    Mathf.Ceil(position.z)
        //);

        rct.position = position;
    }

    public void AddPosition(Vector2 _position) {
        rct.position += (Vector3)_position;
    }

    public void SetFlickerState(bool _isFlicker) {
        //if (gameObject.activeInHierarchy) {
            anim.SetBool("IsFlicker", _isFlicker);
        //}
    }

    public void Show() {
        gameObject.SetActive(true);
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void SetTargetSingle(RectTransform _target) {
        targets.Clear();
        SetCursorPosition(_target);
    }

    public void SetTargetAll(List<RectTransform> rectTransforms) {
        targetIndex = 0;
        targets = rectTransforms;
    }
}
