﻿public static class IndependentSubtype {
    public static readonly string[] INDEPENDENT_SUBTYPE_NAMES = {
        "0: None"
    ,   "1: HP<->MP"
    ,   "2: Underwater"
    ,   "3: Cover"
    ,   "4: Counter Attack"
    ,   "5: Mega All"
    ,   "6: Long Range"
    ,   "7: Pre-Emptive"
    ,   "8: Chocobo Lure"
    ,   "9: Enemy Lure"
    ,   "10: Enemy Away"
    ,   "11: Gil Plus"
    ,   "12: EXP Plus"
    ,   "13: Luck Plus"
    ,   "14: Magic Plus"
    ,   "15: Speed Plus"
    ,   "16: HP Plus"
    ,   "17: MP Plus"
    };

    public static string GetSubTypeLabel(int _subTypeIndex) {
        switch (_subTypeIndex) {
            case 1:  // HP<->MP"
            case 2:  // Underwater"
            case 3:  // Cover"
            case 4:  // Counter Attack
            case 5:  // Mega All"
            case 6:  // Long Range"
            case 7:  // Pre-Emptive"
            case 8:  // Chocobo Lure"
            case 9:  // Enemy Lure"
            case 10: // Enemy Away"
            case 11: // Gil Plus"
            case 12: // EXP Plus"
            case 13: // Luck Plus"
            case 14: // Magic Plus"
            case 15: // Speed Plus"
            case 16: // HP Plus"
            case 17: // MP Plus"
                string sName = INDEPENDENT_SUBTYPE_NAMES[_subTypeIndex];
                const string splitText = ": ";
                int splitIndex = sName.IndexOf(splitText);

                return sName.Substring(splitIndex + splitText.Length);
            default:
                return string.Empty;
        }
    }
}
