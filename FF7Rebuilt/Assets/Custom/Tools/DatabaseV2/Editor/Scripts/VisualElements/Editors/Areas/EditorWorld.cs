using FFVII.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorWorld : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Areas/editor-world.uxml";
    protected override Database database { get; set; } = DBResources.GetAreas;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetAreaList(AreaType.World)[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetAreaList(AreaType.World);

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Area)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);

            listView.RefreshItems();
        });

        ve.Q<ObjectField>("go-battle-scene-prefab").SetGameObject(((Area)target).battleScenePrefab).RegisterValueChangedCallback(e => {
            ((Area)target).battleScenePrefab = (GameObject)e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<SliderInt>("sld-encounter-value").SetValue(((Area)target).encounterValue).RegisterValueChangedCallback(e => {
            ((Area)target).encounterValue = e.newValue;

            EditorUtility.SetDirty(database);
        });

        for (int i = 0; i < ((Area)target).formationsNormal.formations.Length; i++) {
            int j = i;

            DropdownField ddf = ve.Q<DropdownField>($"ddf-normal-{j}");
            ddf.Initialize(DBResources.GetFormationsList(FormationType.Normal), ((Area)target).formationsNormal.formations[j], database);
            ddf.RegisterValueChangedCallback(_e => {
                ((Area)target).formationsNormal.formations[j].SetDataExternally(ddf.index);
                EditorUtility.SetDirty(database);
            });
        }

        for (int i = 0; i < ((Area)target).formationsSpecial.formations.Length; i++) {
            int j = i;

            DropdownField ddf = ve.Q<DropdownField>($"ddf-special-{j}");
            ddf.Initialize(DBResources.GetFormationsList(FormationType.Special), ((Area)target).formationsSpecial.formations[j], database);
            ddf.RegisterValueChangedCallback(_e => {
                ((Area)target).formationsSpecial.formations[j].SetDataExternally(ddf.index);
                EditorUtility.SetDirty(database);
            });
        }

        for (int i = 0; i < ((Area)target).formationsChocobo.formations.Length; i++) {
            int j = i;

            DropdownField ddf = ve.Q<DropdownField>($"ddf-chocobo-{j}");
            ddf.Initialize(DBResources.GetFormationsList(FormationType.Chocobo), ((Area)target).formationsChocobo.formations[j], database);
            ddf.RegisterValueChangedCallback(_e => {
                ((Area)target).formationsChocobo.formations[j].SetDataExternally(ddf.index);
                EditorUtility.SetDirty(database);
            });
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = (target).name;
        ve.Q<ObjectField>("go-battle-scene-prefab").value = ((Area)target).battleScenePrefab;
        ve.Q<SliderInt>("sld-encounter-value").value = ((Area)target).encounterValue;

        for (int i = 0; i < ((Area)target).formationsNormal.formations.Length; i++) {
            ve.Q<DropdownField>($"ddf-normal-{i}").Initialize(DBResources.GetFormationsList(FormationType.Normal), ((Area)target).formationsNormal.formations[i], database);
        }

        for (int i = 0; i < ((Area)target).formationsSpecial.formations.Length; i++) {
            ve.Q<DropdownField>($"ddf-special-{i}").Initialize(DBResources.GetFormationsList(FormationType.Special), ((Area)target).formationsSpecial.formations[i], database);
        }

        for (int i = 0; i < ((Area)target).formationsChocobo.formations.Length; i++) {
            ve.Q<DropdownField>($"ddf-chocobo-{i}").Initialize(DBResources.GetFormationsList(FormationType.Chocobo), ((Area)target).formationsChocobo.formations[i], database);
        }
    }

    protected override void Insert() {
        Area newArea = new Area() { isWorldMap = true };
        itemSource.Insert(hoveredIndex + 1, newArea);

        base.Insert();
    }

    protected override void Add() {
        Area newArea = new Area() { isWorldMap = true };
        itemSource.Add(newArea);

        base.Add();
    }
}
