using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SceneTransition : MonoBehaviour {
    public enum BlendMode {
        Normal,
        Additive,
        Subtractive
    }

    public static SceneTransition Instance;
    
    [SerializeField] public Image imgTransition;
    [SerializeField] public Image imgFade;
    private bool transitionInProgress = false;
    private bool fadeInProgress = false;
    private Material matAdditive;
    private Material matSubtractive;

    private void Awake() {
        if (Instance && Instance != this) {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);

        imgTransition.gameObject.SetActive(false);
        imgTransition.color = Color.clear;

        imgFade.gameObject.SetActive(false);
        imgFade.color = Color.clear;
    }

    public IEnumerator FadeTransition(Color _a, Color _b, float _duration) {
        float t = 0f;
        
        while (t < _duration) {
            t += Time.deltaTime;
            float f = t / _duration;

            imgTransition.color = Color.Lerp(_a, _b, f);
            yield return null;
        }

        imgTransition.color = _b;
        transitionInProgress = false;
    }

    public IEnumerator TransitionFromPreviousScene(bool _isFast) {
        transitionInProgress = true;
        float duration = _isFast ? 0.4f : 0.8f;

        yield return FadeTransition(imgTransition.color, Color.clear, duration);
        
        imgTransition.gameObject.SetActive(false);
        transitionInProgress = false;
    }

    public IEnumerator TransitionToNextScene() {
        transitionInProgress = true;
        imgTransition.gameObject.SetActive(true);

        yield return FadeTransition(imgTransition.color, Color.white, 0.4f);

        transitionInProgress = false;
    }

    public IEnumerator FadeInstant(BlendMode blendingType, Color _color) {
        fadeInProgress = true;
        imgFade.gameObject.SetActive(true);

        imgFade.color = _color;
        
        imgFade.gameObject.SetActive(false);
        fadeInProgress = false;
        
        yield return null;
    }

    // Need to use appropriate blend mode!
    public IEnumerator Fade(BlendMode _blendMode, Color _a, Color _b, float _duration) {
        float t = 0f;

        imgFade.color = _a;
        imgFade.gameObject.SetActive(true);
        imgFade.material = GetBlendMaterial(_blendMode);

        while (t < _duration) {
            t += Time.deltaTime;
            float f = t / _duration;
            imgFade.color = Color.Lerp(_a, _b, f);
            yield return null;
        }
        imgFade.color = _b;
    }

    private Material GetBlendMaterial(BlendMode _blendMode) {
        switch (_blendMode) {
            case BlendMode.Additive:
                return matAdditive;
            case BlendMode.Subtractive:
                return matSubtractive;
        }

        return null;
    }

    private float SpeedToSeconds(float speed) {
        return Mathf.Max(8f / Mathf.Pow(2f, Mathf.Log(speed) / Mathf.Log(2f)), 1f / 30f);
    }

    public IEnumerator FadeOperation(int type, Color color, float speed) {
        float time = SpeedToSeconds(speed) * 1000;

        switch (type) {
            case 1: // ColorInverse4 to field subtractive async, hold field.
                yield return StartCoroutine(Fade(BlendMode.Subtractive, GetColorInverse(color, 4f), Color.black, time));
                break;
            case 2: // Field to colorInverse4 subtractive async, hold color.
                yield return StartCoroutine(Fade(BlendMode.Subtractive, Color.black, GetColorInverse(color, 4f), time));
                break;
            case 3: //  Not in use.
                fadeInProgress = false;
                break;
            case 4: // Instant no wait black, hold black.
                yield return StartCoroutine(FadeInstant(BlendMode.Normal, Color.black));
                break;
            case 5: // ColorStandard to field additive, hold field.
                yield return StartCoroutine(Fade(BlendMode.Additive, color, Color.black, time));
                break;
            case 6: // Field to colorStandard additive, hold color.
                yield return StartCoroutine(Fade(BlendMode.Additive, Color.black, color, time));
                break;
            case 7: // Instant but wait colorInverse1 subtractive, hold field.
                yield return StartCoroutine(Fade(BlendMode.Subtractive, GetColorInverse(color, 1f), Color.black, time));
                break;
            case 8: // Instant but wait colorInverse1 subtractive, hold color.
                yield return StartCoroutine(Fade(BlendMode.Subtractive, GetColorInverse(color, 1f), GetColorInverse(color, 1f), time));
                break;
            case 9: // Instant but wait colorStandard additive, hold field.
                yield return StartCoroutine(Fade(BlendMode.Additive, color, Color.black, time));
                break;
            case 10: // Instant but wait colorStandard additive, hold color.
                yield return StartCoroutine(Fade(BlendMode.Additive, color, color, time));
                break;
        }

        imgTransition.color = (color == Color.white) ? Color.white : Color.black;

        Color GetColorInverse(Color color, float multiplier) {
            return new Color(Mathf.FloorToInt(multiplier * (1 - color.r)),
                             Mathf.FloorToInt(multiplier * (1 - color.g)),
                             Mathf.FloorToInt(multiplier * (1 - color.b)));
        }
    }

    public IEnumerator NfadeOperation(int type, Color color, float frames) {
        float timeMs = frames * (1000 / 30);

        switch (type) {
            case 0: // Instant no wait field, hold field.
                yield return StartCoroutine(FadeInstant(BlendMode.Subtractive, Color.black));
                break;
            case 11: // Field to colorStandard additive async, hold color.
                yield return StartCoroutine(Fade(BlendMode.Additive, Color.black, color, timeMs));
                break;
            case 12: // Field to colorStandard subtractive async, hold color.
                yield return StartCoroutine(Fade(BlendMode.Subtractive, Color.black, color, timeMs));
                break;
        }
    }
}
