﻿using UnityEngine;
using System;

[Serializable]
public class LimitBreakManager {
    public LimitBreakManager() {
        limitModifier = LimitModifierType.Normal;
        limitLevels = new LimitLevel[4] { new LimitLevel(140), new LimitLevel(324), new LimitLevel(435), new LimitLevel(506) };
        
        if (availableLevels > 0) {
            limitLevels[availableLevels - 1].usesNeeded = 0;
        }
    }

    /// <summary>
    /// Copy constructor.
    /// </summary>
    /// <param name="_source"></param>
    public LimitBreakManager(LimitBreakManager _source) {
        currentLimitLevel = _source.currentLimitLevel;
        availableLevels   = _source.availableLevels;
        
        limitLevels = new LimitLevel[4];
        for (int i = 0; i < limitLevels.Length; i++) {
            limitLevels[i] = new LimitLevel(_source.limitLevels[i]);
        }

        limitModifier = _source.limitModifier;
        responseSet = _source.responseSet;
    }

    [Serializable]
    public class LimitLevel {
        public LimitLevel(int _gaugeCapacity) {
            gaugeCapacity = _gaugeCapacity;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public LimitLevel(LimitLevel _source) {
            gaugeFill     = _source.gaugeFill;
            gaugeCapacity = _source.gaugeCapacity;
            usesNeeded    = _source.usesNeeded;
            usesCount     = _source.usesCount;
            killsNeeded   = _source.killsNeeded;
            isUnlocked    = _source.isUnlocked;
            // Might need copy constructors... might not.
            limitBreakA   = _source.limitBreakA;
            limitBreakB   = _source.limitBreakB;
        }

        public int gaugeFill;
        public int gaugeCapacity = 140;
        public int usesNeeded = 8;
        public int usesCount;
        public int killsNeeded = 120;
        public bool isUnlocked;
        public SelectionAbility limitBreakA = new SelectionAbility(AbilityType.LimitBreak);
        public SelectionAbility limitBreakB = new SelectionAbility(AbilityType.LimitBreak);

        public LimitLevel Set() {
            gaugeFill = 0;

            return this;
        }
    }

    [Serializable]
    public class LimitResponseSet {
        public string incompatible = "incompatible";
        public string compatible = "compatible";
        public string success = "success";
    }

    public enum LimitModifierType {
        Normal,
        Sadness,
        Fury
    }

    [Range(1, 4)]
    public int currentLimitLevel;
    [Range(1, 4)]
    public int availableLevels = 4;
    public LimitLevel[] limitLevels = new LimitLevel[4];
    public LimitModifierType limitModifier = LimitModifierType.Normal;
    [SerializeReference]
    public LimitResponseSet responseSet = new LimitResponseSet();

    private void IncrementLimit(int _damage, int _hpMax) {
        limitLevels[currentLimitLevel].gaugeFill += (((300 * _damage) / _hpMax) * ((256 * (int)GetLimitMultiplier()) / limitLevels[currentLimitLevel].gaugeCapacity));
    }

    private float GetLimitMultiplier() {
        switch (limitModifier) {
            case LimitModifierType.Normal:
                return 1;
            case LimitModifierType.Sadness:
                return 0.5f;
            case LimitModifierType.Fury:
                return 2.0f;
            default:
                return 1;
        }
    }
}
