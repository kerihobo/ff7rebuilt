using CharacterTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UICharacterInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public CanvasGroup cnvGrp { get; set; }

    private Color white;
    private Color yellow;
    private Color red;
    public Image imgAvatar;
    public TextMeshProUGUI txtName;
    
    public Player player { get; set; }

    [SerializeField] private TextMeshProUGUI txtLevel;
    [SerializeField] private TextMeshProUGUI txtStatus;
    [SerializeField] private TextMeshProUGUI txtHPCurrent;
    [SerializeField] private TextMeshProUGUI txtHPMax;
    [SerializeField] private Image imgHPGauge;
    [SerializeField] private TextMeshProUGUI txtMPCurrent;
    [SerializeField] private TextMeshProUGUI txtMPMax;
    [SerializeField] private Image imgMPGauge;
    [SerializeField] private TextMeshProUGUI txtEXPCurrent;
    [SerializeField] private TextMeshProUGUI txtEXPNextLevel;
    [SerializeField] private Image imgEXPGauge;
    [SerializeField] private Image imgLimitGauge;
    [SerializeField] private TextMeshProUGUI txtLimitLevel;
    [Tooltip("Whether or not to adjust the avatar icon to show if the character is in the front or back row.")]
    [SerializeField] private bool isAdjustForRow = true;
    private string[] sStatuses = new string[0];
    private float t = 0;
    private int statusIndex = 0;
    private Vector2 frontRowPosition;
    private Vector2 backRowPosition;

    private void Update() {
        if (sStatuses.Length == 0) return;

        t += Time.deltaTime;
        if (t >= 2) {
            t = 0;
            statusIndex++;
            if (statusIndex > sStatuses.Length - 1) {
                statusIndex = 0;
            }
        }
    }

    public void Initialize(Vector2 _frontRowPosition, Vector2 _backRowPosition) {
        frontRowPosition = _frontRowPosition;
        backRowPosition = _backRowPosition;

        rct = GetComponent<RectTransform>();
        cnvGrp = GetComponent<CanvasGroup>();

        white = TextColourContainer.GetInstance().white;
        yellow = TextColourContainer.GetInstance().yellow;
        red = TextColourContainer.GetInstance().red;
    }

    public void Refresh(Player _player) {
        player = _player;
        cnvGrp.alpha = player != null ? 1 : 0;

        if (player == null) {
            Clear();
            return;
        }

        imgAvatar.sprite = _player.avatar;
        imgAvatar.rectTransform.anchoredPosition = _player.statusCurrent.isBackRow ? backRowPosition : frontRowPosition;
        txtName.text = _player.name;
        txtLevel.text = $"<mspace=12>{_player.points.level}";

        txtName.color = white;
        txtLevel.color = white;

        int mpCurrent = _player.mpCurrent;
        int mpMax = _player.points.mpMaxTotal;
        txtMPCurrent.text = $"<mspace=12>{mpCurrent}";
        txtMPMax.text = $"<mspace=12>{mpMax}";
        float mpPct = mpCurrent / (float)mpMax;
        imgMPGauge.fillAmount = mpPct;

        txtMPCurrent.color = white;
        txtMPMax.color = white;
        if (mpCurrent == 0) {
            txtMPCurrent.color = red;
            txtMPMax.color = red;
        } else if (mpPct <= .25f) {
            txtMPCurrent.color = yellow;
            txtMPMax.color = white;
        }

        int hpCurrent = _player.hpCurrent;
        int hpMax = _player.points.hpMaxTotal;
        float hpPct = hpCurrent / (float)hpMax;
        txtHPCurrent.text = $"<mspace=12>{hpCurrent}";
        txtHPMax.text = $"<mspace=12>{hpMax}";
        imgHPGauge.fillAmount = hpPct;

        txtHPCurrent.color = white;
        txtHPMax.color = white;
        if (hpCurrent == 0) {
            txtHPCurrent.color = red;
            txtHPMax.color = red;
            txtMPCurrent.color = red;
            txtMPMax.color = red;
            txtLevel.color = red;
            txtName.color = red;
        } else if (hpPct <= .25f) {
            txtHPCurrent.color = yellow;
            txtHPMax.color = white;
        }

        sStatuses = _player.statusCurrent.GetStatusStringArray();
        txtStatus.text = sStatuses.Length > 0 ? sStatuses[0] : "";

        int expEnd = _player.points.expToNextLevelLocal;
        int expStart = _player.points.expToNextLevelTotal - expEnd;
        int expCurrent = _player.points.exp - expStart;
        
        if (imgEXPGauge) {
            float fillAmount = expCurrent / (float)expEnd;
            imgEXPGauge.fillAmount = fillAmount;
        }

        if (txtEXPCurrent) {
            txtEXPCurrent.text = _player.points.exp.ToString();
        }

        if (txtEXPNextLevel) {
            int tnl = expEnd - expCurrent;
            txtEXPNextLevel.text = tnl.ToString();
        }

        if (imgLimitGauge) {
            LimitBreakManager limitManager = _player.limitBreakManager;
            int limitLevel = limitManager.currentLimitLevel;
            LimitBreakManager.LimitLevel limit = limitManager.limitLevels[limitLevel];
            imgLimitGauge.fillAmount = limit.gaugeFill / (float)limit.gaugeCapacity;

            txtLimitLevel.text = (limitLevel + 1).ToString();
        }
    }

    private void Clear() {
        imgAvatar.sprite = null;
        txtName.text = "";
        txtLevel.text = "";

        txtHPCurrent.text = "";
        txtHPMax.text = "";
        imgHPGauge.fillAmount = 0;

        txtHPCurrent.color = white;
        txtHPMax.color = white;

        txtMPCurrent.text = "";
        txtMPMax.text = "";
        imgMPGauge.fillAmount = 0;

        txtMPCurrent.color = white;
        txtMPMax.color = white;

        txtStatus.text = "";

        if (imgEXPGauge) {
            imgEXPGauge.fillAmount = 0;
        }

        if (txtEXPCurrent) {
            txtEXPCurrent.text = "";
        }

        if (txtEXPNextLevel) {
            txtEXPNextLevel.text = "";
        }

        if (imgLimitGauge) {
            imgLimitGauge.fillAmount = 0;
            txtLimitLevel.text = "";
        }
    }
}
