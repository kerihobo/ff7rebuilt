﻿using System;
using UnityEngine;

[Serializable]
public class NodeGetSet : Node {
    public NodeGetSet(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
    }
    
    [SerializeReference]
    public BlackboardField field;
    public Vector2 minMaxThresholds;

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ResizeNodeToFitName();
    }

    protected void ResizeNodeToFitName() {
        size.x = name.Length * 10;

        if (GetIsSmall()) {
            if (size.x < minMaxThresholds.x) {
                size.x = minMaxThresholds.x;
            }
        } else if (size.x < minMaxThresholds.y) {
            size.x = minMaxThresholds.y;
        }

        // Failsafe.
        if (size.x < 100) {
            size.x = 100;
        }
    }

    public virtual void Initialize(BlackboardField _selectedField) {
        field = _selectedField;
        _selectedField.nodes.Add(this);
    }

    private bool GetIsSmall() => ScaleForConnectedInputs(minMaxThresholds.x, minMaxThresholds.y);
}
