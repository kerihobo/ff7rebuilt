﻿using System;
using UnityEngine;

[Serializable]
public class NodeComparison : Node {
    public NodeComparison(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(170, 75);
        name = "Comparison";

        AddPortIn<PortInt>("A");
        AddPortIn<PortInt>("B");
        AddPortOut<PortBool>("Out", Port.Capacity.MULTIPLE);
        AddPortOut<PortComparison>();
    }
    
    public enum ComparisonType {
        EqualTo
    ,   NotEqualTo
    ,   GreaterThan
    ,   GreaterThanOrEqualTo
    ,   LessThan
    ,   LessThanOrEqualTo
    }

    public ComparisonType comparisonType;

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        comparisonType = (ComparisonType)portContainerOut.ports[1].GetValue();

        ScaleForConnectedInputs(110, 170);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();

        switch (comparisonType) {
            case ComparisonType.EqualTo:
                return a == b;
            case ComparisonType.NotEqualTo:
                return a != b;
            case ComparisonType.GreaterThan:
                return a > b;
            case ComparisonType.GreaterThanOrEqualTo:
                return a >= b;
            case ComparisonType.LessThan:
                return a < b;
            case ComparisonType.LessThanOrEqualTo:
                return a >= b;
            default:
                break;
        }

        return true;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        NodeComparison newNode = new NodeComparison(_nodeGraph, position + duplicationOffset, category, false);
        newNode.portContainerIn.ports[0].SetValue(portContainerIn.ports[0].GetValue());
        newNode.portContainerIn.ports[1].SetValue(portContainerIn.ports[1].GetValue());
        newNode.portContainerOut.ports[1].SetValue(portContainerOut.ports[1].GetValue());
        Debug.Log(newNode.portContainerOut.ports[1].GetValue());

        return newNode;
    }
}