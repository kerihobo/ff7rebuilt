﻿using UnityEngine;

public abstract class NodeGet : NodeGetSet {
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        name = "Get " + field?.name;
    }

    public static NodeGet CreateFromField(NodeGraph _nodeGraph, Vector2 _offset, BlackboardField _field) {
        switch (_field.dataType) {
            case NodeDataType.Bool:
                return Create<NodeGetBool>(_nodeGraph, _offset, Category.BlackboardField);
            case NodeDataType.Int:
                return Create<NodeGetInt>(_nodeGraph, _offset, Category.BlackboardField);
        }

        return Create<NodeGetBool>(_nodeGraph, _offset);
    }
}
