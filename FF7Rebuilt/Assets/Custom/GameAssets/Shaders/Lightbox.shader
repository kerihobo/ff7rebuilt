// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Lightbox" {
	Properties
	{
		_Power("Power", Float) = 1
		_Box1Position("Box 1 Position", Vector) = (0, 0, 0, 0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" "Queue"="Geometry" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Custom vertex:vert noambient noforwardadd
		#pragma target 3.0

		struct Input {
			float3 objPos;
		};

		struct SurfaceOutputCustom
		{
			fixed3 Albedo;
			fixed3 Emission;
			fixed3 Normal;
			fixed Alpha;
			fixed3 objPos;
		};
		
		fixed3 _Box1Position;
		float _Power;

		float InverseLerp1(float a, float b, float t) {
			return (t - a) / (b - a);
		}

		half3 InverseLerp3(half3 a, half3 b, half3 t) {
			return (t - a) / (b - a);
		}

		half3 Remap(half3 In, float2 InMinMax, float2 OutMinMax) {
			return OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
		}
		
		half3 GetBoxPosition(half3 _boxPosition) {
			half3 boxPosition = _boxPosition.xzy/*  / 32768 */;
			boxPosition.y = -boxPosition.y;

			return boxPosition;
		}

		half3 GetBoxOffset(half3 _objPos, half3 _boxPosition) {
			return _objPos + _boxPosition;
		}

		half3 GetGradient(half3 _objPos, half3 _boxPosition) {
			half3 gradient = abs(_objPos + _boxPosition) * 2;
			gradient = Remap(gradient, float2(0, 1), float2(-.5, 1));
			gradient = saturate(gradient);
			gradient = pow(gradient, _Power);
			return gradient;
		}

		half3 GetLight(half3 _boxPosAbs, half3 _boxPos, half3 _n) {
			float nx = dot(_n, mul(saturate(unity_ObjectToWorld), float3(1, 0, 0)));
			float ny = dot(_n, mul(saturate(unity_ObjectToWorld), float3(0, 1, 0)));
			float nz = dot(_n, mul(saturate(unity_ObjectToWorld), float3(0, 0, 1)));

			float x = saturate(_boxPosAbs.x * nz);
			float y = saturate(_boxPosAbs.y * nx);
			float z = saturate(_boxPosAbs.z * ny);

			return (x + y + z);
		}

		float4 LightingCustom(SurfaceOutputCustom s, float3 lightDir, float atten) {
			float4 col;
			col.rgb = s.Albedo;
			col.a = s.Alpha;

			half3 objPos = s.objPos;			
			half3 boxPosition = GetBoxPosition(_Box1Position);
			half3 boxOffset = GetBoxOffset(objPos, boxPosition);
			half3 boxGradient = GetGradient(objPos, boxPosition);			
			half3 light = GetLight(boxGradient, boxOffset, s.Normal);
			
			col.rgb = light;
			
			return col;
		}

		void vert(inout appdata_full v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.objPos = v.vertex;
		}

		void surf (Input IN, inout SurfaceOutputCustom o) {
			o.Albedo = 1;
			o.objPos = IN.objPos;
		}
		ENDCG
	}
	FallBack "Diffuse"
}