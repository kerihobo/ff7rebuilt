﻿using UnityEngine;

public class NodeNot : Node {
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(90, 50);
        name = "Not";

        AddPortIn<PortBool>("In", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_PORT);
        AddPortOut<PortBool>("Out", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        return !portContainerIn.ports[0].GetValue();
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        return Create<NodeNot>(_nodeGraph, position + duplicationOffset, category, false);
    }
}
