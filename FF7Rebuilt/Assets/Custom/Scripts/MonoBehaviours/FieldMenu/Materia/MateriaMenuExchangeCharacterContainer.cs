using CharacterTypes;
using UnityEngine;

public class MateriaMenuExchangeCharacterContainer : MonoBehaviour {
    public MateriaMenuExchangeOptionList optionList;
    public MateriaMenuExchangeSlots slotsExchange;
    
    public MateriaMenuExchange exchange { get; set; }
    public RectTransform rct { get; set; }
    public Player assignedPlayer { get; set; }

    public void Initialize(MateriaMenuExchange _exchange) {
        exchange = _exchange;

        rct = GetComponent<RectTransform>();
        
        slotsExchange.Initialize(exchange, this);
        optionList.Initialize(exchange, this);

        slotsExchange.Disable();
        optionList.Disable();
    }

    public void Refresh(Player _player) {
        assignedPlayer = _player;

        optionList.SetPlayer(assignedPlayer);
        slotsExchange.SetPlayer(assignedPlayer);

        slotsExchange.RefreshEquipment();
        slotsExchange.RefreshMateria();
    }

    public void Clear() {
    }

    public void ReturnFromMateriaList() {
        slotsExchange.Open();
    }
}
