﻿using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class FieldBool : BlackboardField {
    public bool value;

    protected override void Initialize(Blackboard _blackboard) {
        base.Initialize(_blackboard);

        name = "New Bool";
        dataType = NodeDataType.Bool;
    }

    public override string GetValueAsString() {
        return value.ToString();
    }

    protected override void RenderValue() {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Value", GUILayout.Width(40));
        value = EditorGUILayout.Toggle(value);
        GUILayout.EndHorizontal();
    }
}
