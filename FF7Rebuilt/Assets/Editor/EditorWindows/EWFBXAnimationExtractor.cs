using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EWFBXAnimationExtractor : EditorWindow {
    [System.Serializable]
    public class CustomAnimation {
        public string name;
        public float firstFrame;
        public float lastFrame;
    }

    [System.Serializable]
    public class AnimationDataWrapper {
        public List<CustomAnimation> customAnimationClips;
    }

    private Object fbxFile;
    private TextAsset textFile;

    [MenuItem("Tools/FBX Animation Extractor")]
    private static void OpenCustomWindow() {
        EWFBXAnimationExtractor window = GetWindow<EWFBXAnimationExtractor>("FBX Animation Extractor");
        window.minSize = new Vector2(300, 100);
        window.maxSize = new Vector2(300, 100);
    }

    private void OnGUI() {
        GUILayout.Label("Select FBX File:", EditorStyles.boldLabel);
        fbxFile = EditorGUILayout.ObjectField(fbxFile, typeof(Object), false);

        GUILayout.Label("Select Text File:", EditorStyles.boldLabel);
        textFile = (TextAsset)EditorGUILayout.ObjectField(textFile, typeof(TextAsset), false);

        if (GUILayout.Button("Process Files")) {
            AddAnimations();
        }
    }

    private void AddAnimations() {
        if (fbxFile == null || textFile == null) {
            Debug.LogError("Please select an FBX file and provide animation data (JSON).");
            return;
        }

        string fbxPath = AssetDatabase.GetAssetPath(fbxFile);
        ModelImporter modelImporter = AssetImporter.GetAtPath(fbxPath) as ModelImporter;

        string jsonContent = textFile.text;
        AnimationDataWrapper wrapper = JsonUtility.FromJson<AnimationDataWrapper>(jsonContent);

        if (modelImporter != null) {
            ModelImporterClipAnimation[] updatedClips = new ModelImporterClipAnimation[wrapper.customAnimationClips.Count];

            for (int i = 0; i < wrapper.customAnimationClips.Count; i++) {
                CustomAnimation data = wrapper.customAnimationClips[i];
                AnimationClip customClip = new AnimationClip();
                
                updatedClips[i] = new ModelImporterClipAnimation {
                    name = data.name,
                    takeName = data.name,
                    firstFrame = data.firstFrame,
                    lastFrame = data.lastFrame
                };
            }
            
            modelImporter.clipAnimations = updatedClips;

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        Debug.Log($"Added {wrapper.customAnimationClips.Count} animation clips to the FBX model.");
    }
}
