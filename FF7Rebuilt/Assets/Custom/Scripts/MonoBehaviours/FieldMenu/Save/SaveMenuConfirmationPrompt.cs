using TMPro;
using UnityEngine;

public class SaveMenuConfirmationPrompt : MenuControllable {
    public FingerCursorRight cursor;
    
    [SerializeField] private RectTransform optionsParent;
    private int selectedIndex;
    private ScreenFieldSave screenFieldSave;
    private SaveMenuSaveGameList saveGameList;
    private TextMeshProUGUI[] arrOptions;
    private int selectedSlot;
    private int selectedSave;

    // Check flow when saving or not
    public void Initialize(ScreenFieldSave _screenFieldSave, SaveMenuSaveGameList _saveGameList) {
        screenFieldSave = _screenFieldSave;
        saveGameList = _saveGameList;

        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        cursor.Initialize(arrOptions[0].rectTransform);

        Disable();
    }

    public void Open(int _selectedSlot, int _selectedSave) {
        selectedSlot = _selectedSlot;
        selectedSave = _selectedSave;
        
        gameObject.SetActive(true);
        SetControllable();
        ResetSelection();
    }

    private void Disable() {
        gameObject.SetActive(false);
    }
    
    private void Back() {
        Disable();
        saveGameList.Open();
    }

    public void ResetSelection() {
        selectedIndex = 0;
        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputCancelPressed() {
        Back();
    }

    public override void GetInputConfirmPressed() {
        if (selectedIndex == 0) {
            Disable();
            screenFieldSave.Save(selectedSlot, selectedSave);
            return;
        }

        Back();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }
#endregion
}
