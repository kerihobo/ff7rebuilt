using FFVII.Database;
using UnityEditor;
using UnityEngine.UIElements;

public class SelectionCollectibleQuantityEditor : SelectionCollectibleEditor {
    public new class UxmlFactory : UxmlFactory<SelectionCollectibleQuantityEditor, UxmlTraits> { }
    
    protected override string UXMLPath { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/SelectionCollectibles/selection-collectible-quantity-editor.uxml";
    protected override string Name { get; set; } = "selection-collectible-quantity-editor";

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        tc.Q<IntegerField>("int-quantity").SetValue(target.quantity).RegisterValueChangedCallback(e => {
            target.quantity = e.newValue;
            EditorUtility.SetDirty(database);
        });
    }

    protected override void Refresh(SelectionCollectible _target, Database _database) {
        base.Refresh(_target, _database);
        target = _target;
        database = _database;

        tc.Q<IntegerField>("int-quantity").value = _target.quantity;
    }
}
