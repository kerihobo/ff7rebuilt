public enum ItemFieldApplication {
    NONE
,   USE_ABILITY
,   TENT
,   SAVE_CRYSTAL
,   SOURCE
,   LIMIT
}

public enum ItemSourceType {
    NONE
,   POWER
,   GUARD
,   MAGIC
,   MIND
,   SPEED
,   LUCK
}
