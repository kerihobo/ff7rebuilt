using CharacterTypes;
using InventoryTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuCheckInfoWindow : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtMpRequired;
    [SerializeField] private TextMeshProUGUI txtMpAvailable;
    //[SerializeField] private TextMeshProUGUI txtLink;
    //[SerializeField] private TextMeshProUGUI txtUses;
    [SerializeField] private TextMeshProUGUI txtQuadraMagicValue;
    [SerializeField] private TextMeshProUGUI txtAllValue;
    [SerializeField] private Image imgAllInfinity;
    [SerializeField] private TextMeshProUGUI txtSummonValue;
    [SerializeField] private Image imgSummonInfinity;
    private Player player;

    //[SerializeField] private Image imgInfinity;

    public void Show(Player _player) {
        player = _player;
        gameObject.SetActive(true);
        txtMpAvailable.text = "<mspace=12px>" + _player.points.mpMaxTotal;

        Clear();
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void Clear() {
        txtQuadraMagicValue.gameObject.SetActive(false);
        txtQuadraMagicValue.text = string.Empty;

        txtAllValue.gameObject.SetActive(false);
        txtAllValue.text = string.Empty;
        imgAllInfinity.gameObject.SetActive(false);
        
        txtSummonValue.gameObject.SetActive(false);
        txtSummonValue.text = string.Empty;
        imgSummonInfinity.gameObject.SetActive(false);

        txtMpRequired.text = string.Empty;
    }

    public void SetInfoSummon(Materia _referenceMateria, bool _isMaster) {
        Clear();
        
        if (_referenceMateria == null) return;

        Ability summon = _referenceMateria.selectionSpells[0].GetSelectedAbility(AbilityType.Summon);
        txtMpRequired.text = "<mspace=12px>" + summon.mpCost;
        txtSummonValue.gameObject.SetActive(true);

        Player.AbilityAugmentation abilityAugmentation = player.dctAbilityAugmentations[summon];
        int quadraMagic = abilityAugmentation.quadraMagic;
        txtQuadraMagicValue.gameObject.SetActive(quadraMagic > 0);
        txtQuadraMagicValue.text = quadraMagic.ToString();

        if (_isMaster) {
            SetUsesInfinite();
        } else {
            SetUsesFinite();
        }

        void SetUsesFinite() {
            txtSummonValue.text = (_referenceMateria.GetLevel() + 1).ToString();
            imgSummonInfinity.gameObject.SetActive(false);
        }

        void SetUsesInfinite() {
            txtSummonValue.text = string.Empty;
            imgSummonInfinity.gameObject.SetActive(true);
        }
    }

    public void SetInfoEnemySkill(Ability _enemySkill) {
        Clear();
        
        if (_enemySkill == null) return;

        txtMpRequired.text = "<mspace=12px>" + _enemySkill.mpCost;
    }

    public void SetInfoMagic(Ability _magic) {
        Clear();
        if (_magic == null) return;

        txtMpRequired.text = "<mspace=12px>" + _magic.mpCost;

        Player.AbilityAugmentation abilityAugmentation = player.dctAbilityAugmentations[_magic];
        
        int all = abilityAugmentation.all;
        txtAllValue.gameObject.SetActive(all > 0 && _magic.targetOptions.isToggleSingleMultipleTargets);
        txtAllValue.text = all.ToString();
        
        int quadraMagic = abilityAugmentation.quadraMagic;
        txtQuadraMagicValue.gameObject.SetActive(quadraMagic > 0);
        txtQuadraMagicValue.text = quadraMagic.ToString();

        // Need a way to tell if we should show Link infor or not.
    }
}
