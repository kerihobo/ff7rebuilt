﻿using UnityEngine;
using System;
using System.Collections.Generic;
using InventoryTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Weapons", menuName = "Kerihobo/Database/Inventory/Weapons")]
    public class DBWeapons : Database {
        [SerializeField] private List<Weapon> gloves = new List<Weapon>();
        [SerializeField] private List<Weapon> guns = new List<Weapon>();
        [SerializeField] private List<Weapon> gunArms = new List<Weapon>();
        [SerializeField] private List<Weapon> headDresses = new List<Weapon>();
        [SerializeField] private List<Weapon> megaphones = new List<Weapon>();
        [SerializeField] private List<Weapon> shurikens = new List<Weapon>();
        [SerializeField] private List<Weapon> spears = new List<Weapon>();
        [SerializeField] private List<Weapon> staves = new List<Weapon>();
        [SerializeField] private List<Weapon> swords = new List<Weapon>();
        [SerializeField] private List<Weapon> longSwords = new List<Weapon>();

        public WeaponType selectedMenuType { get; private set; }
        
        public List<Weapon> GetSelectedWeaponList(WeaponType _type) => _type switch {
            WeaponType.GLOVE => gloves,
            WeaponType.GUN => guns,
            WeaponType.GUN_ARM => gunArms,
            WeaponType.HEADDRESS => headDresses,
            WeaponType.MEGAPHONE => megaphones,
            WeaponType.SHURIKEN => shurikens,
            WeaponType.SPEAR => spears,
            WeaponType.STAFF => staves,
            WeaponType.SWORD => swords,
            WeaponType.LONG_SWORD => longSwords,
            _ => null
        };
    }
}
