using InventoryTypes;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ItemMenuItemList : MenuControllable {
    public FingerCursorRight cursor;
    public FingerCursorRight cursorArrange;
    public ItemMenuCharacterSelect characterSelect;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private UIItemInfo additionalOption;
    [SerializeField] private RectTransform optionsParent;

    private RectTransform scrollBarFG;
    private int selectedIndex;
    private int selectedIndexArrange;
    private ItemMenuUse itemMenuUse;
    private UIItemInfo[] arrOptions;

    // MAYBE REFACTOR
    private int lowBound = 0;
    private int lowBoundArrange = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 1;
    private bool isArrange;
    private int arrangeFirstSelection = -1;

    private int LowBound {
        get { return isArrange ? lowBoundArrange : lowBound; }
        set {
            if (isArrange) {
                lowBoundArrange = value;
            } else {
                lowBound = value;
            }
        }
    }
    private int SelectedIndex {
        get {
            return isArrange ? selectedIndexArrange : selectedIndex;
        }
        set {
            if (isArrange) {
                selectedIndexArrange = value;
            } else {
                selectedIndex = value;
            }
        }
    }
    
    private Collectible[] arrItems => GameManager.Instance.gameData.inventory.arrItems;

    public void Initialize(ScreenFieldItem _screenFieldItem, ItemMenuUse _itemMenuUse) {
        itemMenuUse = _itemMenuUse;
    // MAYBE REFACTOR
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<UIItemInfo>();
        foreach (UIItemInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOption.Initialize();
        ClearAdditional();

        characterSelect.Initialize(_screenFieldItem, this);
        
        Disable();

        cursor.Initialize(arrOptions[0].rct);
        cursorArrange.Initialize(arrOptions[0].rct);
        cursorArrange.Hide();

    // MAYBE REFACTOR
        listSpace = vlgOptions.spacing;
        listHeight = arrOptions[0].rct.rect.height;
        //listHierarchy = arrOptions[0].rct.parent.GetComponent<RectTransform>();
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = LowBound + i;
            Collectible selectedItem = arrItems[itemIndex];
            arrOptions[i].Refresh(selectedItem);
        }
    }

    // MAYBE REFACTOR
    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = LowBound + arrOptions.Length - columns;
        UIItemInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = LowBound;
        }

        additionalOption.rct.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        Collectible selectedItem = arrItems[anchoredIndex];
        additionalOption.Refresh(selectedItem);
    }

    private void ClearAdditional() {
        additionalOption.Clear();
    }

    public void ResetSelection() {
        selectedIndex = 0;
        selectedIndexArrange = 0;

        lowBound = 0;
        lowBoundArrange = 0;

        ResetArrangeA();
        UpdateScrollBar();
        SetCursor();
        SetCursorArrange();
    }

    public void SetCursor() {
        UIItemInfo selectedItemInfo = arrOptions[SelectedIndex];

        cursor.SetCursorPosition(selectedItemInfo.rct);

        Collectible selectedItem = arrOptions[SelectedIndex].item;
        string description = selectedItem != null ? selectedItem.description : "";

        itemMenuUse.SetDescription(description);
    }

    public void SetCursorArrange() {
        UIItemInfo selectedItemInfo = arrOptions[SelectedIndex];

        cursorArrange.SetCursorPosition(selectedItemInfo.rct);
    }

    private void ResetArrangeA() {
        arrangeFirstSelection = -1;
    }
    
    public void Open() {
        Debug.Log("Opened item list to browse");
        cursor.Show();
        //ResetSelection();
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);

        ResetArrangeA();
    }

    public void OpenViaArrange() {
        isArrange = true;
        LowBound = 0;
        SelectedIndex = 0;

        Open();
        UpdateScrollBar();
        Refresh();
        SetCursor();
        cursorArrange.Hide();
    }

    private void Close() {
        Disable();

        itemMenuUse.Close();
    }

    private void Disable() {
        isArrange = false;
        cursor.Hide();
        cursorArrange.Hide();
        
        ClearAdditional();
        UpdateScrollBar();
        Refresh();
        ResetArrangeA();
    }

    public void ExitCharacterSelection() {
        cursor.SetFlickerState(false);
        SetCursor();
        SetControllable();
    }

    public override void GetInputConfirmPressed() {
        if (isArrange) {
            Customize();
        } else {
            Use();
        }
        
        void Customize() {
            int itemIndex = LowBound + SelectedIndex;
            
            if (arrangeFirstSelection == -1) {
                arrangeFirstSelection = itemIndex;
                cursorArrange.Show();
                cursorArrange.SetFlickerState(true);
                SetCursorArrange();
            } else {
                (arrItems[arrangeFirstSelection], arrItems[itemIndex]) = (arrItems[itemIndex], arrItems[arrangeFirstSelection]);
                cursorArrange.Hide();
                Refresh();
                ResetArrangeA();
            }
        }
        
        void Use() {
            UIItemInfo itemSelected = arrOptions[SelectedIndex];
            Collectible item = itemSelected.item;

            if (itemSelected.IsUsable) {
                if (item.fieldApplication == ItemFieldApplication.SAVE_CRYSTAL) {
                    Close();

                    PromptSaveCrystal.Instance.SetCrystal(item);
                    FieldMenu.Instance.onClosed = PromptSaveCrystal.Instance.Open;

                    FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.QuickCLose());

                    return;
                }

                EnterCharacterSelection(itemSelected);
            } else {
                return;
            }

            // Use our array index as the decider, whether we proceed to use the item on a character or not, etc.

            void EnterCharacterSelection(UIItemInfo _itemSelected) {
                cursor.SetFlickerState(true);
                characterSelect.Open(_itemSelected);
            }
        }
    }

    public override void GetInputCancelPressed() {
        Close();
        itemMenuUse.Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (LowBound + SelectedIndex == 0 && _v > 0) || (LowBound + SelectedIndex == arrItems.Length - columns && _v < 0)) return;
        //Debug.Log(LowBound + SelectedIndex + " : " + arrItems.Length);

        int v = -_v * columns;
        SelectedIndex += v;

        bool isTooLow = SelectedIndex < 0;
        bool isTooHigh = SelectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            LowBound += v;
            LowBound -= LowBound % columns;
        }

        if (SelectedIndex < 0) {
            SelectedIndex += columns;
        } else if (SelectedIndex > arrOptions.Length - 1) {
            SelectedIndex -= columns;
        }
        //SelectedIndex = Mathf.Clamp(SelectedIndex, 0, arrOptions.Length - 1);

        LowBound = Mathf.Clamp(LowBound, 0, arrItems.Length - arrOptions.Length);
        LowBound -= LowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            cursorArrange.AddPosition(newPosition - optionsParent.anchoredPosition);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        Vector2 finalPosition = Vector2.Lerp(start, end, 1);
        cursorArrange.AddPosition(finalPosition - optionsParent.anchoredPosition);

        optionsParent.anchoredPosition = start;
        
        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;
            ClearAdditional();
        }

        int v = _pageDirection * (10 * columns);
        int lowBoundMax = arrItems.Length - arrOptions.Length;
        LowBound += v;
        LowBound = Mathf.Clamp(LowBound, 0, lowBoundMax);
        LowBound -= LowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    private void UpdateScrollBar() {
        float scroll = ((float)LowBound / columns) / (arrItems.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        //Debug.Log($"progress({scroll:f3}), bgHeight({scrollBarBG.rect.height:f3}), potentialLoc({scroll})");
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }
    
    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
}
