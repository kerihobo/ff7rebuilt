using UnityEngine;
using InventoryTypes;
using FFVII.Database;
using System.Linq;
using System.Collections.Generic;
using CharacterTypes;

public class ItemMenuCharacterSelect : MenuControllable {
    public FingerCursorRight cursor;
    public UICharacterInfoContainer characterInfoContainer;

    private int selectedIndex;
    private ScreenFieldItem screenFieldItem;
    private ItemMenuItemList itemList;
    private UIItemInfo itemSelected;
    private Ability selectedAbility;

    private List<UICharacterInfo> AvailableCharacterInfos => characterInfoContainer.arrPlayerInfos.Where(x => x.player != null).ToList();

    public void Initialize(ScreenFieldItem _screenFieldItem, ItemMenuItemList _itemList) {
        screenFieldItem = _screenFieldItem;
        itemList = _itemList;

        characterInfoContainer.Initialize();
        
        Disable();

        RectTransform firstRectTransform = characterInfoContainer.arrPlayerInfos[0].rct;
        cursor.Initialize(firstRectTransform);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        cursor.Show();

        cursor.SetTargetSingle(characterInfoContainer.arrPlayerInfos[selectedIndex].rct);
        SetCursor(cursor);
    }

    public void SetCursor(FingerCursorRight _cursor) {
        _cursor.SetCursorPosition(characterInfoContainer.arrPlayerInfos[selectedIndex].rct);
    }

    public void Open(UIItemInfo _itemSelected) {
        itemSelected = _itemSelected;
        selectedAbility = null;

        ResetSelection();
        SetUsage();
        SetControllable();
        
        void SetUsage() {
            switch (itemSelected.item.fieldApplication) {
                case ItemFieldApplication.USE_ABILITY:
                    selectedAbility = DBResources.GetAbilityList(AbilityType.BattleItem).FirstOrDefault(x => GetAbility(x, itemSelected.item));

                    if (selectedAbility.targetOptions.isMultipleTargetsByDefault) {
                        cursor.SetTargetAll(AvailableCharacterInfos.Select(x => x.rct).ToList());
                    }
                    break;
                case ItemFieldApplication.TENT:
                    cursor.SetTargetAll(AvailableCharacterInfos.Select(x => x.rct).ToList());
                    break;
                default:
                    break;
            }
        }

        bool GetAbility(Ability x, Collectible item) {
            return x.name == item.name;
        }
    }

    private void Disable() {
        cursor.Hide();

        itemSelected = null;
    }

    private void Close() {
        Disable();
        itemList.ExitCharacterSelection();
    }

    public override void GetInputConfirmPressed() {
        UICharacterInfo selectedPlayerInfo = characterInfoContainer.arrPlayerInfos[selectedIndex];
        Player targetPlayer = selectedPlayerInfo.player;
        if (targetPlayer == null) return;

        Collectible item = itemSelected.item;
        bool isUsed = false;

        Debug.Log($"{item.name} > {targetPlayer.name}");

        // Use item on character.
        switch (item.fieldApplication) {
            case ItemFieldApplication.USE_ABILITY:
                TryUseAbility();
                break;
            case ItemFieldApplication.TENT:
                TryUseTent();
                break;
            case ItemFieldApplication.SOURCE:
                TryUseSource();
                break;
            case ItemFieldApplication.LIMIT:
                TryUseLimitItem();
                break;
            default:
                break;
        }

        if (isUsed) {
            if (cursor.IsAll) {
                AvailableCharacterInfos.ForEach(x => x.Refresh(x.player));
            } else {
                selectedPlayerInfo.Refresh(targetPlayer);
            }

            GameManager.Instance.gameData.inventory.RemoveItem(item);
            itemList.Refresh();
            
            if (item.quantity < 1) {
                Close();
            }
        }

        void TryUseAbility() {
            if (selectedAbility != null) {
                if (cursor.IsAll) {
                    foreach (UICharacterInfo info in AvailableCharacterInfos) {
                        if (selectedAbility.TryUse(info.player, info.player, false, false)) {
                            isUsed = true;
                        }
                    }
                } else {
                    isUsed = selectedAbility.TryUse(targetPlayer, targetPlayer, false, false);
                }
            }
        }

        void TryUseTent() {
            List<UICharacterInfo> availableCharacters = AvailableCharacterInfos.Where(x => {
                bool isMissingHP = x.player.hpCurrent < x.player.points.hpMaxTotal;
                bool isAlive     = x.player.hpCurrent > 0;
                bool isMissingMP = x.player.mpCurrent < x.player.points.mpMaxTotal;
                return isAlive && (isMissingHP || isMissingMP);
            }).ToList();

            isUsed = availableCharacters.Count > 0;

            if (isUsed) {
                availableCharacters.ForEach(x => {
                    x.player.hpCurrent = x.player.points.hpMaxTotal;
                    x.player.mpCurrent = x.player.points.mpMaxTotal;
                    //x.player.ailments.sadness.isActive = false;
                    //x.player.ailments.fury.isActive = false;
                });
            }
        }

        void TryUseSource() {
            if (targetPlayer.hpCurrent <= 0) return;

            PrimaryStats sourceStats = targetPlayer.sourceStats;

            switch (((Item)item).sourceType) {
                case ItemSourceType.POWER:
                    IncreaseSource(ref sourceStats.strength);
                    break;
                case ItemSourceType.GUARD:
                    IncreaseSource(ref sourceStats.vitality);
                    break;
                case ItemSourceType.MAGIC:
                    IncreaseSource(ref sourceStats.magic);
                    break;
                case ItemSourceType.MIND:
                    IncreaseSource(ref sourceStats.spirit);
                    break;
                case ItemSourceType.SPEED:
                    IncreaseSource(ref sourceStats.dexterity);
                    break;
                case ItemSourceType.LUCK:
                    IncreaseSource(ref sourceStats.luck);
                    break;
            }

            void IncreaseSource(ref int _stat) {
                if (_stat < 255) {
                    _stat++;
                    targetPlayer.RecalculatePlayerStats();
                
                    isUsed = true;
                }
            }
        }
        
        void TryUseLimitItem() {
            Item limitItem = (Item)item;
            Player intendedPlayer = limitItem.intendedCharacter.GetSelectedPlayer();
            string intendedPlayerID = intendedPlayer.idName;
            string targetPlayerID = targetPlayer.idName;
            bool isIntendedPlayer = intendedPlayerID == targetPlayerID;
            string message = "";

            if (!isIntendedPlayer) {
                message = targetPlayer.limitBreakManager.responseSet.incompatible;
                Debug.Log($"{targetPlayerID}: {message}");
                StartCoroutine(screenFieldItem.DisplayLimitMessage(message));

                return;
            }

            LimitBreakManager limitManager = targetPlayer.limitBreakManager;
            int maxLevel = limitManager.availableLevels - 1;
            LimitBreakManager.LimitLevel[] limitLevels = limitManager.limitLevels;
            bool hasAchievedFinal = limitLevels[maxLevel].isUnlocked;

            IEnumerable<LimitBreakManager.LimitLevel> priorLevels = limitLevels.Take(maxLevel);
            Debug.Log(priorLevels.Count());
            bool hasAchievedAllPrior = priorLevels.All(x => x.isUnlocked && x.usesCount >= x.usesNeeded);

            if (!hasAchievedAllPrior) {
                message = targetPlayer.limitBreakManager.responseSet.compatible;
                Debug.Log($"{targetPlayerID}: {message}");
                StartCoroutine(screenFieldItem.DisplayLimitMessage(message));

                return;
            }

            message = targetPlayer.limitBreakManager.responseSet.success;
            Debug.Log($"{targetPlayerID}: {message}");
            StartCoroutine(screenFieldItem.DisplayLimitMessage(message));

            limitLevels[maxLevel].isUnlocked = true;
            isUsed = true;
        }
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (cursor.IsAll) return;

        int v = -_v;
        selectedIndex = (selectedIndex + v) % characterInfoContainer.arrPlayerInfos.Length;

        if (selectedIndex < 0) {
            selectedIndex = characterInfoContainer.arrPlayerInfos.Length - 1;
        }

        SetCursor(cursor);
    }

    public override void GetInputHorizontal(int h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
}
