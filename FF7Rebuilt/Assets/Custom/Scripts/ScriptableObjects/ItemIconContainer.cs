using InventoryTypes;
using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "ItemIconContainer", menuName = "Kerihobo/Utilities/Item Icon Container")]
public class ItemIconContainer : ScriptableObject {
    [Serializable]
    public class StarIconPair {
        public Sprite sprStarInactive;
        public Sprite sprStarActive;
    }

    public Sprite sprAccessory;
    public Sprite sprArmor;
    public Sprite sprGeneric;
    public Sprite sprGlove;
    public Sprite sprGun;
    public Sprite sprGunArm;
    public Sprite sprHeaddress;
    public Sprite sprMegaphone;
    public Sprite sprShuriken;
    public Sprite sprSpear;
    public Sprite sprStaff;
    public Sprite sprSword;
    public Sprite sprMateriaYellow;
    public Sprite sprMateriaMagenta;
    public Sprite sprMateriaGreen;
    public Sprite sprMateriaRed;
    public Sprite sprMateriaBlue;
    public Sprite sprSlotNoGrowth;
    public Sprite sprSlotNormal;
    public StarIconPair starsMagic;
    public StarIconPair starsSupport;
    public StarIconPair starsCommand;
    public StarIconPair starsIndependent;
    public StarIconPair starsSummon;

    public static ItemIconContainer Instance { get; private set; }

    public Sprite GetIconByCollectible(Collectible _collectible) {
        if      (_collectible is Armor)     return sprArmor;
        else if (_collectible is Accessory) return sprAccessory;
        else if (_collectible is Weapon) {
            Weapon weapon = _collectible as Weapon;
            
            switch (weapon.type) {
                case WeaponType.GLOVE:
                    return sprGlove;
                case WeaponType.GUN:
                    return sprGun;
                case WeaponType.GUN_ARM:
                    return sprGunArm;
                case WeaponType.HEADDRESS:
                    return sprHeaddress;
                case WeaponType.MEGAPHONE:
                    return sprMegaphone;
                case WeaponType.SHURIKEN:
                    return sprShuriken;
                case WeaponType.SPEAR:
                    return sprSpear;
                case WeaponType.STAFF:
                    return sprStaff;
                case WeaponType.SWORD:
                case WeaponType.LONG_SWORD:
                    return sprSword;
            }
        }

        return sprGeneric;
    }

    public Sprite GetIconByMateria(Materia _materia) {
        if (_materia == null) return null;

        switch (_materia.type) {
            case MateriaType.Command:
                return sprMateriaYellow;
            case MateriaType.Support:
                return sprMateriaBlue;
            case MateriaType.Independent:
                return sprMateriaMagenta;
            case MateriaType.Magic:
                return sprMateriaGreen;
            default:
                return sprMateriaRed;
        }
    }

    public static ItemIconContainer GetInstance() {
        if (!Instance) {
            Instance = Resources.Load<ItemIconContainer>("ScriptableObjects/ItemIconContainer");
        }

        return Instance;
    }
}