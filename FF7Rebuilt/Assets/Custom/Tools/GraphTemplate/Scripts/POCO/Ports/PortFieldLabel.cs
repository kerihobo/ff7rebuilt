﻿using UnityEngine;

public class PortFieldLabel : Port {
    public PortFieldLabel(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) : base(_parentNode, _portName, _direction, _capacity, _displayMode) {
        portType = NodeDataType.Bool;
    }

    public string value;

    protected override void RenderAsInput(StyleSet _styleSet) {
        GUILayout.Label(((NodeGetSet)parentNode).field.GetValueAsString().ToString());
    }

    public override void SetValue(dynamic _value) {
        value = _value;
    }
}
