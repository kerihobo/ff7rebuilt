using CharacterTypes;
using FFVII.Database;
using InventoryTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuCheckMagicList : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private RectTransform optionsParent;

    private RectTransform scrollBarFG;
    private int selectedIndex;
    private MateriaMenuCheck check;
    private MateriaMenuCheckCommand commandWindow;
    private MateriaMenuCheckMagicInfo[] arrOptions;

    // MAYBE REFACTOR
    [SerializeField] private RectTransform additionalOptionParent;
    private MateriaMenuCheckMagicInfo[] additionalOptions;
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 3;
    private List<Ability> arrViewItems;

    //private Materia[] arrItems;

    private Player GetSelectedCharacter => check.GetSelectedCharacter;
    private Config config => GameManager.Instance.gameData.config;

    public void Initialize(MateriaMenuCheck _check, MateriaMenuCheckCommand _commandWindow) {
        check = _check;
        commandWindow = _commandWindow;
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        GridLayoutGroup glgOptions = GetComponentInChildren<GridLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(glgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<MateriaMenuCheckMagicInfo>(true);
        foreach (MateriaMenuCheckMagicInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOptions = additionalOptionParent.GetComponentsInChildren<MateriaMenuCheckMagicInfo>(true);
        foreach (MateriaMenuCheckMagicInfo info in additionalOptions) {
            info.Initialize();
        }

        ClearAdditional();

        Disable();

        cursor.Initialize(arrOptions[0].rct);

        listSpace = glgOptions.spacing.y;
        listHeight = arrOptions[0].rct.rect.height;
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = lowBound + i;
            Ability displayedAbility = arrViewItems[itemIndex];
            arrOptions[i].Refresh(GetSelectedCharacter, displayedAbility);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - columns;
        MateriaMenuCheckMagicInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOptionParent.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        for (int i = 0; i < additionalOptions.Length; i++) {
            Ability displayedAbility = arrViewItems[anchoredIndex + i];
            additionalOptions[i].Refresh(GetSelectedCharacter, displayedAbility);
        }
    }

    private void ClearAdditional() {
        foreach (var info in additionalOptions) {
            info.Clear();
        }
    }

    public void ResetSelection() {
        selectedIndex = 0;
        lowBound = 0;

        UpdateScrollBar();
        SetCursor();
    }

    public void SetCursor() {
        MateriaMenuCheckMagicInfo selectedItemInfo = arrOptions[selectedIndex];

        cursor.SetCursorPosition(selectedItemInfo.rct);

        Ability magic = selectedItemInfo.GetSpellAvailable();
        check.SetDescription(magic?.description);
        check.SetInfoMagic(magic);
    }

    public void Open() {
        lowBound = 0;

        SetViewItems();

        //if (arrViewItems.Length <= 0) {
        //    return;
        //}

        Show();
        SetControllable();
        Refresh();
        ResetSelection();
        FieldMenu.Instance.SetCooldownTraverse(0);

        /// <summary>
        /// Gather all Magic spells from the database.
        /// Gather all available Magic spells from the current Character's Materia loadout.
        /// Iterate through the complete list, checking 3 at a time.
        /// If 1 spell is found in that set of 3, keep the set including up to 2 null entries, otherwise discard it.
        /// Remember to order the spells by Magic Order.
        /// </summary>
        void SetViewItems() {
            Ability.MagicOrder magicOrder0 = config.GetMagicOrderArrangement[0];
            Ability.MagicOrder magicOrder1 = config.GetMagicOrderArrangement[1];
            Ability.MagicOrder magicOrder2 = config.GetMagicOrderArrangement[2];
            List<Ability> allSpells = DBResources.GetAbilityList(AbilityType.Magic).OrderBy(x => {
                if (x.magicOrder == magicOrder0) return 0;
                else if (x.magicOrder == magicOrder1) return 1;
                else if (x.magicOrder == magicOrder2) return 2;
                else return 3;
            }).ToList()
            ;

            List<Ability> arrAvailableSpells = GetSelectedCharacter.lsAvailableMagic;

            // =======================================================================================================================================================
            // Old way - Iterate 3 steps at a time through all spells. For every 3 spells, find out if at least 1 of them is available, if so, keep all 3, even nulls.
            // =======================================================================================================================================================
            //arrViewItems = new List<Ability>();
            //for (int i = 0; i < allSpells.Count; i += 3) {
            //    Ability item1 = arrAvailableSpells.Contains(allSpells[i]) ? allSpells[i] : null;
            //    Ability item2 = (i + 1 < allSpells.Count) ? (arrAvailableSpells.Contains(allSpells[i + 1]) ? allSpells[i + 1] : null) : null;
            //    Ability item3 = (i + 2 < allSpells.Count) ? (arrAvailableSpells.Contains(allSpells[i + 2]) ? allSpells[i + 2] : null) : null;

            //    if (item1 != null || item2 != null || item3 != null) {
            //        arrViewItems.Add(item1);

            //        if (i + 1 < allSpells.Count) {
            //            arrViewItems.Add(item2);
            //        }

            //        if (i + 2 < allSpells.Count) {
            //            arrViewItems.Add(item3);
            //        }
            //    }
            //}
            // ==================================================================================================================================================================================================
            // New way - Select everything from allSpells, transform them into anonymous objects containing spell and index. Group by sets of 3, select groups where at least 1 is contained in available spells.
            // ==================================================================================================================================================================================================
            arrViewItems = allSpells
                .Select((spell, index) => new { Spell = spell, Index = index })
                .GroupBy(x => x.Index / 3)
                .Where(x => x.Any(x => {
                    return arrAvailableSpells.Contains(x.Spell);
                }))
                .SelectMany(group => group.Select(x => arrAvailableSpells.Contains(x.Spell) ? x.Spell : null))
                .ToList()
            ;

            int difference = allSpells.Count - arrViewItems.Count;
            for (int i = 0; i < difference; i++) {
                arrViewItems.Add(null);
            }
        }
    }

    public void Disable() {
        cursor.Hide();
        ClearAdditional();

        gameObject.SetActive(false);
    }

    public void Close() {
        Disable();
        commandWindow.ExitSubMenu();
        check.CloseInfoWindow();
    }

    private void Show() {
        gameObject.SetActive(true);
        SetScrollbarHeight();
        UpdateScrollBar();
        cursor.Show();

        void SetScrollbarHeight() {
            float scrollBarHeight = scrollBarBG.sizeDelta.y;
            scrollBarHeight *= arrOptions.Length / (float)arrViewItems.Count;
            scrollBarHeight = Mathf.RoundToInt(scrollBarHeight);
            scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarHeight);
        }
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        float scroll = ((float)lowBound / columns) / (arrViewItems.Count / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

#region Override methods.
    public override void GetInputConfirmPressed() { }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex <= columns - 1 && _v > 0) || (lowBound + selectedIndex >= arrViewItems.Count - columns && _v < 0)) return;

        int v = -_v * columns;
        selectedIndex += v;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
            lowBound -= lowBound % columns;
        }

        if (selectedIndex < 0) {
            selectedIndex += columns;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= columns;
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Count - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _h < 0) || (lowBound + selectedIndex == arrViewItems.Count - 1 && _h > 0)) return;

        selectedIndex += _h;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += _h * columns;
            lowBound -= lowBound % columns;

            if (isTooHigh) {
                selectedIndex -= columns;
            } else if (isTooLow) {
                selectedIndex += columns;
            }
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Count - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        SetCursor();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(_h));
        }
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;

            ClearAdditional();
        }

        int v = _pageDirection * (3 * columns);
        int lowBoundMax = arrViewItems.Count - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
    }

    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
