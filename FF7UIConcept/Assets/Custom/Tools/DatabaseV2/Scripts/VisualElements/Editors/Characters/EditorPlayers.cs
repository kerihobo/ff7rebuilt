using UnityEngine.UIElements;

public class EditorPlayers : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Characters/editor-players.uxml";
    protected override string Name { get; set; } = "editor-players";

    public EditorPlayers() : base() {
    }
}
