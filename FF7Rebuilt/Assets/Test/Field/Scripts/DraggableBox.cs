using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class DraggableBox : MonoBehaviour {
    [SerializeField] private Vector2 samplePosition;
    [SerializeField] private float zoomFactor = 2f;
    [Header("Debug")]
    [SerializeField] private bool isShowGUI = true;
    private Rect sampleRect = new Rect(10, 10, 100, 100);
    private Vector2 dragRect;
    private Vector2 snapRect;
    private bool isDragging = false;
    private Vector2 previousMousePosition;
    private ScreenSampler screenSampler;
    private Texture2D txNormal;
    private Texture2D txHovered;
    private GUIStyle styleBox;

    void OnEnable() {
        if (Application.isPlaying) return;

        EditorApplication.update += Repaint;
    }

    void OnDisable() {
        if (Application.isPlaying) return;
        
        EditorApplication.update -= Repaint;
    }

    private void Awake() {
        if (Application.isPlaying) return;
        
        UpdateDimensions();
        ForceUpdateSampler();

        txHovered = new Texture2D(1, 1);
        txHovered.SetPixel(0, 0, new Color(0, 0, 0, .8f));
        txHovered.Apply();

        txNormal = new Texture2D(1, 1);
        txNormal.SetPixel(0, 0, new Color(0, 0, 0, .4f));
        txNormal.Apply();
    }

    private void Update() {
        if (Application.isPlaying) return;

        UpdateDimensions();
        ForceUpdateSampler();
    }

    void OnGUI() {
        if (Application.isPlaying || !isShowGUI) return;
        
        UpdateDimensions();

        // Calculate the bottom-left anchored position
        float yOffset = Screen.height - sampleRect.height - sampleRect.y;
        Rect anchoredRect = new Rect(sampleRect.x, yOffset, sampleRect.width, sampleRect.height);

        Event e = Event.current;

        if (e.type == EventType.KeyDown) {
            Debug.Log(e.keyCode == KeyCode.Z);
        } else if (e.type == EventType.KeyUp) {
            Debug.Log(e.keyCode == KeyCode.Z);
        }

        switch (e.type) {
            case EventType.MouseDown:
                switch (e.button) {
                    case 0:
                        HandleMouseLeftDown(anchoredRect, e);
                        break;
                }

                break;
            case EventType.MouseDrag:
                if (isDragging) {
                    HandleMouseLeftDrag(e);
                }
                
                break;
            case EventType.MouseUp:
                switch (e.button) {
                    case 0:
                        HandleMouseLeftUp(e);
                        break;
                    case 1:
                        HandleMouseRightUp(e);
                        break;
                }

                break;
        }

        anchoredRect = new Rect(sampleRect.x, yOffset, sampleRect.width, sampleRect.height);
        
        string s =
            $"x: {sampleRect.position.x:.00}" 
        +   $"\ny: {sampleRect.position.y:.00}" 
        +   $"\nw: {sampleRect.width:.00}" 
        +   $"\nh: {sampleRect.height:.00}" 
        +   $"\n" 
        +   $"\nzoom: {zoomFactor:.00}" 
        +   $"\n" 
        +   $"\nClick & Drag" 
        +   $"\nLeft = Pan" 
        +   $"\nMiddle = Zoom"
        ;

        if (styleBox == null) {
            styleBox = new GUIStyle(GUI.skin.box);
            styleBox.alignment = TextAnchor.UpperLeft;
            styleBox.normal.background = txNormal;
            styleBox.hover.background = txHovered;
            styleBox.normal.textColor = GUI.skin.label.normal.textColor * .75f;
            styleBox.hover.textColor = GUI.skin.label.normal.textColor;
        }

        GUI.Box(anchoredRect, s, styleBox);
    }

    private void HandleMouseLeftDown(Rect _anchoredRect, Event _e) {
        if (_anchoredRect.Contains(_e.mousePosition)) {
            isDragging = true;
            previousMousePosition = _e.mousePosition;
            dragRect = sampleRect.position; // Initialize dragRect to the current boxRect
            _e.Use();
        }
    }

    private void HandleMouseLeftDrag(Event _e) {
        if (isDragging) {
            Vector2 mouseDelta = _e.mousePosition - previousMousePosition;
            
            if (Event.current.alt) {
                zoomFactor += mouseDelta.x / 100f;

                UpdateDimensions();
            } else {
                dragRect += new Vector2(mouseDelta.x, -mouseDelta.y);

                snapRect = new Vector2(
                    Mathf.Round(dragRect.x / 10) * 10,
                    Mathf.Round(dragRect.y / 10) * 10
                );

                // Use the snap position if Ctrl is held, otherwise use the normal drag position
                if (Event.current.control) {
                    samplePosition = snapRect;
                } else {
                    samplePosition = dragRect;
                }

                sampleRect.position = samplePosition;
            }

            ForceUpdateSampler();

            previousMousePosition = _e.mousePosition;
            GUI.changed = true;
            _e.Use();
            EditorUtility.SetDirty(this); // Mark the object as dirty to save changes
        }
    }

    private void HandleMouseLeftUp(Event _e) {
        isDragging = false;
        _e.Use();
    }

    private void HandleMouseRightUp(Event _e) {
        screenSampler.RaycastToWorld(_e.mousePosition);
    }

    void Repaint() {
        if (isDragging) {
            EditorUtility.SetDirty(this);
            SceneView.RepaintAll();
        }
    }

    private void UpdateDimensions() {
        sampleRect.position = samplePosition;
        sampleRect.width = Screen.width / zoomFactor;
        sampleRect.height = Screen.height / zoomFactor;
    }

    private void ForceUpdateSampler() {
        if (!screenSampler) {
            screenSampler = GetComponent<ScreenSampler>();
        }
        screenSampler.ForceUpdate(sampleRect, zoomFactor);
    }
}

// This could be useful later.
public static class FloatExtensions
{
    public enum ROUNDING { UP, DOWN, CLOSEST }

    public static float ToNearestMultiple(this float f, float multiple, ROUNDING roundTowards = ROUNDING.CLOSEST)
    {
        f /= multiple;
        return (roundTowards == ROUNDING.UP) ? Mathf.Ceil(f) * multiple : Mathf.Round(f) * multiple;
    }
}