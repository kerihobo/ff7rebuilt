using System.Collections;
using UnityEngine;

public abstract class ScreenField : MonoBehaviour {
    public abstract FieldMenu fieldMenu { get; set; }

    public abstract void Initialize(FieldMenu _fieldMenu);
    public abstract IEnumerator Open();
    public abstract IEnumerator Close();
    public abstract void SetDefaultDisplay();
}
