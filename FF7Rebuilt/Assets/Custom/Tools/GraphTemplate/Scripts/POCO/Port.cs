﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[Serializable]
public class Port {
    public Port(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) {
        name        = _portName;
        parentNode  = _parentNode;
        direction   = _direction;
        capacity    = _capacity;
        displayMode = _displayMode;
        portType    = NodeDataType.Execution;
    }
    
    public enum Direction {
        IN
    ,   OUT
    }

    public enum DisplayMode {
        SHOW_FIELD
    ,   SHOW_PORT
    ,   SHOW_ALL
    }

    public enum Capacity {
        SINGLE
    ,   MULTIPLE
    }

    public string name;
    public Color color = Color.white;
    [SerializeReference, HideInInspector]
    public Node parentNode;
    [SerializeReference, HideInInspector]
    public List<Port> connectedPorts = new List<Port>();
    public DisplayMode displayMode;
    public Direction direction = Direction.IN;
    public Capacity capacity = Capacity.SINGLE;
    public NodeDataType portType;
    public Rect rect;

    [SerializeField]
    private bool isActive = true; 
    
    public bool isHovered { get; set; }
    public bool IsActive {
        get {
            return isActive;
        } set {
            isActive = value;
        }
    }

    public void Render(StyleSet _styleSet) {
        GUILayout.BeginHorizontal();
            if (direction == Direction.IN) {
                RenderAsInput(_styleSet);
            } else {
                RenderAsOutput(_styleSet);
            }
        GUILayout.EndHorizontal();
    }

    public virtual void RenderEdges() {
        if (direction == Direction.IN || connectedPorts.Count == 0) return;

        Vector2 positionOut = rect.position;
        positionOut += parentNode.rect.position;
        Vector2 start = positionOut + rect.size + (Vector2.down * (rect.size.y / 2));

        foreach (Port p in connectedPorts) {
            Vector2 positionIn = p.rect.position;
            positionIn += p.parentNode.rect.position;
            Vector2 end = positionIn + (Vector2.up * (p.rect.size.y / 2));

            float curvature = Mathf.InverseLerp(0, 100, Vector2.Distance(start, end)) * 50;
            Color color = GetColorByType();
            Handles.DrawBezier(
                start
            ,   end
            ,   start + (Vector2.right * curvature)
            ,   end + (Vector2.left * curvature)
            ,   color
            ,   null
            ,   3
            );
        }
    }

    private Color GetColorByType() {
        switch (portType) {
            case NodeDataType.Bool:
                return new Color(229f / 255, 69f / 255, 69f / 255);
            case NodeDataType.Int:
                return new Color(230f / 255, 149f / 255, 69f / 255);
            default:
                return new Color(217f / 255, 217f / 255, 217f / 255);
        }
    }

    public void RenderGhostEdge(Vector2 _mousePosition) {
        // HACK: After a recompile, this seems to think parentNode==null until we click in the window again.
        if (parentNode == null) return;

        Vector2 currentRectPosition = rect.position + parentNode.rect.position;

        Vector2 start = currentRectPosition + rect.size + (Vector2.down * (rect.size.y / 2));
        if (direction == Direction.IN) {
            start.x -= rect.size.x;
        }

        Vector2 end = _mousePosition;

        float curvature = Mathf.InverseLerp(0, 100, Vector2.Distance(start, end)) * 50;
        Vector2 startTangent = start + (Vector2.right * curvature);
        Vector2 endTangent = end + (Vector2.left * curvature);
        if (direction == Direction.IN) {
            startTangent = start + (Vector2.left * curvature);
            endTangent = end + (Vector2.right * curvature);
        }

        Color color = GetColorByType();
        color.a = .55f;
        Handles.DrawBezier(
            start
        ,   end
        ,   startTangent
        ,   endTangent
        ,   color
        ,   null
        ,   3
        );
    }

    protected virtual void RenderField() {
    }

    public static bool WillCauseLoop(Port _initialPort, Port _nextPort) {
        Node n = _nextPort.parentNode;
        PortContainer targetPortContainer = _initialPort.direction == Direction.OUT
        ?   n.portContainerOut
        :   n.portContainerIn
        ;

        foreach (Port x in targetPortContainer.ports) {
            foreach (Port y in x.connectedPorts) {
                if (y.parentNode == _initialPort.parentNode) {
                    Debug.LogWarning("Unable to connect nodes where a loop would occur.");
                    return true;
                } else if (y != null) {
                    //Debug.Log("Trying: " + p.connectedPort.parentNode.name + " | " + p);
                    if (WillCauseLoop(_initialPort, y)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected virtual void RenderAsInput(StyleSet _styleSet) {
        GUILayout.BeginHorizontal(isActive ? GUIStyle.none : _styleSet.portInactive);
            if (displayMode == DisplayMode.SHOW_ALL || displayMode == DisplayMode.SHOW_PORT) {
                GUILayout.Box("", GetPortStyle(_styleSet), GUILayout.Width(16), GUILayout.Height(16));
                GetIsHovered();
            }

            if (displayMode == DisplayMode.SHOW_ALL || displayMode == DisplayMode.SHOW_FIELD) {
                if (connectedPorts.Count < 1) {
                    RenderField();
                }
            }

            GUILayout.Label(name, _styleSet.inLabel);
        GUILayout.EndHorizontal();
    }

    protected virtual void RenderAsOutput(StyleSet _styleSet) {
        GUILayout.Label(name, _styleSet.outLabel);

        GUILayout.Box("", GetPortStyle(_styleSet), GUILayout.Width(16), GUILayout.Height(16));
        GetIsHovered();
    }

    protected void GetIsHovered() {
        Event e = Event.current;

        if (e.type == EventType.Repaint) {
            rect = GUILayoutUtility.GetLastRect();
            isHovered = rect.Contains(Event.current.mousePosition);
        }
    }

    public void Disconnect() {
        foreach (Port p in connectedPorts) {
            p.connectedPorts.Remove(this);
        }

        connectedPorts.Clear();
    }

    protected GUIStyle GetPortStyle(StyleSet _styleSet) {
        if (Event.current.type == EventType.Repaint) {
            if (direction == Direction.IN) {
                return GetPortStyleIn(_styleSet);
            } else {
                return GetPortStyleOut(_styleSet);
            }
        }

        return _styleSet.portInExecution;
    }

    private GUIStyle GetPortStyleIn(StyleSet _styleSet) {
        switch (portType) {
            case NodeDataType.Dynamic:
                return _styleSet.portInDynamic;
            case NodeDataType.Bool:
                return _styleSet.portInBool;
            case NodeDataType.Int:
                return _styleSet.portInInt;
            default:
                return _styleSet.portInExecution;
        }
    }

    private GUIStyle GetPortStyleOut(StyleSet _styleSet) {
        switch (portType) {
            case NodeDataType.Dynamic:
                return _styleSet.portOutDynamic;
            case NodeDataType.Bool:
                return _styleSet.portOutBool;
            case NodeDataType.Int:
                return _styleSet.portOutInt;
            default:
                return _styleSet.portOutExecution;
        }
    }

    public virtual dynamic GetValue() {
        return null;
    }

    public virtual void SetValue(dynamic _value) {
    }
}
