﻿using UnityEngine;

public class NodeComparison : Node {
    public enum ComparisonType {
        EqualTo
    ,   NotEqualTo
    ,   GreaterThan
    ,   GreaterThanOrEqualTo
    ,   LessThan
    ,   LessThanOrEqualTo
    }

    public ComparisonType comparisonType;

    private dynamic copyA;
    private dynamic copyB;
    private dynamic copyC;

    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(170, 75);
        name = "Comparison";

        AddPortIn<PortInt>("A");
        AddPortIn<PortInt>("B");
        AddPortOut<PortBool>("Out", Port.Capacity.MULTIPLE);
        AddPortOut<PortComparison>();

        portContainerIn.ports[0].SetValue(copyA);
        portContainerIn.ports[1].SetValue(copyB);
        portContainerOut.ports[1].SetValue(copyC);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        comparisonType = (ComparisonType)portContainerOut.ports[1].GetValue();

        ScaleForConnectedInputs(110, 170);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();

        switch (comparisonType) {
            case ComparisonType.EqualTo:
                return a == b;
            case ComparisonType.NotEqualTo:
                return a != b;
            case ComparisonType.GreaterThan:
                return a > b;
            case ComparisonType.GreaterThanOrEqualTo:
                return a >= b;
            case ComparisonType.LessThan:
                return a < b;
            case ComparisonType.LessThanOrEqualTo:
                return a >= b;
            default:
                break;
        }

        return true;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeComparison>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeComparison)newNode).copyA = portContainerIn.ports[0].GetValue();
        ((NodeComparison)newNode).copyB = portContainerIn.ports[1].GetValue();
        ((NodeComparison)newNode).copyC = portContainerOut.ports[1].GetValue();

        return newNode;
    }
}