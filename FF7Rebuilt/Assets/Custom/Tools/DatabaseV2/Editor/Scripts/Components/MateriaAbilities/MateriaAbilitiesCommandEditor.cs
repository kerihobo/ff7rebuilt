using FFVII.Database;
using InventoryTypes;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

public class MateriaAbilitiesCommandEditor : MateriaAbilitiesEditor {
    public MateriaAbilitiesCommandEditor(Materia _target, Database _database, EditorMateria _editor) : base(_target, _database) {
        editor = _editor;
    }

    protected override string UXMLPath { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/MateriaAbilities/materia-abilities-command.uxml";

    private EditorMateria editor;

    protected override bool IsDisplaySelections =>
        target.IsMaster
    ||  target.IsEnemySkill
    ||  target.IsWMagic
    ||  target.IsWSummon
    ||  target.IsWItem
    ;

    public override void BindAbilities() {
        //this.Q<Toggle>("tgl-enemy-skill").SetValue(target.IsEnemySkill).RegisterValueChangedCallback(e => {
        //    target.IsEnemySkill = e.newValue;
        //    EditorUtility.SetDirty(database);

        //    RefreshToggleEnemySkill();
        //});

        DropdownField ddfSubtype = this.Q<DropdownField>("drp-subtype");
        ddfSubtype.choices = CommandSubtype.COMMAND_SUBTYPE_NAMES.ToList();
        ddfSubtype.index = target.subtype;
        ddfSubtype.RegisterValueChangedCallback(_e => {
            target.subtype = ddfSubtype.index;

            RefreshSelectionDisplays();

            EditorUtility.SetDirty(database);
        });

        //for (int i = 0; i < target.commandManager.commands.Length; i++) {
        //    int j = i;
        //    this.Q<EnumField>($"enum-ability-{j}").SetCommandIndex(target.commandManager.commands[j]).RegisterValueChangedCallback(e => {
        //        target.commandManager.commands[j] = (CommandManager.CommandIndex)e.newValue;
        //        EditorUtility.SetDirty(database);
        //    });
        //}

        for (int i = 0; i < target.levels - 1; i++) {
            int j = i;
            DropdownField ddf = this.Q<DropdownField>($"drp-command-{j}");
            ddf.Initialize(DBResources.GetAbilityList(AbilityType.Command), target.selectionSpells[j], database);
            ddf.RegisterValueChangedCallback(_e => {
                target.selectionSpells[j].SetDataExternally(ddf.index);
                EditorUtility.SetDirty(database);
            });
        }
    }

    public override void OnLevelsChanged() {
        List<DropdownField> ddfF = this.Query<VisualElement>("ve-ddf").Children<DropdownField>().ToList();
        ddfF.ForEach(x => x.style.display = DisplayStyle.None);

        for (int i = 0; i < target.levels; i++) {
            if (i - 1 < 0) continue;

            ddfF[i - 1].style.display = DisplayStyle.Flex;
        }
    }

    public override void Refresh(Materia _target, VisualElement _ve) {
        target = _target;
        veParent = _ve;

        this.Q<DropdownField>("drp-subtype").index = target.subtype;

        RefreshSelectionDisplays();

        for (int i = 0; i < target.levels - 1; i++) {
            int j = i;
            //this.Q<EnumField>($"enum-ability-{j}").value = target.commandManager.commands[j];
            this.Q<DropdownField>($"drp-command-{j}").Initialize(DBResources.GetAbilityList(AbilityType.Command), target.selectionSpells[j], database);
        }

        RefreshToggleEnemySkill();
    }

    void RefreshToggleEnemySkill() {
        this.Q<VisualElement>("ve-ddf").style.display = target.IsEnemySkill ? DisplayStyle.None : DisplayStyle.Flex;
    }
}
