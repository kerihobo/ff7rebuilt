using CharacterTypes;
using InventoryTypes;
using System.Linq;
using TMPro;
using UnityEngine;

public class MateriaMenuCheckSummonInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    /// <summary>
    /// If a match is found on the player then we reference that.
    /// If a match is only found thanks to Master Summon, we reference the database version.
    /// </summary>
    public Materia materiaReference { get; set; }

    private TextMeshProUGUI txtName;
    private bool isMasterSummon;

    public void Initialize() {
        txtName = GetComponent<TextMeshProUGUI>();
        rct = GetComponent<RectTransform>();
    }

    public void Refresh(Player _player, Materia _materia) {
        Materia foundMateria = GetEquippedMateria(_player, _materia);

        isMasterSummon = _player.Materia.Any(x => x.type == MateriaType.Summon && x.IsMaster);

        if (isMasterSummon) {
            materiaReference = _materia;
            txtName.text = _materia.name;

            return;
        }
        
        materiaReference = foundMateria;
        txtName.text = foundMateria?.name;
    }

    public Materia GetEquippedMateria(Player _player, Materia _materia) {
        if (_materia == null) return null;

        return _player.Materia.FirstOrDefault(x => x.name == _materia.name);
    }

    public void Clear() {
        isMasterSummon = false;
        materiaReference = null;
        txtName.text = "";
    }
}
