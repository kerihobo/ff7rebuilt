Shader "Custom/VertexColorLit" {
	Properties
	{
		_GlobalLight("Global Light", Color) = (0.3411765, 0.3294118, 0.3333333, 0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" "Queue"="Geometry" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Custom vertex:vert noambient noforwardadd
		#pragma target 3.0

		struct Input {
			float4 vertColor;
		};

		fixed4 _GlobalLight;

		float4 LightingCustom(SurfaceOutput s, float3 lightDir, float atten) {
			float n = saturate(dot(s.Normal, lightDir));
			float amount = .5;
			n = n * amount + amount;

			float4 col;
			col.rgb = saturate((s.Albedo) * atten * (_LightColor0.rgb + _GlobalLight) * n);
			col.a = s.Alpha;

			return col;
		}

		void vert(inout appdata_full v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = v.color;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			// o.Albedo = pow(IN.vertColor.rgb, 1 / 2.2);
			o.Albedo = IN.vertColor.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}