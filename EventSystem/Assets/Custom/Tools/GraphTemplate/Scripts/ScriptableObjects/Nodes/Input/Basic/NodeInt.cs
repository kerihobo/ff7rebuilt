﻿using UnityEngine;

public class NodeInt : Node {
    private dynamic copyA;
    
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(165, 55);
        name = "Int";

        AddPortIn<PortInt>("In", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_FIELD);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);

        portContainerIn.ports[0].SetValue(copyA);
    }

    public override dynamic GetValue() {
        return portContainerIn.ports[0].GetValue();
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeInt>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeInt)newNode).copyA = portContainerIn.ports[0].GetValue();

        return newNode;
    }
}