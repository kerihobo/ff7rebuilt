using FFVII.Database;
using InventoryTypes;
using UnityEditor;
using UnityEngine.UIElements;

public abstract class MateriaAbilitiesEditor : VisualElement {
    protected Materia target;
    protected Database database;
    protected TemplateContainer tc;

    protected VisualElement veParent;

    protected virtual string UXMLPath {get;set;} = "";

    private string Name { get; set; } = "materia-abilities";

    public abstract void BindAbilities();
    public abstract void Refresh(Materia _target, VisualElement ve);

    public virtual void OnLevelsChanged() { }

    protected virtual bool IsDisplaySelections => target.IsMaster;

    //public virtual void OnIndependentTypeChanged() { }

    public MateriaAbilitiesEditor(Materia _target, Database _database) {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        tc.name = Name;

        target = _target;
        database = _database;

        OnLevelsChanged();
        //OnIndependentTypeChanged();
    }

    protected void RefreshSelectionDisplays() {
        VisualElement veLevels = veParent.Q<VisualElement>("ve-levels");
        veLevels.style.display = IsDisplaySelections ? DisplayStyle.None : DisplayStyle.Flex;

        VisualElement veDDF = this.Q<VisualElement>("ve-ddf");
        veDDF.style.display = IsDisplaySelections ? DisplayStyle.None : DisplayStyle.Flex;
    }
}
