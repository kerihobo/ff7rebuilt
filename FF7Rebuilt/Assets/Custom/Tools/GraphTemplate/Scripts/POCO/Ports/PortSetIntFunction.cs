﻿using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class PortSetIntFunction : Port {
    public PortSetIntFunction(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) : base(_parentNode, _portName, _direction, _capacity, _displayMode) {
    }
    
    [SerializeField]
    private string[] values = {
        "="
    ,   "++"
    ,   "--"
    };

    public NodeSetInt.Function value;

    protected override void RenderAsOutput(StyleSet _styleSet) {
        value = (NodeSetInt.Function)EditorGUILayout.Popup((int)value, values, GUILayout.Width(40));
    }

    public override void SetValue(dynamic _value) {
        value = _value is int ? (NodeSetInt.Function)_value : NodeSetInt.Function.Equal;
    }

    public override dynamic GetValue() {
        return (int)value;
    }
}
