using FFVII.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

public class EditorFormations : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Characters/editor-formations.uxml";
    protected override Database database { get; set; } = DBResources.GetFormations;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetFormationsList(FormationType.Normal)[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetFormationsList(FormationType.Normal);

    private FormationLsPlacements formationLsEnemies;

    public EditorFormations() : base() {
        SetSelected(ve.Q<Button>("btn-type-select-normal"));
    }

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Formation)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        BindTypeSelectionButtons();

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            
            listView.RefreshItems();
        });

        ve.Q<IntegerField>("int-id").SetValue(((Formation)target).id).RegisterValueChangedCallback(e => {
            ((Formation)target).id = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<IntegerField>("int-probability").SetValue(((Formation)target).probability).RegisterValueChangedCallback(e => {
            ((Formation)target).probability = e.newValue;
            EditorUtility.SetDirty(database);
        });

        formationLsEnemies = ve.Q<FormationLsPlacements>().Initialize((Formation)target, database, ve.Q<VisualElement>("field-2d"));
        ve.Q<Button>("btn-add-enemy").clicked += formationLsEnemies.Add;

        void BindTypeSelectionButtons() {
            ve.Q<Button>("btn-type-select-normal").RegisterCallback<ClickEvent>((e) => ChangeCategory(FormationType.Normal, e));
            ve.Q<Button>("btn-type-select-special").RegisterCallback<ClickEvent>((e) => ChangeCategory(FormationType.Special, e));
            ve.Q<Button>("btn-type-select-chocobo").RegisterCallback<ClickEvent>((e) => ChangeCategory(FormationType.Chocobo, e));

            void ChangeCategory(FormationType _formationType, ClickEvent e) {
                RenewList();
                SetSelected((Button)e.currentTarget);

                void RenewList() {
                    itemSource = DBResources.GetFormationsList(_formationType);
                    bindItem = (e, i) => e.Q<Label>().text = DBResources.GetFormationsList(_formationType)[i].name;

                    listView.bindItem = null;
                    listView.itemsSource = itemSource;
                    listView.bindItem = bindItem;
                    listView.SetSelection(0);
                }
            }
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = target.name;
        ve.Q<IntegerField>("int-id").value = ((Formation)target).id;
        ve.Q<IntegerField>("int-probability").value = ((Formation)target).probability;

        formationLsEnemies?.Refresh((Formation)target);
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Formation());

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new Formation());

        base.Add();
    }

    void SetSelected(VisualElement _ve) {
        VisualElement typeList = ve.Q<VisualElement>("type-list");
        typeList.Query<Button>().ForEach(x => x.RemoveFromClassList("selected"));
        _ve.AddToClassList("selected");
    }
}
