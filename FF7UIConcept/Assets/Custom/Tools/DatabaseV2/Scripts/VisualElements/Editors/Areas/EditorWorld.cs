using UnityEngine.UIElements;

public class EditorWorld : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Areas/editor-world.uxml";
    protected override string Name { get; set; } = "editor-world";

    public EditorWorld() : base() {
    }
}
