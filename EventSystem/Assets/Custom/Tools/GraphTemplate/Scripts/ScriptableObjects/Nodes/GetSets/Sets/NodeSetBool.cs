﻿using UnityEngine;

public class NodeSetBool : NodeSet {
    public enum Function {
        Set
    ,   Toggle
    }

    public Function function;

    private dynamic copyA;
    private dynamic copyB;

    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        minMaxThresholds = new Vector2(155, 175);
        size.y += 20;

        AddPortIn<PortBool>("Value", Port.Capacity.SINGLE);
        AddPortOut<PortSetBoolFunction>();
        
        portContainerIn.ports[1].SetValue(copyA);
        portContainerOut.ports[1].SetValue(copyB);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        function = (Function)portContainerOut.ports[1].GetValue();
        portContainerIn.ports[1].IsActive = function == Function.Set;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeSetBool>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeSetBool)newNode).copyA = portContainerIn.ports[1].GetValue();
        ((NodeSetBool)newNode).copyB = portContainerOut.ports[1].GetValue();

        ((NodeSetBool)newNode).field = field;

        field.nodes.Add((NodeSetBool)newNode);

        return newNode;
    }

    public override void Run(NodeGraph _nodeGraph) {
        ((FieldBool)field).value = portContainerIn.ports[1].GetValue();
    }
}
