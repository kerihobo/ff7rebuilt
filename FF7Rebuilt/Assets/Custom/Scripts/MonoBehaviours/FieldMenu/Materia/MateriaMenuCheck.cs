using CharacterTypes;
using InventoryTypes;
using TMPro;
using UnityEngine;

public class MateriaMenuCheck : MonoBehaviour {
    public MateriaMenuCheckInfoWindow infoWindow;
    
    [SerializeField] private MateriaMenuCheckCommand commandWindow;
    [SerializeField] private TextMeshProUGUI txtDescription;
    [SerializeField] private MateriaMenuCheckMagicList menuMagic;
    [SerializeField] private MateriaMenuCheckSummonList menuSummon;
    [SerializeField] private MateriaMenuCheckEnemySkillList menuEnemySkill;
    private ScreenFieldMateria screenFieldMateria;
    private MateriaMenuOptionList optionList;
    private Player assignedPlayer;

    public Player GetSelectedCharacter => screenFieldMateria.GetSelectedCharacter;

    public void Initialize(ScreenFieldMateria _screenFieldMateria, MateriaMenuOptionList _optionList) {
        screenFieldMateria = _screenFieldMateria;
        optionList = _optionList;

        commandWindow.Initialize(this);

        //menuMagic.Initialize();
        menuMagic.Initialize(this, commandWindow);
        menuSummon.Initialize(this, commandWindow);
        menuEnemySkill.Initialize(this, commandWindow);
    }

    public void Refresh(Player _player) {
        assignedPlayer = _player;
        commandWindow.Refresh(assignedPlayer);
    }

    public void Open() {
        commandWindow.Open();
    }

    public void Close() {
        optionList.Open();
    }

    public void Show() {
        menuSummon.Disable();
        menuMagic.Disable();
        menuEnemySkill.Disable();
        screenFieldMateria.exchange.Hide();
        screenFieldMateria.ToggleMateriaView(false);
        infoWindow.Hide();
        gameObject.SetActive(true);
        SetDescription("");
        
        Refresh(screenFieldMateria.GetSelectedCharacter);
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void SetDescription(string _description) {
        txtDescription.text = _description;
    }

    public void OpenWindowMagic() {
        infoWindow.Show(GetSelectedCharacter);
        menuMagic.Open();
    }

    public void SetInfoMagic(Ability _magic) {
        infoWindow.SetInfoMagic(_magic);
    }

    public void OpenWindowSummon() {
        infoWindow.Show(GetSelectedCharacter);
        menuSummon.Open();
    }

    public void SetInfoSummon(Materia materiaReference, bool isMasterSummon) {
        infoWindow.SetInfoSummon(materiaReference, isMasterSummon);
    }

    public void OpenWindowEnemySkill() {
        infoWindow.Show(GetSelectedCharacter);
        menuEnemySkill.Open();
    }

    public void SetInfoEnemySkill(Ability _enemySkill) {
        infoWindow.SetInfoEnemySkill(_enemySkill);
    }
    
    public void CloseInfoWindow() {
        infoWindow.Hide();
    }
}
