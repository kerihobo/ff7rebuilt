﻿using UnityEngine;

public class NodeBool : Node {
    public bool isTrue = true;

    private bool copyA;

    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(130, 50);
        name = "Bool";

        AddPortIn<PortBool>("In", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_FIELD);
        AddPortOut<PortBool>("Out", Port.Capacity.MULTIPLE);
        
        portContainerIn.ports[0].SetValue(copyA);
    }

    public override dynamic GetValue() {
        return portContainerIn.ports[0].GetValue();
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeBool>(_nodeGraph, position + duplicationOffset, category, false);
        Debug.Log(portContainerIn.ports[0].GetValue());
        ((NodeBool)newNode).copyA = portContainerIn.ports[0].GetValue();

        return newNode;
    }
}