﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Data {
    public Data() {
    }

    public Data(Data _source) {
        name = _source.name;
        id = _source.id;
    }

    public string name;
    public int id;

    public static void ConvertDBListToNamesArray<T>(List<T> _list, ref List<string> _names) where T : Data {
        if (_list == null) {
            return;
        }

        _names = _list.Select(x => x.name).ToList();
        _names.Insert(0, "-");
    }

    /// <summary>
    /// Useful for names that have '/' in them like "T/S Bomb" which Unity's dropdowns will read as a sub-menu.
    /// </summary>
    /// <returns></returns>
    public string GetNonPathName() {
        return name.Replace("/", "_");
    }
}
