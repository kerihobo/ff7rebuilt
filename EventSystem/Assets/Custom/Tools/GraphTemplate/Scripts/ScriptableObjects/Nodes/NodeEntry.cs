﻿using UnityEngine;

public class NodeEntry : Node {
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(65, 50);
        name = "Entry";
     
        AddPortOut<Port>();
    }

    public override void Run(NodeGraph _nodeGraph) {
        Port port = portContainerOut.ports[0];

        if (port.connectedPorts.Count > 0) {
            port.connectedPorts[0].parentNode.Run(_nodeGraph);
        }
    }
}
