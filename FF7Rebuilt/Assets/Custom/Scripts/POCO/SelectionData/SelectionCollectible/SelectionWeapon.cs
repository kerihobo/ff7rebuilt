﻿using System;
using System.Collections.Generic;
using FFVII.Database;
using InventoryTypes;

[Serializable]
public class SelectionWeapon : SelectionData {
    public WeaponType type = WeaponType.SWORD;

    public override string Reset<T>(List<T> _list) {
        return base.Reset(DBResources.GetWeaponsList(type));
    }

    public Weapon GetSelectedWeapon() {
        return index >= 0 && DBResources.GetWeaponsList(type).Count > index ? DBResources.GetWeaponsList(type)[index] : null;
    }
}
