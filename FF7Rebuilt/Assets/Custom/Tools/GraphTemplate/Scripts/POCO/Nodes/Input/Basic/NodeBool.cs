﻿using System;
using UnityEngine;

[Serializable]
public class NodeBool : Node {
    public NodeBool(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(130, 50);
        name = "Bool";

        AddPortIn<PortBool>("In", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_FIELD);
        AddPortOut<PortBool>("Out", Port.Capacity.MULTIPLE);
    }

    public bool isTrue = true;

    public override dynamic GetValue() {
        return portContainerIn.ports[0].GetValue();
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = new NodeBool(_nodeGraph, position + duplicationOffset, category, false);
        Debug.Log(portContainerIn.ports[0].GetValue());
        ((NodeBool)newNode).portContainerIn.ports[0].SetValue(portContainerIn.ports[0].GetValue());

        return newNode;
    }
}