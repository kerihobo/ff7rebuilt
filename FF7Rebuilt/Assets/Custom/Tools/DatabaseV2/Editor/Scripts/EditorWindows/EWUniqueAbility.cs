using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using FFVII.Database;
using CharacterTypes;
using System.Linq;
using System.Collections.Generic;

public class EWUniqueAbility : EditorWindow {    
    private const int SECTION_WIDTH = 450;
    private const int SECTION_MARGIN_WIDTH = 5;
    private const int HEIGHT = 600;
    private const int TOTAL_WIDTH = SECTION_WIDTH + SECTION_MARGIN_WIDTH + SECTION_WIDTH;
    private const string Path = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Windows/ew-editor-abilities.uxml";

    public static EWUniqueAbility Instance;

    private (Button, ElementalAffinityManager.Affinity)[] arrPairElementButtons;
    private (Button, StatusEffectManager.StatusEffect)[] arrPairStatusResistanceButtons;
    private VisualElement root;
    private VisualElement ve;
    private Database database;
    private Enemy.UniqueAbiity target;
    private Label referenceLabel;

    public static void OpenWindow() {
        Instance = GetWindow<EWUniqueAbility>();
        Instance.titleContent = new GUIContent("Unique Ability Editor");
        
        Instance.minSize = new Vector2(TOTAL_WIDTH, HEIGHT);
        Instance.maxSize = new Vector2(TOTAL_WIDTH, HEIGHT);
    }

    public void CreateGUI() {
        Instance = this;

        root = rootVisualElement;
        
        ve = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path).Instantiate();
        root.Add(ve);
    }

    //private void OnLostFocus() {
    //    Close();
    //}

    private void OnLostFocus() {
        // TODO: HACK - ListView's scroll view steals focus using the scheduler.
        EditorApplication.update += HackDueToCloseOnLostFocusCrashing;

        // See: https://fogbugz.unity3d.com/f/cases/1004504/
        void HackDueToCloseOnLostFocusCrashing() {
            Close();

            EditorApplication.update -= HackDueToCloseOnLostFocusCrashing;
        }
    }

    public void Initialize(Enemy.UniqueAbiity _target, Label _label, Database _database) {
        target = _target;
        referenceLabel = _label;
        database = _database;
        Debug.Log(_database.name);

        Refresh();
        BindUniqueControls();
    }

    private void BindUniqueControls() {
        Debug.Log(target.ability);

        root.Q<TextField>("txt-name").SetValue(target.ability.name).RegisterValueChangedCallback(e => {
            target.ability.name = e.newValue;
            EditorUtility.SetDirty(database);
            referenceLabel.text = target.ability.name;
        });

        root.Q<TextField>("txt-description").SetValue(target.ability.description).RegisterValueChangedCallback(e => {
            target.ability.description = e.newValue;
            EditorUtility.SetDirty(database);
        });

        root.Q<ObjectField>("go-prefab-effect").SetGameObject(target.ability.prefabEffect).RegisterValueChangedCallback(e => {
            target.ability.prefabEffect = (GameObject)e.newValue;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfSpecialEffect = root.Q<DropdownField>("drp-special-effect");
        ddfSpecialEffect.choices = SpecialEffectManager.SPECIAL_EFFECT_NAMES.ToList();
        ddfSpecialEffect.index = target.ability.specialEffect;
        ddfSpecialEffect.RegisterValueChangedCallback(_e => {
            target.ability.specialEffect = ddfSpecialEffect.index;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfDamageType = root.Q<DropdownField>("drp-damage-type");
        ddfDamageType.choices = DamageType.DAMAGE_TYPE_NAMES.ToList();
        ddfDamageType.index = target.ability.accuracyCalculation;
        ddfDamageType.RegisterValueChangedCallback(_e => {
            target.ability.accuracyCalculation = ddfDamageType.index;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfDamageFormula = root.Q<DropdownField>("drp-damage-formula");
        ddfDamageFormula.choices = DamageFormula.DAMAGE_FORMULA_NAMES.ToList();
        ddfDamageFormula.index = target.ability.damageFormula;
        ddfDamageFormula.RegisterValueChangedCallback(_e => {
            target.ability.damageFormula = ddfDamageFormula.index;
            EditorUtility.SetDirty(database);
        });

        ve.Q<EnumField>("enum-restore-type").SetRestoreType((RestoreType)target.ability.restoreType).RegisterValueChangedCallback(e => {
            target.ability.restoreType = (int)(RestoreType)e.newValue;
            EditorUtility.SetDirty(database);
        });

        BindSpellStats();
        BindTargetOptions();
        BindExecutionOptions();
        BindAttackElements();
        BindAfflictions();

        void BindSpellStats() {
            root.Q<IntegerField>("int-power").SetValue(target.ability.power).RegisterValueChangedCallback(e => {
                target.ability.power = e.newValue;
                EditorUtility.SetDirty(database);
            });

            root.Q<IntegerField>("int-attack-percent").SetValue(target.ability.attackPercent).RegisterValueChangedCallback(e => {
                target.ability.attackPercent = e.newValue;
                EditorUtility.SetDirty(database);
            });

            root.Q<IntegerField>("int-mp-cost").SetValue(target.ability.mpCost).RegisterValueChangedCallback(e => {
                target.ability.mpCost = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindTargetOptions() {
            VisualElement targetOptions = root.Q<VisualElement>("ve-target-options");

            targetOptions.Q<Button>("btn-enable-selection").SetBoolClass(target.ability.targetOptions.isEnableSelection).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isEnableSelection);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-start-on-enemies").SetBoolClass(target.ability.targetOptions.isStartOnEnemies).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isStartOnEnemies);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-multiple-by-default").SetBoolClass(target.ability.targetOptions.isMultipleTargetsByDefault).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isMultipleTargetsByDefault);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-toggle-single-multiple-targets").SetBoolClass(target.ability.targetOptions.isToggleSingleMultipleTargets).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isToggleSingleMultipleTargets);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-one-row-only").SetBoolClass(target.ability.targetOptions.isOneRowOnly).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isOneRowOnly);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-short-range").SetBoolClass(target.ability.targetOptions.isShortRange).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isShortRange);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-all-rows").SetBoolClass(target.ability.targetOptions.isAllRows).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isAllRows);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-random-target").SetBoolClass(target.ability.targetOptions.isRandomTarget).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isRandomTarget);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-fallen").SetBoolClass(target.ability.targetOptions.isFallen).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.targetOptions.isFallen);
                EditorUtility.SetDirty(database);
            });
        }

        void BindExecutionOptions() {
            VisualElement executionOptions = root.Q<VisualElement>("ve-execution-options");

            executionOptions.Q<Button>("btn-affects-mp").SetBoolClass(target.ability.executionProperties.isAffectsMP).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isAffectsMP);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-drains-some-damage").SetBoolClass(target.ability.executionProperties.isDrainsSomeDamage).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isDrainsSomeDamage);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-ignores-status-defense").SetBoolClass(target.ability.executionProperties.isIgnoreStatusDefense).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isIgnoreStatusDefense);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-ignores-defense").SetBoolClass(target.ability.executionProperties.isIgnoreDefense).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isIgnoreDefense);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-drains-hp-and-mp").SetBoolClass(target.ability.executionProperties.isDrainHPAndMP).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isDrainHPAndMP);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-miss-if-alive").SetBoolClass(target.ability.executionProperties.isMissIfAlive).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isMissIfAlive);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-no-retarget-if-dead").SetBoolClass(target.ability.executionProperties.isNoRetargetIfDead).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isNoRetargetIfDead);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-affected-by-darkness").SetBoolClass(target.ability.executionProperties.isAffectedByDarkness).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isAffectedByDarkness);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-reflectable").SetBoolClass(target.ability.executionProperties.isReflectable).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isReflectable);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-always-critical-hit").SetBoolClass(target.ability.executionProperties.isAlwaysCriticalHit).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref target.ability.executionProperties.isAlwaysCriticalHit);
                EditorUtility.SetDirty(database);
            });
        }

        void BindAttackElements() {
            VisualElement elementalAffinities = root.Q<VisualElement>("ve-elemental-affinities");

            RestockButtonArrayElements();

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                int j = i;

                arrPairElementButtons[j].Item1.SetBoolClass(arrPairElementButtons[j].Item2.isActive).RegisterCallback<ClickEvent>(e => {
                    ((VisualElement)e.currentTarget).FlipBool(ref arrPairElementButtons[j].Item2.isActive);
                    EditorUtility.SetDirty(database);
                });
            }
        }

        void BindAfflictions() {
            VisualElement statusEffects = root.Q<VisualElement>("ve-afflictions");

            ve.Q<EnumField>("enum-status-effect-action").SetStatusEffectAction(target.ability.statusEffectManager.statusEffectAction).RegisterValueChangedCallback(e => {
                target.ability.statusEffectManager.statusEffectAction = (StatusEffectManager.StatusEffectAction)e.newValue;

                RefreshStatusResistances();
                EditorUtility.SetDirty(database);
            });

            IntegerField intFChance = statusEffects.Q<IntegerField>("int-chance");
            intFChance.SetValue(target.ability.statusEffectManager.chance).RegisterValueChangedCallback(e => {
                if (e.newValue < 0) intFChance.value = 0;
                if (e.newValue > 64) intFChance.value = 64;

                target.ability.statusEffectManager.chance = intFChance.value;
                EditorUtility.SetDirty(database);

                statusEffects.Q<Label>("lbl-chance").text = $"{(int)((intFChance.value / 64f) * 256)}%";
            });

            RestockButtonArrayStatusResistance();

            for (int i = 0; i < arrPairStatusResistanceButtons.Length; i++) {
                int j = i;
                arrPairStatusResistanceButtons[j].Item1.RegisterCallback<ClickEvent>(e => {
                    arrPairStatusResistanceButtons[j].Item1.ToggleAfflictionBehaviour(ref arrPairStatusResistanceButtons[j].Item2, target.ability.statusEffectManager.statusEffectAction);
                    EditorUtility.SetDirty(database);
                });
            }
        }
    }

    protected void Refresh() {
        if (target == null) return;

        ve.Q<TextField>("txt-name").value = target.ability.name;
        ve.Q<TextField>("txt-description").value = target.ability.description;
        ve.Q<ObjectField>("go-prefab-effect").value = target.ability.prefabEffect;
        ve.Q<DropdownField>("drp-special-effect").index = target.ability.specialEffect;
        ve.Q<DropdownField>("drp-damage-type").index = target.ability.accuracyCalculation;
        ve.Q<DropdownField>("drp-damage-formula").index = target.ability.damageFormula;
        ve.Q<EnumField>("enum-restore-type").value = (RestoreType)target.ability.restoreType;

        RefreshTargetOptions();
        RefreshExecutionOptions();
        RefreshSpellStats();
        RefreshAttackElements();
        RefreshStatusResistances();

        void RefreshTargetOptions() {
            VisualElement targetOptions = ve.Q<VisualElement>("ve-target-options");

            targetOptions.Q<Button>("btn-enable-selection").SetBoolClass(target.ability.targetOptions.isEnableSelection);
            targetOptions.Q<Button>("btn-start-on-enemies").SetBoolClass(target.ability.targetOptions.isStartOnEnemies);
            targetOptions.Q<Button>("btn-multiple-by-default").SetBoolClass(target.ability.targetOptions.isMultipleTargetsByDefault);
            targetOptions.Q<Button>("btn-toggle-single-multiple-targets").SetBoolClass(target.ability.targetOptions.isToggleSingleMultipleTargets);
            targetOptions.Q<Button>("btn-one-row-only").SetBoolClass(target.ability.targetOptions.isOneRowOnly);
            targetOptions.Q<Button>("btn-short-range").SetBoolClass(target.ability.targetOptions.isShortRange);
            targetOptions.Q<Button>("btn-all-rows").SetBoolClass(target.ability.targetOptions.isAllRows);
            targetOptions.Q<Button>("btn-random-target").SetBoolClass(target.ability.targetOptions.isRandomTarget);
            targetOptions.Q<Button>("btn-fallen").SetBoolClass(target.ability.targetOptions.isFallen);
        }

        void RefreshExecutionOptions() {
            VisualElement executionOptions = ve.Q<VisualElement>("ve-execution-options");

            executionOptions.Q<Button>("btn-affects-mp").SetBoolClass(target.ability.executionProperties.isAffectsMP);
            executionOptions.Q<Button>("btn-drains-some-damage").SetBoolClass(target.ability.executionProperties.isDrainsSomeDamage);
            executionOptions.Q<Button>("btn-ignores-status-defense").SetBoolClass(target.ability.executionProperties.isIgnoreStatusDefense);
            executionOptions.Q<Button>("btn-ignores-defense").SetBoolClass(target.ability.executionProperties.isIgnoreDefense);
            executionOptions.Q<Button>("btn-drains-hp-and-mp").SetBoolClass(target.ability.executionProperties.isDrainHPAndMP);
            executionOptions.Q<Button>("btn-miss-if-alive").SetBoolClass(target.ability.executionProperties.isMissIfAlive);
            executionOptions.Q<Button>("btn-no-retarget-if-dead").SetBoolClass(target.ability.executionProperties.isNoRetargetIfDead);
            executionOptions.Q<Button>("btn-affected-by-darkness").SetBoolClass(target.ability.executionProperties.isAffectedByDarkness);
            executionOptions.Q<Button>("btn-reflectable").SetBoolClass(target.ability.executionProperties.isReflectable);
            executionOptions.Q<Button>("btn-always-critical-hit").SetBoolClass(target.ability.executionProperties.isAlwaysCriticalHit);
        }

        void RefreshSpellStats() {
            VisualElement spellStats = ve.Q<VisualElement>("ve-spell-stats");

            spellStats.Q<IntegerField>("int-power").value = target.ability.power;
            spellStats.Q<IntegerField>("int-attack-percent").value = target.ability.attackPercent;
            spellStats.Q<IntegerField>("int-mp-cost").value = target.ability.mpCost;
        }

        void RefreshAttackElements() {
            VisualElement elementalAffinities = ve.Q<VisualElement>("ve-elemental-affinities");

            RestockButtonArrayElements();

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                arrPairElementButtons[i].Item1.SetBoolClass(arrPairElementButtons[i].Item2.isActive);
            }
        }
    }

    void RefreshStatusResistances() {
        VisualElement statusEffects = ve.Q<VisualElement>("ve-afflictions");

        ve.Q<EnumField>("enum-status-effect-action").value = target.ability.statusEffectManager.statusEffectAction;

        IntegerField intFChance = statusEffects.Q<IntegerField>("int-chance");
        StatusEffectManager statusEffectManager = target.ability.statusEffectManager;
        intFChance.value = statusEffectManager.chance;
        statusEffects.Q<Label>("lbl-chance").text = $"{(int)((intFChance.value / 64f) * 256)}%";

        RestockButtonArrayStatusResistance();

        for (int i = 0; i < arrPairStatusResistanceButtons.Length; i++) {
            arrPairStatusResistanceButtons[i].Item1.SetAfflictionBehaviourClass(arrPairStatusResistanceButtons[i].Item2.isActive, target.ability.statusEffectManager.statusEffectAction);
        }
    }

    void RestockButtonArrayElements() {
        GroupBox elementalAffinities = ve.Q<GroupBox>("grp-elemental-affinities");
        List<Button> lsButtons = elementalAffinities.Query<Button>().ToList();
        Debug.Log(lsButtons.Count);

        arrPairElementButtons = new (Button, ElementalAffinityManager.Affinity)[] {
                (lsButtons[0],  target.ability.elements.fire)
            ,   (lsButtons[1],  target.ability.elements.ice)
            ,   (lsButtons[2],  target.ability.elements.lightning)
            ,   (lsButtons[3],  target.ability.elements.earth)
            ,   (lsButtons[4],  target.ability.elements.poison)
            ,   (lsButtons[5],  target.ability.elements.gravity)
            ,   (lsButtons[6],  target.ability.elements.water)
            ,   (lsButtons[7],  target.ability.elements.wind)
            ,   (lsButtons[8],  target.ability.elements.holy)
            ,   (lsButtons[9],  target.ability.elements._10th)
            ,   (lsButtons[10], target.ability.elements.restorative)
            ,   (lsButtons[11], target.ability.elements.punch)
            ,   (lsButtons[12], target.ability.elements.cut)
            ,   (lsButtons[13], target.ability.elements.hit)
            ,   (lsButtons[14], target.ability.elements.shoot)
            ,   (lsButtons[15], target.ability.elements.shout)
            ,   (lsButtons[16], target.ability.elements.death)
            ,   (lsButtons[17], target.ability.elements.sleep)
            ,   (lsButtons[18], target.ability.elements.confusion)
            };
    }

    void RestockButtonArrayStatusResistance() {
        GroupBox statusEffects = ve.Q<GroupBox>("grp-afflictions");
        List<Button> lsButtons = statusEffects.Query<Button>().ToList();

        arrPairStatusResistanceButtons = new (Button, StatusEffectManager.StatusEffect)[] {
                (lsButtons[0],  target.ability.statusEffectManager.death)
            ,   (lsButtons[1],  target.ability.statusEffectManager.sleep)
            ,   (lsButtons[2],  target.ability.statusEffectManager.poison)
            ,   (lsButtons[3],  target.ability.statusEffectManager.sadness)
            ,   (lsButtons[4],  target.ability.statusEffectManager.fury)
            ,   (lsButtons[5],  target.ability.statusEffectManager.confusion)
            ,   (lsButtons[6],  target.ability.statusEffectManager.silence)
            ,   (lsButtons[7],  target.ability.statusEffectManager.haste)
            ,   (lsButtons[8],  target.ability.statusEffectManager.slow)
            ,   (lsButtons[9],  target.ability.statusEffectManager.stop)
            ,   (lsButtons[10], target.ability.statusEffectManager.frog)
            ,   (lsButtons[11], target.ability.statusEffectManager.small)
            ,   (lsButtons[12], target.ability.statusEffectManager.slowNumb)
            ,   (lsButtons[13], target.ability.statusEffectManager.petrify)
            ,   (lsButtons[14], target.ability.statusEffectManager.regen)
            ,   (lsButtons[15], target.ability.statusEffectManager.barrier)
            ,   (lsButtons[16], target.ability.statusEffectManager.mBarrier)
            ,   (lsButtons[17], target.ability.statusEffectManager.reflect)
            ,   (lsButtons[18], target.ability.statusEffectManager.shield)
            ,   (lsButtons[19], target.ability.statusEffectManager.deathSentence)
            ,   (lsButtons[20], target.ability.statusEffectManager.berserk)
            ,   (lsButtons[21], target.ability.statusEffectManager.paralyzed)
            ,   (lsButtons[22], target.ability.statusEffectManager.darkness)
            ,   (lsButtons[23], target.ability.statusEffectManager.deathForce)
            ,   (lsButtons[24], target.ability.statusEffectManager.resist)
            ,   (lsButtons[25], target.ability.statusEffectManager.manipulate)
            ,   (lsButtons[26], target.ability.statusEffectManager.sense)
            ,   (lsButtons[27], target.ability.statusEffectManager.seizure)
            ,   (lsButtons[28], target.ability.statusEffectManager.imprisoned)
            ,   (lsButtons[29], target.ability.statusEffectManager.peerless)
            };
    }
}
