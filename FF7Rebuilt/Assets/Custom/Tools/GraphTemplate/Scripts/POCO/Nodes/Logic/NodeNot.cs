﻿using System;
using UnityEngine;

[Serializable]
public class NodeNot : Node {
    public NodeNot(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(90, 50);
        name = "Not";

        AddPortIn<PortBool>("In", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_PORT);
        AddPortOut<PortBool>("Out", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        return !portContainerIn.ports[0].GetValue();
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        return new NodeNot(_nodeGraph, position + duplicationOffset, category, false);
    }
}
