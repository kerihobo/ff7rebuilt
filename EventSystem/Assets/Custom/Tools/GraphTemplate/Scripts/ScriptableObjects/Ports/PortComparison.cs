﻿using UnityEditor;
using UnityEngine;

public class PortComparison : Port {
    private readonly string[] values = {
        "=="
    ,   "!="
    ,   ">"
    ,   ">="
    ,   "<"
    ,   "<="
    };

    public NodeComparison.ComparisonType value;

    protected override void RenderAsOutput(StyleSet _styleSet) {
        value = (NodeComparison.ComparisonType)EditorGUILayout.Popup((int)value, values, GUILayout.Width(40));
    }

    public override void SetValue(dynamic _value) {
        if (_value == null) {
            _value = NodeComparison.ComparisonType.EqualTo;
        }

        value = (NodeComparison.ComparisonType)_value;
    }

    public override dynamic GetValue() {
        return (int)value;
    }
}
