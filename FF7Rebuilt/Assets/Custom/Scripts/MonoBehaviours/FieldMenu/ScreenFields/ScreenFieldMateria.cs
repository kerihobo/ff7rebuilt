using CharacterTypes;
using InventoryTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ScreenFieldMateria : ScreenField {
    public override FieldMenu fieldMenu { get; set; }

    /// <summary>
    /// An immediate child of this object. Set active state to switch from the standard Materia menu to the Exchange menu.
    /// </summary>
    [Tooltip("An immediate child of this object. Set active state to switch from the standard Materia menu to the Exchange menu.")]
    public RectTransform rctMateria;
    [Tooltip("An immediate child of rctMateria. Set active state to switch from the Check to standard Materia menu.")]
    public RectTransform rctMateriaView;
    public MateriaMenuOptionList optionList;
    public MateriaMenuArrangeMenu arrangeMenu;
    public MateriaMenuExchange exchange;
    public MateriaMenuCheck check;
    public MateriaMenuSlotsMain slotsMain;
    public MateriaMenuMateriaList materiaList;
    public MateriaMenuTrashMenu trashMenu;

    [SerializeField] private MateriaMenuStatInfo statInfo;
    [SerializeField] private TextMeshProUGUI txtDescription;
    [SerializeField] private UICharacterInfo playerInfo;
    private List<Player> lsActivePlayers = new List<Player>();
    private int selectedCharacterIndex;

    private Player GetInitiallySelectedCharacter => GameManager.Instance.gameData.phs.arrCurrent[fieldMenu.selectedCharacterIndex];

    public Player GetSelectedCharacter => lsActivePlayers.Count > selectedCharacterIndex ? lsActivePlayers[selectedCharacterIndex] : null;

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        statInfo.Initialize();
        slotsMain.Initialize(this);
        optionList.Initialize(this);
        arrangeMenu.Initialize(optionList, materiaList, this);
        playerInfo.Initialize(Vector2.zero, Vector2.zero);
        materiaList.Initialize(this);
        trashMenu.Initialize(this);
        exchange.Initialize(this);
        check.Initialize(this, optionList);
    }

    public override void SetDefaultDisplay() {
        optionList.Disable();
    }

    public override IEnumerator Open() {
        lsActivePlayers = GameManager.Instance.gameData.phs.arrCurrent.Where(x => x != null).ToList();
        selectedCharacterIndex = lsActivePlayers.IndexOf(GetInitiallySelectedCharacter);

        exchange.Hide();
        check.Hide();
        rctMateria.gameObject.SetActive(true);
        ToggleMateriaView(true);
        RefreshPlayerInfo();
        slotsMain.ResetSelection();
        slotsMain.Open();
        materiaList.ResetSelection();

        yield return null;
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public void CycleCharacter(int _direction) {
        selectedCharacterIndex = (selectedCharacterIndex + _direction) % lsActivePlayers.Count;
        if (selectedCharacterIndex < 0) {
            selectedCharacterIndex = lsActivePlayers.Count - 1;
        }

        RefreshPlayerInfo();
        slotsMain.RefreshEquipment();
        slotsMain.RefreshMateria(null);
        slotsMain.RefreshMateriaInfo();
    }

    public void RefreshPlayerInfo() {
        playerInfo.Refresh(GetSelectedCharacter);
    }

    public void RefreshInfo(Materia _materia) {
        SetDescription(_materia);
        statInfo.Refresh(_materia);
    }

    public void SetDescription(Materia _materia) {
        txtDescription.text = _materia?.description;
    }

    public void ToggleMateriaView(bool _isActive) {
        rctMateriaView.gameObject.SetActive(_isActive);
    }
}
