using FFVII.Database;
using InventoryTypes;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class MateriaSlotEditor : VisualElement {
    public new class UxmlFactory : UxmlFactory<MateriaSlotEditor, UxmlTraits> { }

    public MateriaSlotEditor() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        name = Name;
    }
    
    public Equipment target;
    public Database database;
    private const string UXMLPath = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/materia-slot-editor.uxml";
    private const string Name = "materia-slots-editor";
    private TemplateContainer tc;

    public MateriaSlotEditor Initialize(Equipment _target, Database _database) {
        target = _target;
        database = _database;

        BindMateria();
        Refresh(_target);

        return this;
    }

    public void BindMateria() {
        tc.Q<SliderInt>("sld-slots").SetValue(target.materiaSlotCount).RegisterValueChangedCallback(e => {
            target.materiaSlotCount = e.newValue;
            RefreshSlots();

            EditorUtility.SetDirty(database);
        });

        tc.Q<SliderInt>("sld-links").SetValue(target.links).RegisterValueChangedCallback(e => {
            target.links = e.newValue;
            RefreshLinks();

            EditorUtility.SetDirty(database);
        });

        tc.Q<EnumField>("enum-growth").SetMateriaGrowthType(target.materiaGrowth).RegisterValueChangedCallback(e => {
            target.materiaGrowth = (MateriaGrowthType)e.newValue;
            EditorUtility.SetDirty(database);
        });
    }

    public void Refresh(Equipment _target) {
        target = _target;

        tc.Q<SliderInt>("sld-slots").value = target.materiaSlotCount;
        tc.Q<SliderInt>("sld-links").value = target.links;
        tc.Q<EnumField>("enum-growth").value = target.materiaGrowth;
        RefreshSlots();
    }

    private void RefreshSlots() {
        VisualElement[] slots = tc.Q<VisualElement>("grp-slot-images").Children().ToArray();
        foreach (VisualElement slot in slots) {
            slot.visible = false;
        }

        for (int i = 0; i < target.materiaSlotCount; i++) {
            slots[i].visible = true;
        }

        RefreshLinks();
    }

    private void RefreshLinks() {
        SliderInt sldSlots = tc.Q<SliderInt>("sld-slots");
        SliderInt sldLinks = tc.Q<SliderInt>("sld-links");

        sldLinks.highValue = sldSlots.value / 2;
        sldLinks.value = Mathf.Clamp(target.links, 0, sldLinks.highValue);
        target.links = sldLinks.value;

        VisualElement[] slots = tc.Q<VisualElement>("grp-slot-images").Children().ToArray();
        for (int i = 0; i < slots.Length; i++) {
            slots[i].style.borderTopRightRadius = 100;
            slots[i].style.borderBottomRightRadius = 100;
            slots[i].style.borderTopLeftRadius = 100;
            slots[i].style.borderBottomLeftRadius = 100;

            if (sldLinks.value > i / 2) {
                if (i % 2 == 0) {
                    slots[i].style.borderTopRightRadius = 0;
                    slots[i].style.borderBottomRightRadius = 0;
                    slots[i].style.borderTopLeftRadius = 100;
                    slots[i].style.borderBottomLeftRadius = 100;
                } else {
                    slots[i].style.borderTopRightRadius = 100;
                    slots[i].style.borderBottomRightRadius = 100;
                    slots[i].style.borderTopLeftRadius = 0;
                    slots[i].style.borderBottomLeftRadius = 0;
                }
            }
        }
    }
}