﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

// TODO: Fix issue with assets re-arranging themselves if you duplicate or rename a graph.
// TODO: Abstract all required graph classes for the purpose of inheriting when developing new graph systems.
public class EWGraphTemplate : EditorWindow {
    private List<Node> draggedNodes = new List<Node>();
    private List<Node> selectedNodes = new List<Node>();
    private Port portCandidateA;
    private Port portCandidateB;
    private Texture2D txGridSml;
    private Texture2D txGridLg;
    private Rect rectDragSelection;
    private bool isRequestDelete;
    private bool isModifyingSelection;
    private bool isClickStartedOnNothing;
    private bool[] nodesIsSelectedSnapshot;
    private List<Node> selectedNodesPreview;
    private List<Node> duplicatedNodes = new List<Node>();
    private bool isAltPanning;

    protected Vector2 pan = Vector2.zero;
    protected NodeGraph selectedGraph;
    protected StyleSet styleSet;

    private static EWGraphTemplate window;

    protected bool IsGraphSelected<T>() where T : NodeGraph => Selection.activeObject?.GetType() == typeof(T);
    
    [MenuItem("Tools/Node Graph")]
    public static void Open() {
        if (!window) {
            // Ensure that the right window is opened when double-clicking and reselecting an asset.
            // TODO: Beware, potential to open a duplicate window.
            EWGraphTemplate discoveredWindow = GetWindow<EWGraphTemplate>();
            if (discoveredWindow.GetType() != typeof(EWGraphTemplate)) {
                window = CreateWindow<EWGraphTemplate>();
            } else {
                window = discoveredWindow;
            }
        }

        window.Initialize();
    }

    protected virtual void Initialize() {
        Show();
        // TODO: Find a way to make these lines suitable for inherited window types.
        selectedGraph = IsGraphSelected<NodeGraph>() ? (NodeGraph)Selection.activeObject : null;
        titleContent = new GUIContent("Node Graph" + (IsGraphSelected<NodeGraph>() ? " - " + selectedGraph.name : ""));
        styleSet = (StyleSet)Resources.Load("StyleSets/StyleSet");

        pan = position.size / 3;

        Debug.Log("Opened Default?");
    }

    protected virtual void OnSelectionChange() {
        if (!Selection.activeObject
        || !IsGraphSelected<NodeGraph>()) {
            return;
        }

        if (selectedGraph?.searchWindow != null) {
            selectedGraph.searchWindow.Close();
        }

        Initialize();

        Debug.Log(Selection.activeObject?.GetType());

        Repaint();
    }

    [OnOpenAsset(1)]
    public static bool OnOpenAsset(int instanceID, int line) {
        if (Selection.activeObject?.GetType() != typeof(NodeGraph)) {
            return false;
        }

        Open();

        return true;
    }

    private void OnGUI() {
        if (!styleSet) {
            styleSet = (StyleSet)Resources.Load("StyleSets/StyleSet");
        }

        if (!txGridSml || !txGridLg) {
            txGridSml = (Texture2D)Resources.Load("Textures/GridSml");
            txGridLg = (Texture2D)Resources.Load("Textures/GridLg");
        }

        if (selectedGraph && selectedGraph.IsShowGrid) {
            DrawGrid();
        } else {
            GUI.Box(new Rect(0, 0, position.width, position.height), "", styleSet.bg);
        }

        if (!selectedGraph) return;

        Event e = Event.current;
        selectedGraph.Process(e, styleSet, ref pan, position);
        
        if (position.Contains(position.position + e.mousePosition)) {
            switch (e.type) {
                case EventType.MouseDown:
                    OnClick(e);
                    break;
                case EventType.MouseUp:
                    OnRelease(e);
                    break;
                case EventType.MouseDrag:
                    OnDrag(e);
                    break;
                default:
                    break;
            }

            RenderGhostEdge(e.mousePosition);

            selectedGraph.blackboard.DragField(e);

            if (e.keyCode == KeyCode.Return || e.keyCode == KeyCode.KeypadEnter || e.keyCode == KeyCode.Escape) {
                NullifyFocus();
            }
        }

        ProcessDuplicatedNodes();
        DuplicateNodes(e);
        MoveAndSnapNodesViaKeyPress(e);
        DeleteNodesViaKeyPress(e);
        RenderRectSelection();

        Repaint();
    }

    private void ProcessDuplicatedNodes() {
        if (duplicatedNodes.Count > 0) {
            bool isReady = true;
            foreach (Node node in duplicatedNodes) {
                if (!node.isPersistent) {
                    isReady = false;
                    break;
                }
            }

            if (isReady) {
                foreach (Node node in duplicatedNodes) {
                    node.ProcessDuplication(duplicatedNodes);
                }

                duplicatedNodes.Clear();
            }
        }
    }

    private void DuplicateNodes(Event e) {
        if (e.type == EventType.KeyDown && e.modifiers == EventModifiers.Control && e.keyCode == KeyCode.D) {
            foreach (Node node in selectedNodes) {
                Node newNode = node.Duplicate(selectedGraph);
                duplicatedNodes.Add(newNode);

                List<Port> connectedOutPorts = node.portContainerOut.ports.Where(x => x.connectedPorts.Count > 0).ToList();
                List<int> outPortIndices = new List<int>();
                List<int> connectedNodeIndices = new List<int>();
                List<int> targetPortIndices = new List<int>();
                foreach (Port p in connectedOutPorts) {
                    foreach (Port cp in p.connectedPorts) {
                        if (selectedNodes.Contains(cp.parentNode)) {
                            outPortIndices.Add(node.portContainerOut.ports.IndexOf(p));
                            connectedNodeIndices.Add(selectedNodes.IndexOf(cp.parentNode));
                            targetPortIndices.Add(cp.parentNode.portContainerIn.ports.IndexOf(cp));
                        }
                    }
                }

                newNode.duplication = new Node.Duplication(selectedGraph, outPortIndices, connectedNodeIndices, targetPortIndices);
            }

            AssetDatabase.SaveAssets();

            ClearSelection();
            
            selectedNodes.AddRange(new List<Node>(duplicatedNodes));
            foreach (Node node in selectedNodes) {
                node.isSelected = true;
            }

        }
    }

    private void DrawGrid() {
        float division = 2;

        // Small grid.
        GUI.DrawTextureWithTexCoords(new Rect(0, 0, position.width, position.height), txGridSml, new Rect(
            position.x + (-pan.x / txGridSml.width * division)
        ,   position.y + (pan.y / txGridSml.height * division)
        ,   position.width * division / txGridSml.width
        ,   position.height * division / txGridSml.height
        ));

        // Large grid.
        GUI.DrawTextureWithTexCoords(new Rect(0, 0, position.width, position.height), txGridLg, new Rect(
            position.x + (-pan.x / txGridLg.width)
        ,   position.y + (pan.y / txGridLg.height)
        ,   position.width / txGridLg.width
        ,   position.height / txGridLg.height
        ));
    }

    private void OnClick(Event _e) {
        if (selectedGraph.searchWindow.isOpen) {
            return;
        }

        if (selectedGraph.blackboard.isHovered) {
            ClearSelection();
            selectedGraph.blackboard.GetHoveredField(_e);
            return;
        }

        rectDragSelection = new Rect(_e.mousePosition, Vector2.zero);
        GetNodeSelection(out List<Node> selectedNodeList, out Node selectedNode);

        nodesIsSelectedSnapshot = selectedGraph.nodes.ConvertAll(x => x.isSelected).ToArray();
        selectedNodesPreview = new List<Node>(selectedGraph.nodes);
        if (_e.modifiers == EventModifiers.Control || _e.modifiers == EventModifiers.Shift) {
            isModifyingSelection = true;
        }

        if (_e.button == 0) {
            portCandidateA = selectedNode?.GetHoveredPort(_e);

            if (portCandidateA) {
                return;
            }

            bool isNodeEntry = selectedNode?.GetType() == typeof(NodeEntry);

            if (_e.modifiers == EventModifiers.Alt) {
                isAltPanning = true;
            } else if (selectedNode && !isNodeEntry) {
                NullifyFocus();

                if (!isModifyingSelection) {
                    if (selectedNodes.Contains(selectedNode)) {
                        Debug.Log("Same Node" + " | " + selectedNodes.Count);
                        draggedNodes = new List<Node>(selectedNodes);
                    } else if (selectedNodes.Count > 0) {
                        Debug.Log("Different Node");
                        ClearSelection();
                        draggedNodes.Add(selectedNode);
                        selectedNode.isSelected = true;
                    } else if (selectedNodes.Count == 0) {
                        Debug.Log("New Selection");
                        draggedNodes.Add(selectedNode);
                        selectedNode.isSelected = true;
                    }
                }
            } else if ((!selectedNode && !isModifyingSelection) || isNodeEntry) {
                Debug.Log("Deselect");
                isClickStartedOnNothing = true;
                NullifyFocus();
            }
        }

        if (_e.button == 1 && !selectedNode && !isModifyingSelection) {
            ClearSelection();
        }
    }

    private void OnDrag(Event _e) {
        if (selectedGraph.blackboard.OnDrag()/* || rectDragSelection == Rect.zero*/) return;

        if (_e.button == 0) {
            if (isAltPanning) {
                pan += _e.delta;
                return;
            }

            if (portCandidateA != null) {
                SeekPortCandidateB(_e);
            } else if (draggedNodes.Count > 0) {
                selectedNodes = new List<Node>(draggedNodes);
                foreach (Node node in draggedNodes) {
                    if (node.GetType() != typeof(NodeEntry)) {
                        node.position += _e.delta;
                    }
                }

                InvalidateRectDragSelection();

                EditorUtility.SetDirty(selectedGraph);
            } else if (!(_e.modifiers == EventModifiers.Alt)) {
                SelectNodeByDragRect(_e);
                rectDragSelection.size += _e.delta;
            }
        }

        if (_e.button == 2) {
            pan += _e.delta;
        }
    }

    private void InvalidateRectDragSelection() {
        rectDragSelection = new Rect(-1, -1, -1, -1);
    }

    private void OnRelease(Event _e) {
        if (_e.button == 0) {
            if (selectedGraph.searchWindow.isOpen) {
                return;
            }

            if (portCandidateA) {
                ClearSelection();
            } else if (isModifyingSelection) {
                ProcessModifiedSelection(_e);
            } else if (_e.modifiers == EventModifiers.Alt || isAltPanning) {
                isAltPanning = false;
                return;
            } else {
                ProcessStandardSelection();
            }

            selectedNodes = selectedNodes.Where(x => x.GetType() != typeof(NodeEntry)).ToList();
            foreach (Node node in selectedNodes) {
                node.isSelected = true;
            }

        } else if (_e.button == 1) {
            if (selectedGraph.blackboard.isHovered) {
                selectedGraph.blackboard.GetHoveredField(_e);
                selectedGraph.blackboard.ProcessFieldContext();
            } else if (selectedNodes.Count > 0) {
                ProcessContextMenuNodeSelected(_e);
            } else {
                ProcessContextMenuUnselected(_e);
            }
        }
        
        ConnectPorts();

        portCandidateA = null;
        portCandidateB = null;
        draggedNodes.Clear();

        selectedGraph.blackboard.OnRelease(_e, pan);
        rectDragSelection = new Rect();

        isAltPanning = false;
        isModifyingSelection = false;
    }

    private void ProcessStandardSelection() {
        GetNodeSelection(out List<Node> selectedNodeList, out Node selectedNode);

        if (isClickStartedOnNothing) {
            ClearSelection();
            isClickStartedOnNothing = false;
        }

        if (selectedNode && !selectedNodes.Contains(selectedNode)) {
            selectedNodes = new List<Node>(selectedNodeList);
        }
    }

    private void ProcessModifiedSelection(Event _e) {
        GetNodeSelection(out List<Node> selectedNodeList, out Node selectedNode);

        foreach (Node node in selectedNodeList) {
            if (_e.modifiers == EventModifiers.Control) {
                if (selectedNodes.Contains(node)) {
                    node.isSelected = false;
                    selectedNodes.Remove(node);
                } else {
                    selectedNodes.Add(node);
                }
            } else if (!selectedNodes.Contains(node)) {
                selectedNodes.Add(node);
            }
        }
    }

    private void GetNodeSelection(out List<Node> selectedNodeList, out Node selectedNode) {
        Rect dragRect = GetAbsoluteDragRect(new Rect(rectDragSelection));

        selectedNodeList = selectedGraph.nodes.Where(x => x.rect.Overlaps(dragRect)).ToList();
        selectedNode = selectedNodeList.Count > 0 ? selectedNodeList.Last() : null;
    }

    private void RenderRectSelection() {
        GUI.Box(rectDragSelection, "", styleSet.rectSelection);
    }

    private void SelectNodeByDragRect(Event _e) {
        Rect dragRect = GetAbsoluteDragRect(rectDragSelection);

        for (int i = 0; i < selectedNodesPreview.Count; i++) {
            selectedNodesPreview[i].isSelected = nodesIsSelectedSnapshot[i];
            bool isNodeEntry = selectedNodesPreview[i].GetType() == typeof(NodeEntry);

            if (dragRect.Overlaps(selectedNodesPreview[i].rect) && !isNodeEntry) {
                if (isModifyingSelection) {
                    if (_e.modifiers == EventModifiers.Control) {
                        selectedNodesPreview[i].isSelected = !nodesIsSelectedSnapshot[i];
                    } else {
                        selectedNodesPreview[i].isSelected = true;
                    }
                } else if (!isNodeEntry) {
                    selectedNodesPreview[i].isSelected = dragRect.Overlaps(selectedNodesPreview[i].rect);
                }
            }
        }
    }
    
    private void RenderGhostEdge(Vector2 _mousePosition) {
        if (!portCandidateA) return;

        portCandidateA.RenderGhostEdge(_mousePosition);
    }

    private Rect GetAbsoluteDragRect(Rect _rectDragSelection) {
        if (_rectDragSelection.width < 0) {
            _rectDragSelection.width = Mathf.Abs(_rectDragSelection.width);
            _rectDragSelection.x -= _rectDragSelection.width;
        }

        if (_rectDragSelection.height < 0) {
            _rectDragSelection.height = Mathf.Abs(_rectDragSelection.height);
            _rectDragSelection.y -= _rectDragSelection.height;
        }

        return _rectDragSelection;
    }

    private void ClearSelection() {
        foreach (Node node in selectedGraph.nodes) {
            node.isSelected = false;
        }

        selectedNodes.Clear();
        draggedNodes.Clear();
    }

    private void ConnectPorts() {
        if (portCandidateA && portCandidateB
        &&  portCandidateA.parentNode != portCandidateB.parentNode
        &&  !Port.WillCauseLoop(portCandidateA, portCandidateB)
        ) {
            if (portCandidateA.capacity == Port.Capacity.SINGLE) {
                portCandidateA.Disconnect();
                Debug.Log("Disconnecting A's ports.");
            }

            if (portCandidateB.capacity == Port.Capacity.SINGLE) {
                portCandidateB.Disconnect();
                Debug.Log("Disconnecting B's ports.");
            }

            portCandidateA.connectedPorts.Add(portCandidateB);
            portCandidateB.connectedPorts.Add(portCandidateA);
        }
    }

    private void SeekPortCandidateB(Event _e) {
        if (portCandidateA) {
            Node hoveredNode = SelectNodeBySingleClick(_e);
            portCandidateB = hoveredNode?.GetHoveredPort(_e);
            
            if (ArePortsSameDirection()
            || IsDynamicMatchingExecution()
            || (ArePortTypesUnmatched() && AreNeitherPortsDynamic())) {
                portCandidateB = null;
            }
        }
    }

    private bool IsDynamicMatchingExecution() =>
        portCandidateA.portType == NodeDataType.Dynamic && portCandidateB?.portType == NodeDataType.Execution
    ||  portCandidateA.portType == NodeDataType.Execution && portCandidateB?.portType == NodeDataType.Dynamic
    ;

    private bool ArePortsSameDirection() => portCandidateA.direction == portCandidateB?.direction;

    private bool AreNeitherPortsDynamic() => !(portCandidateA.portType == NodeDataType.Dynamic || portCandidateB?.portType == NodeDataType.Dynamic);

    private bool ArePortTypesUnmatched() => portCandidateA.portType != portCandidateB?.portType;

    private Node SelectNodeBySingleClick(Event _e) {
        Node selection = null;
        foreach (Node node in selectedGraph.nodes) {
            if (selection) return selection;

            selection = node.GetMouseOver(_e.mousePosition);
        }

        return selection;
    }

    private void ProcessContextMenuUnselected(Event _e) {
        selectedGraph.nodeSpawnPosition = _e.mousePosition;

        if (selectedGraph.searchWindow == null) {
            selectedGraph.searchWindow = new SearchWindow(selectedGraph);
        }

        selectedGraph.searchWindow.Open(selectedGraph.nodeSpawnPosition, pan);
    }

    private void ProcessContextMenuNodeSelected(Event _e) {
        if (selectedNodes.Count == 1 && selectedNodes[0].GetType() == typeof(NodeEntry)) return;

        GenericMenu menu = new GenericMenu();
        menu.AddItem(new GUIContent("Delete")  , false, () => {
            selectedGraph.DeleteNodes(selectedNodes);
        });

        menu.ShowAsContext();
        _e.Use();
    }
    
    private void MoveAndSnapNodesViaKeyPress(Event e) {
        if (e.type == EventType.KeyDown) {
            Vector2 direction = Vector2.zero;
            if (e.keyCode == KeyCode.LeftArrow) {
                direction = Vector2.left;
            } else if (e.keyCode == KeyCode.RightArrow) {
                direction = Vector2.right;
            } else if (e.keyCode == KeyCode.UpArrow) {
                direction = Vector2.down;
            } else if (e.keyCode == KeyCode.DownArrow) {
                direction = Vector2.up;
            }

            if (direction != Vector2.zero) {
                NullifyFocus();

                foreach (Node node in selectedNodes) {
                    node.Move(direction * 10);
                }
            }
        }
    }

    private void DeleteNodesViaKeyPress(Event e) {
        if (e.type == EventType.Repaint) {
            if (isRequestDelete) {
                selectedGraph.DeleteNodes(selectedNodes);
                isRequestDelete = false;
            }
        } else if (e.keyCode == KeyCode.Delete && selectedNodes.Count > 0) {
            isRequestDelete = true;
        }
    }

    private void NullifyFocus() {
        GUI.FocusControl(null);
    }
}
