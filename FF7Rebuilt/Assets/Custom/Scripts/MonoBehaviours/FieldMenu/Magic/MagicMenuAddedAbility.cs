using CharacterTypes;
using InventoryTypes;
using TMPro;
using UnityEngine;

public class MagicMenuAddedAbility : MonoBehaviour {
    [SerializeReference] private RectTransform rctNothing;
    [SerializeReference] private RectTransform rctAddedAbilities;
    [SerializeReference] private RectTransform rctAll;
    [SerializeReference] private TextMeshProUGUI txtAllValue;
    [SerializeReference] private RectTransform rct4xM;
    [SerializeReference] private TextMeshProUGUI txt4xMValue;

    private void Clear() {
        rctAddedAbilities.gameObject.SetActive(false);

        rctAll.gameObject.SetActive(false);
        txtAllValue.text = string.Empty;

        rct4xM.gameObject.SetActive(false);
        txt4xMValue.text = string.Empty;

        rctNothing.gameObject.SetActive(false);
    }

    public void SetInfoMagic(Player _player, Ability _magic) {
        Clear();

        if (_magic == null) return;

        Player.AbilityAugmentation abilityAugmentation = _player.dctAbilityAugmentations[_magic];
        int all = abilityAugmentation.all;
        int quadraMagic = abilityAugmentation.quadraMagic;
        bool hasAll = all > 0;
        bool hasQuadraMagic = quadraMagic > 0;

        rctAddedAbilities.gameObject.SetActive(hasAll || hasQuadraMagic);

        rctAll.gameObject.SetActive(hasAll);
        txtAllValue.text = all.ToString();

        rct4xM.gameObject.SetActive(hasQuadraMagic);
        txt4xMValue.text = quadraMagic.ToString();

        rctNothing.gameObject.SetActive(!(hasAll || hasQuadraMagic));
    }

    public void SetInfoSummon(Player _player, Materia _summonMateria) {
        Clear();

        if (_summonMateria == null) return;

        Ability summonAbility = _summonMateria.selectionSpells[0].GetSelectedAbility(AbilityType.Summon);
        Player.AbilityAugmentation abilityAugmentation = _player.dctAbilityAugmentations[summonAbility];
        int quadraMagic = abilityAugmentation.quadraMagic;
        bool hasQuadraMagic = quadraMagic > 0;

        rctAddedAbilities.gameObject.SetActive(hasQuadraMagic);

        rctAll.gameObject.SetActive(false);
        txtAllValue.text = string.Empty;

        rct4xM.gameObject.SetActive(hasQuadraMagic);
        txt4xMValue.text = quadraMagic.ToString();

        rctNothing.gameObject.SetActive(!hasQuadraMagic);
    }

    public void SetInfoEnemySkill(Ability _ability) {
        Clear();

        if (_ability == null) return;

        rctAddedAbilities.gameObject.SetActive(false);

        rctAll.gameObject.SetActive(false);
        txtAllValue.text = string.Empty;

        rct4xM.gameObject.SetActive(false);
        txt4xMValue.text = string.Empty;

        rctNothing.gameObject.SetActive(true);
    }
}
