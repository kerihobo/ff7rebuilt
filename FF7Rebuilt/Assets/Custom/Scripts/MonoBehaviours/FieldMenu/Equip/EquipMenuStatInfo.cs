using CharacterTypes;
using InventoryTypes;
using TMPro;
using UnityEngine;

public class EquipMenuStatInfo : MonoBehaviour {
    [Header("Current")]
    public TextMeshProUGUI txtAttackCurrent;
    public TextMeshProUGUI txtAttackPercentCurrent;
    public TextMeshProUGUI txtDefenseCurrent;
    public TextMeshProUGUI txtDefensePercentCurrent;
    public TextMeshProUGUI txtMagicAttackCurrent;
    public TextMeshProUGUI txtMagicDefenseCurrent;
    public TextMeshProUGUI txtMagicDefensePercentCurrent;
    [Header("Changed")]
    public TextMeshProUGUI txtAttackChanged;
    public TextMeshProUGUI txtAttackPercentChanged;
    public TextMeshProUGUI txtDefenseChanged;
    public TextMeshProUGUI txtDefensePercentChanged;
    public TextMeshProUGUI txtMagicAttackChanged;
    public TextMeshProUGUI txtMagicDefenseChanged;
    public TextMeshProUGUI txtMagicDefensePercentChanged;
        
    private Color white;
    private Color yellow;
    private Color red;

    public void Initialize() {
        white = TextColourContainer.GetInstance().white;
        yellow = TextColourContainer.GetInstance().yellow;
        red = TextColourContainer.GetInstance().red;
    }

    public void ToggleStatInfoChangedText(bool _isActive) {
        txtAttackChanged             .gameObject.SetActive(_isActive);
        txtAttackPercentChanged      .gameObject.SetActive(_isActive);
        txtMagicAttackChanged        .gameObject.SetActive(_isActive);
        txtDefenseChanged            .gameObject.SetActive(_isActive);
        txtDefensePercentChanged     .gameObject.SetActive(_isActive);
        txtMagicDefenseChanged       .gameObject.SetActive(_isActive);
        txtMagicDefensePercentChanged.gameObject.SetActive(_isActive);
    }

    public void RefreshWeapon(Player _player, Weapon _weapon) {
        RefreshBaseInfo(
            _player
        ,   out int attackCurrent
        ,   out int attackPercentCurrent
        //,   out int magicAttackCurrent
        ,   out int defenseCurrent
        ,   out int defensePercentCurrent
        ,   out int magicDefenseCurrent
        ,   out int magicDefensePercentCurrent
        );

        if (_weapon != null) {
            int attackChanged        = _weapon.equipEffects.derived.attack;
            int attackPercentChanged = _weapon.equipEffects.derived.attackPercent;
            //int magicAttackChanged   = _weapon.equipEffects.magic;

            txtAttackChanged.text = attackChanged.ToString();
            txtAttackPercentChanged.text = attackPercentChanged.ToString();
            //txtMagicAttackChanged.text = magicAttackChanged.ToString();
                
            RefreshTextColour(txtAttackChanged       , attackCurrent       , attackChanged);
            RefreshTextColour(txtAttackPercentChanged, attackPercentCurrent, attackPercentChanged);
            //RefreshTextColour(txtMagicAttackChanged  , magicAttackCurrent  , magicAttackChanged);
        }
    }

    public void RefreshArmor(Player _player, Armor _armor) {
        RefreshBaseInfo(
            _player
        ,   out int attackCurrent
        ,   out int attackPercentCurrent
        //,   out int magicAttackCurrent
        ,   out int defenseCurrent
        ,   out int defensePercentCurrent
        ,   out int magicDefenseCurrent
        ,   out int magicDefensePercentCurrent
        );

        if (_armor != null) {
            int defenseChanged              = _armor.equipEffects.derived.defense;
            int defensePercentattackChanged = _armor.equipEffects.derived.defensePercent;
            int magicDefenseChanged         = _armor.equipEffects.derived.magicDefense;
            int magicDefensePercentChanged  = _armor.equipEffects.derived.magicDefensePercent;

            txtDefenseChanged.text             = defenseChanged.ToString();
            txtDefensePercentChanged.text      = defensePercentattackChanged.ToString();
            txtMagicDefenseChanged.text        = magicDefenseChanged.ToString();
            txtMagicDefensePercentChanged.text = magicDefensePercentChanged.ToString();

            RefreshTextColour(txtDefenseChanged            , defenseCurrent            , defenseChanged);
            RefreshTextColour(txtDefensePercentChanged     , defensePercentCurrent     , defensePercentattackChanged);
            RefreshTextColour(txtMagicDefenseChanged       , magicDefenseCurrent       , magicDefenseChanged);
            RefreshTextColour(txtMagicDefensePercentChanged, magicDefensePercentCurrent, magicDefensePercentChanged);
        }
    }

    public void RefreshBaseInfo(
        Player _player
    ,   out int attackCurrent
    ,   out int attackPercentCurrent
    //,   out int magicAttackCurrent
    ,   out int defenseCurrent
    ,   out int defensePercentCurrent
    ,   out int magicDefenseCurrent
    ,   out int magicDefensePercentCurrent
    ) {
        Weapon weaponCurrent = _player.weapon;
        Armor armorCurrent   = _player.armor;

        attackCurrent              = weaponCurrent.equipEffects.derived.attack;
        attackPercentCurrent       = weaponCurrent.equipEffects.derived.attackPercent;
        //magicAttackCurrent         = weaponCurrent.equipEffects.magic;
        defenseCurrent             = armorCurrent.equipEffects.derived.defense;
        defensePercentCurrent      = armorCurrent.equipEffects.derived.defensePercent;
        magicDefenseCurrent        = armorCurrent.equipEffects.derived.magicDefense;
        magicDefensePercentCurrent = armorCurrent.equipEffects.derived.magicDefensePercent;

        txtAttackCurrent.text              = attackCurrent.ToString();
        txtAttackPercentCurrent.text       = attackPercentCurrent.ToString();
        //txtMagicAttackCurrent.text         = magicAttackCurrent.ToString();
        txtDefenseCurrent.text             = defenseCurrent.ToString();
        txtDefensePercentCurrent.text      = defensePercentCurrent.ToString();
        txtMagicDefenseCurrent.text        = magicDefenseCurrent.ToString();
        txtMagicDefensePercentCurrent.text = magicDefensePercentCurrent.ToString();

        txtAttackChanged.text              = txtAttackCurrent.text;
        txtAttackPercentChanged.text       = txtAttackPercentCurrent.text;
        txtMagicAttackChanged.text         = txtMagicAttackCurrent.text;
        txtDefenseChanged.text             = txtDefenseCurrent.text;
        txtDefensePercentChanged.text      = txtDefensePercentCurrent.text;
        txtMagicDefenseChanged.text        = txtMagicDefenseCurrent.text;
        txtMagicDefensePercentChanged.text = txtMagicDefensePercentCurrent.text;

        txtAttackChanged.color              = white;
        txtAttackPercentChanged.color       = white;
        txtMagicAttackChanged.color         = white;
        txtDefenseChanged.color             = white;
        txtDefensePercentChanged.color      = white;
        txtMagicDefenseChanged.color        = white;
        txtMagicDefensePercentChanged.color = white;
    }

    private void RefreshTextColour(TextMeshProUGUI _txt, int _valueCurrent, int _valueChanged) {
        if (_valueChanged != _valueCurrent) {
            _txt.color = _valueChanged > _valueCurrent ? yellow : red;
        }
    }
}
