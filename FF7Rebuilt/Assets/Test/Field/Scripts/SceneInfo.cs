using UnityEngine;

public class SceneInfo : MonoBehaviour {
    public static SceneInfo Instance;

    public string locationName;

    private void Awake() {
        Instance = this;
    }

    public Vector3 GetDirection(Vector3 _input) {
        return transform.TransformDirection(_input);
    }
}
