using UnityEngine;
using UnityEngine.UIElements;

public static class ExtVector2Field {
    public static Vector2Field SetValue(this Vector2Field _self, Vector2 _value) {
        _self.value = _value;
        return _self;
    }
}