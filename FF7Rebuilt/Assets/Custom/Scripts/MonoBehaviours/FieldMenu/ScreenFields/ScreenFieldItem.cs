using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFieldItem : ScreenField {
    public override FieldMenu fieldMenu { get; set; }

    [SerializeField] private ItemMenuOptionList optionList;
    [SerializeField] private RectTransform rctLimitResponse;
    [SerializeField] private TextMeshProUGUI txtLimitResponse;
    private float limitResponseHeight;

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        optionList.Initialize(this);

        limitResponseHeight = rctLimitResponse.rect.size.y;
        rctLimitResponse.gameObject.SetActive(false);
    }

    public override void SetDefaultDisplay() {
        optionList.SetDefaultDisplay();
    }

    public override IEnumerator Open() {
        optionList.itemMenuUse.itemList.Refresh();
        optionList.itemMenuUse.itemList.characterSelect.characterInfoContainer.Refresh();
        optionList.itemMenuKeyItems.Refresh();

        optionList.EnterItemSelection();

        yield return null;
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public IEnumerator DisplayLimitMessage(string _message) {
        txtLimitResponse.text = _message;
        rctLimitResponse.gameObject.SetActive(true);
        rctLimitResponse.sizeDelta = Vector2.zero;

        //yield return null;

        ContentSizeFitter csf = txtLimitResponse.GetComponent<ContentSizeFitter>();
        LayoutRebuilder.ForceRebuildLayoutImmediate(csf.GetComponent<RectTransform>());

        //yield return null;

        float width = txtLimitResponse.rectTransform.rect.width + 32;
        Vector2 targetSize = new Vector2(width, limitResponseHeight);

        float t = 0;
        float duration = .1f;

        while (t < duration) {
            rctLimitResponse.sizeDelta = Vector2.Lerp(Vector2.zero, targetSize, t / duration);

            t += Time.deltaTime;
            yield return null;
        }
        
        rctLimitResponse.sizeDelta = targetSize;
        t = 0;

        yield return new WaitForSeconds(.33f);

        while (t < duration) {
            rctLimitResponse.sizeDelta = Vector2.Lerp(targetSize, Vector2.zero, t / duration);

            t += Time.deltaTime;
            yield return null;
        }

        rctLimitResponse.gameObject.SetActive(false);
    }
}
