﻿using System;

public static class DamageType {
    public static readonly string[] DAMAGE_TYPE_NAMES = {
        "0: Physical, 100% Hit Chance"
    ,   "1: Physical, 100% Hit Chance, Ignore Barrier, No Crit"
    ,   "2: Physical, PAcc Check, No Crit Checks"
    ,   "3: Physical, PAcc and Crit Checks"
    ,   "4: Physical, PAcc and Crit Checks, Special Formulae, No Def"
    ,   "5: Physical, PAcc and Crit Checks, Special Damage Multipliers"
    ,   "6: Magical, 100% Hit Chance, Ignore MBarrier"
    ,   "7: Magical, 100% Hit Chance, Ignore MBarrier (Duplicate)"
    ,   "8: Magical, MAcc Check"
    ,   "9: Magical, MAcc Check, Special Formulae, No Def"
    ,   "10: Magical, Hit Chance MOD Target Level = Hit, No Def"
    ,   "11: Magical, Uses Manipulate Accuracy"
    };

    public static int CalculateTotal(int damageType, int power) {
        switch (damageType) {
            case 0:     // Physical, 100% Hit Chance
                return power;
            case 1:     // Physical, 100% Hit Chance, Ignore Barrier, No Crit
                return power;
            case 2:     // Physical, PAcc Check, No Crit Checks
                return power;
            case 3:     // Physical, PAcc and Crit Checks
                return power;
            case 4:     // Physical, PAcc and Crit Checks, Special Formulae, No Def
                return power;
            case 5:     // Physical, PAcc and Crit Checks, Special Damage Multipliers
                return power;
            case 6:     // Magical, 100% Hit Chance, Ignore MBarrier
                return power;
            case 7:     // Magical, 100% Hit Chance, Ignore MBarrier (Duplicate)
                return power;
            case 8:     // Magical, MAcc Check
                return power;
            case 9:     // Magical, MAcc Check, Special Formulae, No Def
                return power;
            case 10:    // Magical, Hit Chance MOD Target Level = Hit, No Def
                return power;
            case 11:    // Magical, Uses Manipulate Accuracy
                return power;
        }

        return power;
    }
}