﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace CharacterTypes {
    [Serializable]
    public class Enemy : Character {
        public Enemy() : base() {
            name = "New Enemy";
            aiGraph = new NodeGraphAI(this);

            for (int i = 0; i < dropItems.Length; i++) {
                dropItems[i] = new DropItem();
            }
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Enemy(Enemy _source) : base(_source) {
            aiGraph             = _source.aiGraph;
            battlePrefab        = _source.battlePrefab;
            gil                 = _source.gil;
            ap                  = _source.ap;
            dropCount           = _source.dropCount;
            morphItem           = _source.morphItem;
            standardAbilities   = _source.standardAbilities;
            uniqueAbilities     = _source.uniqueAbilities;
            elementalDefense    = _source.elementalDefense;
        }

        [Serializable]
        public class DropItem {
            public string name;
            public SelectionCollectible item = new SelectionCollectible();
            public int chance = 8;
            public bool isSteal;
        }

        [Serializable]
        public class UniqueAbiity {
            [SerializeReference]
            public Ability ability = new Ability();
            public AnimationClip animation;
        }

        [SerializeReference]
        public NodeGraphAI aiGraph;
        public Animator battlePrefab;
        public int gil;
        public int ap;
        [Range(0, 4)]
        public int dropCount;
        [SerializeReference]
        public DropItem[] dropItems = new DropItem[4];
        [SerializeReference]
        public SelectionCollectible morphItem = new SelectionCollectible();
        [SerializeReference]
        public List<SelectionAbility> standardAbilities = new List<SelectionAbility>();
        [SerializeReference]
        public List<UniqueAbiity> uniqueAbilities = new List<UniqueAbiity>();
        [SerializeReference]
        public ElementalAffinityManager elementalDefense = new ElementalAffinityManager();
        public DerivedStats derived = new DerivedStats();

        public bool hasBeenRobbed { get; set; }

        public override int Attack => derived.attack;
        public override int AttackPercent => derived.attackPercent;
        public override int Defense => derived.defense;
        public override int DefensePercent => derived.defensePercent;
        public override int MagicAttack => derived.magicAttack;
        public override int MagicDefense => derived.magicDefense;
        public override int MagicDefensePercent => derived.magicDefensePercent;
    }
}