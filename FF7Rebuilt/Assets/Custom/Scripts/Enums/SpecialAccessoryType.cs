﻿/// <summary>
/// https://finalfantasy.fandom.com/wiki/Steal#Final_Fantasy_VII
/// </summary>
public enum SpecialAccessoryType {
    None
,   SneakGlove
,   CatsBell
    // TODO: Add method for Manipulate boost.
,   HypnoCrown
}