﻿public static class SupportSubtype {
    public static readonly string[] SUPPORT_SUBTYPE_NAMES = {
        "0: None"
    ,   "1: All"
    ,   "2: Counter"
    ,   "3: Magic Counter"
    ,   "4: Sneak Attack"
    ,   "5: Final Attack"
    ,   "6: MP Turbo"
    ,   "7: MP Absorb"
    ,   "8: HP Absorb"
    ,   "9: Added Cut"
    ,   "10: Steal As Well"
    ,   "11: Elemental"
    ,   "12: Added Effect"
    ,   "13: Quadra Magic"
    };
}
