using TMPro;
using UnityEngine;

public class MateriaMenuOptionList : MenuControllable {
    public FingerCursorRight cursor;
    
    [SerializeField] private RectTransform optionsParent;
    private ScreenFieldMateria screenFieldMateria;
    private TextMeshProUGUI[] arrOptions;
    private int selectedIndex;

    private MateriaMenuMateriaList materiaList => screenFieldMateria.materiaList;
    private MateriaMenuCheck check => screenFieldMateria.check;
    private MateriaMenuArrangeMenu arrangeMenu => screenFieldMateria.arrangeMenu;

    public void Initialize(ScreenFieldMateria _screenFieldMateria) {
        screenFieldMateria = _screenFieldMateria;

        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        cursor.Initialize(arrOptions[0].rectTransform);
        cursor.Hide();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);

        if (selectedIndex == 1) {
            materiaList.Show(true);
            screenFieldMateria.ToggleMateriaView(true);
            check.Hide();
        } else {
            materiaList.Hide();
            screenFieldMateria.ToggleMateriaView(false);
            check.Show();
        }
    }

    public void Open() {
        SetControllable();
        SetCursor();
        cursor.Show();
    }

    public void Disable() {
        cursor.Hide();
    }

    public void SetSelectedIndex(int optionsSelectedIndex) {
        selectedIndex = optionsSelectedIndex;
    }

    private void Close() {
        Disable();
        screenFieldMateria.slotsMain.SetEquipmentSelected(selectedIndex);
        screenFieldMateria.slotsMain.Open();
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    
    public override void GetInputPage(int pageDirection, bool isPageXPressed) {
        screenFieldMateria.CycleCharacter(pageDirection);
    }

    public override void GetInputConfirmPressed() {
        // For whatever reason, pressing Confirm from the Options menu is the only thing that should...
        // ...reset the lowBoundArrange & selectionIndexArrange.

        switch (selectedIndex) {
            case 0: // Check
                screenFieldMateria.check.Open();
                break;
            case 1: // Arrange
                materiaList.ResetArrange();
                Disable();
                arrangeMenu.ResetSelection();
                arrangeMenu.Open();

                break;
        }
    }
    
    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (_h > 0) {
            Close();
        }
    }

    public override void GetInputCancelPressed() {
        Close();
    }
#endregion
}
