using TMPro;
using UnityEngine;

public class UIGil : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtGil;
    
    private void OnEnable() {
        txtGil.text = "<mspace=12>" + GameManager.Instance.gameData.gil.ToString();
    }
}
