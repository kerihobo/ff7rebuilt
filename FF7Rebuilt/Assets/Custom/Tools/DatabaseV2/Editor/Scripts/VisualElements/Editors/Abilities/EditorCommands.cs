using FFVII.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorCommands : EditorElement {
    protected override bool IsListCustomizable { get; set; } = false;
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Abilities/editor-commands.uxml";
    protected override Database database { get; set; } = DBResources.GetAbilities;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetAbilityList(AbilityType.Command)[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetAbilityList(AbilityType.Command);

    //public EditorCommands() : base() {
    //    listView.reorderable = false;
    //}

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Ability)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        if (target == null) return;

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<TextField>("txt-description").SetValue(((Ability)target).description).RegisterValueChangedCallback(e => {
            ((Ability)target).description = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<ObjectField>("go-prefab-effect").SetGameObject(((Ability)target).prefabEffect).RegisterValueChangedCallback(e => {
            ((Ability)target).prefabEffect = (GameObject)e.newValue;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfCursorAction = ve.Q<DropdownField>("drp-cursor-action");
        ddfCursorAction.choices = CursorAction.CURSOR_ACTION_NAMES.ToList();
        ddfCursorAction.index = ((Ability)target).cursorAction;
        ddfCursorAction.RegisterValueChangedCallback(_e => {
            ((Ability)target).cursorAction = ddfCursorAction.index;
            EditorUtility.SetDirty(database);
        });

        BindTargetOptions();

        void BindTargetOptions() {
            VisualElement targetOptions = ve.Q<VisualElement>("ve-target-options");

            targetOptions.Q<Button>("btn-enable-selection").SetBoolClass(((Ability)target).targetOptions.isEnableSelection).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isEnableSelection);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-start-on-enemies").SetBoolClass(((Ability)target).targetOptions.isStartOnEnemies).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isStartOnEnemies);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-multiple-by-default").SetBoolClass(((Ability)target).targetOptions.isMultipleTargetsByDefault).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isMultipleTargetsByDefault);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-toggle-single-multiple-targets").SetBoolClass(((Ability)target).targetOptions.isToggleSingleMultipleTargets).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isToggleSingleMultipleTargets);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-one-row-only").SetBoolClass(((Ability)target).targetOptions.isOneRowOnly).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isOneRowOnly);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-short-range").SetBoolClass(((Ability)target).targetOptions.isShortRange).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isShortRange);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-all-rows").SetBoolClass(((Ability)target).targetOptions.isAllRows).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isAllRows);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-random-target").SetBoolClass(((Ability)target).targetOptions.isRandomTarget).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isRandomTarget);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-fallen").SetBoolClass(((Ability)target).targetOptions.isFallen).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isFallen);
                EditorUtility.SetDirty(database);
            });
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = target.name;
        ve.Q<TextField>("txt-description").value = ((Ability)target).description;
        ve.Q<ObjectField>("go-prefab-effect").value = ((Ability)target).prefabEffect;
        ve.Q<DropdownField>("drp-cursor-action").index = ((Ability)target).cursorAction;

        RefreshTargetOptions();

        void RefreshTargetOptions() {
            VisualElement targetOptions = ve.Q<VisualElement>("ve-target-options");

            targetOptions.Q<Button>("btn-enable-selection").SetBoolClass(((Ability)target).targetOptions.isEnableSelection);
            targetOptions.Q<Button>("btn-start-on-enemies").SetBoolClass(((Ability)target).targetOptions.isStartOnEnemies);
            targetOptions.Q<Button>("btn-multiple-by-default").SetBoolClass(((Ability)target).targetOptions.isMultipleTargetsByDefault);
            targetOptions.Q<Button>("btn-toggle-single-multiple-targets").SetBoolClass(((Ability)target).targetOptions.isToggleSingleMultipleTargets);
            targetOptions.Q<Button>("btn-one-row-only").SetBoolClass(((Ability)target).targetOptions.isOneRowOnly);
            targetOptions.Q<Button>("btn-short-range").SetBoolClass(((Ability)target).targetOptions.isShortRange);
            targetOptions.Q<Button>("btn-all-rows").SetBoolClass(((Ability)target).targetOptions.isAllRows);
            targetOptions.Q<Button>("btn-random-target").SetBoolClass(((Ability)target).targetOptions.isRandomTarget);
            targetOptions.Q<Button>("btn-fallen").SetBoolClass(((Ability)target).targetOptions.isFallen);
        }
    }

    protected override void Insert() {
        return;
        //itemSource.Insert(hoveredIndex + 1, new Ability());

        //base.Insert();
    }

    protected override void Add() {
        return;
        //itemSource.Add(new Ability());

        //base.Add();
    }
}
