using InventoryTypes;
using UnityEngine;

public class MateriaMenuEnemySkillInfo : MonoBehaviour {
    [SerializeField] private MateriaMenuEnemySkillList skillList;
    [SerializeField] private MateriaMenuEnemySkillStarSet starSet;
    private CanvasGroup cnvGroupEnemySkill;

    public void Initialize() {
        skillList.Initialize();
        starSet.Initialize();

        cnvGroupEnemySkill = GetComponent<CanvasGroup>();
    }

    public void Refresh(Materia _materia) {
        SetVisibility(true);

        skillList.Refresh(_materia);
        starSet.Refresh(_materia);
    }

    public void SetVisibility(bool _isVisible) {
        cnvGroupEnemySkill.alpha = _isVisible ? 1 : 0;
    }
}
