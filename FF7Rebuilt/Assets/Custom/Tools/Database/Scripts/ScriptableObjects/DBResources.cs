﻿using UnityEngine;
using System;
using System.Collections.Generic;
using InventoryTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New DB Resources", menuName = "Kerihobo/DB Resources")]
    public class DBResources : ScriptableObject {
        [Serializable]
        public class Icons {
            [Serializable]
            public class Weapons {
                public Texture2D glove;
                public Texture2D gun;
                public Texture2D gunArm;
                public Texture2D headDress;
                public Texture2D megaphone;
                public Texture2D shuriken;
                public Texture2D spear;
                public Texture2D staff;
                public Texture2D sword;
            }

            [Serializable]
            public class Materia {
                public Texture2D command;
                public Texture2D independent;
                public Texture2D magic;
                public Texture2D summon;
                public Texture2D support;
            }

            public Weapons weapons;
            public Materia materia;
        }

        [SerializeField] private DBInitial initial;
        [SerializeField] private DBItems items;
        [SerializeField] private DBKeyItems keyItems;
        [SerializeField] private DBWeapons weapons;
        [SerializeField] private DBArmor armor;
        [SerializeField] private DBAccessories accessories;
        [SerializeField] private DBMateria materia;
        [SerializeField] private DBEnemies enemies;
        [SerializeField] private DBPlayers players;
        [SerializeField] private DBAbilities abilities;
        [SerializeField] private DBFormations formations;
        [SerializeField] private DBAreas areas;

        public static DBInitial     GetInitial     => GetInstance().initial;
        public static DBItems       GetItems       => GetInstance().items;
        public static DBKeyItems    GetKeyItems    => GetInstance().keyItems;
        public static DBWeapons     GetWeapons     => GetInstance().weapons;
        public static DBArmor       GetArmor       => GetInstance().armor;
        public static DBAccessories GetAccessories => GetInstance().accessories;
        public static DBMateria     GetMateria     => GetInstance().materia;
        public static DBEnemies     GetEnemies     => GetInstance().enemies;
        public static DBPlayers     GetPlayers     => GetInstance().players;
        public static DBAbilities   GetAbilities   => GetInstance().abilities;
        public static DBFormations  GetFormations  => GetInstance().formations;
        public static DBAreas       GetAreas       => GetInstance().areas;
        
        private static DBResources Instance;

        public static DBResources GetInstance() {
            if (!Instance) {
                Instance = Resources.Load<DBResources>("ScriptableObjects/DBResources");
            }
            
            return Instance;
        }

        public static List<Weapon> GetWeaponsList(WeaponType _type) {
            return GetWeapons.GetSelectedWeaponList(_type);
        }
        
        public static List<Formation> GetFormationsList(FormationType _type) {
            return GetFormations.GetSelectedFormationList(_type);
        }

        public static List<Materia> GetMateriaList(MateriaType _type) {
            return GetMateria.GetSelectedMateriaList(_type);
        }

        public static List<Ability> GetAbilityList(AbilityType _type) {
            return GetAbilities.GetSelectedAbilityList(_type);
        }

        public static List<Area> GetAreaList(AreaType _type) {
            return GetAreas.GetSelectedAreaList(_type);
        }
    }
}