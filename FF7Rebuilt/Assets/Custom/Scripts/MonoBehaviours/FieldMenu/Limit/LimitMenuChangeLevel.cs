using TMPro;
using UnityEngine;

public class LimitMenuChangeLevel : MenuControllable {
    public FingerCursorRight cursor;
    
    [SerializeField] private RectTransform optionsParent;
    private int selectedLevel;
    private int selectedIndex;
    private ScreenFieldLimit screenFieldLimit;
    private LimitMenuLevelSelect levelSelect;
    private TextMeshProUGUI[] arrOptions;
    
    public void Initialize(ScreenFieldLimit _screenFieldLimit, LimitMenuLevelSelect _levelSelect) {
        screenFieldLimit = _screenFieldLimit;
        levelSelect = _levelSelect;
        
        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        cursor.Initialize(arrOptions[1].rectTransform);

        Disable();
    }

    public void Open(int _selectedLevel) {
        gameObject.SetActive(true);
        SetControllable();
        ResetSelection();
        //arrangeMenu.Disable();

        selectedLevel = _selectedLevel;
        selectedIndex = 1;
        SetCursor();

        screenFieldLimit.SetDescription("");
    }

    private void Disable() {
        gameObject.SetActive(false);
    }
    
    private void Close() {
        Disable();
        levelSelect.Open();
    }

    public void ResetSelection() {
        selectedIndex = 1;
        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputConfirmPressed() {
        if (selectedIndex == 0) {
            FieldMenu.Instance.menuControllable = null;
            screenFieldLimit.StartCoroutine(screenFieldLimit.SetCharacterLimitLevel(selectedLevel));

            Disable();

            return;
        }

        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }
#endregion
}
