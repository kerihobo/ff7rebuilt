using System.Collections;
using UnityEngine;

public class ScreenFieldMain : ScreenField {
    public FieldMenuCharacterSelect characterSelect;
    
    [SerializeField] private RectTransform rctCharacters;
    [SerializeField] private RectTransform rctTime;
    [SerializeField] private RectTransform rctLocation;

    public override FieldMenu fieldMenu { get; set; }

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        characterSelect.Initialize(fieldMenu, fieldMenu.pnlFieldOptions);
    }

    public override void SetDefaultDisplay() { }

    public override IEnumerator Open() {
        FieldMenuOptionList pnlFieldOptions = fieldMenu.pnlFieldOptions;
        RectTransform rctOptions = pnlFieldOptions.rct;
        pnlFieldOptions.gameObject.SetActive(true);

        characterSelect.characterInfoContainer.Refresh();
        fieldMenu.pnlFieldOptions.RefreshOptions();

        fieldMenu.pnlFieldOptions.ResetSelection();
        yield return StartCoroutine(TransitionIn());

        IEnumerator TransitionIn() {
            float t = 0;
            float duration = .2f;
            float scaleFactor = fieldMenu.canvasScaler.scaleFactor;
            Vector2 charactersEnd = rctCharacters.anchoredPosition;
            Vector2 optionsEnd = rctOptions.anchoredPosition;
            Vector2 timeEnd = rctTime.anchoredPosition;
            Vector2 locationEnd = rctLocation.anchoredPosition;
            Vector2 charactersStart = charactersEnd + (Vector2.right * Screen.width * scaleFactor);
            Vector2 optionsStart = optionsEnd + (Vector2.down * Screen.height * scaleFactor);
            Vector2 timeStart = timeEnd + (Vector2.left * Screen.width * scaleFactor);
            Vector2 locationStart = locationEnd + (Vector2.up * Screen.height * scaleFactor);

            while (t < duration) {
                rctCharacters.anchoredPosition = Vector2.Lerp(charactersStart, charactersEnd, t / duration);
                rctOptions.anchoredPosition = Vector2.Lerp(optionsStart, optionsEnd, t / duration);
                rctTime.anchoredPosition = Vector2.Lerp(timeStart, timeEnd, t / duration);
                rctLocation.anchoredPosition = Vector2.Lerp(locationStart, locationEnd, t / duration);

                pnlFieldOptions.SetCursor();

                t += Time.deltaTime;
                yield return null;
            }

            rctCharacters.anchoredPosition = charactersEnd;
            rctOptions.anchoredPosition = optionsEnd;
            rctTime.anchoredPosition = timeEnd;
            rctLocation.anchoredPosition = locationEnd;

            fieldMenu.pnlFieldOptions.SetControllable();
            pnlFieldOptions.SetCursor();
        }
    }

    public override IEnumerator Close() {
        FieldMenuOptionList pnlFieldOptions = fieldMenu.pnlFieldOptions;
        RectTransform rctOptions = pnlFieldOptions.rct;

        yield return StartCoroutine(TransitionOut());

        gameObject.SetActive(false);

        IEnumerator TransitionOut() {
            float t = 0;
            float duration = .2f;
            float scaleFactor = fieldMenu.canvasScaler.scaleFactor;
            Vector2 charactersStart = rctCharacters.anchoredPosition;
            Vector2 optionsStart = rctOptions.anchoredPosition;
            Vector2 timeStart = rctTime.anchoredPosition;
            Vector2 locationStart = rctLocation.anchoredPosition;
            Vector2 charactersEnd = charactersStart + (Vector2.right * Screen.width * scaleFactor);
            Vector2 optionsEnd = optionsStart + (Vector2.down * Screen.height * scaleFactor);
            Vector2 timeEnd = timeStart + (Vector2.left * Screen.width * scaleFactor);
            Vector2 locationEnd = locationStart + (Vector2.up * Screen.height * scaleFactor);

            while (t < duration) {
                rctCharacters.anchoredPosition = Vector2.Lerp(charactersStart, charactersEnd, t / duration);
                rctOptions.anchoredPosition = Vector2.Lerp(optionsStart, optionsEnd, t / duration);
                rctTime.anchoredPosition = Vector2.Lerp(timeStart, timeEnd, t / duration);
                rctLocation.anchoredPosition = Vector2.Lerp(locationStart, locationEnd, t / duration);

                pnlFieldOptions.SetCursor();

                t += Time.deltaTime;
                yield return null;
            }

            rctCharacters.anchoredPosition = charactersEnd;
            rctOptions.anchoredPosition = optionsEnd;
            rctTime.anchoredPosition = timeEnd;
            rctLocation.anchoredPosition = locationEnd;

            pnlFieldOptions.SetCursor();

            yield return null;

            rctCharacters.anchoredPosition = charactersStart;
            rctOptions.anchoredPosition = optionsStart;
            rctTime.anchoredPosition = timeStart;
            rctLocation.anchoredPosition = locationStart;
        }
    }
}
