using UnityEngine.UIElements;

public class MenuCharacters : MenuSecondary {
    public new class UxmlFactory : UxmlFactory<MenuCharacters, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/MenuSecondary/menu-characters.uxml";
    protected override string Name { get; set; } = "menu-characters";

    public MenuCharacters() : base() {
        ve.Q<Button>("btn-players").clickable.clickedWithEventInfo    += _e => { Show(_e, new EditorPlayers()   , EWTest.ColumnWidths.TWO); };
        ve.Q<Button>("btn-enemies").clickable.clickedWithEventInfo    += _e => { Show(_e, new EditorEnemies()   , EWTest.ColumnWidths.TWO); };
        ve.Q<Button>("btn-formations").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorFormations()); };

        Show(ve.Q<Button>("btn-players"), new EditorPlayers(), EWTest.ColumnWidths.TWO);
    }
}
