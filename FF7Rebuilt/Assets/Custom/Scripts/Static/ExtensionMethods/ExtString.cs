using System;

public static class ExtString {
    public static string GetUntilOrEmpty(this string text, char splitChar) {
        if (!string.IsNullOrWhiteSpace(text)) {
            int charLocation = text.IndexOf(splitChar, StringComparison.Ordinal);

            if (charLocation > 0) {
                return text.Substring(0, charLocation);
            }
        }

        return string.Empty;
    }
}
