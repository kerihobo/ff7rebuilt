﻿using UnityEngine;
using System;
using FFVII.Database;

[Serializable]
public class FormationManager {
    public FormationManager(FormationType _type) {
        type = _type;

        switch (_type) {
            case FormationType.Normal:
                Initialize(6, FormationType.Normal);
                break;
            case FormationType.Special:
                Initialize(4, FormationType.Special);
                break;
            case FormationType.Chocobo:
                Initialize(6, FormationType.Chocobo);
                break;
            default:
                break;
        }
    }

    public FormationType type = FormationType.Normal;
    public SelectionFormation[] formations = new SelectionFormation[6];

    private void Initialize(int _arrCount, FormationType _type) {
        formations = new SelectionFormation[_arrCount];

        for (int i = 0; i < formations.Length; i++) {
            formations[i] = new SelectionFormation(_type);
        }
    }
}
