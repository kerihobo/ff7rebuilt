using UnityEngine.UIElements;

public static class ExtTextField {
    public static TextField SetValue(this TextField _self, string _value) {
        _self.value = _value;
        return _self;
    }
}