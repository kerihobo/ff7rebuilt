﻿using FFVII.Database;
using Field;
using InventoryTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CharacterTypes {
    [Serializable]
    public class Player : Character {
        public class AbilityAugmentation {
            public int all          { get; set; }
            public int quadraMagic  { get; set; }
            public int mpTurbo      { get; set; }
            public bool hpAbsorb    { get; set; }
            public bool mpAbsorb    { get; set; }
            public bool addedCut    { get; set; }
            public bool stealAsWell { get; set; }

            public void Log(Ability _ability) {
                Debug.Log($"ability({_ability.name})" +
                    $"\nall({all})" +
                    $"\nquadraMagic({quadraMagic})" +
                    $"\nmpTurbo({mpTurbo})" +
                    $"\nhpAbsorb({hpAbsorb})" +
                    $"\nmpAbsorb({mpAbsorb})" +
                    $"\naddedCut({addedCut})" +
                    $"\nstealAsWell({stealAsWell})"
                );
            }
        }

        public Player() : base() {
            name = "New Player";
            idName = name;
            for (int i = 0; i < defaultMateriaWeapon.Length; i++) {
                defaultMateriaWeapon[i] = new();
            }

            for (int i = 0; i < defaultMateriaArmor.Length; i++) {
                defaultMateriaArmor[i] = new();
            }
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Player(Player _source) : base(_source) {
            idName = _source.idName;
            gender = _source.gender;
            everyonesGrudge = _source.everyonesGrudge;
            avatar = _source.avatar;
            prefabField = _source.prefabField;
            prefabBattle = _source.prefabBattle;
            prefabAttackEffect = _source.prefabAttackEffect;
            selectionStartingWeapon = _source.selectionStartingWeapon;
            selectionStartingArmor = _source.selectionStartingArmor;
            selectionStartingAccessory = _source.selectionStartingAccessory;
            killsCount = _source.killsCount;

            // Materia
            defaultMateriaWeapon = _source.defaultMateriaWeapon;
            defaultMateriaArmor = _source.defaultMateriaWeapon;

            SetMateriaSlots(defaultMateriaWeapon, materiaWeapon);
            SetMateriaSlots(defaultMateriaArmor, materiaArmor);

            // Additional properties.
            limitBreakManager = new LimitBreakManager(_source.limitBreakManager);
            sourceStats = new PrimaryStats(_source.sourceStats);

            RecalculatePlayerStats();

            // Should be ok to refer to the original database source for this.
            levelUpManager = _source.levelUpManager;
            levelUpManager.GetExperiencePointsStatistics(this);

            hpCurrent = points.hpMaxTotal;
            mpCurrent = points.mpMaxTotal;

            void SetMateriaSlots(SelectionMateria[] _sourceSlots, Materia[] _destinationSlots) {
                _destinationSlots = new Materia[8];

                for (int i = 0; i < _destinationSlots.Length; i++) {
                    Materia selectedMateria = _sourceSlots[i].GetSelectedMateria();
                    if (selectedMateria != null) {
                        _destinationSlots[i] = new Materia(selectedMateria);
                    }
                }
            }
        }

        public string idName;
        public Gender gender;
        [Range(0, 9999)]
        public int everyonesGrudge;
        public int killsCount;
        public Sprite avatar;
        public PlayerControllerNavMesh prefabField;
        public Animator prefabBattle;
        public GameObject prefabAttackEffect;
        public SelectionWeapon selectionStartingWeapon = new();
        public SelectionData selectionStartingArmor = new();
        public SelectionData selectionStartingAccessory = new();
        public SelectionMateria[] defaultMateriaWeapon = new SelectionMateria[8];
        public SelectionMateria[] defaultMateriaArmor = new SelectionMateria[8];
        [SerializeReference] public Weapon weapon;
        [SerializeReference] public Armor armor;
        [SerializeReference] public Accessory accessory;
        [SerializeReference] public Materia[] materiaWeapon = new Materia[8];
        [SerializeReference] public Materia[] materiaArmor = new Materia[8];
        public LimitBreakManager limitBreakManager = new LimitBreakManager();
        /// <summary>
        /// Stat boosts granted from Source consumables. Eg. Luck Source, Magic Source, Power Source, etc..
        /// </summary>
        public PrimaryStats sourceStats = new PrimaryStats();
        [SerializeReference]
        public LevelUpManager levelUpManager = new LevelUpManager();

        public bool isHPSwitchMP { get; set; }
        // TODO: Add these new stats to independent outcomes during recalculation.
        // TODO: Need to read up about any default values for these.
        // Check GameData as there are some early attempts at deciphering the wiki in order to reach a conclusion.
        // That was before I knew about the Wall Market and Scarlet tools.
        public bool isUnderWater { get; set; }
        public int megaAll       { get; set; }
        public int coverChance   { get; set; }
        // Due to some heavy quirks with how counterattack works...
        // and countering up to 8 times if stacked,
        // with those 8 possible attacks also having to include: Final Attack, Sneak Attack, Counter, Magic Counter, Counter Attack
        // I have decided to store materia instead of a counter chance.
        public List<(Materia, Materia)> counterMateriaPairs { get; set; } = new List<(Materia, Materia)>();
        // public int counterAttackChance  { get; set; }
        public int gilDropped           { get; set; }
        public int encounterRate        { get; set; }
        public int chocoboEncounterRate { get; set; }
        public int preEmptiveChance     { get; set; }
        public int expEarned            { get; set; }
        public PrimaryStats totalPrimary        { get; set; }
        public DerivedStats equipmentDerived    { get; set; }
        public List<Ability> lsAvailableMagic   { get; set; }
        public List<Ability> lsAvailableSummons { get; set; }
        public Ability[] arrAvailableCommands   { get; set; }
        public Dictionary<Ability, AbilityAugmentation> dctAbilityAugmentations { get; set; }
        //public IndependentManager independentManager = new();

        public override int Attack         => totalPrimary.strength  + equipmentDerived.attack;
        public override int AttackPercent  => totalPrimary.dexterity + equipmentDerived.attackPercent;
        public override int Defense        => totalPrimary.vitality  + equipmentDerived.defense;
        public override int DefensePercent => totalPrimary.dexterity + equipmentDerived.defensePercent;
        // TODO: Check that MagicAttack is derived this way.
        public override int MagicAttack    => totalPrimary.magic     + equipmentDerived.magicAttack;
        // Originally, a bug prevented armor from contributing to magic defense, though it was intended.
        // This was fixed in the 2012 PC release.
        public override int MagicDefense   => totalPrimary.spirit/* + armor.magicDefense*/;
        /*
            A spell will hit its target 100% of the time under any of the following conditions:
                * The hit rate of the spell is 255.
                *  The spell is of an element to which the target is immune or absorbs.
                *  The target has Reflect status and the spell can be Reflected.
                *  The spell does not inflict a status ailment and the target already has any of the following statuses:
                    *  Sleep
                    *  Confusion
                    *  Stop
                    *  Paralyze
                    *  Petrify
                    *  Death
            If none of the above conditions apply, the party member's chance of evading a magical attack is simply the 
            Magic def% listed on his or her armor. Even if the character does not evade the spell, the caster still has
            a chance to miss.
        */
        public override int MagicDefensePercent => equipmentDerived.magicDefensePercent;

        /// <summary>
        /// A short way to get a flattened collection of only the currently equipped materia
        /// from this character's weapon and armour.
        /// </summary>
        public IEnumerable<Materia> Materia => materiaWeapon.Concat(materiaArmor).Where(x => x != null);

        /* This needs to be called everytime
         * - Equipment (Weapon, Armor, Accessory) is changed.
         * - Materia is:
         *      - set in one slot (in regular menu AND in exchange).
         *      - materia is removed in one slot (in regular menu AND in exchange)
         *      - materia is unequipped as a result of RemoveAll
         *      - exchanged (for both players involved in a selection), whether the exchange was between
         *          - slots
         *          - weapon
         *          - armor
         *          - entire character
         *  - A Source item is consumed (Power Source, Speed Source, Mind Source etc.)
         *          
         *  Remember that equipping weapons or armor should also remove excess materia that can't be fitted
         *  into the equipment's slots.
         */
        /// <summary>
        /// Gather all stat information from a player's loadout
        /// including from weapons, armor, accessories, materia or sources
        /// (Power Source, Speed Source, Mind Source etc.)
        /// and from Independent materia (HP Plus, MP Plus, Luck Plus etc.).
        /// Flatten all the data and apply it as the character's total stats.
        /// </summary>
        public void RecalculatePlayerStats() {

            IEnumerable<EquipEffect> materiaEquipEffects = Materia.Select(x => x.equipEffects);
            EquipEffect materiaEquipEffectsTotal = new EquipEffect();
            if (materiaEquipEffects.Count() > 0) {
                materiaEquipEffectsTotal = materiaEquipEffects.Aggregate((a, b) => a + b);
            }

            EquipEffect equipEffectsTotal = materiaEquipEffectsTotal + weapon?.equipEffects + armor?.equipEffects + accessory?.equipEffects;
            IndependentManager.AddIndependentMateriaBonuses(this, equipEffectsTotal);
            totalPrimary = primary + equipEffectsTotal.primary + sourceStats;
            equipmentDerived = equipEffectsTotal.derived;

            //Debug.Log(name + " +total str: " + totalPrimary.strength);

            totalPrimary.Clamp();
            equipmentDerived.Clamp();

            //(EquipEffectsTotal.primary + sourceStats).Log();
            //totalDerived.Log();

            isUnderWater         = equipEffectsTotal.isUnderWater;
            isHPSwitchMP         = equipEffectsTotal.isHPSwitchMP;
            megaAll              = equipEffectsTotal.megaAll;
            // TODO: Need to add these values to base/default values.
            // Need to know what those values are though.
            /* counterAttackChance = equipEffectsTotal.counterAttackChance;
             * Counter Attack materia actually stacks up to 8 times.
             * This means if you get hit, you might attack the enemy 8 times afterwards.
             * It can be used alongside Cover
             * https://finalfantasy.fandom.com/wiki/Counter_Attack_(Final_Fantasy_VII)
             * 
             * Final Attack, Sneak Attack, Counter, Magic Counter and Counter Attack all
             * share the same command space, and there are only room for 8 such attacks.
             * Only the first eight Counter Effects placed chronologically onto a
             * character will be used
             * https://gamefaqs.gamespot.com/ps/197341-final-fantasy-vii/faqs/36775
             * 
             * ... So maybe I need to store each counter type of attack up to a maximum of 8 in an array.
             * the counter chance can then be calculated from each stored materia level at runtime.
             */
            //counterMateria       = equipEffectsTotal.counterAttackMateria;
            coverChance          = equipEffectsTotal.coverChance % 256;
            /* gilDropped
             * The materia is supposed to be Lv1 +50% and Lv2 +100%
             * but due to a bug, both Lv are +100%.
             * gilDropped can't receiver more than +100% and therefore will benefit nothing from stacking.
             * Don't worry about calculating this until we do so for the whole party. Just store the 100 value now.
             * https://gamefaqs.gamespot.com/ps/197341-final-fantasy-vii/faqs/45703
            */
            gilDropped           = equipEffectsTotal.gilDropped;
            /* encounterRate
             * Should be between 2 and 32, with 16 being the default.
             * Lv1 EnemyAway/Lure adds (-/+)7, while Lv2 adds (-/+)14.
             * I should clamp this between 2 and 32... but not here.
             * That can be handled when we calculate it for the whole party.
             * https://finalfantasy.fandom.com/wiki/Final_Fantasy_VII_battle_system#Random_encounters
             * https://finalfantasy.fandom.com/wiki/Enemy_Lure_(Final_Fantasy_VII)
             * https://finalfantasy.fandom.com/wiki/Enemy_Away_(Final_Fantasy_VII)
            */
            encounterRate        = equipEffectsTotal.encounterRate;
            /* chocoboEncounterRate
             * Standard Chocobo Lure Rating is 0
             * Additional Chocobo Lures stack additively
             * Maximum Chocobo Lure Rating is 32
             * 
             * I can tally this up later. Storing raw values on the player for now is fine.
             */
            chocoboEncounterRate = equipEffectsTotal.chocoboEncounterRate;
            /* preEmptiveChance
             * Chance added is % by 256 to reach a %.
             * Pre-Emptive strike is when your whole party's ATB is full first.
             * So this can just sit here as raw values for now until the whole-party chance is
             * calculated later.
             */
            preEmptiveChance     = equipEffectsTotal.preEmptiveChance;
            expEarned            = equipEffectsTotal.expEarned;
            // TODO: Probably need to call an event so that GameData can update party-wide values
            // For things like encounter rate from the sum of all player values.

            statusAttack   = new StatusEffectManager();
            statusDefense  = new StatusEffectManager();
            elementAttack  = new ElementalAffinityManager();
            elementHalve   = new ElementalAffinityManager();
            elementInvalid = new ElementalAffinityManager();
            elementAbsorb  = new ElementalAffinityManager();

            if (accessory != null) {
                statusDefense += accessory.statusResistance;
            }

            elementAttack += weapon?.elements;
            
            if (armor != null) {
                AddEquipmentElementalAffinities(armor.elements);
            }

            if (accessory != null) {
                AddEquipmentElementalAffinities(accessory.elements);
            }

            dctAbilityAugmentations = new Dictionary<Ability, AbilityAugmentation>();
            
            RecalculateAvailableSpells();
            RecalculateAvailableSummons();
            SetCommands();
            
            IndependentManager.ApplyMegaAll(this);
            SupportManager.RecalculateSupportEffects(this);
            SupportManager.RecalculateCounterAttacks(this);

            //foreach (KeyValuePair<Ability, AbilityAugmentation> abilityEntry in dctAbilityAugmentations) {
            //    abilityEntry.Value.Log(abilityEntry.Key);
            //}

            RecalculateVital(
                ref isHPSwitchMP ? ref mpCurrent : ref hpCurrent
            ,   ref isHPSwitchMP ? ref points.mpMaxTotal : ref points.hpMaxTotal
            ,   points.hpMaxBase
            ,   equipEffectsTotal.maxHPPercent
            ,   9999
            );
            
            RecalculateVital(
                ref isHPSwitchMP ? ref hpCurrent : ref mpCurrent
            ,   ref isHPSwitchMP ? ref points.hpMaxTotal : ref points.mpMaxTotal
            ,   points.mpMaxBase
            ,   equipEffectsTotal.maxMPPercent
            ,   999
            );

            void RecalculateVital(ref int _current, ref int _maxTotal, int _maxBase, int _percent, int _clampMax) {
                int value = (int)(_maxBase * (1 + (_percent / 100f)));
                _maxTotal = Mathf.Clamp(value, 10, _clampMax);
                _current = Mathf.Clamp(_current, 0, _maxTotal);
            }

            void AddEquipmentElementalAffinities(ElementalAffinityManager elements) {
                switch (elements.fire.damageModifier) {
                    case ElementalAffinityManager.DamageModifier.HALVE:
                        elementHalve += elements;
                        break;
                    case ElementalAffinityManager.DamageModifier.INVALID:
                        elementInvalid += elements;
                        break;
                    case ElementalAffinityManager.DamageModifier.ABSORB:
                        elementAbsorb += elements;
                        break;
                }
            }

            void SetCommands() {
                arrAvailableCommands = new Ability[16];

                List<Ability> additionalCommands;
                List<Ability> nonAdditionalCommands;
                DBAbilities.CommandList cmd = DBResources.GetAbilities.commandList;

                AddInitialCommands();
                AddAdditionalCommands();

                //string s = "";
                //foreach (Ability c in arrAvailableCommands) {
                //    s += c?.name + " | ";
                //}
                //Debug.Log(s);

                void AddInitialCommands() {
                    bool hasMagic = HasMagic();
                    bool hasSummon = HasSummon();

                    arrAvailableCommands[0] = cmd.cmdAttack;
                    arrAvailableCommands[3] = cmd.cmdItem;

                    if (hasMagic) {
                        arrAvailableCommands[1] = cmd.cmdMagic;
                    }

                    if (hasSummon) {
                        arrAvailableCommands[2] = cmd.cmdSummon;
                    }

                    additionalCommands = GetAdditionalCommands();
                    nonAdditionalCommands = additionalCommands.Intersect(CommandManager.NonAdditionalCommands).ToList();
                    Ability cmdWItem = cmd.cmdWItem;
                    if (nonAdditionalCommands.Contains(cmdWItem)) {
                        arrAvailableCommands[3] = cmdWItem;
                    }

                    Ability cmdWMagic = cmd.cmdWMagic;
                    if (nonAdditionalCommands.Contains(cmdWMagic)) {
                        arrAvailableCommands[1] = cmdWMagic;
                    }

                    Ability cmdWSummon = cmd.cmdWSummon;
                    if (nonAdditionalCommands.Contains(cmdWSummon)) {
                        arrAvailableCommands[2] = cmdWSummon;
                    }

                    Ability cmd2XCut = cmd.cmd2xCut;
                    if (nonAdditionalCommands.Contains(cmd2XCut)) {
                        arrAvailableCommands[0] = cmd2XCut;
                    }

                    Ability cmd4XCut = cmd.cmd4xCut;
                    if (nonAdditionalCommands.Contains(cmd4XCut)) {
                        arrAvailableCommands[0] = cmd4XCut;
                    }

                    Ability cmdSlashAll = cmd.cmdSlashAll;
                    if (nonAdditionalCommands.Contains(cmdSlashAll) || megaAll > 0) {
                        arrAvailableCommands[0] = cmdSlashAll;
                    }

                    Ability cmdFlash = cmd.cmdFlash;
                    if (nonAdditionalCommands.Contains(cmdFlash)) {
                        arrAvailableCommands[0] = cmdFlash;
                    }
                }

                void AddAdditionalCommands() {
                    additionalCommands = additionalCommands.Except(nonAdditionalCommands).ToList();

                    if (additionalCommands.Count == 0) return;

                    int additionalCommandCount = additionalCommands.Count;
                    int additionalCommandsPlaced = 0;

                    for (int i = 0; i < arrAvailableCommands.Length && additionalCommandsPlaced < additionalCommandCount; i++) {
                        if (arrAvailableCommands[i] == null) {
                            Ability ability = additionalCommands[additionalCommandsPlaced++];
                            arrAvailableCommands[i] = ability;
                        }
                    }
                }
            }
            
            void RecalculateAvailableSpells() {
                Config config = GameManager.Instance.gameData.config;

                Ability.MagicOrder magicOrder0 = config.GetMagicOrderArrangement[0];
                Ability.MagicOrder magicOrder1 = config.GetMagicOrderArrangement[1];
                Ability.MagicOrder magicOrder2 = config.GetMagicOrderArrangement[2];
                List<Ability> allSpells = DBResources.GetAbilityList(AbilityType.Magic).OrderBy(x => {
                    if (x.magicOrder == magicOrder0) return 0;
                    else if (x.magicOrder == magicOrder1) return 1;
                    else if (x.magicOrder == magicOrder2) return 2;
                    else return 3;
                }).ToList()
                ;

                List<Materia> arrMateria = Materia
                    .Where(x => x.type == MateriaType.Magic)
                    .ToList()
                ;

                Materia masterMagic = arrMateria.FirstOrDefault(x => x.type == MateriaType.Magic && x.IsMaster);

                List<Ability> spellsToAssign = new List<Ability>();
                if (masterMagic != null) {
                    spellsToAssign = allSpells;
                } else {
                    arrMateria = arrMateria.Where(x => !x.IsMaster).ToList();

                    // =============================================================================================================================
                    // Old way - Look at our character's materia, iterate through spells up to the highest achieved level. Add any that aren't null.
                    // =============================================================================================================================
                    //List<Ability> arrAvailableSpells = new List<Ability>();
                    //foreach (Materia m in arrMateria) {
                    //    for (int i = 0; i <= m.GetLevel(); i++) {
                    //        Ability ability = m.selectionSpells[i].GetSelectedAbility(AbilityType.Magic);

                    //        if (ability != null) {
                    //            arrAvailableSpells.Add(ability);
                    //        }
                    //    }
                    //}
                    // ==============================================================================================================
                    // New way - Grab all spells from our materia up until each one's level, select abilities where they aren't null.
                    // ==============================================================================================================
                    spellsToAssign = arrMateria
                        // Select the ability selectors from each materia up till its corresponding level (levels start at zero so we must add 1)
                        .SelectMany(x => x.selectionSpells.Take(x.GetLevel() + 1))
                        // From all the ability selectors it found, grab the Magic ability from each one
                        .Select(x => x.GetSelectedAbility(AbilityType.Magic))
                        // Provided they aren't null.
                        .Where(x => x != null)
                        .ToList()
                    ;
                }

                lsAvailableMagic = spellsToAssign;

                dctAbilityAugmentations = new Dictionary<Ability, AbilityAugmentation>();
                foreach (Ability ability in lsAvailableMagic) {
                    dctAbilityAugmentations.TryAdd(ability, new AbilityAugmentation());
                }
            }
        }

        void RecalculateAvailableSpells() {
            Config config = GameManager.Instance.gameData.config;

            Ability.MagicOrder magicOrder0 = config.GetMagicOrderArrangement[0];
            Ability.MagicOrder magicOrder1 = config.GetMagicOrderArrangement[1];
            Ability.MagicOrder magicOrder2 = config.GetMagicOrderArrangement[2];
            List<Ability> allSpells = DBResources.GetAbilityList(AbilityType.Magic).OrderBy(x => {
                if (x.magicOrder == magicOrder0) return 0;
                else if (x.magicOrder == magicOrder1) return 1;
                else if (x.magicOrder == magicOrder2) return 2;
                else return 3;
            }).ToList()
            ;

            List<Materia> arrMateria = Materia
                .Where(x => x.type == MateriaType.Magic)
                .ToList()
            ;

            Materia masterMagic = arrMateria.FirstOrDefault(x => x.IsMaster);

            List<Ability> spellsToAssign = new List<Ability>();
            if (masterMagic != null) {
                spellsToAssign = allSpells;
            } else {
                arrMateria = arrMateria.Where(x => !x.IsMaster).ToList();

                // =============================================================================================================================
                // Old way - Look at our character's materia, iterate through spells up to the highest achieved level. Add any that aren't null.
                // =============================================================================================================================
                //List<Ability> arrAvailableSpells = new List<Ability>();
                //foreach (Materia m in arrMateria) {
                //    for (int i = 0; i <= m.GetLevel(); i++) {
                //        Ability ability = m.selectionSpells[i].GetSelectedAbility(AbilityType.Magic);

                //        if (ability != null) {
                //            arrAvailableSpells.Add(ability);
                //        }
                //    }
                //}
                // ==============================================================================================================
                // New way - Grab all spells from our materia up until each one's level, select abilities where they aren't null.
                // ==============================================================================================================
                spellsToAssign = arrMateria
                    // Select the ability selectors from each materia up till its corresponding level (levels start at zero so we must add 1)
                    .SelectMany(x => x.selectionSpells.Take(x.GetLevel() + 1))
                    // From all the ability selectors it found, grab the Magic ability from each one
                    .Select(x => x.GetSelectedAbility(AbilityType.Magic))
                    // Provided they aren't null.
                    .Where(x => x != null)
                    .ToList()
                ;
            }

            lsAvailableMagic = spellsToAssign;

            foreach (Ability ability in lsAvailableMagic) {
                dctAbilityAugmentations.TryAdd(ability, new AbilityAugmentation());
            }
        }

        void RecalculateAvailableSummons() {
            List<Materia> arrMateria = Materia
                .Where(x => x.type == MateriaType.Summon)
                .ToList()
            ;

            Materia masterSummon = arrMateria.FirstOrDefault(x => x.IsMaster);

            List<Ability> summonsToAssign = new List<Ability>();
            if (masterSummon != null) {
                List<Ability> allSummons = GameManager.Instance.gameData.inventory.lsAllMateria
                    .Where(x => x != null && x.type == MateriaType.Summon && !x.IsMaster)
                    .OrderByDescending(x => x.id)
                    .Select(x => x.selectionSpells[0].GetSelectedAbility(AbilityType.Summon))
                    .ToList()
                ;
                
                summonsToAssign = allSummons;
            } else {
                arrMateria = arrMateria.Where(x => !x.IsMaster).ToList();

                summonsToAssign = arrMateria
                    .Select(x => x.selectionSpells[0].GetSelectedAbility(AbilityType.Summon))
                    .Where(x => x != null)
                    .ToList()
                ;
            }

            lsAvailableSummons = summonsToAssign;

            foreach (Ability ability in lsAvailableSummons) {
                dctAbilityAugmentations.TryAdd(ability, new AbilityAugmentation());
            }
        }

        public void Log() {
            LimitBreakManager.LimitLevel limitLevel = limitBreakManager.limitLevels[limitBreakManager.currentLimitLevel];

            //Debug.Log($""
            //+   $"name({name})"
            //+   $"\nLv({points.level}), exp({points.exp}), tnl({points.expToNextLevelLocal}), expForThisLevel({points.expToNextLevelTotal})"
            //+   $"\nHPBase({points.hpMaxBase}), MPBase({mpCurrent}/{points.mpMaxBase})"
            //+   $"\nHP({hpCurrent}/{points.hpMaxTotal}), MP({mpCurrent}/{points.mpMaxTotal})"
            //+   $"\nLimitLevel({limitBreakManager.currentLimitLevel}), LimitNames({limitLevel.limitBreakA.selectionName}, {limitLevel.limitBreakB.selectionName}), LimitGauge({limitLevel.gaugeFill}/{limitLevel.gaugeCapacity})"
            //+   $"\nPrimary base: Str({primary.strength}), Dex({primary.dexterity}), Vit({primary.vitality}), Mag({primary.magic}), Spr({primary.spirit}), Lck({primary.luck})"
            //+   $"\nPrimary source: Str({sourceStats.strength}), Dex({sourceStats.dexterity}), Vit({sourceStats.vitality}), Mag({sourceStats.magic}), Spr({sourceStats.spirit}), Lck({sourceStats.luck})"
            //+   $"\nPrimary total: Str({totalPrimary.strength}), Dex({totalPrimary.dexterity}), Vit({totalPrimary.vitality}), Mag({totalPrimary.magic}), Spr({totalPrimary.spirit}), Lck({totalPrimary.luck})"
            //+   $"\nDerived base: Atk({Attack}), Atk%({AttackPercent}), Def({Defense}), Def%({DefensePercent}), MAtk({MagicAttack}), MDef({MagicDefense}), MDef%({MagicDefensePercent})"
            //+   $"\nDerived total: Atk({equipmentDerived.attack}), Atk%({equipmentDerived.attackPercent}), Def({equipmentDerived.defense}), Def%({equipmentDerived.defensePercent}), MAtk({equipmentDerived.magicAttack}), MDef({equipmentDerived.magicDefense}), MDef%({equipmentDerived.magicDefensePercent})"
            //+   $"\nWeapon({weapon.name})"
            //+   $"\nArmor({armor.name})"
            //+   $"\nAccessory({accessory?.name})"
            //);
        }

        public bool HasMagic() {
            List<Materia> arrAllMagicMateria = Materia
                .Where(x => x.type == MateriaType.Magic)
                .ToList()
            ;

            if (arrAllMagicMateria.Any(x => x.IsMaster)) return true;

            List<Materia> arrMateriaCheck = arrAllMagicMateria
                .Where(x => !x.IsMaster)
                .ToList()
            ;

            foreach (Materia m in arrMateriaCheck) {
                int materiaLevel = m.GetLevel();

                for (int i = 0; i <= materiaLevel; i++) {
                    if (m.selectionSpells[i].GetSelectedAbility(AbilityType.Magic) != null) {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool HasSummon() {
            List<Materia> arrAllSummonMateria = Materia
                .Where(x => x.type == MateriaType.Summon)
                .ToList()
            ;

            if (arrAllSummonMateria.Any(x => x.IsMaster)) return true;

            List<Materia> arrMateriaCheck = arrAllSummonMateria
                .Where(x => !x.IsMaster)
                .ToList()
            ;

            foreach (Materia m in arrMateriaCheck) {
                int materiaLevel = m.GetLevel();

                for (int i = 0; i <= materiaLevel; i++) {
                    if (m.selectionSpells[i].GetSelectedAbility(AbilityType.Summon) != null) {
                        return true;
                    }
                }
            }

            return false;
        }

        public List<Ability> GetAdditionalCommands() {
            List<Materia> arrAllCommandMateria = Materia
                .Where(x => x.type == MateriaType.Command)
                .ToList();
            List<Materia> arrMateriaCheck = arrAllCommandMateria
                .Where(x => !x.IsMaster)
                .ToList()
            ;

            List<Ability> commands = new List<Ability>();

            if (arrAllCommandMateria.FirstOrDefault(x => x.IsMaster) != null) {
                commands.AddRange(CommandManager.MasterCommands);
            }

            DBAbilities.CommandList cmd = DBResources.GetAbilities.commandList;

            foreach (Materia m in arrMateriaCheck) {
                // Add Enemy Skill.
                if (m.IsEnemySkill) {
                    Ability eSkill = cmd.cmdEnemySkill;
                    
                    if (!commands.Contains(eSkill)) {
                        commands.Add(eSkill);
                    }

                    continue;
                }

                int materiaLevels = m.levels;
                int materiaLevel = m.GetLevel();
                materiaLevel = Mathf.Clamp(materiaLevel, 0, materiaLevels - 1);

                Ability command = m.selectionSpells[materiaLevel].GetSelectedAbility(AbilityType.Command);

                if (!commands.Contains(command)) {
                    commands.Add(command);
                }
            }

            return commands;
        }
    }
}
