﻿using System;

public class SearchEntry {
    public string path;
    public Action action;

    public SearchEntry(string _path, Action _action) {
        path = _path;
        action = _action;
    }
}
