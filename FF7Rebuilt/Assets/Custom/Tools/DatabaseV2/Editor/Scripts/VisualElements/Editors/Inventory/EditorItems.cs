using FFVII.Database;
using InventoryTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine.UIElements;

public class EditorItems : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Inventory/editor-items.uxml";
    protected override Database database { get; set; } = DBResources.GetItems;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetItems.itemsList[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetItems.itemsList;

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Item)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        if (target == null) return;

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<TextField>("txt-description").SetValue(((Item)target).description).RegisterValueChangedCallback(e => {
            ((Item)target).description = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<IntegerField>("int-sale-cost").SetValue(((Item)target).saleCost).RegisterValueChangedCallback(e => {
            ((Item)target).saleCost = e.newValue;
            EditorUtility.SetDirty(database);
        });

        //DropdownField ddf = ve.Q<DropdownField>("drp-battle-item");
        //ddf.Initialize(DBResources.GetAbilityList(AbilityType.BattleItem), ((Item)target).selectionAbility, database);
        //ddf.RegisterValueChangedCallback(_e => {
        //    ((Item)target).selectionAbility.SetDataExternally(ddf.index);
        //    //((Item)target).selectionAbility.index = ddf.index - 1;
        //    EditorUtility.SetDirty(database);
        //});

        ve.Q<Toggle>("tgl-is-battle-item").SetValue(((Item)target).isBattleItem).RegisterValueChangedCallback(e => {
            ((Item)target).isBattleItem = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<EnumField>("enum-field-application").SetItemFieldApplication(((Item)target).fieldApplication).RegisterValueChangedCallback(e => {
            ((Item)target).fieldApplication = (ItemFieldApplication)e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<EnumField>("enum-source-type").SetItemSourceType(((Item)target).sourceType).RegisterValueChangedCallback(e => {
            ((Item)target).sourceType = (ItemSourceType)e.newValue;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddf = ve.Q<DropdownField>($"drp-player-intended");
        ddf.Initialize(DBResources.GetPlayers.playerList, ((Item)target).intendedCharacter, database);
        ddf.RegisterValueChangedCallback(_e => {
            ((Item)target).intendedCharacter.SetDataExternally(ddf.index);
            EditorUtility.SetDirty(database);
        });
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = target.name;
        Item item = (Item)target;
        ve.Q<TextField>("txt-description").value = item.description;
        ve.Q<IntegerField>("int-sale-cost").value = item.saleCost;
        //ve.Q<DropdownField>("drp-battle-item").Initialize(DBResources.GetAbilityList(AbilityType.BattleItem), item.selectionAbility, database);
        ve.Q<Toggle>("tgl-is-battle-item").value = item.isBattleItem;
        ve.Q<EnumField>("enum-field-application").value = (ItemFieldApplication)item.fieldApplication;
        ve.Q<EnumField>("enum-source-type").value = (ItemSourceType)item.sourceType;
        ve.Q<DropdownField>($"drp-player-intended").Initialize(DBResources.GetPlayers.playerList, item.intendedCharacter, database);

        RefreshTypes();
    }

    private void RefreshTypes() {
        Item item = (Item)target;
        EnumField fieldApplication = ve.Q<EnumField>("enum-field-application");

        bool isSource = (ItemFieldApplication)fieldApplication.value == ItemFieldApplication.SOURCE;
        ve.Q<EnumField>("enum-source-type").style.display = isSource ? DisplayStyle.Flex : DisplayStyle.None;

        bool isLimit = (ItemFieldApplication)fieldApplication.value == ItemFieldApplication.LIMIT;
        ve.Q<DropdownField>($"drp-player-intended").style.display = isLimit ? DisplayStyle.Flex : DisplayStyle.None;
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Item());

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new Item());

        base.Add();
    }
}