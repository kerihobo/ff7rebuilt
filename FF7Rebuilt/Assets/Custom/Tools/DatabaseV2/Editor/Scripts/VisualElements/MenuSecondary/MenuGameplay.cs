using UnityEngine.UIElements;

public class MenuGameplay : MenuSecondary {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/MenuSecondary/menu-gameplay.uxml";
    protected override string Name { get; set; } = "menu-gameplay";

    public MenuGameplay() : base() {
        ve.Q<Button>("btn-initial").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorInitial(), EWDatabaseEditor.ColumnWidths.ONE_NO_LIST); };

        Show(ve.Q<Button>("btn-initial"), new EditorInitial(), EWDatabaseEditor.ColumnWidths.ONE_NO_LIST);
    }
}
