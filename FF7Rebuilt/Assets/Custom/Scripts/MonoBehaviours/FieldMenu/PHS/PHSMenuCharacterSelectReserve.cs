using CharacterTypes;
using UnityEngine;

public class PHSMenuCharacterSelectReserve : MenuControllable {
    public FingerCursorRight cursor;
    public FingerCursorRight cursorFlicker;

    public UICharacterInfoContainerReserve characterInfoContainer;

    private ScreenFieldPHS screenFieldPHS;
    private int selectedIndex;

    private PHS phs => GameManager.Instance.gameData.phs;

    public void Initialize(ScreenFieldPHS _screenFieldPHS) {
        screenFieldPHS = _screenFieldPHS;

        characterInfoContainer.Initialize();

        RectTransform firstRectTransform = characterInfoContainer.arrPlayerInfos[0].imgAvatar.rectTransform;
        cursor.Initialize(firstRectTransform);
        cursorFlicker.Initialize(firstRectTransform);
        cursorFlicker.Hide();

        Disable();
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor(cursor);
    }

    public void SetCursor(FingerCursorRight _cursor) {
        _cursor.SetCursorPosition(characterInfoContainer.arrPlayerInfos[selectedIndex].imgAvatar.rectTransform);

        RefreshInfo();
    }

    private void RefreshInfo() {
        Player player = phs.arrReserve[selectedIndex];
        screenFieldPHS.characterInfo.Refresh(player);
    }

    public void Open() {
        cursor.Show();
        cursor.SetFlickerState(false);
        SetControllable();
        screenFieldPHS.characterInfo.gameObject.SetActive(true);
        SetCursor(cursor);

        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    public void Disable() {
        screenFieldPHS.characterInfo.gameObject.SetActive(false);
        cursor.Hide();
    }

    private void Close() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }

#region Override methods.
    public override void GetInputPage(int _pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (screenFieldPHS.IsInfoBShown) return;

        int columns = 3;
        int selectedIndexPrior = selectedIndex;
        int nextSelectedIndex = selectedIndex + _h;
        int lowEnd = selectedIndex - (selectedIndex % columns);
        int highEnd = lowEnd + (columns - 1);

        selectedIndex = nextSelectedIndex;

        bool tooLow = selectedIndex < lowEnd;
        bool tooHigh = selectedIndex > highEnd;
        bool isOutOfRange = tooLow || tooHigh;

        if (isOutOfRange) {
            selectedIndex = selectedIndexPrior;
        }
        
        if (tooLow) {
            Disable();
            screenFieldPHS.OpenMain();
        }

        SetCursor(cursor);
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (screenFieldPHS.IsInfoBShown) return;

        int columns = 3;
        int selectedIndexPrior = selectedIndex;
        selectedIndex += columns * -_v;

        bool tooLow = selectedIndex < 0;
        bool tooHigh = selectedIndex >= characterInfoContainer.arrPlayerInfos.Length;

        if (tooLow || tooHigh) {
            selectedIndex = selectedIndexPrior;
        }

        SetCursor(cursor);
    }

    public override void GetInputCancelPressed() {
        if (screenFieldPHS.IsInfoBShown) {
            screenFieldPHS.HideInfoB();
            return;
        }

        if (screenFieldPHS.arrSelectedA == null) {
            Close();
        } else {
            screenFieldPHS.CancelSwap();
        }
    }

    public override void GetInputConfirmPressed() {
        if (screenFieldPHS.IsInfoBShown) {
            screenFieldPHS.HideInfoB();
            return;
        }

        Player player = phs.arrReserve[selectedIndex];
        if (player == null) return;

        if (screenFieldPHS.arrSelectedA == null) {
            cursorFlicker.Show();
            SetCursor(cursorFlicker);
            //cursorFlicker.SetCursorPosition(characterInfoContainer.arrPlayerInfos[selectedIndex].imgAvatar.rectTransform);
            cursorFlicker.SetFlickerState(true);

            screenFieldPHS.arrSelectedA = phs.arrReserve;
            screenFieldPHS.selectedIndexA = selectedIndex;
        } else {
            if (screenFieldPHS.arrSelectedA[screenFieldPHS.selectedIndexA] == player) {
                screenFieldPHS.ShowInfoB(player);
            } else {
                screenFieldPHS.SwapPlayers(phs.arrReserve, selectedIndex);
            }
        }
    }
#endregion
}
