﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Abilities", menuName = "Kerihobo/Database/Abilities")]
    public class DBAbilities : Database {
        [Serializable]
        public class CommandList {
            public CommandList() {
                cmdAttack           = new Ability("Attack"   , AbilityType.Command);
                cmdMagic            = new Ability("Magic"    , AbilityType.Command);
                cmdSummon           = new Ability("Summon"   , AbilityType.Command);
                cmdItem             = new Ability("Item"     , AbilityType.Command);
                cmdSteal            = new Ability("Steal"    , AbilityType.Command);
                cmdSense            = new Ability("Sense"    , AbilityType.Command);
                cmdCoin             = new Ability("Coin"     , AbilityType.Command);
                cmdThrow            = new Ability("Throw"    , AbilityType.Command);
                cmdMorph            = new Ability("Morph"    , AbilityType.Command);
                cmdDeathblow        = new Ability("D.blow"   , AbilityType.Command);
                cmdManipulate       = new Ability("Manip"    , AbilityType.Command);
                cmdMime             = new Ability("Mime"     , AbilityType.Command);
                cmdEnemySkill       = new Ability("E.Skill"  , AbilityType.Command);
                cmdMug              = new Ability("Mug"      , AbilityType.Command);
                cmdChange           = new Ability("Change"   , AbilityType.Command);
                cmdDefend           = new Ability("Defend"   , AbilityType.Command);
                cmdLimit            = new Ability("Limit"    , AbilityType.Command);
                cmdWMagic           = new Ability("W-Magic"  , AbilityType.Command);
                cmdWSummon          = new Ability("W-Sum."   , AbilityType.Command);
                cmdWItem            = new Ability("W-Item"   , AbilityType.Command);
                cmdSlashAll         = new Ability("Slash-All", AbilityType.Command);
                cmd2xCut            = new Ability("2x-Cut"   , AbilityType.Command);
                cmdFlash            = new Ability("Flash"    , AbilityType.Command);
                cmd4xCut            = new Ability("4x-Cut"   , AbilityType.Command);
            }

            public Ability cmdAttack;
            public Ability cmdMagic;
            public Ability cmdSummon;
            public Ability cmdItem;
            public Ability cmdSteal;
            public Ability cmdSense;
            public Ability cmdCoin;
            public Ability cmdThrow;
            public Ability cmdMorph;
            public Ability cmdDeathblow;
            public Ability cmdManipulate;
            public Ability cmdMime;
            public Ability cmdEnemySkill;
            public Ability cmdMug;
            public Ability cmdChange;
            public Ability cmdDefend;
            public Ability cmdLimit;
            public Ability cmdWMagic;
            public Ability cmdWSummon;
            public Ability cmdWItem;
            public Ability cmdSlashAll;
            public Ability cmd2xCut;
            public Ability cmdFlash;
            public Ability cmd4xCut;

            public List<Ability> GetCommandsAsList => new List<Ability>() {
                cmdAttack
            ,   cmdMagic
            ,   cmdSummon
            ,   cmdItem
            ,   cmdSteal
            ,   cmdSense
            ,   cmdCoin
            ,   cmdThrow
            ,   cmdMorph
            ,   cmdDeathblow
            ,   cmdManipulate
            ,   cmdMime
            ,   cmdEnemySkill
            ,   cmdMug
            ,   cmdChange
            ,   cmdDefend
            ,   cmdLimit
            ,   cmdWMagic
            ,   cmdWSummon
            ,   cmdWItem
            ,   cmdSlashAll
            ,   cmd2xCut
            ,   cmdFlash
            ,   cmd4xCut
            };
        }

        public CommandList commandList;
        public List<Ability> magic;
        public List<Ability> summons;
        public List<Ability> enemySkills;
        public List<Ability> limitBreaks;
        public List<Ability> battleItems;

        public AbilityType selectedMenuType { get; private set; }

        public List<Ability> GetSelectedAbilityList(AbilityType _type) => _type switch {
            AbilityType.Magic => magic,
            AbilityType.Summon => summons,
            AbilityType.EnemySkill => enemySkills,
            AbilityType.LimitBreak => limitBreaks,
            AbilityType.BattleItem => battleItems,
            AbilityType.Command => commandList.GetCommandsAsList,
            _ => null
        };
    }
}
