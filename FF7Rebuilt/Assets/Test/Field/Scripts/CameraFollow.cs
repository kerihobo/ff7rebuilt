using UnityEngine;

[ExecuteInEditMode]
public class CameraFollow : MonoBehaviour {
    [SerializeField] private Transform tCharacter;
    [SerializeField] private bool isFollow;
    [SerializeField] private float left;
    [SerializeField] private float top;
    [SerializeField] private float right;
    [SerializeField] private float bottom;
    [Header("Debug")]
    [SerializeField] private bool isShowBox = true;
    [SerializeField] private Canvas cnvRenderedSample;
    private ScreenSampler ss;
    private Texture2D txNormal;

    private void Awake() {
        if (Application.isPlaying) {
            ss = FindFirstObjectByType<ScreenSampler>();
            cnvRenderedSample.gameObject.SetActive(true);
        } else {
            txNormal = new Texture2D(1, 1);
            txNormal.SetPixel(0, 0, new Color(1, 1, 1, .15f));
            txNormal.Apply();
        }
    }

    private void Update() {
        if (!Application.isPlaying || !isFollow) return;

        Vector2 screenPositionCharacter = Camera.main.WorldToViewportPoint(tCharacter.position);
        screenPositionCharacter.x *= Screen.width;
        screenPositionCharacter.y *= Screen.height;
        ss.sampleRect.position = screenPositionCharacter - (ss.sampleRect.size / 2);

        if (ss.sampleRect.x < left) {
            ss.sampleRect.x = left;
        }

        if (ss.sampleRect.y + ss.sampleRect.height > Screen.height - top) {
            ss.sampleRect.y = Screen.height - top - ss.sampleRect.height;
        }

        if (ss.sampleRect.y < bottom) {
            ss.sampleRect.y = bottom;
        }

        if (ss.sampleRect.x + ss.sampleRect.width > Screen.width - right) {
            ss.sampleRect.x = Screen.width - right - ss.sampleRect.width;
        }
    }

    private void OnGUI() {
        
        if (Application.isPlaying || !isShowBox) return;
        
        float width = Screen.width - left - right;
        float height = Screen.height - top - bottom;

        //GUI.Box(new Rect(left, top, width, height), "", styleBox);

        GUI.DrawTexture(new Rect(left, top, width, 1), Texture2D.whiteTexture);
        GUI.DrawTexture(new Rect(left, top + height - 1, width, 1), Texture2D.whiteTexture);
        GUI.DrawTexture(new Rect(left, top, 1, height), Texture2D.whiteTexture);
        GUI.DrawTexture(new Rect(left + width - 1, top, 1, height), Texture2D.whiteTexture);

        if (GUI.Button(new Rect(left + 10, top + 10, 140, 20), "Toggle Sample View")) {
            Debug.Log("Toggle Sample");
            cnvRenderedSample.gameObject.SetActive(!cnvRenderedSample.gameObject.activeSelf);
        }

        GUI.Label(new Rect(left + 10, top + 30, 140, 20), $"left: \t\t{left}");
        GUI.Label(new Rect(left + 10, top + 50, 140, 20), $"top: \t\t{top}");
        GUI.Label(new Rect(left + 10, top + 70, 140, 20), $"right: \t\t{right}");
        GUI.Label(new Rect(left + 10, top + 90, 140, 20), $"bottom: \t{bottom}");
        
        // =======================================================
        // Box at center of screen to prove character is centered.
        // =======================================================
        //Vector2 boxSize = new Vector2(100, 100);
        //Vector2 screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);
        //Rect boxRect = new Rect(screenCenter.x - boxSize.x / 2,
        //                        screenCenter.y - boxSize.y / 2,
        //                        boxSize.x, boxSize.y);
        //GUI.Box(boxRect, "");
    }
}
