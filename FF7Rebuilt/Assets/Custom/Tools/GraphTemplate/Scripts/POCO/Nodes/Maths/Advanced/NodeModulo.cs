﻿using System;
using UnityEngine;

[Serializable]
public class NodeModulo : Node {
    public NodeModulo(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(90, 75);
        name = "Modulo";

        AddPortIn<PortInt>("A");
        AddPortIn<PortInt>("B");
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(90, 170);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();

        return a % b;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        //Node newNode = Create<NodeModulo>(_nodeGraph, position + duplicationOffset, category, false);
        Node newNode = new NodeModulo(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeModulo)newNode).portContainerIn.ports[0].SetValue(portContainerIn.ports[0].GetValue());
        ((NodeModulo)newNode).portContainerIn.ports[1].SetValue(portContainerIn.ports[1].GetValue());

        return newNode;
    }
}