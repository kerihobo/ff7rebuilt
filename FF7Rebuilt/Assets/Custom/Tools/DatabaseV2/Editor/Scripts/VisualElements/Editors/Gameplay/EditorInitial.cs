using FFVII.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorInitial : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Gameplay/editor-initial.uxml";
    protected override Database database { get; set; } = DBResources.GetInitial;
    protected override Action<VisualElement, int> bindItem { get; set; } = null;
    protected override IList itemSource { get; set; } = null;

    protected override void OnSelectionChange(IEnumerable<object> _o) { return; }

    private LsItemQuantities lsItemQualities;

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        DBInitial db = (DBInitial)database;
        Debug.Log(db);
        if (!db) return;

        ve.Q<IntegerField>("int-gil").SetValue(db.gil).RegisterValueChangedCallback(e => {
            db.gil = e.newValue;
            EditorUtility.SetDirty(database);
        });

        List<DropdownField> ddfsPHS = ve.Q($"ve-phs").Query<DropdownField>().ToList();
        for (int i = 0; i < db.currentPlayers.Length; i++) {
            int index = i;
            DropdownField ddf = ve.Q<DropdownField>($"drp-player-current-{index}");
            ddf.Initialize(DBResources.GetPlayers.playerList, db.currentPlayers[index], database);
            ddf.RegisterValueChangedCallback(_e => {
                db.currentPlayers[index].SetDataExternally(ddf.index);

                DropdownField matchingDropDown = ddfsPHS.FirstOrDefault(x => _e.newValue != "-" && x.value == _e.newValue && x != _e.target);
                if (matchingDropDown != null) {
                    matchingDropDown.value = _e.previousValue;
                }
                
                EditorUtility.SetDirty(database);
            });
        }

        for (int i = 0; i < db.reservePlayers.Length; i++) {
            int index = i;
            DropdownField ddf = ve.Q<DropdownField>($"drp-player-reserve-{index}");
            ddf.Initialize(DBResources.GetPlayers.playerList, db.reservePlayers[index], database);
            ddf.RegisterValueChangedCallback(_e => {
                db.reservePlayers[index].SetDataExternally(ddf.index);

                DropdownField matchingDropDown = ddfsPHS.FirstOrDefault(x => _e.newValue != "-" && x.value == _e.newValue && x != _e.target);
                if (matchingDropDown != null) {
                    matchingDropDown.value = _e.previousValue;
                }
                
                EditorUtility.SetDirty(database);
            });
        }

        lsItemQualities = ve.Q<LsItemQuantities>().Initialize((DBInitial)database);
        ve.Q<Button>("btn-add-item").clicked += lsItemQualities.Add;
    }

    protected override void Refresh() {
        base.Refresh();

        DBInitial db = (DBInitial)database;
        if (!db) return;

        ve.Q<IntegerField>("int-gil").value = db.gil;

        for (int i = 0; i < db.currentPlayers.Length; i++) {
            ve.Q<DropdownField>($"drp-player-current-{i}").Initialize(DBResources.GetPlayers.playerList, db.currentPlayers[i], database);
        }

        for (int i = 0; i < db.reservePlayers.Length; i++) {
            ve.Q<DropdownField>($"drp-player-reserve-{i}").Initialize(DBResources.GetPlayers.playerList, db.reservePlayers[i], database);
        }

        lsItemQualities?.Refresh((DBInitial)database);
    }

    protected override void Insert() { return; }
    protected override void Add() { return; }
}