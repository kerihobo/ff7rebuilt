using UnityEditor;
using UnityEngine.UIElements;

public class MenuPrimary : VisualElement {
    private VisualTreeAsset vta;
    private VisualElement ve;

    public MenuPrimary() {
        vta = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom/Tools/DatabaseV2/DOM/UXML/menu-primary.uxml");
        ve = vta.Instantiate();
        name = "menu-primary";
        Add(ve);

        EWDatabaseEditor.Instance.rootVisualElement.Add(this);

        ve.Q<Button>("btn-gameplay").clickable.clickedWithEventInfo += _e => Show(_e, new MenuGameplay());
        ve.Q<Button>("btn-characters").clickable.clickedWithEventInfo += _e => Show(_e, new MenuCharacters());
        ve.Q<Button>("btn-inventory").clickable.clickedWithEventInfo  += _e => Show(_e, new MenuInventory());
        ve.Q<Button>("btn-abilities").clickable.clickedWithEventInfo  += _e => Show(_e, new MenuAbilities());
        ve.Q<Button>("btn-areas").clickable.clickedWithEventInfo      += _e => Show(_e, new MenuAreas());

        Show(ve.Q<Button>("btn-characters"), new MenuCharacters());
    }

    private void Show(EventBase _e, MenuSecondary _menu) {
        Show((VisualElement)_e.currentTarget, _menu);
    }

    protected void Show(VisualElement _ve, MenuSecondary _menu) {
        SetSelected(_ve);
        EWDatabaseEditor.Instance.ShowSecondary(_menu);
    }

    protected void Show(VisualElement _ve, VisualElement _menu, EWDatabaseEditor.ColumnWidths _width = EWDatabaseEditor.ColumnWidths.ONE) {
        SetSelected(_ve);
        EWDatabaseEditor.Instance.ShowEditor(_menu, _width);
    }

    public void SetSelected(VisualElement _ve) {
        ve.Query<Button>().ForEach(x => x.RemoveFromClassList("selected-primary"));
        _ve.AddToClassList("selected-primary");
    }
}
