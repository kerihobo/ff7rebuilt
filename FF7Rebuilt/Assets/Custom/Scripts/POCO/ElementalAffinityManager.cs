﻿using System;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;

[Serializable]
public class ElementalAffinityManager {
    public enum DamageModifier {
        NORMAL  = 0
    ,   WEAK    = 1
    ,   HALVE   = 2
    ,   INVALID = 3
    ,   ABSORB  = 4
    }

    public ElementalAffinityManager(ElementalAffinityManager _source) {
        fire        = new Affinity(_source.fire);
        ice         = new Affinity(_source.ice);
        lightning   = new Affinity(_source.lightning);
        earth       = new Affinity(_source.earth);
        poison      = new Affinity(_source.poison);
        gravity     = new Affinity(_source.gravity);
        water       = new Affinity(_source.water);
        wind        = new Affinity(_source.wind);
        holy        = new Affinity(_source.holy);
        _10th       = new Affinity(_source._10th);
        restorative = new Affinity(_source.restorative);
        backAttack  = new Affinity(_source.backAttack);
        punch       = new Affinity(_source.punch);
        cut         = new Affinity(_source.cut);
        hit         = new Affinity(_source.hit);
        shoot       = new Affinity(_source.shoot);
        shout       = new Affinity(_source.shout);
        death       = new Affinity(_source.death);
        sleep       = new Affinity(_source.sleep);
        confusion   = new Affinity(_source.confusion);
    }

    public ElementalAffinityManager(bool _isClear = false) {
        if (!_isClear) {
            return;
        }

        fire        = new Affinity(0);
        ice         = new Affinity(0);
        lightning   = new Affinity(0);
        earth       = new Affinity(0);
        poison      = new Affinity(0);
        gravity     = new Affinity(0);
        water       = new Affinity(0);
        wind        = new Affinity(0);
        holy        = new Affinity(0);
        _10th       = new Affinity(0);
        restorative = new Affinity(0);
        backAttack  = new Affinity(0);
        punch       = new Affinity(0);
        cut         = new Affinity(0);
        hit         = new Affinity(0);
        shoot       = new Affinity(0);
        shout       = new Affinity(0);
        death       = new Affinity(0);
        sleep       = new Affinity(0);
        confusion   = new Affinity(0);
    }

    [Serializable]
    public class Affinity {
        public Affinity(Affinity _source) {
            damageModifier = _source.damageModifier;
            isActive = _source.isActive;
        }

        public Affinity(DamageModifier _damageModifier = DamageModifier.NORMAL) {
            damageModifier = _damageModifier;
        }

        /// <summary>
        /// Enemies: 0(normal, 100%, grey), 1(weak, 200%, red), 2(half, 50%, orange), 3(immune, 0%, blue), 4(absorbs, -100%, green)<br/>
        /// Armor + Accessories: 0(normal, 0%, grey), 2(half, 50%, orange), 3(immune, 0%, blue), 4(absorbs, -100%, green)<br/>
        /// </summary>
        public DamageModifier damageModifier = DamageModifier.NORMAL;
        public bool isActive;

        public static Affinity operator +(Affinity a, Affinity b) {
            Affinity temp = new Affinity();
            
            if (b == null) return a;

            temp.isActive = a.isActive || b.isActive;

            return temp;
        }
    }

    public Affinity fire = new Affinity();
    public Affinity ice = new Affinity();
    public Affinity lightning = new Affinity();
    public Affinity earth = new Affinity();
    public Affinity poison = new Affinity();
    public Affinity gravity = new Affinity();
    public Affinity water = new Affinity();
    public Affinity wind = new Affinity();
    public Affinity holy = new Affinity();
    public Affinity _10th = new Affinity();
    public Affinity restorative = new Affinity(DamageModifier.ABSORB);
    public Affinity backAttack = new Affinity(DamageModifier.WEAK);
    public Affinity punch = new Affinity();
    public Affinity cut = new Affinity();
    public Affinity hit = new Affinity();
    public Affinity shoot = new Affinity();
    public Affinity shout = new Affinity();
    public Affinity death = new Affinity();
    public Affinity sleep = new Affinity();
    public Affinity confusion = new Affinity();
    //public Vector2Int gravityFraction = new Vector2Int(1, 2);

    public List<Affinity> GetAsList() {
        return new List<Affinity> {
            fire
        ,   ice
        ,   lightning
        ,   earth
        ,   poison
        ,   gravity
        ,   water
        ,   wind
        ,   holy
        ,   _10th
        ,   restorative
        ,   backAttack
        ,   punch
        ,   cut
        ,   hit
        ,   shoot
        ,   shout
        ,   death
        ,   sleep
        ,   confusion
        };
    }

    public static ElementalAffinityManager operator +(ElementalAffinityManager a, ElementalAffinityManager b) {
        ElementalAffinityManager temp = new ElementalAffinityManager();

        if (b == null) return a;

        temp.fire        = a.fire        + b.fire;
        temp.ice         = a.ice         + b.ice;
        temp.lightning   = a.lightning   + b.lightning;
        temp.earth       = a.earth       + b.earth;
        temp.poison      = a.poison      + b.poison;
        temp.gravity     = a.gravity     + b.gravity;
        temp.water       = a.water       + b.water;
        temp.wind        = a.wind        + b.wind;
        temp.holy        = a.holy        + b.holy;
        temp._10th       = a._10th       + b._10th;
        temp.restorative = a.restorative + b.restorative;
        temp.backAttack  = a.backAttack  + b.backAttack;
        temp.punch       = a.punch       + b.punch;
        temp.cut         = a.cut         + b.cut;
        temp.hit         = a.hit         + b.hit;
        temp.shoot       = a.shoot       + b.shoot;
        temp.shout       = a.shout       + b.shout;
        temp.death       = a.death       + b.death;
        temp.sleep       = a.sleep       + b.sleep;
        temp.confusion   = a.confusion   + b.confusion;

        return temp;
    }
}