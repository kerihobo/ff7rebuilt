﻿using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class PortComparison : Port {
    public PortComparison(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) : base(_parentNode, _portName, _direction, _capacity, _displayMode) {
    }
    
    [SerializeField]
    private string[] values = {
        "=="
    ,   "!="
    ,   ">"
    ,   ">="
    ,   "<"
    ,   "<="
    };

    public NodeComparison.ComparisonType value;

    protected override void RenderAsOutput(StyleSet _styleSet) {
        value = (NodeComparison.ComparisonType)EditorGUILayout.Popup((int)value, values, GUILayout.Width(40));
    }

    public override void SetValue(dynamic _value) {
        value = _value is int ? (NodeComparison.ComparisonType)_value : NodeComparison.ComparisonType.EqualTo;
    }

    public override dynamic GetValue() {
        return (int)value;
    }
}
