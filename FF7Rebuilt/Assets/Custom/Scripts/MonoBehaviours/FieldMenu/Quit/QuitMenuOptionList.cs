using System;
using TMPro;
using UnityEngine;

public class QuitMenuOptionList : MenuControllable {
    public FingerCursorRight cursor;

    /// <summary>
    /// Yes: 0<br/>
    /// No: 1<br/>
    /// </summary>
    private TextMeshProUGUI[] arrOptions;
    private int selectedIndex;
    private ScreenFieldQuit screenFieldQuit;

    public void Initialize(ScreenFieldQuit _screenFieldQuit) {
        screenFieldQuit = _screenFieldQuit;
        
        arrOptions = GetComponentsInChildren<TextMeshProUGUI>(true);

        cursor.Initialize(arrOptions[1].rectTransform);
    }

    private void ResetSelection() {
        selectedIndex = 1;

        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void Open() {
        SetControllable();
        ResetSelection();
    }

    public void Close() {
        FieldMenu.Instance.ReturnFromSelectedScreenQuick();
    }

#region Override methods.
    public override void GetInputVertical(int _v, bool isVerticalPressed) { }
    public override void GetInputPage(int _pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputCancelPressed() { }

    public override void GetInputConfirmPressed() {
        switch (selectedIndex) {
            case 0: // Yes
                Application.Quit();
                break;
            case 1: // No
                Close();
                break;
        }
    }

    public override void GetInputCancel() {
        GetInputCancelPressed();
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        selectedIndex = (selectedIndex + _h) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
