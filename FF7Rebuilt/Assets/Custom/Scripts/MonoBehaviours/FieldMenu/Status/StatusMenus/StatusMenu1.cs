using CharacterTypes;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class StatusMenu1 : StatusMenu {
    [SerializeField] private RectTransform pnlAttack;
    [SerializeField] private RectTransform pnlHalve;
    [SerializeField] private RectTransform pnlInvalid;
    [SerializeField] private RectTransform pnlAbsorb;
    private ScreenFieldStatus screenFieldStatus;
    private Color white;
    private Color grey;
    private List<TextMeshProUGUI> lsTxtAttack;
    private List<TextMeshProUGUI> lsTxtHalve;
    private List<TextMeshProUGUI> lsTxtInvalid;
    private List<TextMeshProUGUI> lsTxtAbsorb;

    public override void Initialize(ScreenFieldStatus _screenFieldStatus) {
        screenFieldStatus = _screenFieldStatus;
        
        white = TextColourContainer.GetInstance().white;
        grey = TextColourContainer.GetInstance().grey;

        lsTxtAttack  = pnlAttack .GetComponentsInChildren<TextMeshProUGUI>(true).ToList();
        lsTxtHalve   = pnlHalve  .GetComponentsInChildren<TextMeshProUGUI>(true).ToList();
        lsTxtInvalid = pnlInvalid.GetComponentsInChildren<TextMeshProUGUI>(true).ToList();
        lsTxtAbsorb  = pnlAbsorb .GetComponentsInChildren<TextMeshProUGUI>(true).ToList();
    }

    public override void Refresh(Player _player) {
        ElementalAffinityManager elAttack = _player.elementAttack;

        lsTxtAttack[0].color = GetActiveColor(elAttack.fire);
        lsTxtAttack[1].color = GetActiveColor(elAttack.ice);
        lsTxtAttack[2].color = GetActiveColor(elAttack.lightning);
        lsTxtAttack[3].color = GetActiveColor(elAttack.earth);
        lsTxtAttack[4].color = GetActiveColor(elAttack.poison);
        lsTxtAttack[5].color = GetActiveColor(elAttack.gravity);
        lsTxtAttack[6].color = GetActiveColor(elAttack.water);
        lsTxtAttack[7].color = GetActiveColor(elAttack.wind);
        lsTxtAttack[8].color = GetActiveColor(elAttack.holy);
        
        ElementalAffinityManager elHalve = _player.elementHalve;

        lsTxtHalve[0].color = GetActiveColor(elHalve.fire);
        lsTxtHalve[1].color = GetActiveColor(elHalve.ice);
        lsTxtHalve[2].color = GetActiveColor(elHalve.lightning);
        lsTxtHalve[3].color = GetActiveColor(elHalve.earth);
        lsTxtHalve[4].color = GetActiveColor(elHalve.poison);
        lsTxtHalve[5].color = GetActiveColor(elHalve.gravity);
        lsTxtHalve[6].color = GetActiveColor(elHalve.water);
        lsTxtHalve[7].color = GetActiveColor(elHalve.wind);
        lsTxtHalve[8].color = GetActiveColor(elHalve.holy);

        ElementalAffinityManager elInvalid = _player.elementInvalid;

        lsTxtInvalid[0].color = GetActiveColor(elInvalid.fire);
        lsTxtInvalid[1].color = GetActiveColor(elInvalid.ice);
        lsTxtInvalid[2].color = GetActiveColor(elInvalid.lightning);
        lsTxtInvalid[3].color = GetActiveColor(elInvalid.earth);
        lsTxtInvalid[4].color = GetActiveColor(elInvalid.poison);
        lsTxtInvalid[5].color = GetActiveColor(elInvalid.gravity);
        lsTxtInvalid[6].color = GetActiveColor(elInvalid.water);
        lsTxtInvalid[7].color = GetActiveColor(elInvalid.wind);
        lsTxtInvalid[8].color = GetActiveColor(elInvalid.holy);

        ElementalAffinityManager elAbsorb = _player.elementAbsorb;

        lsTxtAbsorb[0].color = GetActiveColor(elAbsorb.fire);
        lsTxtAbsorb[1].color = GetActiveColor(elAbsorb.ice);
        lsTxtAbsorb[2].color = GetActiveColor(elAbsorb.lightning);
        lsTxtAbsorb[3].color = GetActiveColor(elAbsorb.earth);
        lsTxtAbsorb[4].color = GetActiveColor(elAbsorb.poison);
        lsTxtAbsorb[5].color = GetActiveColor(elAbsorb.gravity);
        lsTxtAbsorb[6].color = GetActiveColor(elAbsorb.water);
        lsTxtAbsorb[7].color = GetActiveColor(elAbsorb.wind);
        lsTxtAbsorb[8].color = GetActiveColor(elAbsorb.holy);

        Color GetActiveColor(ElementalAffinityManager.Affinity _affinity) {
            return _affinity.isActive ? white : grey;
        }
    }

    public override void Show() {
        gameObject.SetActive(true);
    }

    public override void Hide() {
        gameObject.SetActive(false);
    }
}
