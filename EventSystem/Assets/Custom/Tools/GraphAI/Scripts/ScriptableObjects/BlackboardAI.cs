﻿using UnityEditor;
using UnityEngine;

public class BlackboardAI : Blackboard {
    protected override void PopulateContextMenuNewField(GenericMenu menu) {
        menu.AddItem(new GUIContent("Bool"), false, () => BlackboardField.Create<FieldBool>(this));
        menu.AddItem(new GUIContent("Int"), false, () => BlackboardField.Create<FieldInt>(this));
    }
}
