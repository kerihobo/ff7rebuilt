using InventoryTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIItemInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public Collectible item { get; set; }

    private CanvasGroup cnvGrp;
    private Color white;
    private Color grey;

    private TextMeshProUGUI txtColon;
    private TextMeshProUGUI txtQuantity;
    private Image imgIcon;
    private TextMeshProUGUI txtName;

    public bool IsUsable => !string.IsNullOrEmpty(txtName.text) && txtName.color == white;

    public void Initialize() {
        rct = GetComponent<RectTransform>();
        cnvGrp = GetComponent<CanvasGroup>();
        txtName = GetComponent<TextMeshProUGUI>();
        imgIcon = transform.GetChild(0).GetComponent<Image>();
        txtColon = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        txtQuantity = transform.GetChild(2).GetComponent<TextMeshProUGUI>();

        white = TextColourContainer.GetInstance().white;
        grey = TextColourContainer.GetInstance().grey;
    }

    public void Refresh(Collectible _collectible) {
        item = _collectible;
        cnvGrp.alpha = item != null ? 1 : 0;

        if (item == null) {
            Clear();
            return;
        }
        
        imgIcon.sprite = ItemIconContainer.GetInstance().GetIconByCollectible(_collectible);
        imgIcon.SetNativeSize();
        txtName.text = item.name;
        txtQuantity.text = item.quantity.ToString();

        Color currentColor = item.IsUsableInField() ? white : grey;

        txtName.color = currentColor;
        txtColon.color = currentColor;
        txtQuantity.color = currentColor;
    }

    public void Clear() {
        txtName.text = "";
        txtQuantity.text = "";
        imgIcon.sprite = null;
        cnvGrp.alpha = 0;

    }
}
