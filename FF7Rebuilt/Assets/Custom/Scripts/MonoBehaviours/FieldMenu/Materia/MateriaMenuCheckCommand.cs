using CharacterTypes;
using FFVII.Database;
using System;
using System.Linq;
using UnityEngine;

public class MateriaMenuCheckCommand : MenuControllable {
    public FingerCursorRight cursor;
    [SerializeField] private RectTransform rctWidthParent;
    [SerializeField] private RectTransform rctCommandsParent;

    private MateriaMenuCheckCommandInfo[] arrCommandInfo;
    private RectTransform rctSelf;
    private MateriaMenuCheck check;
    private float[] widths;
    private int selectedIndex;
    private int widthIndex;
    private int commandCount;

    private DBAbilities.CommandList cmd => DBResources.GetAbilities.commandList;

    public void Initialize(MateriaMenuCheck _check) {
        check = _check;

        SetWidths();

        arrCommandInfo = rctCommandsParent.GetComponentsInChildren<MateriaMenuCheckCommandInfo>();
        foreach (MateriaMenuCheckCommandInfo txt in arrCommandInfo) {
            txt.Initialize(this);
        }

        if (cursor) {
            cursor.Initialize(arrCommandInfo[0].rct);
            cursor.Hide();
        }

        void SetWidths() {
            rctSelf = GetComponent<RectTransform>();

            widths = new float[rctWidthParent.childCount + 1];
            widths[0] = rctSelf.rect.width;

            for (int i = 0; i < rctWidthParent.childCount; i++) {
                widths[i + 1] = rctWidthParent.GetChild(i).GetComponent<RectTransform>().rect.width;
            }

            rctWidthParent.gameObject.SetActive(false);
        }
    }

    private void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void SetCursor() {
        if (!cursor) return;

        cursor.SetCursorPosition(arrCommandInfo[selectedIndex].rct);
        arrCommandInfo[selectedIndex].OnHover();
    }

    private void Clear() {
        foreach (MateriaMenuCheckCommandInfo txt in arrCommandInfo) {
            txt.Clear();
        }
    }

    public void Refresh(Player _player) {
        Clear();

        for (int i = 0; i < arrCommandInfo.Length; i++) {
            Action action = null;
            Ability cmd = _player.arrAvailableCommands[i];
            DBAbilities.CommandList commandList = DBResources.GetAbilities.commandList;

            // This approach might be ok for the Check menu
            // but the actual battle-menu should probably use the properties from
            // the abilities themselves to decide.
            if (cmd == commandList.cmdMagic || cmd == commandList.cmdWMagic) {
                action = OpenMagic;
            } else if (cmd == commandList.cmdSummon || cmd == commandList.cmdWSummon) {
                action = OpenSummon;
            } else if (cmd == commandList.cmdEnemySkill) {
                action = OpenEnemySkill;
            }

            arrCommandInfo[i].Refresh(cmd, action);
        }

        commandCount = arrCommandInfo.Count(x => x.command != null);

        ResizeWindow();

        void ResizeWindow() {
            int fillCount = arrCommandInfo.Count(x => x.command != null);
            widthIndex = 0;
            if (fillCount > 12) {
                widthIndex = 3;
            } else if (fillCount > 8) {
                widthIndex = 2;
            } else if (fillCount > 4) {
                widthIndex = 1;
            }

            Vector2 size = rctSelf.sizeDelta;
            size.x = widths[widthIndex];
            rctSelf.sizeDelta = size;
        }
    }

    private void OpenSummon() {
        Debug.Log("Summon");
        check.OpenWindowSummon();
        Disable();
    }

    private void OpenMagic() {
        Debug.Log("Magic");
        check.OpenWindowMagic();
        Disable();
    }

    private void OpenEnemySkill() {
        Debug.Log("Enemy Skill");
        check.OpenWindowEnemySkill();
        Disable();
    }

    public void SetDescription(string _description) {
        check.SetDescription(_description);
    }

    public void Open() {
        SetActive();
        ResetSelection();
    }

    public void ExitSubMenu() {
        SetActive();
        SetCursor();
    }

    public void SetActive() {
        SetControllable();

        if (!cursor) return;
        cursor.Show();
    }

    public void Close() {
        Disable();
        check.Close();
    }

    private void Disable() {
        if (!cursor) return;
        cursor.Hide();
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    
    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (widthIndex == 0) return;

        int step = 4;
        selectedIndex += step * _h;

        int lowEnd = selectedIndex - (selectedIndex % 4);
        int difference = selectedIndex - lowEnd;
        int highEnd = lowEnd + 3;
        
        LoopWithinRow();

        while (arrCommandInfo[selectedIndex].command == null) {
            selectedIndex += step * _h;

            LoopWithinRow();
        }

        SetCursor();

        void LoopWithinRow() {
            if (selectedIndex < 0) {
                selectedIndex = (commandCount - (commandCount % 4)) + difference;
            } else if (selectedIndex > commandCount - 1) {
                selectedIndex = 0 + difference;
            }
        }
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int nextSelectedIndex = selectedIndex -_v;

        int lowEnd = selectedIndex - (selectedIndex % 4);
        int highEnd = lowEnd + 3;
        
        LoopWithinBracket();

        while (arrCommandInfo[nextSelectedIndex].command == null) {
            nextSelectedIndex += -_v;

            LoopWithinBracket();
        }
        
        selectedIndex = nextSelectedIndex;

        SetCursor();

        void LoopWithinBracket() {
            if (nextSelectedIndex < lowEnd) {
                nextSelectedIndex = highEnd;
            } else if (nextSelectedIndex > highEnd) {
                nextSelectedIndex = lowEnd;
            }
        }
    }
    
    public override void GetInputConfirmPressed() {
        Action selectedAction = arrCommandInfo[selectedIndex].action;
        
        if (selectedAction != null) {
            selectedAction();
        }
    }
    
    public override void GetInputCancelPressed() {
        Close();
    }
#endregion
}
