using CharacterTypes;
using TMPro;
using UnityEngine;

public class LimitMenuOptionList : MenuControllable {
    public FingerCursorRight cursor;
    public LimitMenuLevelSelect limitMenuLevelSelect;

    public RectTransform rct { get; private set; }

    /// <summary>
    /// Set: 0<br/>
    /// Check: 1<br/>
    /// </summary>
    private TextMeshProUGUI[] arrOptions;
    private int selectedIndex;
    private ScreenFieldLimit screenFieldLimit;

    public TextMeshProUGUI txtOptionUse => arrOptions[0];

    public void Initialize(ScreenFieldLimit _screenFieldLimit) {
        screenFieldLimit = _screenFieldLimit;
        
        rct = GetComponent<RectTransform>();

        arrOptions = rct.GetComponentsInChildren<TextMeshProUGUI>();

        cursor.Initialize(txtOptionUse.rectTransform);
        limitMenuLevelSelect.Initialize(this, _screenFieldLimit);
    }

    public void Open() {
        SetControllable();
        SetCursor();
        cursor.SetFlickerState(false);

        screenFieldLimit.SetDescription("");
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();

        limitMenuLevelSelect.ResetSelection();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void RefreshLevels(Player _selectedCharacter) {
        limitMenuLevelSelect.Refresh(_selectedCharacter);
    }

#region Override methods.
    public override void GetInputVertical(int _v, bool isVerticalPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void GetInputConfirmPressed() {
        cursor.SetFlickerState(true);

        screenFieldLimit.isSet = selectedIndex == 0;

        cursor.SetFlickerState(true);

        limitMenuLevelSelect.Open();
    }

    public override void GetInputCancelPressed() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        selectedIndex = (selectedIndex + _h) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputPage(int pageDirection, bool isPageXPressed) {
        screenFieldLimit.CycleCharacter(pageDirection);
        SetCursor();
    }
#endregion
}
