using UnityEngine.UIElements;

public class EditorFormations : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Characters/editor-formations.uxml";
    protected override string Name { get; set; } = "editor-formations";

    public EditorFormations() : base() {
    }
}
