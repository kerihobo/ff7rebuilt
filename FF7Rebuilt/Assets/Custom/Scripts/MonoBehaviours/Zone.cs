using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Zone : MonoBehaviour {
    [SerializeField] private string sceneName;
    [SerializeField] private int id;
    [SerializeField] private int targetID;
    [SerializeField] private Transform tEntryPoint;

    public static Zone ZoneInProgress;

    public IEnumerator ChangeScene() {
        if (ZoneInProgress) {
            ZoneInProgress.StopAllCoroutines();
            Destroy(ZoneInProgress.gameObject);
            ZoneInProgress = null;
        }

        ZoneInProgress = this;

        DontDestroyOnLoad(this);

        EventManagerField.Engage();

        SceneTransition.Instance.StopAllCoroutines();
        yield return SceneTransition.Instance.StartCoroutine(SceneTransition.Instance.TransitionToNextScene());

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        while (!asyncLoad.isDone) yield return null;

        yield return new WaitForEndOfFrame();

        Transform newLocation = 
            FindObjectsByType<Zone>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                .Where(x => x != this && x.id == targetID)
                .First().tEntryPoint
        ;

        GameManager.SetScenePlayer(newLocation);

        yield return SceneTransition.Instance.StartCoroutine(SceneTransition.Instance.TransitionFromPreviousScene(false));

        ZoneInProgress = null;
        Destroy(gameObject);
    }
}
