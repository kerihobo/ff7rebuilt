using FFVII.Database;
using InventoryTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

public class EditorKeyItems : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Inventory/editor-key-items.uxml";
    protected override Database database { get; set; } = DBResources.GetKeyItems;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetKeyItems.keyItemsList[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetKeyItems.keyItemsList;

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((KeyItem)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<TextField>("txt-description").SetValue(((KeyItem)target).description).RegisterValueChangedCallback(e => {
            ((KeyItem)target).description = e.newValue;
            EditorUtility.SetDirty(database);
        });
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = (target).name;
        ve.Q<TextField>("txt-description").value = ((KeyItem)target).description;
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new KeyItem());

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new KeyItem());

        base.Add();
    }
}