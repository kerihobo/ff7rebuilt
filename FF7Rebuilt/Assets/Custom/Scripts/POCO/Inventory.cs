using FFVII.Database;
using InventoryTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Inventory {
    [SerializeReference] public Collectible[] arrItems   = new Collectible[320];
    [SerializeReference] public KeyItem[]     arrKeyItem = new KeyItem[64];
    [SerializeReference] public Materia[]     arrMateria = new Materia[200];
    
    public List<Materia> lsAllMateria { get; set; }
    
    private List<Collectible> lsAllCollectibles;

    /// <summary>
    /// Only to be used for New Games. It is meant to populate starting data.
    /// </summary>
    public void Initialize() {
        // Bad way to assign IDs to all the items.
        AssignCollectibleIDs();
        AssignMateriaIDs();

        //AddStartingItems();
        AddAllItems();
        AddAllMateria();
        AddAllKeyItems();

        void AddStartingItems() {
            List<SelectionCollectible> startingItems = DBResources.GetInitial.startingItems;
            foreach (SelectionCollectible selCollectible in startingItems) {
                Collectible item = (Collectible)selCollectible.GetSelectedCollectible();
                AddItem(item, selCollectible.quantity);
            }
        }

        //void AddAllKeyItems() {
        //    List<KeyItem> haxxKeyItems = new List<KeyItem>(DBResources.GetKeyItems.keyItemsList);
        //    foreach (KeyItem keyItem in haxxKeyItems) {
        //        AddKeyItem(keyItem);
        //    }
        //}

        // Might be nice to allow starting materia at some point in the future.
        void AddAllKeyItems() {
            List<KeyItem> haxxKeyItems = new List<KeyItem>(DBResources.GetKeyItems.keyItemsList);
            foreach (KeyItem keyItem in haxxKeyItems) {
                AddKeyItem(keyItem);
            }
        }

        void AddAllItems() {
            foreach (Collectible c in GetAllCollectibles()) {
                AddItem(c, UnityEngine.Random.Range(1, 10));
            }
        }

        void AddAllMateria() {
            foreach (Materia m in GetAllMateria()) {
                AddMateria(m);
            }
        }
    }

    /// <summary>
    /// There NEEDS to be a better way to do this.
    /// How else can I dynamically assign ids to all the collectibles based on their list index
    /// when they are held in different lists and not necessarily viewed in the same place
    /// at any one time during editing/designing?
    /// Note that the whole purpose of this is for sorting via "Arrange" in the Item menu.
    /// </summary>
    private void AssignCollectibleIDs() {
        List<Collectible> allCollectibles = GetAllCollectibles();

        string s = "";

        for (int i = 0; i < allCollectibles.Count; i++) {
            allCollectibles[i].id = i;
            s += $"{string.Format("{0:000}", i)}: {allCollectibles[i].name}\n";
        }

        Debug.Log(s);
    }

    private List<Collectible> GetAllCollectibles() {
        if (lsAllCollectibles != null) {
            return lsAllCollectibles;
        } else {
            lsAllCollectibles = DBResources.GetItems.itemsList.Cast<Collectible>()
                .Concat(DBResources.GetWeaponsList(WeaponType.SWORD).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.GLOVE).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.GUN_ARM).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.HEADDRESS).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.STAFF).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.SPEAR).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.SHURIKEN).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.MEGAPHONE).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.GUN).Cast<Collectible>())
                .Concat(DBResources.GetWeaponsList(WeaponType.LONG_SWORD).Cast<Collectible>())
                .Concat(DBResources.GetArmor.armorList.Cast<Collectible>())
                .Concat(DBResources.GetAccessories.accessoryList.Cast<Collectible>())
                .ToList()
            ;
        }

        return lsAllCollectibles;
    }

    private void AssignMateriaIDs() {
        List<Materia> allMateria = GetAllMateria();

        string s = "";

        for (int i = 0; i < allMateria.Count; i++) {
            allMateria[i].id = i;
            s += $"{string.Format("{0:000}", i)}: {allMateria[i].name}\n";
        }

        Debug.Log(s);
    }

    private List<Materia> GetAllMateria() {
        if (lsAllMateria != null) {
            return lsAllMateria;
        } else {
            lsAllMateria = DBResources.GetMateriaList(MateriaType.Magic)
                .Concat(DBResources.GetMateriaList(MateriaType.Support))
                .Concat(DBResources.GetMateriaList(MateriaType.Command))
                .Concat(DBResources.GetMateriaList(MateriaType.Independent))
                .Concat(DBResources.GetMateriaList(MateriaType.Summon))
                .ToList()
            ;
        }

        return lsAllMateria;
    }

    public void AddItem(Collectible _item, int _quantity) {
        if (_item == null) return;

        AddToExistingEntry(_item, _quantity, out bool foundMatchingItem);

        if (!foundMatchingItem) {
            AddNewEntry(_item);
        }

        void AddToExistingEntry(Collectible _item, int _quantity, out bool _foundMatchingItem) {
            _foundMatchingItem = false;

            for (int i = 0; i < arrItems.Length; i++) {
                if (arrItems[i] != null && arrItems[i].name == _item.name) {
                    arrItems[i].quantity = Mathf.Clamp(arrItems[i].quantity + _quantity, 0, 99);
                    _foundMatchingItem = true;
                    break;
                }
            }
        }

        void AddNewEntry(Collectible _item) {
            Collectible newItem = (Collectible)Activator.CreateInstance(_item.GetType(), _item);
            newItem.quantity = _quantity;
            newItem.id = _item.id;

            for (int i = 0; i < arrItems.Length; i++) {
                if (arrItems[i] == null) {
                    arrItems[i] = newItem;

                    break;
                }
            }
        }
    }

    public void AddKeyItem(KeyItem _keyItem) {
        if (_keyItem == null || arrKeyItem.Contains(_keyItem)) return;
        
        KeyItem newKeyItem = (KeyItem)Activator.CreateInstance(_keyItem.GetType(), _keyItem);

        for (int i = 0; i < arrItems.Length; i++) {
            if (arrKeyItem[i] == null) {
                arrKeyItem[i] = newKeyItem;

                break;
            }
        }
    }

    public void AddMateria(Materia _materia) {
        if (_materia == null) return;
        
        Materia newMateria = (Materia)Activator.CreateInstance(_materia.GetType(), _materia);

        for (int i = 0; i < arrItems.Length; i++) {
            if (arrMateria[i] == null) {
                arrMateria[i] = newMateria;

                break;
            }
        }
    }

    public void RemoveItem(Collectible _item) {
        for (int i = 0; i < arrItems.Length; i++) {
            if (arrItems[i] != null && arrItems[i].name == _item.name) {
                arrItems[i].quantity = Mathf.Clamp(--arrItems[i].quantity, 0, 99);

                if (arrItems[i].quantity <= 0) {
                    arrItems[i] = null;
                }

                break;
            }
        }
    }

    public void ArrangeMateria() {
        arrMateria = arrMateria.OrderBy(x => x == null).ThenBy(x => x?.id).ToArray();

        string s = "";
        foreach (var item in arrMateria) {
            if (item != null) {
                s += $"{item.name} ({item.id})\n";
            }
        }

        Debug.Log(s);
    }
}
