using CharacterTypes;
using System;
using UnityEngine;

public class LimitMenuLevelSelect : MenuControllable {
    public FingerCursorRight cursorLevel;
    public FingerCursorRight cursorCheck;

    [SerializeField] private LimitMenuChangeLevel changeLevel;
    private LimitMenuLevel[] limitLevels;
    private LimitMenuOptionList limitMenuOptionList;
    private ScreenFieldLimit screenFieldLimit;
    private int selectedIndex;
    private Player selectedCharacter;

    public void Initialize(LimitMenuOptionList _limitMenuOptionList, ScreenFieldLimit _screenFieldLimit) {
        screenFieldLimit = _screenFieldLimit;
        limitMenuOptionList = _limitMenuOptionList;

        limitLevels = GetComponentsInChildren<LimitMenuLevel>(true);
        foreach (LimitMenuLevel ll in limitLevels) {
            ll.Initialize(this, cursorCheck, screenFieldLimit);
        }

        Disable();

        changeLevel.Initialize(screenFieldLimit, this);

        cursorLevel.Initialize(limitLevels[0].txtTitle.rectTransform);
        cursorLevel.Hide();
        cursorCheck.Initialize(limitLevels[0].arrOptions[0].rct);
        cursorCheck.Hide();
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void SetCursor() {
        cursorLevel.SetCursorPosition(limitLevels[selectedIndex].txtTitle.rectTransform);
    }

    public void Open() {
        cursorLevel.Show();
        cursorLevel.SetFlickerState(false);
        SetControllable();

        screenFieldLimit.SetDescription(screenFieldLimit.isSet ? "Set BREAK LEVEL" : "Select LEVEL.");

        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void Disable() {
        cursorLevel.Hide();
    }

    private void Close() {
        Disable();
        limitMenuOptionList.Open();
    }

    public void Refresh(Player _selectedCharacter) {
        selectedCharacter = _selectedCharacter;
        
        for (int i = 0; i < limitLevels.Length; i++) {
            limitLevels[i].Refresh(_selectedCharacter, i);
        }
    }

#region Override methods.
    public override void GetInputPage(int _pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    // Undo if you wanna see what was here before.
    // What's here now was taken from Command Window. What was here before was taken from Key Item list.
    // Basically this needs the same horizontal/vertical looping behaviour as the command window.
    // Might just be better off coding it from scratch because Command Window's is a little OTT.
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        int rows = 2;
        int nextSelectedIndex = selectedIndex + _h;

        int lowEnd = selectedIndex - (selectedIndex % rows);
        int highEnd = lowEnd + 1;

        LoopWithinBracket();

        selectedIndex = nextSelectedIndex;

        SetCursor();

        void LoopWithinBracket() {
            if (nextSelectedIndex < lowEnd) {
                nextSelectedIndex = highEnd;
            } else if (nextSelectedIndex > highEnd) {
                nextSelectedIndex = lowEnd;
            }
        }
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int rows = 2;
        selectedIndex += rows * -_v;

        int lowEnd = selectedIndex - (selectedIndex % rows);
        int difference = selectedIndex - lowEnd;
        int highEnd = lowEnd + 1;

        LoopWithinRow();

        SetCursor();

        void LoopWithinRow() {
            if (selectedIndex < 0) {
                selectedIndex = (limitLevels.Length - rows) + -difference;
            } else if (selectedIndex >= limitLevels.Length) {
                selectedIndex = difference;
            }
        }
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputConfirmPressed() {
        LimitMenuLevel limitLevel = limitLevels[selectedIndex];
        if (!limitLevel.isAvailable) return;

        if (screenFieldLimit.isSet) {
            cursorLevel.Hide();
            changeLevel.Open(selectedIndex);
        } else {
            cursorLevel.SetFlickerState(true);
            Disable();
            limitLevel.Open();
        }

    }
#endregion
}
