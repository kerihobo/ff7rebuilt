﻿using UnityEngine;

public class PortBool : Port {
    public bool value;

    public override void Initialize(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) {
        base.Initialize(_parentNode, _portName, _direction, _capacity, _displayMode);

        portType = NodeDataType.Bool;
    }

    protected override void RenderField() {
        value = GUILayout.Toggle(value, "", GUILayout.Width(20));
    }

    public override dynamic GetValue() {
        if (direction == Direction.IN) {
            if (connectedPorts.Count > 0) {
                return connectedPorts[0].GetValue();
            }

            return value;
        }

        return parentNode.GetValue();
    }

    public override void SetValue(dynamic _value) {
        value = _value;
    }
}
