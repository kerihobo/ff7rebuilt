using UnityEngine;

public class UIConfigInfo : MonoBehaviour {
    public string description;
    
    public RectTransform rct { get; set; }

    public void Initialize() {
        rct = GetComponent<RectTransform>();
    }
}
