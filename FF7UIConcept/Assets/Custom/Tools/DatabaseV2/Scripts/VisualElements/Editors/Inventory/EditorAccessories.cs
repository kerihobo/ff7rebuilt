using UnityEngine.UIElements;

public class EditorAccessories : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Inventory/editor-accessories.uxml";
    protected override string Name { get; set; } = "editor-accessories";

    public EditorAccessories() : base() {
    }
}
