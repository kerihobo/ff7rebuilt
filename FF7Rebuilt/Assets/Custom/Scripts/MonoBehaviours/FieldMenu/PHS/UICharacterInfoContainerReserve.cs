using UnityEngine;
using UnityEngine.UI;

public class UICharacterInfoContainerReserve : MonoBehaviour {
    public UICharacterInfoReserve[] arrPlayerInfos { get; set; }

    public void Initialize() {
        RectTransform rct = GetComponent<RectTransform>();

        LayoutRebuilder.ForceRebuildLayoutImmediate(rct);

        arrPlayerInfos = GetComponentsInChildren<UICharacterInfoReserve>();
        foreach (UICharacterInfoReserve playerInfo in arrPlayerInfos) {
            playerInfo.Initialize();
        }
    }

    /// <summary>
    /// An easy way to call Refresh for all characters in your party.
    /// </summary>
    public void Refresh() {
        for (int i = 0; i < arrPlayerInfos.Length; i++) {
            arrPlayerInfos[i].Refresh(GameManager.Instance.gameData.phs.arrReserve[i]);
        }
    }
}
