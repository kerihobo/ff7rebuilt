﻿using System;
using UnityEngine;
using CharacterTypes;

/// <summary>
/// Stat growth as described at: https://gamefaqs.gamespot.com/pc/130791-final-fantasy-vii/faqs/36775
/// </summary>
[Serializable]
public class LevelUpManager {
    [Serializable]
    public class StatGradeSet {
        public int strength = 1;
        public int vitality = 6;
        public int magic = 3;
        public int spirit = 4;
        public int dexterity = 26;
    }

    [Serializable]
    public class Rank {
        public Rank(int _valueGradient, int _valueBase) {
            valueGradient = _valueGradient;
            valueBase = _valueBase;
        }

        public int valueBase;
        public int valueGradient;
    }

    public int[] expModifiers = new int[8];
    public Rank[] rankCurvesHP = new Rank[8];
    public Rank[] rankCurvesMP = new Rank[8];
    public Rank[] rankCurvesLuck = new Rank[8];
    public StatGradeSet statGrades = new StatGradeSet();

    private float[] gradeMultipliersHP = { .4f, .5f, .5f, .6f, .7f, .8f, .9f, 1f, 1.1f, 1.2f, 1.3f, 1.5f };
    private float[] gradeMultipliersMP = { .2f, .3f, .3f, .5f, .7f, .8f, .9f, 1f, 1.1f, 1.2f, 1.4f, 1.6f };
    private Rank[,] rankCurvesPrimary = {
            { new Rank(130, 12), new Rank(160,  9), new Rank(160,  9), new Rank(120, 21), new Rank( 70, 44), new Rank(60, 50), new Rank(50, 57), new Rank(30, 73) }
        ,   { new Rank(120, 13), new Rank(130, 12), new Rank(133, 11), new Rank(135, 11), new Rank(120, 17), new Rank(72, 43), new Rank(55, 53), new Rank(21, 80) }
        ,   { new Rank(130, 12), new Rank(140, 10), new Rank(140, 11), new Rank(110, 21), new Rank( 90, 32), new Rank(70, 43), new Rank(48, 56), new Rank(27, 73) }
        ,   { new Rank(130, 12), new Rank(120, 13), new Rank(135, 11), new Rank(120, 15), new Rank( 85, 33), new Rank(70, 40), new Rank(53, 51), new Rank(32, 69) }
        ,   { new Rank(120, 10), new Rank(128,  9), new Rank(130,  8), new Rank(130,  8), new Rank( 77, 30), new Rank(72, 33), new Rank(61, 40), new Rank(35, 61) }
        ,   { new Rank(120, 12), new Rank(125, 12), new Rank(117, 14), new Rank(118, 14), new Rank( 93, 23), new Rank(52, 49), new Rank(44, 55), new Rank(35, 62) }
        ,   { new Rank(110, 10), new Rank(130,  8), new Rank(145,  5), new Rank(110, 17), new Rank(100, 17), new Rank(75, 30), new Rank(44, 50), new Rank(31, 61) }
        ,   { new Rank(120, 11), new Rank(135, 10), new Rank(130, 11), new Rank(110, 16), new Rank( 85, 27), new Rank(70, 33), new Rank(60, 37), new Rank(35, 58) }
        ,   { new Rank(100, 12), new Rank(130,  9), new Rank(125, 10), new Rank(120, 11), new Rank( 77, 29), new Rank(67, 34), new Rank(43, 49), new Rank(31, 58) }
        ,   { new Rank(110,  9), new Rank(120,  8), new Rank(122,  8), new Rank(123,  8), new Rank( 80, 26), new Rank(75, 29), new Rank(55, 42), new Rank(44, 48) }
        ,   { new Rank(100,  9), new Rank(115,  9), new Rank(124,  7), new Rank(118,  8), new Rank(107, 11), new Rank(78, 26), new Rank(42, 48), new Rank(36, 53) }
        ,   { new Rank(110, 11), new Rank(120, 10), new Rank(115, 12), new Rank(102, 17), new Rank( 91, 21), new Rank(37, 49), new Rank(40, 48), new Rank(40, 48) }
        ,   { new Rank(100,  9), new Rank(122,  9), new Rank(140,  6), new Rank(135,  8), new Rank( 83, 29), new Rank(40, 51), new Rank(30, 57), new Rank(25, 62) }
        ,   { new Rank(110, 10), new Rank(122,  9), new Rank(130,  7), new Rank( 98, 16), new Rank( 83, 22), new Rank(45, 43), new Rank(44, 45), new Rank(33, 54) }
        ,   { new Rank(110,  8), new Rank(105,  9), new Rank(104, 11), new Rank(102, 13), new Rank( 93, 16), new Rank(87, 18), new Rank(51, 40), new Rank(25, 60) }
        ,   { new Rank(115,  9), new Rank(127,  9), new Rank(121, 11), new Rank(108, 15), new Rank( 86, 23), new Rank(68, 32), new Rank(41, 48), new Rank(24, 62) }
        ,   { new Rank(114, 10), new Rank(118,  9), new Rank(114, 10), new Rank( 95, 16), new Rank( 82, 22), new Rank(71, 28), new Rank(37, 49), new Rank(30, 55) }
        ,   { new Rank(112, 10), new Rank(115, 10), new Rank(111, 10), new Rank(103, 13), new Rank( 83, 21), new Rank(48, 39), new Rank(39, 45), new Rank(25, 57) }
        ,   { new Rank(100, 10), new Rank(108, 10), new Rank(115,  9), new Rank(108, 11), new Rank( 83, 21), new Rank(55, 35), new Rank(31, 51), new Rank(24, 57) }
        ,   { new Rank(100,  9), new Rank(111,  8), new Rank(112,  9), new Rank( 87, 17), new Rank( 53, 32), new Rank(45, 37), new Rank(39, 42), new Rank(34, 47) }
        ,   { new Rank(100, 10), new Rank(108,  9), new Rank(114,  8), new Rank(106, 11), new Rank( 63, 29), new Rank(45, 39), new Rank(33, 47), new Rank(26, 53) }
        ,   { new Rank(100,  8), new Rank(110,  7), new Rank(127,  4), new Rank( 77, 20), new Rank( 50, 31), new Rank(41, 36), new Rank(40, 37), new Rank(31, 46) }
        ,   { new Rank(100,  9), new Rank(102,  9), new Rank(101, 10), new Rank( 88, 15), new Rank( 70, 21), new Rank(57, 28), new Rank(45, 35), new Rank(24, 53) }
        ,   { new Rank(100,  9), new Rank(100,  9), new Rank(107,  8), new Rank( 85, 14), new Rank( 77, 18), new Rank(60, 25), new Rank(30, 44), new Rank(24, 50) }
        ,   { new Rank( 95,  8), new Rank( 90,  9), new Rank( 88, 12), new Rank( 85, 13), new Rank( 62, 22), new Rank(52, 29), new Rank(39, 38), new Rank(18, 55) }
        ,   { new Rank( 80,  7), new Rank( 85,  7), new Rank(115,  1), new Rank( 92,  8), new Rank( 78, 13), new Rank(64, 20), new Rank(27, 42), new Rank(21, 46) }
        ,   { new Rank( 72,  6), new Rank( 69,  7), new Rank( 76,  6), new Rank( 77,  6), new Rank( 68, 10), new Rank(50, 19), new Rank(22, 36), new Rank(21, 37) }
        ,   { new Rank( 70,  6), new Rank( 53,  9), new Rank( 63,  8), new Rank( 70,  6), new Rank( 69,  7), new Rank(58, 13), new Rank(28, 31), new Rank(20, 37) }
        ,   { new Rank( 70,  5), new Rank( 70,  6), new Rank( 70,  7), new Rank( 71,  7), new Rank( 67,  9), new Rank(48, 18), new Rank(16, 38), new Rank(16, 38) }
        ,   { new Rank( 65,  5), new Rank( 63,  6), new Rank( 76,  4), new Rank( 61,  9), new Rank( 49, 14), new Rank(36, 20), new Rank(28, 24), new Rank(20, 30) }
    };

    private void LogStats(Player _player) {
        Debug.Log($""
        +   $"lv({_player.points.level})"
        +   $", <color=#f08261>hp({_player.points.hpMaxBase})</color>"
        +   $", <color=#f08261>hp({_player.points.hpMaxTotal})</color>"
        +   $", <color=#fabe75>mp({_player.points.mpMaxBase})</color>"
        +   $", <color=#fabe75>mp({_player.points.mpMaxTotal})</color>"
        +   $", <color=#fff280>str({_player.primary.strength})</color>"
        +   $", <color=#c3da82>vit({_player.primary.vitality})</color>"
        +   $", <color=#6dbc83>mag({_player.primary.magic})</color>"
        +   $", <color=#62c0bc>spr({_player.primary.spirit})</color>"
        +   $", <color=#56c5f2>dex({_player.primary.dexterity})</color>"
        +   $", <color=#6e9dd1>lck({_player.primary.luck})</color>"
        );
    }

    private int GetLevelBracket(Player _player) {
        int i = 0;
        int level = _player.points.level;

        if (level >= 2)
            i = 0;
        if (level >= 12)
            i = 1;
        if (level >= 22)
            i = 2;
        if (level >= 32)
            i = 3;
        if (level >= 42)
            i = 4;
        if (level >= 52)
            i = 5;
        if (level >= 62)
            i = 6;
        if (level >= 82)
            i = 7;

        return i;
    }

#region Exp and level increases.
    public void RunInitialLevelUp(Player _newPlayer) {
        GetExperiencePointsStatistics(_newPlayer);

        int exp = _newPlayer.points.exp;
        _newPlayer.points.exp = 0;

        AddExp(_newPlayer, exp);
    }

    public void AddExp(Player _player, int _exp) {
        GetExperiencePointsStatistics(_player);

        int targetExp = _player.points.exp + _exp;

        Debug.Log($"{_player.name} gains {_exp}p");

        while (_player.points.exp < targetExp) {
            if (++_player.points.exp >= _player.points.expToNextLevelTotal) {
                LevelUp(_player);
            }
        }
    }

    private void LevelUp(Player _player) {
        _player.points.level++;

        int baseline = GetLevelBracket(_player);
        PrimaryStats primary = _player.primary;

        IncrementPrimaryBase(_player, rankCurvesPrimary[statGrades.strength, baseline] , ref primary.strength);
        IncrementPrimaryBase(_player, rankCurvesPrimary[statGrades.vitality, baseline] , ref primary.vitality);
        IncrementPrimaryBase(_player, rankCurvesPrimary[statGrades.magic, baseline]    , ref primary.magic);
        IncrementPrimaryBase(_player, rankCurvesPrimary[statGrades.spirit, baseline]   , ref primary.spirit);
        IncrementPrimaryBase(_player, rankCurvesPrimary[statGrades.dexterity, baseline], ref primary.dexterity);
        IncrementPrimaryBase(_player, rankCurvesLuck[baseline]                         , ref primary.luck);
        IncrementHP(_player);
        IncrementMP(_player);

        GetExperiencePointsStatistics(_player);
        _player.RecalculatePlayerStats();

        LogStats(_player);
    }

    public void GetExperiencePointsStatistics(Player _player) {
        int modifier = expModifiers[GetLevelBracket(_player)];

        //Debug.Log("mod: " + modifier);

        _player.points.expToNextLevelTotal = 0;
        for (int i = 1; i < _player.points.level + 1; i++) {
            _player.points.expToNextLevelTotal += modifier * ((int)Mathf.Pow(i, 2)) / 10;
        }

        //Debug.Log("total xp to reach lv" + (_player.points.level + 1) + ":" + _player.points.expToNextLevelTotal);
        //Debug.Log("total xp to reach lv" + (_player.points.level + 1) + ":" + _player.points.expToNextLevelLocal);

        // to get to lv from lv-1.
        //experiencePointsToNextLevel = modifier * (int)Mathf.Pow(level - 1, 2) / 10;
        //Debug.Log("xp to reach lv" + level + ": " + experiencePointsToNextLevel);

        // to get to lv+1.
        _player.points.expToNextLevelLocal = modifier * (int)Mathf.Pow(_player.points.level, 2) / 10;
        //Debug.Log("xp to reach lv" + (level + 1) + ": " + experiencePointsToNextLevelLocal);
    }
#endregion

#region Primary stat increases.
    void IncrementPrimaryBase(Player _player, Rank _rank, ref int _currentStat) {
        int baseline = _rank.valueBase + (_rank.valueGradient * _player.points.level / 100);
        int difference = UnityEngine.Random.Range(1, 8) + baseline - _currentStat;
        //Debug.Log(difference);

        _currentStat += GetStatGain(difference);
        _currentStat = Mathf.Clamp(_currentStat, 0, 100);
    }

    private int GetStatGain(int _difference) {
        int i = 0;

        if (_difference >= 4)
            i = 1;
        if (_difference >= 7)
            i = 2;
        if (_difference >= 10)
            i = 3;

        return i;
    }
#endregion

#region HP/MP increases.
    private void IncrementHP(Player _player) {
        int b = rankCurvesHP[GetLevelBracket(_player)].valueBase;
        int g = rankCurvesHP[GetLevelBracket(_player)].valueGradient;

        int baseline = b + (_player.points.level - 1) * g;

        int r = UnityEngine.Random.Range(1, 8);

        int difference = r + (100 * baseline / _player.points.mpMaxBase) - 100;
        int cappedDifference = Mathf.Clamp(difference, 0, 11);

        _player.points.hpMaxBase += (int)(gradeMultipliersHP[cappedDifference] * g);

        //Debug.Log("Dif: " + difference + " | Cap: " + cappedDifference + " | Mul: " + gradeMultipliersHP[cappedDifference] + " | Gain: " + (int)gradeMultipliersHP[cappedDifference] * g);
    }

    private void IncrementMP(Player _player) {
        int b = rankCurvesMP[GetLevelBracket(_player)].valueBase;
        int g = rankCurvesMP[GetLevelBracket(_player)].valueGradient;

        int baseline = b + ((_player.points.level - 1) * g / 10);

        int r = UnityEngine.Random.Range(1, 8);
        int difference = r + (100 * baseline / _player.points.mpMaxBase) - 100;
        int cappedDifference = Mathf.Clamp(difference, 0, 11);

        int baseGain = (_player.points.level * g / 10) - ((_player.points.level - 1) * g / 10);

        _player.points.mpMaxBase += (int)(gradeMultipliersMP[cappedDifference] * baseGain);
    }
#endregion
}
