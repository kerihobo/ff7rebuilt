using InventoryTypes;
using TMPro;
using UnityEngine;

public class EquipMenuOptionList : MenuControllable {
    public FingerCursorRight cursor;
    public EquipMenuItemList equipMenuItemList;

    public RectTransform rct { get; private set; }
    public ScreenFieldEquip screenFieldEquip { get; set; }

    [SerializeField] private RectTransform optionsParent;
    /// <summary>
    /// Weapon: 0<br/>
    /// Armor: 1<br/>
    /// Accessory: 2<br/>
    /// </summary>
    private TextMeshProUGUI[] arrOptions;
    private int selectedIndex;
    
    public TextMeshProUGUI txtOptionWeapon => arrOptions[0];
    public TextMeshProUGUI txtOptionArmor => arrOptions[1];
    public TextMeshProUGUI txtOptionAccessory => arrOptions[2];

    public void Initialize(ScreenFieldEquip _screenFieldEquip) {
        screenFieldEquip = _screenFieldEquip;

        rct = GetComponent<RectTransform>();

        arrOptions = new TextMeshProUGUI[optionsParent.childCount];
        for (int i = 0; i < arrOptions.Length; i++) {
            arrOptions[i] = optionsParent.GetChild(i).GetChild(0).GetComponent<TextMeshProUGUI>();
        }

        cursor.Initialize(txtOptionWeapon.rectTransform);
        equipMenuItemList.Initialize(this);
    }

    public void SetDefaultDisplay() {
        //ShowWeapons();
        // Hide item list
        ResetSelection();
    }

    private void EnterWeaponSelection() {
        WeaponType weaponType = screenFieldEquip.GetSelectedCharacter.selectionStartingWeapon.type;
        equipMenuItemList.Open(EquipMenuItemList.ItemType.WEAPON, weaponType);
    }

    private void EnterArmorSelection() {
        equipMenuItemList.Open(EquipMenuItemList.ItemType.ARMOR);
    }

    private void EnterAccessorySelection() {
        equipMenuItemList.Open(EquipMenuItemList.ItemType.ACCESSORY);
    }

#region Override methods.
    public override void GetInputConfirmPressed() {
        cursor.SetFlickerState(true);

        switch (selectedIndex) {
            case 0: // Weapon
                EnterWeaponSelection();
                break;
            case 1: // Armor
                EnterArmorSelection();
                break;
            case 2: // Accessory
                EnterAccessorySelection();
                break;
            default:
                break;
        }
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
    
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputTrianglePressed() { }

    public override void GetInputSquarePressed() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.SwitchScreens(3));
    }

    public override void GetInputPage(int pageDirection, bool isPageXPressed) {
        screenFieldEquip.CycleCharacter(pageDirection);
        SetCursor();
    }

    public override void GetInputCancelPressed() {
        if (screenFieldEquip.isChanged) {
            GetInputSquarePressed();
            return;
        }

        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }
    
    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
        Refresh();
    }
#endregion

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void ExitSubMenu() {
        Open();
        FieldMenu.Instance.SetCooldownNavigate(FieldMenu.COOLDOWN_MAIN);
    }

    public void Refresh() {
        CharacterTypes.Player selectedCharacter = screenFieldEquip.GetSelectedCharacter;

        if (selectedCharacter == null) return;
        
        Weapon weapon = selectedCharacter.weapon;
        Armor armor = selectedCharacter.armor;
        Accessory accessory = selectedCharacter.accessory;
        txtOptionWeapon.text    = weapon?.name;
        txtOptionArmor.text     = armor?.name;
        txtOptionAccessory.text = accessory?.name;

        switch (selectedIndex) {
            case 0:
                screenFieldEquip.RefreshInfoWeapon(weapon);
                break;
            case 1:
                screenFieldEquip.RefreshInfoArmor(armor);
                break;
            default:
                screenFieldEquip.RefreshInfoAccessory(accessory);
                break;
        }
    }

    public void Open() {
        cursor.SetFlickerState(false);
        Refresh();
        screenFieldEquip.ToggleStatInfoChangedText(false);
        SetControllable();
        SetCursor();
    }
}
