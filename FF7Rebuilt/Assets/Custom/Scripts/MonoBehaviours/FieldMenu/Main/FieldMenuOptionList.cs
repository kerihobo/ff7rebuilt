using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FieldMenuOptionList : MenuControllable {
    public const float TITLE_BORDER_HEIGHT = 50.55f;
    
    public FingerCursorRight cursor;

    public RectTransform rct { get; private set; }

    [SerializeField] private RectTransform optionsParent;
    private int selectedIndex;
    private VerticalLayoutGroup vlgOptions;
    private TextMeshProUGUI txtSelected;
    private FieldMenu fieldMenu;
    private ScreenFieldMain screenFieldMain;
    private Vector2 positionOption;
    private Vector2 positionTitle;
    private Vector2 sizeOption;
    private Vector2 sizeTitle;
    /// <summary>
    /// Item: 0<br/>
    /// Magic: 1<br/>
    /// Materia: 2<br/>
    /// Equip: 3<br/>
    /// Status: 4<br/>
    /// Order: 5<br/>
    /// Limit: 6<br/>
    /// Config: 7<br/>
    /// PHS: 8<br/>
    /// Save: 9<br/>
    /// Quit: 10<br/>
    /// </summary>
    private TextMeshProUGUI[] arrOptions;

    // TODO: Make these point to dynamic data at some point.
    public TextMeshProUGUI txtOptionItem => arrOptions[0];
    public TextMeshProUGUI txtOptionMateria => arrOptions[2];
    public TextMeshProUGUI txtOptionPHS => arrOptions[8];
    public TextMeshProUGUI txtOptionSave => arrOptions[9];

    public void Initialize(FieldMenu _fieldMenu, ScreenFieldMain _screenFieldMain) {
        fieldMenu = _fieldMenu;
        screenFieldMain = _screenFieldMain;
        
        rct = GetComponent<RectTransform>();
        vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        sizeOption = rct.sizeDelta;
        sizeTitle = sizeOption;
        sizeTitle.y = TITLE_BORDER_HEIGHT;

        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        positionTitle = arrOptions[0].rectTransform.anchoredPosition;
        cursor.Initialize(arrOptions[0].rectTransform);

        //characterSelect = _screenFieldMain.characterSelect;
        //characterSelect.Initialize(_fieldMenu, this);
    }

    public void RefreshOptions() {
        txtOptionMateria.enabled = GameManager.Instance.gameData.IsMateriaUnlocked;
        txtOptionPHS.enabled = GameManager.Instance.gameData.IsPHSUnlocked;

        Color saveDependentOptionColor = GameManager.Instance.IsSaveAvailable ? TextColourContainer.GetInstance().white : TextColourContainer.GetInstance().grey;
        txtOptionPHS.color = saveDependentOptionColor;
        txtOptionSave.color = saveDependentOptionColor;
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void ShowOnlySelected(int _selected) {
        foreach (TextMeshProUGUI txt in arrOptions) {
            txt.enabled = false;
        }

        txtSelected = arrOptions[_selected];
        txtSelected.enabled = true;
        
        vlgOptions.enabled = false;
    }

    public void ShowAll() {
        vlgOptions.enabled = true;

        foreach (TextMeshProUGUI txt in arrOptions) {
            txt.enabled = true;
        }

        RefreshOptions();
    }

    public void ProcessScreenTransition(float _t) {
        rct.sizeDelta = Vector2.Lerp(sizeOption, sizeTitle, _t);
        txtSelected.rectTransform.anchoredPosition = Vector2.Lerp(positionOption, positionTitle, _t);
    }

    private void EnterCharacterSelection(int selectedIndex) {
        cursor.SetFlickerState(true);
        screenFieldMain.characterSelect.Open(selectedIndex);
    }

    public void ExitCharacterSelection() {
        cursor.SetFlickerState(false);
        SetControllable();
    }

    public void ResetDisplay() {
        txtSelected.rectTransform.anchoredPosition = positionOption;
        rct.sizeDelta = sizeOption;

        cursor.Show();

        ShowAll();
    }

    public void SwapTextSelected(int _i) {
        txtSelected.rectTransform.anchoredPosition = positionOption;

        selectedIndex = _i;
        txtSelected = arrOptions[selectedIndex];

        positionOption = txtSelected.rectTransform.anchoredPosition;
        SetCursor();
        txtSelected.rectTransform.anchoredPosition = positionTitle;

        ShowOnlySelected(selectedIndex);
    }

#region Override methods
    public override void GetInputConfirmPressed() {
        txtSelected = arrOptions[selectedIndex];

        Debug.Log(txtSelected.text);
        if (!txtSelected.enabled) {
            return;
        }

        positionOption = txtSelected.rectTransform.anchoredPosition;

        switch (selectedIndex) {
            case 1: // Magic
            case 2: // Materia
            case 3: // Equip
            case 4: // Status
            case 5: // Order
            case 6: // Limit
                EnterCharacterSelection(selectedIndex);
                break;
            case 0: // Item
            case 7: // Config
            case 8: // PHS
            case 9: // Save
                fieldMenu.StartCoroutine(fieldMenu.OpenSelectedScreen(selectedIndex));
                break;
            case 10: // Quit
                fieldMenu.OpenSelectedScreenQuick(selectedIndex);
                break;
            default:
                break;
        }
    }

    public override void GetInputCancelPressed() {
        fieldMenu.StartCoroutine(fieldMenu.Close());
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
