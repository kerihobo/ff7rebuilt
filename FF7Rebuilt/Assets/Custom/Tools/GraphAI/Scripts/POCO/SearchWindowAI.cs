﻿using System.Collections.Generic;

public class SearchWindowAI : SearchWindow {
    public SearchWindowAI(NodeGraph _selectedNodeGraph) : base(_selectedNodeGraph) {
    }

    protected override void Populate() {
        dictSearchEntries = new Dictionary<string, SearchEntry>() {
            { "Print"       , new SearchEntry("Print"                   , () => AddNode<NodePrint>      (Node.Category.None)) },
            { "Bool"        , new SearchEntry("Input/Bool"              , () => AddNode<NodeBool>       (Node.Category.Input))},
            { "Int"         , new SearchEntry("Input/Int"               , () => AddNode<NodeInt>        (Node.Category.Input))},
            { "And"         , new SearchEntry("Logic/And"               , () => AddNode<NodeAnd>        (Node.Category.Logic))},
            { "Comparison"  , new SearchEntry("Logic/Comparison"        , () => AddNode<NodeComparison> (Node.Category.Logic))},
            { "If"          , new SearchEntry("Logic/If"                , () => AddNode<NodeIf>         (Node.Category.Logic))},
            { "Not"         , new SearchEntry("Logic/Not"               , () => AddNode<NodeNot>        (Node.Category.Logic))},
            { "Or"          , new SearchEntry("Logic/Or"                , () => AddNode<NodeOr>         (Node.Category.Logic))},
            { "Absolute"    , new SearchEntry("Maths/Advanced/Absolute" , () => AddNode<NodeAbsolute>   (Node.Category.Maths))},
            { "Modulo"      , new SearchEntry("Maths/Advanced/Modulo"   , () => AddNode<NodeModulo>     (Node.Category.Maths))},
            { "Add"         , new SearchEntry("Maths/Basic/Add"         , () => AddNode<NodeAdd>        (Node.Category.Maths))},
            { "Divide"      , new SearchEntry("Maths/Basic/Divide"      , () => AddNode<NodeDivide>     (Node.Category.Maths))},
            { "Multiply"    , new SearchEntry("Maths/Basic/Multiply"    , () => AddNode<NodeMultiply>   (Node.Category.Maths))},
            { "Subtract"    , new SearchEntry("Maths/Basic/Subtract"    , () => AddNode<NodeSubtract>   (Node.Category.Maths))},
            { "Clamp"       , new SearchEntry("Maths/Range/Clamp"       , () => AddNode<NodeClamp>      (Node.Category.Maths))},
            { "Max"         , new SearchEntry("Maths/Range/Max"         , () => AddNode<NodeMax>        (Node.Category.Maths))},
            { "Min"         , new SearchEntry("Maths/Range/Min"         , () => AddNode<NodeMin>        (Node.Category.Maths))},
            { "Random Range", new SearchEntry("Maths/Range/Random Range", () => AddNode<NodeRandomRange>(Node.Category.Maths))}
        };
    }
}
