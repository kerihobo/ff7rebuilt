using CharacterTypes;
using InventoryTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ScreenFieldMagic : ScreenField {
    public UICharacterInfo characterInfo;
    
    public override FieldMenu fieldMenu { get; set; }

    [SerializeField] private MagicMenuInfoWindow infoWindow;
    [SerializeField] private TextMeshProUGUI txtDescription;
    [SerializeField] private MagicMenuOptions magicMenuOptions;
    [SerializeField] private MagicMenuMagicList listMagic;
    [SerializeField] private MagicMenuSummonList listSummon;
    [SerializeField] private MagicMenuEnemySkillList listEnemySkill;
    [SerializeField] private MagicMenuAddedAbility addedAbility;
    private List<Player> lsActivePlayers = new List<Player>();
    private int selectedCharacterIndex;

    public Player GetSelectedCharacter => lsActivePlayers.Count > selectedCharacterIndex ? lsActivePlayers[selectedCharacterIndex] : null;
    private Player GetInitiallySelectedCharacter => GameManager.Instance.gameData.phs.arrCurrent[fieldMenu.selectedCharacterIndex];

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        characterInfo.Initialize(Vector2.zero, Vector2.zero);
        magicMenuOptions.Initialize(this);
        listMagic.Initialize(this, magicMenuOptions);
        listSummon.Initialize(this, magicMenuOptions);
        listEnemySkill.Initialize(this, magicMenuOptions);

        // Initialize options list.
    }

    public override void SetDefaultDisplay() { }

    private void Refresh() {
        characterInfo.Refresh(GetSelectedCharacter);
    }

    public override IEnumerator Open() {
        lsActivePlayers = GameManager.Instance.gameData.phs.arrCurrent.Where(x => x != null).ToList();
        selectedCharacterIndex = lsActivePlayers.IndexOf(GetInitiallySelectedCharacter);

        Refresh();
        magicMenuOptions.ResetSelection();
        listMagic       .ResetSelection();
        listSummon      .ResetSelection();
        listEnemySkill  .ResetSelection();
        OpenOptions();

        yield return null;
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public void CycleCharacter(int _direction) {
        selectedCharacterIndex = (selectedCharacterIndex + _direction) % lsActivePlayers.Count;
        if (selectedCharacterIndex < 0) {
            selectedCharacterIndex = lsActivePlayers.Count - 1;
        }

        Refresh();
    }

    public void SetDescription(string _description) {
        Debug.Log(_description);
        txtDescription.text = _description;
    }

    public void OpenWindowMagic() {
        infoWindow.Show(GetSelectedCharacter);
        listMagic.Open();
    }

    public void SetInfoMagic(Ability _magic) {
        infoWindow.SetInfoMagic(_magic);
        addedAbility.SetInfoMagic(GetSelectedCharacter, _magic);
    }

    public void OpenWindowSummon() {
        infoWindow.Show(GetSelectedCharacter);
        listSummon.Open();
    }

    public void SetInfoSummon(Materia materiaReference, bool isMasterSummon) {
        infoWindow.SetInfoSummon(materiaReference, isMasterSummon);
        addedAbility.SetInfoSummon(GetSelectedCharacter, materiaReference);
    }

    public void OpenWindowEnemySkill() {
        infoWindow.Show(GetSelectedCharacter);
        listEnemySkill.Open();
    }

    public void SetInfoEnemySkill(Ability _enemySkill) {
        infoWindow.SetInfoEnemySkill(_enemySkill);
        addedAbility.SetInfoEnemySkill(_enemySkill);
    }

    private void CloseInfoWindow() {
        infoWindow.Hide();
    }

    public void OpenOptions() {
        CloseInfoWindow();
        magicMenuOptions.Open();
        SetDescription("");
    }
}
