﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[Serializable, CreateAssetMenu(fileName = "New Node Graph", menuName = "Custom/Node Graph")]
public class NodeGraph : ScriptableObject {
    public List<Node> nodes = new List<Node>();
    public NodeEntry nodeEntry;
    public Blackboard blackboard;

    public Vector2 nodeSpawnPosition { get; set; }
    public SearchWindow searchWindow { get; set; }
    public bool IsShowGrid { get { return isShowGrid; } }
    
    [HideInInspector]
    public bool isPersistent;

    private bool isShowGrid = true;

    public void Initialize() {
        nodeEntry = Node.Create<NodeEntry>(this, Vector2.zero);
        InitializeBlackboard();

        isPersistent = true;
    }

    protected virtual void InitializeBlackboard() {
        Blackboard.Create<Blackboard>(this);
    }

    protected virtual void InitializeSearchWindow() {
        searchWindow = new SearchWindow(this);
    }

    public void Process(Event _e, StyleSet _styleSet, ref Vector2 _pan, Rect _windowRect) {
        if (searchWindow == null) {
            InitializeSearchWindow();
        }

        if (isPersistent) {
            Render(_e, _styleSet, ref _pan, _windowRect);
        } else if (Event.current.type == EventType.Repaint && EditorUtility.IsPersistent(this)) {
            Initialize();
        }
    }

    private void Render(Event _e, StyleSet _styleSet, ref Vector2 _pan, Rect _windowRect) {
        foreach (Node node in nodes) {
            node.RenderEdges();
        }
        
        foreach (Node node in nodes) {
            node.RenderNode(_pan, _styleSet);
        }

        GUILayout.BeginHorizontal();
            blackboard.Render(_e, _styleSet, _pan);

            GUILayout.BeginVertical(_styleSet.buttonContainer);
                if (GUILayout.Button("Save")) {
                    AssetDatabase.SaveAssets();
                }

                if (GUILayout.Button((isShowGrid ? "Hide " : "Show ") + "Grid")) {
                    isShowGrid = !isShowGrid;
                }

                if (GUILayout.Button("Reset View")) {
                    _pan = _windowRect.size / 3;
                }

        //GUILayout.Label(_e.mousePosition.ToString("f0"), _styleSet.debugText);
        //GUILayout.Label(_pan.ToString("f0"), _styleSet.debugText);
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        searchWindow?.Render(_e, _styleSet);
    }

    public void DeleteNodes(List<Node> _selectedNodes) {
        for (int i = 0; i < _selectedNodes.Count; i++) {
            if (_selectedNodes[i].GetType() == typeof(NodeEntry)) continue;

            DeleteNode(_selectedNodes[i]);
        }
    }

    public void DeleteNode(Node _targetNode) {
        if (_targetNode == null) return;

        _targetNode.DisconnectPorts();

        nodes.Remove(_targetNode);

        // Do a little extra cleanup if we're deleting a Get/Set node.
        if (_targetNode.GetType().IsSubclassOf(typeof(NodeGetSet))) {
            NodeGetSet getset = (NodeGetSet)_targetNode;
            getset?.field.RemoveNode(getset);
            Debug.Log(getset?.name +" is totally a GetSet");
        }

        DestroyImmediate(_targetNode, true);

        EditorUtility.SetDirty(this);
    }

    public void Run() {
        nodeEntry.Run(this);
    }
}