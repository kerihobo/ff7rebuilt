using CharacterTypes;
using InventoryTypes;
using UnityEngine;

public class MateriaMenuSlotsMain : MenuControllable {
    public FingerCursorRight cursor;

    public UIMateriaSlotInfo[] arrSelected { get; set; }
    public ScreenFieldMateria screenFieldMateria { get; set; }
    public UIMateriaSlotInfo[] arrOptionsWeapon { get; set; }
    public UIMateriaSlotInfo[] arrOptionsArmor { get; set; }
    public int selectedIndex { get; set; }

    [SerializeField] private RectTransform slotsParentWeapon;
    [SerializeField] private RectTransform slotsParentArmor;
    [SerializeField] private MateriaMenuEquipmentContainer equipmentContainerWeapon;
    [SerializeField] private MateriaMenuEquipmentContainer equipmentContainerArmor;

    public bool IsWeapon => arrSelected == arrOptionsWeapon;
    
    private MateriaMenuMateriaList materiaList => screenFieldMateria.materiaList;
    private MateriaMenuCheck check => screenFieldMateria.check;

    private Player GetSelectedCharacter => screenFieldMateria.GetSelectedCharacter;

    public void Initialize(ScreenFieldMateria _screenFieldMateria) {
        screenFieldMateria = _screenFieldMateria;

        arrOptionsWeapon = slotsParentWeapon.GetComponentsInChildren<UIMateriaSlotInfo>();
        arrOptionsArmor = slotsParentArmor.GetComponentsInChildren<UIMateriaSlotInfo>();

        foreach (UIMateriaSlotInfo msi in arrOptionsWeapon) {
            msi.Initialize();
        }

        foreach (UIMateriaSlotInfo msi in arrOptionsArmor) {
            msi.Initialize();
        }

        equipmentContainerWeapon.Initialize();
        equipmentContainerArmor.Initialize();
        // Gotta refresh these when opening materia menu or switching characters

        arrSelected = arrOptionsWeapon;

        cursor?.Initialize(arrSelected[0].rct);
    }

    public void SetDefaultDisplay() {
        //ShowWeapons();
        // Hide item list
        ResetSelection();
    }

    public void ResetSelection() {
        selectedIndex = 0;
        arrSelected = arrOptionsWeapon;

        SetCursor();
    }

    public void SetCursor() {
        cursor?.SetCursorPosition(arrSelected[selectedIndex].rct);
        RefreshMateriaInfo();
    }

    public void ExitSubMenu() {
        Open();
        FieldMenu.Instance.SetCooldownNavigate(FieldMenu.COOLDOWN_MAIN);
    }

    public void RefreshEquipment() {
        equipmentContainerWeapon.Refresh(GetSelectedCharacter.weapon);
        equipmentContainerArmor.Refresh(GetSelectedCharacter.armor);
    }

    public void RefreshEquipment(Player _player) {
        equipmentContainerWeapon.Refresh(_player.weapon);
        equipmentContainerArmor.Refresh(_player.armor);
    }

    /// <summary>
    /// Refresh materia occupying a player's slots.
    /// </summary>
    public void RefreshMateria(Player _player) {
        _player ??= GetSelectedCharacter;
        if (_player == null) return;

        Weapon weapon = _player.weapon;
        Armor armor = _player.armor;

        for (int i = 0; i < arrOptionsWeapon.Length; i++) {
            arrOptionsWeapon[i].Clear();
            arrOptionsArmor[i].Clear();
            
            arrOptionsWeapon[i].Refresh(_player.materiaWeapon[i]);
            arrOptionsArmor[i].Refresh(_player.materiaArmor[i]);
        }
    }

    /// <summary>
    /// Refresh the information shown in the info panel.
    /// This includes a Materia's level, name, icon, abilities, stats etc.
    /// </summary>
    public void RefreshMateriaInfo() {
        screenFieldMateria.RefreshInfo(arrSelected[selectedIndex].materia);
    }

    public void Open() {
        RefreshEquipment();
        RefreshMateria(null);
        //screenFieldMateria.ToggleStatInfoChangedText(false);
        SetControllable();
        SetCursor();
        cursor?.Show();
        cursor?.SetFlickerState(false);
        materiaList.Hide();
        check.Hide();
        screenFieldMateria.ToggleMateriaView(true);
    }

    private void Disable() {
        cursor?.Hide();
        screenFieldMateria.RefreshInfo(null);
    }

    public void SetEquipmentSelected(int verticalIndex) {
        arrSelected = verticalIndex == 0 ? arrOptionsWeapon : arrOptionsArmor;
    }

    public void RemoveMateriaFromSlot(int _slotIndex, Materia[] _arrMateria, UIMateriaSlotInfo[] _arrInfo) {
        UIMateriaSlotInfo slotInfo = _arrInfo[_slotIndex];

        GameManager.Instance.gameData.inventory.AddMateria(_arrMateria[_slotIndex]);
        _arrMateria[_slotIndex] = null;

        slotInfo.Refresh(null);
    }

#region Override methods.
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }

    public override void GetInputSquarePressed() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.SwitchScreens(4));
    }

    public override void GetInputConfirmPressed() {
        int iSlotsWeapon = GetSelectedCharacter.weapon.materiaSlotCount;
        int iSlotsArmor = GetSelectedCharacter.armor.materiaSlotCount;
        int slotCount = IsWeapon ? iSlotsWeapon : iSlotsArmor;
        if (selectedIndex >= slotCount) return;

        cursor?.SetFlickerState(true);
        materiaList.Open(false);
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputTrianglePressed() {
        Materia[] arrMateria = IsWeapon
        ?   GetSelectedCharacter.materiaWeapon
        :   GetSelectedCharacter.materiaArmor
        ;

        RemoveMateriaFromSlot(selectedIndex, arrMateria, arrSelected);
        GetSelectedCharacter.RecalculatePlayerStats();
        screenFieldMateria.RefreshPlayerInfo();
        RefreshMateriaInfo();

        FieldMenu.Instance.SetCooldownTraverse(2 / 60f);
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (selectedIndex + _h == -1) {
            Disable();

            int optionsSelectedIndex = IsWeapon ? 0 : 1;
            screenFieldMateria.optionList.SetSelectedIndex(optionsSelectedIndex);

            screenFieldMateria.optionList.Open();

            return;
        }

        selectedIndex = Mathf.Clamp(selectedIndex + _h, 0, arrSelected.Length - 1);

        SetCursor();
        //RefreshMateria();
        screenFieldMateria.RefreshInfo(arrSelected[selectedIndex].materia);
    }

    public override void GetInputPage(int pageDirection, bool isPageXPressed) {
        screenFieldMateria.CycleCharacter(pageDirection);
        SetCursor();
    }

    public override void GetInputCancelPressed() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        arrSelected = _v < 0 ? arrOptionsArmor : arrOptionsWeapon;

        SetCursor();
        //RefreshMateria();
        screenFieldMateria.RefreshInfo(arrSelected[selectedIndex].materia);
    }
#endregion
}
