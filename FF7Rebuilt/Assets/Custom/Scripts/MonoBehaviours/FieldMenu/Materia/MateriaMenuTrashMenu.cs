using TMPro;
using UnityEngine;

public class MateriaMenuTrashMenu : MenuControllable {
    public FingerCursorRight cursor;
    
    [SerializeField] private RectTransform optionsParent;
    private int selectedIndex;
    private ScreenFieldMateria screenFieldMateria;
    private TextMeshProUGUI[] arrOptions;
    
    private MateriaMenuMateriaList materiaList => screenFieldMateria.materiaList;
    private MateriaMenuArrangeMenu arrangeMenu => screenFieldMateria.arrangeMenu;

    public void Initialize(ScreenFieldMateria _screenFieldMateria) {
        screenFieldMateria = _screenFieldMateria;
        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        cursor.Initialize(arrOptions[0].rectTransform);

        Disable();
    }

    public void Open() {
        gameObject.SetActive(true);
        SetControllable();
        ResetSelection();
        arrangeMenu.Disable();
    }

    private void Disable() {
        gameObject.SetActive(false);
    }
    
    private void Close() {
        Disable();
        materiaList.Open(true);
        arrangeMenu.Enable();
    }

    public void ResetSelection() {
        selectedIndex = 1;
        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputConfirmPressed() {
        if (selectedIndex == 0) {
            materiaList.TrashSelectedMateria();
        }

        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }
#endregion
}
