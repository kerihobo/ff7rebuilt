﻿using System;
using FFVII.Database;

[Serializable]
public class SelectionFormation : SelectionData {
    public SelectionFormation(FormationType _type) {
        type = _type;
    }

    public FormationType type = FormationType.Normal;

    public Formation GetSelectedFormation() {
        if (DBResources.GetFormationsList(type) != null && DBResources.GetFormationsList(type).Count > index) {
            return DBResources.GetFormationsList(type)[index];
        }

        return null;
    }
}