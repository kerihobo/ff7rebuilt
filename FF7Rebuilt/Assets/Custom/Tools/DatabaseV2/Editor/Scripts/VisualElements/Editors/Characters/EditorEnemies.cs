﻿using CharacterTypes;
using FFVII.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorEnemies : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Characters/editor-enemies.uxml";
    protected override Database database { get; set; } = DBResources.GetEnemies;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetEnemies.enemyList[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetEnemies.enemyList;

    private (Button, ElementalAffinityManager.Affinity)[] arrPairElementButtons;
    private (Button, StatusEffectManager.StatusEffect)[] arrPairStatusResistanceButtons;
    private SelectionCollectibleEditor slCMorph;
    private EnemyDropItemEditor[] slcDropItems = new EnemyDropItemEditor[4];
    private EnemyLsStandardAbilities enemyLsStandardAbilities;
    private EnemyLsUniqueAbilities enemyLsUniqueAbilities;

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Enemy)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        if (target == null) return;

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<Button>("btn-ai").RegisterCallback<ClickEvent>(e => {
            // HELP
            // TODO: Make a UIElements version of the graph.
            Debug.LogError("Are we making a UIElements version of this or no?");
            EWGraphAI.Open(((Enemy)target).aiGraph);
        });

        ve.Q<ObjectField>("anim-battle-prefab").SetAnimator(((Enemy)target).battlePrefab).RegisterValueChangedCallback(e => {
            ((Enemy)target).battlePrefab = (Animator)e.newValue;
            EditorUtility.SetDirty(database);
        });
        
        ve.Q<SliderInt>("sld-level").SetValue(((Enemy)target).points.level).RegisterValueChangedCallback(e => {
            ((Enemy)target).points.level = e.newValue;
            EditorUtility.SetDirty(database);
        });

        BindStats();
        BindResistanceElements();
        BindResistanceStatuses();

        BindDropItems();

        slCMorph = ve.Q<SelectionCollectibleEditor>("slct-collectible-morph");
        slCMorph.Initialize(((Enemy)target).morphItem, database);

        enemyLsStandardAbilities = ve.Q<EnemyLsStandardAbilities>().Initialize((Enemy)target, database);
        ve.Q<Button>("btn-add-standard-ability").clicked += enemyLsStandardAbilities.Add;

        enemyLsUniqueAbilities = ve.Q<EnemyLsUniqueAbilities>().Initialize((Enemy)target, database);
        ve.Q<Button>("btn-add-unique-ability").clicked += enemyLsUniqueAbilities.Add;

        void BindStats() {
            GroupBox stats = ve.Q<GroupBox>("grp-stats");

            stats.Q<IntegerField>("int-exp").SetValue(((Enemy)target).points.exp).RegisterValueChangedCallback(e => {
                ((Enemy)target).points.exp = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-ap").SetValue(((Enemy)target).ap).RegisterValueChangedCallback(e => {
                ((Enemy)target).ap = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-gil").SetValue(((Enemy)target).gil).RegisterValueChangedCallback(e => {
                ((Enemy)target).gil = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-hp").SetValue(((Enemy)target).points.hpMaxBase).RegisterValueChangedCallback(e => {
                ((Enemy)target).points.hpMaxBase = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-mp").SetValue(((Enemy)target).points.mpMaxTotal).RegisterValueChangedCallback(e => {
                ((Enemy)target).points.mpMaxBase = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-atk").SetValue(((Enemy)target).derived.attack).RegisterValueChangedCallback(e => {
                ((Enemy)target).derived.attack = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-matk").SetValue(((Enemy)target).derived.magicAttack).RegisterValueChangedCallback(e => {
                ((Enemy)target).derived.magicAttack = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-def").SetValue(((Enemy)target).derived.defense).RegisterValueChangedCallback(e => {
                ((Enemy)target).derived.defense = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-def-percent").SetValue(((Enemy)target).derived.defensePercent).RegisterValueChangedCallback(e => {
                ((Enemy)target).derived.defensePercent = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-mdef").SetValue(((Enemy)target).derived.magicDefense).RegisterValueChangedCallback(e => {
                ((Enemy)target).derived.magicDefense = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-dex").SetValue(((Enemy)target).primary.dexterity).RegisterValueChangedCallback(e => {
                ((Enemy)target).primary.dexterity = e.newValue;
                EditorUtility.SetDirty(database);
            });

            stats.Q<IntegerField>("int-lck").SetValue(((Enemy)target).primary.luck).RegisterValueChangedCallback(e => {
                ((Enemy)target).primary.luck = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindResistanceElements() {
            GroupBox elementalVulnerabilities = ve.Q<GroupBox>("grp-elemental-vulnerabilities");

            RestockButtonArrayElements();

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                int j = i;
                arrPairElementButtons[j].Item1.RegisterCallback<ClickEvent>(e => {
                    arrPairElementButtons[j].Item1.CycleElementalVulnerabilityEnemies(ref arrPairElementButtons[j].Item2);
                    EditorUtility.SetDirty(database);
                });
            }
        }

        void BindResistanceStatuses() {
            GroupBox statusResistances = ve.Q<GroupBox>("grp-immunities");

            RestockButtonArrayStatusResistance();

            for (int i = 0; i < arrPairStatusResistanceButtons.Length; i++) {
                int j = i;
                arrPairStatusResistanceButtons[j].Item1.RegisterCallback<ClickEvent>(e => {
                    arrPairStatusResistanceButtons[j].Item1.ToggleAfflictionBehaviour(ref arrPairStatusResistanceButtons[j].Item2);
                    EditorUtility.SetDirty(database);
                });
            }
        }
        
        void BindDropItems() {
            for (int i = 0; i < 4; i++) {
                int j = i;
                slcDropItems[j] = ve.Q<EnemyDropItemEditor>($"drp-item-{j}");
                slcDropItems[j].Initialize((Enemy)target, database, j);
            }
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = target.name;
        ve.Q<ObjectField>("anim-battle-prefab").value = ((Enemy)target).battlePrefab;
        ve.Q<SliderInt>("sld-level").value = ((Enemy)target).points.level;

        RefreshStats();
        RefreshElementalVulnerabilities();
        RefreshImmunities();

        RefreshDropItems();

        slCMorph?.Rebuild(((Enemy)target).morphItem);

        enemyLsStandardAbilities?.Refresh((Enemy)target);
        enemyLsUniqueAbilities?.Refresh((Enemy)target);

        void RefreshStats() {
            GroupBox stats = ve.Q<GroupBox>("grp-stats");

            stats.Q<IntegerField>("int-exp").value = ((Enemy)target).points.exp;
            stats.Q<IntegerField>("int-ap").value = ((Enemy)target).ap;
            stats.Q<IntegerField>("int-gil").value = ((Enemy)target).gil;
            stats.Q<IntegerField>("int-hp").value = ((Enemy)target).points.hpMaxBase;
            stats.Q<IntegerField>("int-mp").value = ((Enemy)target).points.mpMaxBase;
            stats.Q<IntegerField>("int-atk").value = ((Enemy)target).derived.attack;
            stats.Q<IntegerField>("int-matk").value = ((Enemy)target).derived.magicAttack;
            stats.Q<IntegerField>("int-def").value = ((Enemy)target).derived.defense;
            stats.Q<IntegerField>("int-def-percent").value = ((Enemy)target).derived.defensePercent;
            stats.Q<IntegerField>("int-mdef").value = ((Enemy)target).derived.magicDefense;
            stats.Q<IntegerField>("int-dex").value = ((Enemy)target).primary.dexterity;
            stats.Q<IntegerField>("int-lck").value = ((Enemy)target).primary.luck;
        }

        void RefreshElementalVulnerabilities() {
            GroupBox elementalVulnerabilities = ve.Q<GroupBox>("grp-elemental-vulnerabilities");

            RestockButtonArrayElements();

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                arrPairElementButtons[i].Item1.SetElementalVulnerabilityClass(arrPairElementButtons[i].Item2, false);
            }
        }

        void RefreshImmunities() {
            GroupBox statusResistances = ve.Q<GroupBox>("grp-immunities");

            RestockButtonArrayStatusResistance();

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                arrPairStatusResistanceButtons[i].Item1.SetAfflictionBehaviourClass(arrPairStatusResistanceButtons[i].Item2.isActive, StatusEffectManager.StatusEffectAction.AFFLICT);
            }
        }
        
        void RefreshDropItems() {
            for (int i = 0; i < 4; i++) {
                int j = i;
                slcDropItems[j]?.Refresh((Enemy)target);
            }
        }
    }

    void RestockButtonArrayElements() {
        VisualElement resistanceElements = ve.Q<VisualElement>("ve-elemental-vulnerabilities");
        List<Button> lsButtons = resistanceElements.Query<Button>().ToList();

        arrPairElementButtons = new (Button, ElementalAffinityManager.Affinity)[] {
                (lsButtons[0],  ((Enemy)target).elementalDefense.fire)
            ,   (lsButtons[1],  ((Enemy)target).elementalDefense.ice)
            ,   (lsButtons[2],  ((Enemy)target).elementalDefense.lightning)
            ,   (lsButtons[3],  ((Enemy)target).elementalDefense.earth)
            ,   (lsButtons[4],  ((Enemy)target).elementalDefense.poison)
            ,   (lsButtons[5],  ((Enemy)target).elementalDefense.gravity)
            ,   (lsButtons[6],  ((Enemy)target).elementalDefense.water)
            ,   (lsButtons[7],  ((Enemy)target).elementalDefense.wind)
            ,   (lsButtons[8],  ((Enemy)target).elementalDefense.holy)
            ,   (lsButtons[9],  ((Enemy)target).elementalDefense._10th)
            ,   (lsButtons[10], ((Enemy)target).elementalDefense.restorative)
            ,   (lsButtons[11], ((Enemy)target).elementalDefense.backAttack)
            ,   (lsButtons[12], ((Enemy)target).elementalDefense.punch)
            ,   (lsButtons[13], ((Enemy)target).elementalDefense.cut)
            ,   (lsButtons[14], ((Enemy)target).elementalDefense.hit)
            ,   (lsButtons[15], ((Enemy)target).elementalDefense.shoot)
            ,   (lsButtons[16], ((Enemy)target).elementalDefense.shout)
            ,   (lsButtons[17], ((Enemy)target).elementalDefense.death)
            ,   (lsButtons[18], ((Enemy)target).elementalDefense.sleep)
            ,   (lsButtons[19], ((Enemy)target).elementalDefense.confusion)
            };
    }

    void RestockButtonArrayStatusResistance() {
        GroupBox statusResistances = ve.Q<GroupBox>("grp-immunities");
        List<Button> lsButtons = statusResistances.Query<Button>().ToList();

        arrPairStatusResistanceButtons = new (Button, StatusEffectManager.StatusEffect)[] {
                (lsButtons[0],  ((Enemy)target).statusDefense.death)
            ,   (lsButtons[1],  ((Enemy)target).statusDefense.sleep)
            ,   (lsButtons[2],  ((Enemy)target).statusDefense.poison)
            ,   (lsButtons[3],  ((Enemy)target).statusDefense.sadness)
            ,   (lsButtons[4],  ((Enemy)target).statusDefense.fury)
            ,   (lsButtons[5],  ((Enemy)target).statusDefense.confusion)
            ,   (lsButtons[6],  ((Enemy)target).statusDefense.silence)
            ,   (lsButtons[7],  ((Enemy)target).statusDefense.haste)
            ,   (lsButtons[8],  ((Enemy)target).statusDefense.slow)
            ,   (lsButtons[9],  ((Enemy)target).statusDefense.stop)
            ,   (lsButtons[10], ((Enemy)target).statusDefense.frog)
            ,   (lsButtons[11], ((Enemy)target).statusDefense.small)
            ,   (lsButtons[12], ((Enemy)target).statusDefense.slowNumb)
            ,   (lsButtons[13], ((Enemy)target).statusDefense.barrier)
            ,   (lsButtons[14], ((Enemy)target).statusDefense.mBarrier)
            ,   (lsButtons[15], ((Enemy)target).statusDefense.reflect)
            ,   (lsButtons[16], ((Enemy)target).statusDefense.shield)
            ,   (lsButtons[17], ((Enemy)target).statusDefense.deathSentence)
            ,   (lsButtons[18], ((Enemy)target).statusDefense.berserk)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.paralyzed)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.darkness)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.deathForce)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.resist)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.manipulate)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.sense)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.seizure)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.imprisoned)
            ,   (lsButtons[19], ((Enemy)target).statusDefense.peerless)
            };
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Enemy());

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new Enemy());

        base.Add();
    }
}
