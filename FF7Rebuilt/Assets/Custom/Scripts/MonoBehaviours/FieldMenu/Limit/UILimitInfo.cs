using System;
using TMPro;
using UnityEngine;

public class UILimitInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public Ability limit { get; set; }

    private CanvasGroup cnvGrp;

    private TextMeshProUGUI txtName;

    public void Initialize() {
        rct = GetComponent<RectTransform>();
        cnvGrp = GetComponent<CanvasGroup>();
        txtName = GetComponent<TextMeshProUGUI>();
    }

    public void Refresh(Ability _limit) {
        limit = _limit;
        cnvGrp.alpha = limit != null ? 1 : 0;

        if (limit == null) {
            Clear();
            return;
        }

        string limitName = limit.name;
        
        int charLocation = limit.name.IndexOf('|');
        if (charLocation >= 0) {
            limitName = limit.name.Substring(0, charLocation);
        }

        txtName.text = limitName;
    }

    public void Clear() {
        txtName.text = "";
        cnvGrp.alpha = 0;
    }
}
