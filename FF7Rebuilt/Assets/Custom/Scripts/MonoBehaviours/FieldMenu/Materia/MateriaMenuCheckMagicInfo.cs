using CharacterTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuCheckMagicInfo : MonoBehaviour {
    public RectTransform rct { get; private set; }
    public Ability abilityReference { get; private set; }
    public int all { get; private set; }
    public int quadraMagic { get; private set; }

    [SerializeField] private Image imgAll;
    private TextMeshProUGUI txtName;

    public void Initialize() {
        txtName = GetComponent<TextMeshProUGUI>();
        rct = GetComponent<RectTransform>();
    }

    public void Refresh(Player _player, Ability _ability) {
        if (_ability == null) {
            Clear();
            return;
        }

        abilityReference = _ability;
        txtName.text = abilityReference.name;
        all = _player.dctAbilityAugmentations[abilityReference].all;
        quadraMagic = _player.dctAbilityAugmentations[abilityReference].quadraMagic;


        bool isAll = abilityReference.targetOptions.isToggleSingleMultipleTargets && all > 0;
        Debug.Log($"ability({abilityReference.name}), toggleAll({abilityReference.targetOptions.isToggleSingleMultipleTargets}, all({all}))");
        imgAll.enabled = isAll;
    }

    public Ability GetSpellAvailable() {
        return abilityReference;
    }

    public void Clear() {
        abilityReference = null;
        imgAll.enabled = false;
        txtName.text = "";
    }
}
