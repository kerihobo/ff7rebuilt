using UnityEngine;

public class FieldInputManager : MonoBehaviour {
    public static FieldInputManager Instance;

    public bool x;
    public bool xDown { get; set; }
    public bool xUp { get; set; }

    private void Awake() {
        if (Instance && Instance != this) {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.X)) {
            x = true;
        }

        if (Input.GetKeyUp(KeyCode.X)) {
            x = false;
        }
    }
}
