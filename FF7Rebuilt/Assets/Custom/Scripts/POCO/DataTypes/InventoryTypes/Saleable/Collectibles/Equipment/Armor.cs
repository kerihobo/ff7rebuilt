﻿using System;
using UnityEngine;

namespace InventoryTypes {
    [Serializable]
    public class Armor : Equipment {
        public Armor() : base() {
            name = "New Armor";
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Armor(Armor _source) : base(_source) {
            equipEffects       = _source.equipEffects;
            elements           = _source.elements;
            genderExclusivity  = _source.genderExclusivity;
            isZiedrich         = _source.isZiedrich;
        }

        [SerializeReference]
        public EquipEffect equipEffects;
        [SerializeReference]
        public ElementalAffinityManager elements = new ElementalAffinityManager(true);
        // TODO: Consider adding status resistances in again at some point.
        //public StatusEffects resistanceStatuses = new StatusEffects();
        public Gender genderExclusivity;
        public bool isZiedrich;

        public int GetElementalDamageAfterZiedrich(ElementalAffinityManager _affinities, int _elementalDamage) {
            if (isZiedrich && (
                _affinities.fire.isActive
            ||  _affinities.ice.isActive
            ||  _affinities.lightning.isActive
            ||  _affinities.earth.isActive
            ||  _affinities.poison.isActive
            ||  _affinities.gravity.isActive
            ||  _affinities.water.isActive
            ||  _affinities.wind.isActive
            ||  _affinities.holy.isActive
            ||  _affinities.cut.isActive
            ||  _affinities.hit.isActive
            ||  _affinities.punch.isActive
            ||  _affinities.shoot.isActive
            )) {
                _elementalDamage /= 2;
            }

            return _elementalDamage;
        }
    }
}