using UnityEngine.UIElements;

public static class ExtToggle {
    public static Toggle SetValue(this Toggle _self, bool _value) {
        _self.value = _value;
        return _self;
    }
}
