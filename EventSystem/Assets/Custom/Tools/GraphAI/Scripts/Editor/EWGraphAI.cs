﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class EWGraphAI : EWGraphTemplate {
    private static EWGraphAI window;

    [MenuItem("Tools/AI Graph")]
    public static new void Open() {
        if (!window) {
            //window = window = CreateWindow<EWGraphAI>();
            window = GetWindow<EWGraphAI>();
        }

        window.Initialize();
    }

    // TODO: Find a way to make this work nicely in inheriting classes like this one.
    protected override void OnSelectionChange() {
        if (!Selection.activeObject
        || !IsGraphSelected<NodeGraphAI>()) {
            return;
        }

        if (selectedGraph?.searchWindow != null) {
            selectedGraph.searchWindow.Close();
        }

        Initialize();

        Debug.Log(Selection.activeObject?.GetType());

        Repaint();
    }

    protected override void Initialize() {
        Show();
        // TODO: Find a way to make these lines suitable for inherited window types.
        selectedGraph = IsGraphSelected<NodeGraphAI>() ? (NodeGraphAI)Selection.activeObject : null;
        titleContent = new GUIContent("AI Graph" + (IsGraphSelected<NodeGraphAI>() ? " - " + selectedGraph.name : ""));
        styleSet = (StyleSet)Resources.Load("StyleSets/StyleSet");

        pan = position.size / 3;

        Debug.Log("Opened AI?");
    }

    // TODO: Find a way to make this work nicely in inheriting classes like this one.
    [OnOpenAsset(1)]
    public static new bool OnOpenAsset(int instanceID, int line) {
        if (Selection.activeObject?.GetType() != typeof(NodeGraphAI)) {
            return false;
        }

        Open();

        return true;
    }
}
