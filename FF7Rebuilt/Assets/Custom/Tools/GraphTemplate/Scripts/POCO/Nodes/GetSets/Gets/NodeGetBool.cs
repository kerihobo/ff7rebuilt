﻿using System;
using UnityEngine;

[Serializable]
public class NodeGetBool : NodeGet {
    public NodeGetBool(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        minMaxThresholds = new Vector2(140, 140);
        size = new Vector2(minMaxThresholds.y, 50);

        AddPortIn<PortFieldLabel>();
        AddPortOut<PortBool>("Value", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        bool value = ((FieldBool)field).value;
        
        return value;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        NodeGetBool newNode = new NodeGetBool(_nodeGraph, position + duplicationOffset, category, false);

        newNode.Initialize(field);

        return newNode;
    }
}
