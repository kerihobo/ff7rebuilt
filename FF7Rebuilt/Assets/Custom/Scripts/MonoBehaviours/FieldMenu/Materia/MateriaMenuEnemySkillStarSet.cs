using InventoryTypes;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuEnemySkillStarSet : MonoBehaviour {
    public Sprite sprStarInactive;
    public Sprite sprStarActive;
    private Image[] arrStarsIcons;

    // Can a more accurate graphic be used for the scrollbarBG we use?
    // And also for the EnemySkill list background.
    public void Initialize() {
        arrStarsIcons = GetComponentsInChildren<Image>();
    }

    public void Refresh(Materia _materia) {
        ClearAllStars();

        for (int i = 0; i < EnemySkillManager.MAX_INDEX; i++) {
            bool isSkillLearned = _materia.enemySkillManager.skillsUnlocked[i];
            arrStarsIcons[i].sprite = isSkillLearned ? sprStarActive : sprStarInactive;
        }

        void ClearAllStars() {
            for (int i = 0; i < arrStarsIcons.Length; i++) {
                arrStarsIcons[i].sprite = sprStarInactive;
            }
        }
    }
}
