﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StatusEffectManager {
    public enum StatusEffectAction {
        AFFLICT
    ,   RECOVER
    ,   TOGGLE
    }

    [Serializable]
    public class StatusEffect {
        public StatusEffect() { }

        public StatusEffect(StatusEffect _source) {
            isActive = _source.isActive;
        }

        public bool isActive;

        public static StatusEffect operator +(StatusEffect a, StatusEffect b) {
            StatusEffect temp = new StatusEffect();
            
            if (b == null) return a;

            temp.isActive = a.isActive || b.isActive;

            return temp;
        }
    }

    public StatusEffectManager() { }

    public StatusEffectManager(StatusEffectManager _source) {
        //Debug.Log(_source);
        death         = new StatusEffect(_source.death);
        sleep         = new StatusEffect(_source.sleep);
        poison        = new StatusEffect(_source.poison);
        sadness       = new StatusEffect(_source.sadness);
        fury          = new StatusEffect(_source.fury);
        confusion     = new StatusEffect(_source.confusion);
        silence       = new StatusEffect(_source.silence);
        haste         = new StatusEffect(_source.haste);
        slow          = new StatusEffect(_source.slow);
        stop          = new StatusEffect(_source.stop);
        frog          = new StatusEffect(_source.frog);
        small         = new StatusEffect(_source.small);
        slowNumb      = new StatusEffect(_source.slowNumb);
        petrify       = new StatusEffect(_source.petrify);
        regen         = new StatusEffect(_source.regen);
        barrier       = new StatusEffect(_source.barrier);
        mBarrier      = new StatusEffect(_source.mBarrier);
        reflect       = new StatusEffect(_source.reflect);
        shield        = new StatusEffect(_source.shield);
        deathSentence = new StatusEffect(_source.deathSentence);
        berserk       = new StatusEffect(_source.berserk);
        paralyzed     = new StatusEffect(_source.paralyzed);
        darkness      = new StatusEffect(_source.darkness);
        deathForce    = new StatusEffect(_source.deathForce);
        resist        = new StatusEffect(_source.resist);
        manipulate    = new StatusEffect(_source.manipulate);
        sense         = new StatusEffect(_source.sense);
        seizure       = new StatusEffect(_source.seizure);
        imprisoned    = new StatusEffect(_source.imprisoned);
        peerless      = new StatusEffect(_source.peerless);
        chance        = _source.chance;
        gaugeBarrier  = _source.gaugeBarrier;
        gaugeMBarrier = _source.gaugeMBarrier;
        heroDrinks    = _source.heroDrinks;
        dragonForces  = _source.dragonForces;
        isLuckyGirl   = _source.isLuckyGirl;
        isDefend      = _source.isDefend;
        isBackAttack  = _source.isBackAttack;
        isNearDeath   = _source.isNearDeath;
        isBackRow     = _source.isBackRow;
        isAllLucky7s  = _source.isAllLucky7s;
        //isRecover     = _source.isRecover;
        statusEffectAction  = _source.statusEffectAction;
    }

    // Resistable.
    public StatusEffect death = new StatusEffect();
    public StatusEffect sleep = new StatusEffect();
    // Player glows green.
    public StatusEffect poison = new StatusEffect();
    public StatusEffect sadness = new StatusEffect();
    public StatusEffect fury = new StatusEffect();
    public StatusEffect confusion = new StatusEffect();
    public StatusEffect silence = new StatusEffect();
    public StatusEffect haste = new StatusEffect();
    public StatusEffect slow = new StatusEffect();
    public StatusEffect stop = new StatusEffect();
    public StatusEffect frog = new StatusEffect();
    public StatusEffect small = new StatusEffect();
    public StatusEffect slowNumb = new StatusEffect();
    public StatusEffect petrify = new StatusEffect();
    public StatusEffect regen = new StatusEffect();
    public StatusEffect barrier = new StatusEffect();
    public StatusEffect mBarrier = new StatusEffect();
    public StatusEffect reflect = new StatusEffect();
    public StatusEffect shield = new StatusEffect();
    public StatusEffect deathSentence = new StatusEffect();
    // Player glows red.
    public StatusEffect berserk = new StatusEffect();
    public StatusEffect paralyzed = new StatusEffect();
    // Player glows black.
    public StatusEffect darkness = new StatusEffect();
    public StatusEffect deathForce = new StatusEffect();
    public StatusEffect resist = new StatusEffect();
    public StatusEffect manipulate = new StatusEffect();
    public StatusEffect sense = new StatusEffect();
    // Anti-regen that afflicts you along-side imprisoned Bottomswell traps you in a bubble.
    public StatusEffect seizure = new StatusEffect();
    // Inflicted by CarryArmor's LeftArm or RightArm, Reno in his first battle, or by Bottomswell, when you're stuck in the bubble.
    // Also flagged as defeated, so the GameOver sequence should occur if all party members are either dead or imprisoned.
    public StatusEffect imprisoned = new StatusEffect();
    // Granted by Cait Sith's "slots" and allows party to always deal critical hits with all physical attacks.
    // Immune to HP/MP damage and status effects. Player glows yellow.
    // Sephiroth is peerless for the entire Nibelheim retelling.
    public StatusEffect peerless = new StatusEffect();
    public int chance = 63;

#region Special statuses.
    public int gaugeBarrier;
    public int gaugeMBarrier;
    [Range(0, 4)]
    public int heroDrinks = 0;
    [Range(0, 2)]
    public int dragonForces = 0;
    // Cait Sith Limit Break. Makes all critical.
    public bool isLuckyGirl;
    public bool isDefend;
    public bool isBackAttack;
    public bool isNearDeath;
    public bool isBackRow;
    // An easter egg if your char reaches precisely 7777 HP. Enemies do 7777 for every action.
    // Players become uncontrollable, and attack enemies repeatedly for 489,951 damage.
    public bool isAllLucky7s;
#endregion

    public StatusEffectAction statusEffectAction;
    //public bool isRecover;

    public bool IsRecover => statusEffectAction == StatusEffectAction.RECOVER;

    public void Afflict(StatusEffectManager _source) {
        if (true) {

        }
    }
    
    public string[] GetStatusStringArray() {
        List<string> sStatuses = new();

        //if (death.isActive)         sStatuses.Add("Death");
        if (sleep.isActive)         sStatuses.Add("Sleep");
        if (poison.isActive)        sStatuses.Add("Poison");
        if (sadness.isActive)       sStatuses.Add("Sadness");
        if (fury.isActive)          sStatuses.Add("Fury");
        if (confusion.isActive)     sStatuses.Add("Confusion");
        if (silence.isActive)       sStatuses.Add("Silence");
        if (haste.isActive)         sStatuses.Add("Haste");
        if (slow.isActive)          sStatuses.Add("Slow");
        if (stop.isActive)          sStatuses.Add("Stop");
        if (frog.isActive)          sStatuses.Add("Frog");
        if (small.isActive)         sStatuses.Add("Small");
        if (slowNumb.isActive)      sStatuses.Add("Slow-Numb");
        if (petrify.isActive)       sStatuses.Add("Petrify");
        if (regen.isActive)         sStatuses.Add("Regen");
        if (barrier.isActive)       sStatuses.Add("Barrier");
        if (mBarrier.isActive)      sStatuses.Add("MBarrier");
        if (reflect.isActive)       sStatuses.Add("Reflect");
        if (shield.isActive)        sStatuses.Add("Shield");
        if (deathSentence.isActive) sStatuses.Add("Death-Sentence");
        if (berserk.isActive)       sStatuses.Add("Berserk");
        if (paralyzed.isActive)     sStatuses.Add("Paralyzed");
        if (darkness.isActive)      sStatuses.Add("Darkness");
        if (deathForce.isActive)    sStatuses.Add("Death Force");
        if (resist.isActive)        sStatuses.Add("Resist");
        if (manipulate.isActive)    sStatuses.Add("Manipulate");
        //if (sense.isActive)         sStatuses.Add("Sense");
        if (seizure.isActive)       sStatuses.Add("Seizure");
        if (imprisoned.isActive)    sStatuses.Add("Imprisoned");
        if (peerless.isActive)      sStatuses.Add("Peerless");

        return sStatuses.ToArray();
    }

    public List<StatusEffect> GetAsList() {
        return new List<StatusEffect> {
            death
        ,   sleep
        ,   poison
        ,   sadness
        ,   fury
        ,   confusion
        ,   silence
        ,   haste
        ,   slow
        ,   stop
        ,   frog
        ,   small
        ,   slowNumb
        ,   petrify
        ,   regen
        ,   barrier
        ,   mBarrier
        ,   reflect
        ,   shield
        ,   deathSentence
        ,   berserk
        ,   paralyzed
        ,   darkness
        ,   deathForce
        ,   resist
        ,   manipulate
        ,   sense
        ,   seizure
        ,   imprisoned
        ,   peerless
        };
    }

    public static StatusEffectManager operator +(StatusEffectManager a, StatusEffectManager b) {
        StatusEffectManager temp = new StatusEffectManager();
        
        if (b == null) return a;

        temp.death         = a.death         + b.death;
        temp.sleep         = a.sleep         + b.sleep;
        temp.poison        = a.poison        + b.poison;
        temp.sadness       = a.sadness       + b.sadness;
        temp.fury          = a.fury          + b.fury;
        temp.confusion     = a.confusion     + b.confusion;
        temp.silence       = a.silence       + b.silence;
        temp.haste         = a.haste         + b.haste;
        temp.slow          = a.slow          + b.slow;
        temp.stop          = a.stop          + b.stop;
        temp.frog          = a.frog          + b.frog;
        temp.small         = a.small         + b.small;
        temp.slowNumb      = a.slowNumb      + b.slowNumb;
        temp.petrify       = a.petrify       + b.petrify;
        temp.regen         = a.regen         + b.regen;
        temp.barrier       = a.barrier       + b.barrier;
        temp.mBarrier      = a.mBarrier      + b.mBarrier;
        temp.reflect       = a.reflect       + b.reflect;
        temp.shield        = a.shield        + b.shield;
        temp.deathSentence = a.deathSentence + b.deathSentence;
        temp.berserk       = a.berserk       + b.berserk;
        temp.paralyzed     = a.paralyzed     + b.paralyzed;
        temp.darkness      = a.darkness      + b.darkness;
        temp.deathForce    = a.deathForce    + b.deathForce;
        temp.resist        = a.resist        + b.resist;
        temp.manipulate    = a.manipulate    + b.manipulate;
        temp.sense         = a.sense         + b.sense;
        temp.seizure       = a.seizure       + b.seizure;
        temp.imprisoned    = a.imprisoned    + b.imprisoned;
        temp.peerless      = a.peerless      + b.peerless;
        temp.chance        = a.chance        + b.chance;
        temp.gaugeBarrier  = a.gaugeBarrier  + b.gaugeBarrier;
        temp.gaugeMBarrier = a.gaugeMBarrier + b.gaugeMBarrier;
        temp.heroDrinks    = a.heroDrinks    + b.heroDrinks;
        temp.dragonForces  = a.dragonForces  + b.dragonForces;
        temp.isLuckyGirl   = a.isLuckyGirl   || b.isLuckyGirl;
        temp.isDefend      = a.isDefend      || b.isDefend;
        temp.isBackAttack  = a.isBackAttack  || b.isBackAttack;
        temp.isNearDeath   = a.isNearDeath   || b.isNearDeath;
        temp.isBackRow     = a.isBackRow     || b.isBackRow;
        temp.isAllLucky7s  = a.isAllLucky7s  || b.isAllLucky7s;

        return temp;
    }
}