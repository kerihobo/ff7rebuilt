using InventoryTypes;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemMenuKeyItems : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private RectTransform optionsParent;
    [SerializeField] private TextMeshProUGUI txtDescription;
    private RectTransform scrollBarFG;
    private int selectedIndex;
    private ItemMenuOptionList itemMenuOptionList;
    private UIKeyItemInfo[] arrOptions;

    // MAYBE REFACTOR
    [SerializeField] private RectTransform additionalOptionParent;
    private UIKeyItemInfo[] additionalOptions;
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 2;

    private KeyItem[] arrKeyItems => GameManager.Instance.gameData.inventory.arrKeyItem;

    public void Initialize(ItemMenuOptionList _itemMenuOptionList) {
        itemMenuOptionList = _itemMenuOptionList;
        // MAYBE REFACTOR
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        GridLayoutGroup glgOptions = GetComponentInChildren<GridLayoutGroup>();
        LayoutRebuilder.ForceRebuildLayoutImmediate(glgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<UIKeyItemInfo>();
        foreach (UIKeyItemInfo info in arrOptions) {
            info.Initialize();
        }

        additionalOptions = additionalOptionParent.GetComponentsInChildren<UIKeyItemInfo>();
        foreach (UIKeyItemInfo info in additionalOptions) {
            info.Initialize();
        }

        ClearAdditional();
        Disable();

        cursor.Initialize(arrOptions[0].rct);

        listSpace = glgOptions.spacing.y;
        listHeight = arrOptions[0].rct.rect.height;
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int keyItemIndex = lowBound + i;
            KeyItem selectedKeyItem = arrKeyItems[keyItemIndex];
            arrOptions[i].Refresh(selectedKeyItem);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - columns;
        UIKeyItemInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOptionParent.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        for (int i = 0; i < additionalOptions.Length; i++) {
            KeyItem displayedKeyItem = arrKeyItems[anchoredIndex + i];
            additionalOptions[i].Refresh(displayedKeyItem);
        }
    }

    private void ClearAdditional() {
        foreach (var info in additionalOptions) {
            info.Clear();
        }
    }

    public void ResetSelection() {
        selectedIndex = 0;

        lowBound = 0;
        UpdateScrollBar();

        SetCursor();
    }

    public void SetCursor() {
        UIKeyItemInfo selectedKeyItemInfo = arrOptions[selectedIndex];

        cursor.SetCursorPosition(selectedKeyItemInfo.rct);

        KeyItem selectedKeyItem = arrOptions[selectedIndex].keyItem;
        string description = selectedKeyItem != null ? selectedKeyItem.description : "";
        SetDescription(description);
    }

    public void Open() {
        cursor.Show();
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void Close() {
        Disable();

        itemMenuOptionList.ExitSubMenu();
    }

    private void Disable() {
        cursor.Hide();
        SetDescription(null);
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);
        Debug.Log(_v);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        float scroll = ((float)lowBound / columns) / (arrKeyItems.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

    public void SetDescription(string _description) {
        txtDescription.text = _description;
    }

#region Override methods.
    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex <= 1 && _v > 0) || (lowBound + selectedIndex >= arrKeyItems.Length - columns && _v < 0)) return;

        int v = -_v * columns;
        selectedIndex += v;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
            lowBound -= lowBound % columns;
        }

        if (selectedIndex < 0) {
            selectedIndex += columns;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= columns;
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrKeyItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _h < 0) || (lowBound + selectedIndex == arrKeyItems.Length - 1 && _h > 0)) return;

        selectedIndex += _h;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += _h * 2;
            lowBound -= lowBound % columns;

            if (isTooHigh) {
                selectedIndex -= columns;
            } else if (isTooLow) {
                selectedIndex += columns;
            }
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrKeyItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        SetCursor();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(_h));
        }
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;

            ClearAdditional();
        }

        int v = _pageDirection * (10 * columns);
        int lowBoundMax = arrKeyItems.Length - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
    }

    public override void GetInputConfirmPressed() { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
