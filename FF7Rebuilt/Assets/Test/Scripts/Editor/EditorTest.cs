using FFVII.Database;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[CustomEditor(typeof(Test))]
public class CustomTest : Editor {
    private Test self;

    private void OnEnable() {
        self = (Test)target;
    }

    public override VisualElement CreateInspectorGUI() {
        VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Test/Resources/UXML/EditorTest.uxml");
        TemplateContainer ui = visualTree.CloneTree();

        DropdownField ddf = ui.Q<DropdownField>("drp-selection");
        ddf.Initialize(DBResources.GetEnemies.enemyList, self.enemy, DBResources.GetEnemies);
        ddf.RegisterValueChangedCallback(_e => {
            self.enemy.SetDataExternally(ddf.index);
            EditorUtility.SetDirty(target);
        });
        Debug.Log(self.enemy);

        return ui;
    }
}