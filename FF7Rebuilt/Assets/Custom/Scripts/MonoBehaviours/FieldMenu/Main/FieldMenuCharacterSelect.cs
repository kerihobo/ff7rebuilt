using UnityEngine;

public class FieldMenuCharacterSelect : MenuControllable {
    public UICharacterInfoContainer characterInfoContainer;
    public FingerCursorRight cursorStatic;
    public FingerCursorRight cursorDynamic;
    
    public int selectedIndex;
    
    private FieldMenuOptionList fieldOptions;
    private FieldMenu fieldMenu;
    private int screenIndex;
    private int indexToSwap = -1;

    public void Initialize(FieldMenu _fieldMenu, FieldMenuOptionList _pnlFieldOptions) {
        fieldMenu = _fieldMenu;
        fieldOptions = _pnlFieldOptions;

        characterInfoContainer.Initialize();
        RectTransform firstRectTransform = characterInfoContainer.arrPlayerInfos[0].rct;
        cursorDynamic.Initialize(firstRectTransform);
        cursorStatic.Initialize(firstRectTransform);
        //cursorStatic.SetFlickerState(true);

        Disable();
    }

    public void ResetSelection() {
        selectedIndex = 0;

        cursorDynamic.gameObject.SetActive(true);
        cursorStatic.gameObject.SetActive(false);

        SetCursor(cursorStatic);
        SetCursor(cursorDynamic);
    }

    public void SetCursor(FingerCursorRight _cursor) {
        _cursor.SetCursorPosition(characterInfoContainer.arrPlayerInfos[selectedIndex].rct);
    }

    public void Open(int _screenIndex) {
        screenIndex = _screenIndex;
        indexToSwap = -1;
        
        ResetSelection();
        SetControllable();
    }

    public void Close() {
        Disable();

        fieldOptions.ExitCharacterSelection();
    }

    private void Disable() {
        cursorStatic.gameObject.SetActive(false);
        cursorDynamic.gameObject.SetActive(false);

        screenIndex = -1;
        indexToSwap = -1;
    }

    void CancelOrderSelection() {
        cursorStatic.Hide();
        indexToSwap = -1;
    }

    public override void GetInputConfirmPressed() {
        UICharacterInfo selectedCharacterInfo = characterInfoContainer.arrPlayerInfos[selectedIndex];

        switch (screenIndex) {
            case 1: // Magic
            case 2: // Materia
            case 3: // Equip
            case 4: // Status
            case 6: // Limit
                if (selectedCharacterInfo.player != null) {
                    fieldMenu.StartCoroutine(fieldMenu.OpenSelectedScreen(screenIndex));
                }
                break;
            case 5: // Order
                ProcessOrderSelection();

                break;
            case 0: // Item
            case 7: // Config
            case 8: // PHS
            case 9: // Save
            case 10: // Quit
                break;
            default:
                break;
        }

        void ProcessOrderSelection() {
            if (indexToSwap == -1) {
                FirstOrderSelection();
            } else {
                SecondOrderSelection();
            }

            void FirstOrderSelection() {
                indexToSwap = selectedIndex;

                cursorStatic.Show();
                cursorStatic.SetFlickerState(true);

                SetCursor(cursorStatic);
            }

            void SecondOrderSelection() {
                CharacterTypes.Player[] arrCurrent = GameManager.Instance.gameData.phs.arrCurrent;

                if (indexToSwap == selectedIndex) {
                    // Toggle Backrow.
                    arrCurrent[selectedIndex].statusCurrent.isBackRow = !arrCurrent[selectedIndex].statusCurrent.isBackRow;
                } else {
                    // Swap characters.
                    (arrCurrent[selectedIndex], arrCurrent[indexToSwap]) = (arrCurrent[indexToSwap], arrCurrent[selectedIndex]);
                }

                characterInfoContainer.Refresh();
                CancelOrderSelection();
            }
        }
    }

    public override void GetInputCancelPressed() {
        if (screenIndex == 5 && indexToSwap != -1) {
            CancelOrderSelection();
        } else {
            Close();
        }
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % characterInfoContainer.arrPlayerInfos.Length;

        if (selectedIndex < 0) {
            selectedIndex = characterInfoContainer.arrPlayerInfos.Length - 1;
        }

        SetCursor(cursorDynamic);
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
}
