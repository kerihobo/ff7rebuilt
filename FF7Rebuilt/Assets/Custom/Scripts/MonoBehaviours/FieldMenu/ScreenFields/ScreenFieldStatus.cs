using CharacterTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScreenFieldStatus : ScreenField {
    public override FieldMenu fieldMenu { get; set; }

    [SerializeField] private UICharacterInfo characterInfo;
    [SerializeField] private RectTransform statusMenuParent;
    private ScreenFieldStatusController controller;
    private StatusMenu[] statusMenus;
    private List<Player> lsActivePlayers = new List<Player>();
    private int selectedScreenIndex;
    private int selectedCharacterIndex;
    
    public Player GetSelectedCharacter => lsActivePlayers.Count > selectedCharacterIndex ? lsActivePlayers[selectedCharacterIndex] : null;
    private Player GetInitiallySelectedCharacter => GameManager.Instance.gameData.phs.arrCurrent[fieldMenu.selectedCharacterIndex];

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        controller = GetComponent<ScreenFieldStatusController>();
        controller.Initialize(this);

        characterInfo.Initialize(Vector2.zero, Vector2.zero);

        statusMenus = statusMenuParent.GetComponentsInChildren<StatusMenu>(true);

        foreach (StatusMenu sm in statusMenus) {
            sm.Initialize(this);
        }
    }

    public override void SetDefaultDisplay() { }

    public override IEnumerator Open() {
        lsActivePlayers = GameManager.Instance.gameData.phs.arrCurrent.Where(x => x != null).ToList();
        selectedCharacterIndex = lsActivePlayers.IndexOf(GetInitiallySelectedCharacter);

        selectedScreenIndex = 0;

        ShowStatusMenu();
        Refresh();
        controller.Open();

        yield return null;
    }

    private void Refresh() {
        foreach (StatusMenu sm in statusMenus) {
            sm.Refresh(GetSelectedCharacter);
        }

        characterInfo.Refresh(GetSelectedCharacter);
    }

    private void ShowStatusMenu() {
        foreach (StatusMenu sm in statusMenus) {
            sm.Hide();
        }

        statusMenus[selectedScreenIndex].Show();
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public void GoToNext() {
        selectedScreenIndex = ++selectedScreenIndex % statusMenus.Length;
        ShowStatusMenu();
    }

    public void CycleCharacter(int _direction) {
        selectedCharacterIndex = (selectedCharacterIndex + _direction) % lsActivePlayers.Count;
        if (selectedCharacterIndex < 0) {
            selectedCharacterIndex = lsActivePlayers.Count - 1;
        }

        Refresh();
    }
}
