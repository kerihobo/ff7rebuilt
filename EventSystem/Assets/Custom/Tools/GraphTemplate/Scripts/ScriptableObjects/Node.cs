﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[Serializable]
public abstract class Node : ScriptableObject {
    public class Duplication {
        public Duplication(NodeGraph _nodeGraph, List<int> _outPortIndex, List<int> _targetNodeIndex, List<int> _inPortIndex) {
            nodeGraph = _nodeGraph;
            outPortIndex = _outPortIndex;
            targetNodeIndex = _targetNodeIndex;
            inPortIndex = _inPortIndex;
        }

        public NodeGraph nodeGraph;
        public List<int> outPortIndex;
        public List<int> targetNodeIndex;
        public List<int> inPortIndex;
    }

    public enum Category {
        None
    ,   Input
    ,   Logic
    ,   Maths
    ,   BlackboardField
    }

    public Vector2 position;
    public PortContainer portContainerIn = new PortContainer(Port.Direction.IN);
    public PortContainer portContainerOut = new PortContainer(Port.Direction.OUT);
    public bool isPersistent;
    public Category category;

    public Rect rect { get; set; }
    public bool isSelected { get; set; }
    public Duplication duplication { get; set; }

    [SerializeField]
    protected Vector2 size = new Vector2(100, 50);
    protected Vector2 duplicationOffset = new Vector2(100, -100);

    public static T Create<T>(NodeGraph _nodeGraph, Vector2 offset, Category _category = Category.None, bool _isSave = true) where T : Node {
        T newNode = CreateInstance<T>();
        newNode.Initialize(offset, newNode.GetType().ToString());
        newNode.category = _category;

        AssetDatabase.AddObjectToAsset(newNode, _nodeGraph);
        _nodeGraph.nodes.Add(newNode);

        EditorUtility.SetDirty(_nodeGraph);

        if (_isSave) {
            AssetDatabase.SaveAssets();
        }

        return newNode;
    }

    public void Initialize(Vector2 _offset, string _name) {
        position  = _offset;
        name      = _name;
        hideFlags = HideFlags.HideInHierarchy;
    }

    protected virtual void OnBecamePersistent() {
        portContainerIn = new PortContainer(Port.Direction.IN);
        portContainerOut = new PortContainer(Port.Direction.OUT);

        isPersistent = true;

        Debug.LogWarning(name + " has become persistent.");
    }

    public void ProcessDuplication(List<Node> _duplicatedNodes) {
        if (duplication != null) {
            for (int i = 0; i < duplication.inPortIndex.Count; i++) {
                portContainerOut.ports[duplication.outPortIndex[i]].connectedPorts.Add(_duplicatedNodes[duplication.targetNodeIndex[i]].portContainerIn.ports[duplication.inPortIndex[i]]);
                _duplicatedNodes[duplication.targetNodeIndex[i]].portContainerIn.ports[duplication.inPortIndex[i]].connectedPorts.Add(portContainerOut.ports[duplication.outPortIndex[i]]);
            }
        }
    }
    
    protected void AddPortIn<T>(string _name = "In", Port.Capacity _capacity = Port.Capacity.SINGLE, Port.DisplayMode _displayMode = Port.DisplayMode.SHOW_ALL) where T : Port {
        portContainerIn.Add<T>(this, _name, Port.Direction.IN, _capacity, _displayMode);
    }

    protected void AddPortOut<T>(string _name = "Out", Port.Capacity _capacity = Port.Capacity.SINGLE, Port.DisplayMode _displayMode = Port.DisplayMode.SHOW_ALL) where T : Port {
        portContainerOut.Add<T>(this, _name, Port.Direction.OUT, _capacity, _displayMode);
    }

    public virtual void Run(NodeGraph _nodeGraph) {
    }

    public virtual void RenderEdges() {
        if (!IsPersistent()) return;
        foreach (Port p in portContainerOut.ports) {
            p.RenderEdges();
        }
    }

    private bool IsPersistent() {
        if (Event.current.type != EventType.Repaint && !isPersistent && EditorUtility.IsPersistent(this)) {
            OnBecamePersistent();
        }

        return isPersistent;
    }

    public virtual void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        if (!IsPersistent()) return;

        if (Event.current.type == EventType.Repaint) {
            rect = new Rect(position + _pan, size);
        }

        Color initialBGColour = GUI.backgroundColor;
        GUI.backgroundColor = isSelected 
        ?   initialBGColour
        :   GetColorByCategory()
        ;

        GUILayout.BeginArea(rect, isSelected ? _styleSet.nodeSelected : _styleSet.node);
            GUI.backgroundColor = GetColorByCategory();
            GUILayout.Label(name, _styleSet.nodeTitle);
            GUI.backgroundColor = initialBGColour;

            GUILayout.BeginHorizontal(_styleSet.nodeContent);
                portContainerIn.Render(_styleSet, this);
                portContainerOut.Render(_styleSet, this);
            GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    public Node GetMouseOver(Vector2 _mousePosition) {
        if (rect.Contains(_mousePosition)) {
            return this;
        }

        return null;
    }

    public Port GetHoveredPort(Event _e) {
        List<Port> ports = new List<Port>();
        ports.AddRange(portContainerIn.ports);
        ports.AddRange(portContainerOut.ports);

        foreach (Port p in ports) {
            if (p.isHovered) {
                if (_e.modifiers == EventModifiers.Alt) {
                    if (p.connectedPorts.Count > 0) {
                        p.Disconnect();
                 
                        return null;
                    }
                }

                return p;
            }
        }

        return null;
    }

    public void DisconnectPorts() {
        portContainerIn.DisconnectPorts();
        portContainerOut.DisconnectPorts();
    }

    protected bool ScaleForConnectedInputs(float _scaleSmall, float _scaleLarge) {
        List<Port> dataPorts = portContainerIn.ports.Where(x => x.portType != NodeDataType.Execution).ToList();
        size.x = dataPorts.All(x => x.connectedPorts.Count > 0) ? _scaleSmall : _scaleLarge;

        return size.x == _scaleSmall;
    }

    public virtual Node Duplicate(NodeGraph _nodeGraph) {
        return null;
    }

    public virtual dynamic GetValue() {
        return null;
    }

    public void Move(Vector2 _direction) {
        position += _direction;
        Debug.Log(_direction);

        if (_direction.x != 0) {
            position.x = Round((int)position.x);
        }

        if (_direction.y != 0) {
            position.y = Round((int)position.y);

        }
    }

    private int Round(int n) {
        // Smaller multiple
        int a = (n / 10) * 10;

        // Larger multiple
        int b = a + 10;

        // Return of closest of two
        return (n - a > b - n) ? b : a;
    }

    private Color GetColorByCategory() {
        switch (category) {
            case Category.Input:
                return new Color(255 / 255f, 192 / 255f, 255 / 255f);
            case Category.Logic:
                return new Color(255 / 255f, 192 / 255f, 192 / 255f);
            case Category.Maths:
                return new Color(255 / 255f, 255 / 255f, 192 / 255f);
            case Category.BlackboardField:
                return new Color(192 / 255f, 192 / 255f, 255 / 255f);
        }

        return Color.white;
    }
}
