using CharacterTypes;
using FFVII.Database;
using UnityEditor;
using UnityEngine.UIElements;

public class PlayerDefaultArmor : VisualElement {
    public new class UxmlFactory : UxmlFactory<PlayerDefaultArmor, UxmlTraits> { }

    public PlayerDefaultArmor() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        name = Name;
    }
    
    public Player target;
    public Database database;
    private const string UXMLPath = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/player-default-armor.uxml";
    private const string Name = "player-default-armor";
    private TemplateContainer tc;
    private int selectedMateriaIndex = -1;

    public PlayerDefaultArmor Initialize(Player _target, Database _database) {
        target = _target;
        database = _database;

        BindUniqueControls();
        Refresh(target, database);

        return this;
    }

    public void BindUniqueControls() {
        DropdownField ddfArmorSelect = this.Q<DropdownField>("drp-armor");
        ddfArmorSelect.Initialize(DBResources.GetArmor.armorList, target.selectionStartingArmor, database);
        ddfArmorSelect.RegisterValueChangedCallback(_e => {
            target.selectionStartingArmor.SetDataExternally(ddfArmorSelect.index);
            EditorUtility.SetDirty(database);

            RefreshButtons();

            selectedMateriaIndex = -1;
            
            RefreshGrpMateriaSelect();
        });

        this.Q<Button>("btn-clear-materia").RegisterCallback<ClickEvent>(e => {
            foreach (SelectionMateria sm in target.defaultMateriaArmor) {
                sm.Nullify();
            }
            RefreshButtons();

            EditorUtility.SetDirty(database);
        });

        BindButtons();

        this.Q<EnumField>("enum-materia-type").RegisterValueChangedCallback(e => {
            target.defaultMateriaArmor[selectedMateriaIndex].type = (MateriaType)e.newValue;
            EditorUtility.SetDirty(database);

            RefreshGrpMateriaSelect();
        });

        DropdownField ddfMateriaSelect = this.Q<DropdownField>("drp-materia");
        ddfMateriaSelect.RegisterValueChangedCallback(_e => {
            if (selectedMateriaIndex > -1) {
                target.defaultMateriaArmor[selectedMateriaIndex].SetDataExternally(ddfMateriaSelect.index);
                EditorUtility.SetDirty(database);
            }
            
            RefreshButtons();
        });

        void BindButtons() {
            for (int i = 0; i < 8; i++) {
                int j = i;
                this.Q<Button>($"btn-materia-{j}").RegisterCallback<ClickEvent>(e => {
                    selectedMateriaIndex = j;
                    RefreshGrpMateriaSelect();
                });
            }
        }
    }

    public void Refresh(Player _target, Database _database) {
        target = _target;
        database = _database;
        selectedMateriaIndex = -1;

        this.Q<DropdownField>("drp-armor").Initialize(DBResources.GetArmor.armorList, target.selectionStartingArmor, database);

        RefreshGrpMateriaSelect();
    }

    private void RefreshGrpMateriaSelect() {
        this.Q<GroupBox>("grp-materia-select").style.display = selectedMateriaIndex > -1 ? DisplayStyle.Flex : DisplayStyle.None;

        if (selectedMateriaIndex > -1) {
            this.Q<EnumField>("enum-materia-type").value = target.defaultMateriaArmor[selectedMateriaIndex].type;
        }
        
        RefreshDrpMateria();
    }

    private void RefreshButtons() {
        for (int i = 0; i < 8; i++) {
            Button btn = this.Q<Button>($"btn-materia-{i}");
            ClearMateriaClass(btn);

            string materiaClass = AddMateriaClass(i);
            if (target.defaultMateriaArmor[i].index > -1) {
                btn.AddToClassList(materiaClass);
                btn.text = target.defaultMateriaArmor[i].GetSelectedMateria().name;
            }

            btn.style.display = i < target.selectionStartingArmor.GetSelectedArmor()?.materiaSlotCount ? DisplayStyle.Flex : DisplayStyle.None;
        }

        void ClearMateriaClass(Button _btn) {
            _btn.RemoveFromClassList("magic");
            _btn.RemoveFromClassList("summon");
            _btn.RemoveFromClassList("command");
            _btn.RemoveFromClassList("independent");
            _btn.RemoveFromClassList("support");

            _btn.text = "Empty";
        }

        string AddMateriaClass(int _index) => (target.defaultMateriaArmor[_index].type) switch {
            MateriaType.Magic => "magic",
            MateriaType.Summon => "summon",
            MateriaType.Command => "command",
            MateriaType.Independent => "independent",
            MateriaType.Support => "support",
            _ => null
        };
    }
    
    private void RefreshDrpMateria() {
        if (selectedMateriaIndex > -1) {
            this.Q<DropdownField>("drp-materia").Initialize(
                DBResources.GetMateriaList(target.defaultMateriaArmor[selectedMateriaIndex].type)
            ,   target.defaultMateriaArmor[selectedMateriaIndex], database
            );
        }

        RefreshButtons();
    }
}