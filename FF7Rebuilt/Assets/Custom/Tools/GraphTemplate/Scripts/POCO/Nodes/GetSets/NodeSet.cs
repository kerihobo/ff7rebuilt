﻿using System;
using UnityEngine;

[Serializable]
public abstract class NodeSet : NodeGetSet {
    protected NodeSet(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(140, 50);

        AddPortIn<Port>();
        AddPortOut<Port>();
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

    }

    public static NodeSet CreateFromField(NodeGraph _nodeGraph, Vector2 _offset, BlackboardField _field) {
        switch (_field.dataType) {
            case NodeDataType.Bool:
                return new NodeSetBool(_nodeGraph, _offset, Category.BlackboardField);
            case NodeDataType.Int:
                return new NodeSetInt(_nodeGraph, _offset, Category.BlackboardField);
        }

        return new NodeSetBool(_nodeGraph, _offset);
    }

    public override void Initialize(BlackboardField _selectedField) {
        base.Initialize(_selectedField);
     
        name = "Set " + _selectedField.name;
    }
}
