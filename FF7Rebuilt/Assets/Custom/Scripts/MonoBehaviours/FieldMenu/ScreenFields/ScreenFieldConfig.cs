using System.Collections;
using TMPro;
using UnityEngine;

public class ScreenFieldConfig : ScreenField {
    public override FieldMenu fieldMenu { get; set; }

    [SerializeField] private ConfigMenuMain menuMain;
    [SerializeField] private ConfigMenuController menuController;
    [SerializeField] private TextMeshProUGUI txtDescription;

    public override void Initialize(FieldMenu _fieldMenu) {
        fieldMenu = _fieldMenu;

        menuMain.Initialize(this);
        menuController.Initialize(this);
    }

    public override void SetDefaultDisplay() { }

    public override IEnumerator Open() {
        // Here we'll refresh every config object.

        menuMain.ResetSelection();
        OpenMain();

        yield return null;
    }

    public void OpenMain() {
        menuController.Disable();
        menuMain.Open();
    }

    public void OpenController() {
        menuMain.Disable();
        menuController.Open();
    }

    public override IEnumerator Close() {
        yield return null;
    }

    public void SetDescription(string _description) {
        txtDescription.text = _description;
    }
}
