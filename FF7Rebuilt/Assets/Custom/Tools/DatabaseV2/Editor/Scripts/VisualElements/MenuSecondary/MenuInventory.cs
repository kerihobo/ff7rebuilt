using UnityEngine.UIElements;

public class MenuInventory : MenuSecondary {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/MenuSecondary/menu-inventory.uxml";
    protected override string Name { get; set; } = "menu-inventory";

    public MenuInventory() : base() {
        ve.Q<Button>("btn-items").clickable.clickedWithEventInfo       += _e => { Show(_e, new EditorItems()      ); };
        ve.Q<Button>("btn-key-items").clickable.clickedWithEventInfo   += _e => { Show(_e, new EditorKeyItems()   ); };
        ve.Q<Button>("btn-weapons").clickable.clickedWithEventInfo     += _e => { Show(_e, new EditorWeapons()    ); };
        ve.Q<Button>("btn-armor").clickable.clickedWithEventInfo       += _e => { Show(_e, new EditorArmor()      ); };
        ve.Q<Button>("btn-accessories").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorAccessories()); };
        ve.Q<Button>("btn-materia").clickable.clickedWithEventInfo     += _e => { Show(_e, new EditorMateria()    ); };

        Show(ve.Q<VisualElement>("btn-items"), new EditorItems());
    }
}
