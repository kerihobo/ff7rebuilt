﻿using UnityEngine;
using System;
using System.Collections.Generic;
using InventoryTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Materia", menuName = "Kerihobo/Database/Inventory/Materia")]
    public class DBMateria : Database {
        public List<Materia> magic       = new List<Materia>();
        public List<Materia> summon      = new List<Materia>();
        public List<Materia> command     = new List<Materia>();
        public List<Materia> independent = new List<Materia>();
        public List<Materia> support     = new List<Materia>();

        public MateriaType selectedMenuType { get; private set; }

        public List<Materia> GetSelectedMateriaList(MateriaType _type) => _type switch {
            MateriaType.Magic => magic,
            MateriaType.Summon => summon,
            MateriaType.Command => command,
            MateriaType.Independent => independent,
            MateriaType.Support => support,
            _ => null
        };
    }
}