using TMPro;
using UnityEngine;

public class UIPlayTime : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtHours;
    [SerializeField] private TextMeshProUGUI txtMinutes;
    [SerializeField] private TextMeshProUGUI txtSeconds;
    [SerializeField] private TextMeshProUGUI txtSemicolon;

    private Color white;
    private Color grey;

    private void Start() {
        white = TextColourContainer.GetInstance().white;
        grey = TextColourContainer.GetInstance().grey;
    }

    private void Update() {
        int secondsTotal = (int)GameManager.Instance.timeInSeconds;
        int seconds = secondsTotal % 60;
        int minutes = ((int)(secondsTotal / 60f)) % 60;
        int hours = ((int)(secondsTotal / (60f * 12)));
        txtSeconds.text = "<mspace=12>" + seconds.ToString("00");
        txtMinutes.text = "<mspace=12>" + minutes.ToString("00");
        txtHours.text = "<mspace=12>" + hours.ToString();

        txtSemicolon.color = Time.time % 1 < .5f ? grey : white;
    }
}
