﻿using UnityEngine;

public class NodeGetInt : NodeGet {
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        minMaxThresholds = new Vector2(140, 140);
        size = new Vector2(minMaxThresholds.y, 50);

        AddPortIn<PortFieldLabel>();
        AddPortOut<PortInt>("Value", Port.Capacity.MULTIPLE);
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeGetInt>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeGetInt)newNode).field = field;

        field.nodes.Add((NodeGetInt)newNode);

        return newNode;
    }

    public override dynamic GetValue() {
        int value = ((FieldInt)field).value;
        return value;
    }
}
