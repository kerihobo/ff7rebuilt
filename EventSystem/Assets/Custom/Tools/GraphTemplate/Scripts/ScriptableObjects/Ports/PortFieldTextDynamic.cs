﻿using UnityEngine;

public class PortFieldTextDynamic : Port {
    public string value;

    public override void Initialize(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) {
        base.Initialize(_parentNode, _portName, _direction, _capacity,  _displayMode);

        capacity = Capacity.SINGLE;
        portType = NodeDataType.Dynamic;
    }

    protected override void RenderAsInput(StyleSet _styleSet) {
        GUILayout.Box("", GetPortStyle(_styleSet), GUILayout.Width(16), GUILayout.Height(16));
        GetIsHovered();

        if (connectedPorts.Count == 0) {
            value = GUILayout.TextField(value, GUILayout.Width(100));
        }

        GUILayout.Label("String", _styleSet.inLabel);
    }

    public override dynamic GetValue() {
        //List<Port> connectedPorts = portContainerOut.ports[0].connectedPorts;
        if (connectedPorts.Count > 0) {
            return connectedPorts[0].GetValue();
        }

        return value;
    }

    public override void SetValue(dynamic _value) {
        value = _value;
    }
}
