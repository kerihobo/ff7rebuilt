﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[Serializable]
public abstract class BlackboardField : ScriptableObject {
    public Blackboard parentBlackboard;
    public NodeDataType dataType;
    public string renameString;
    public List<NodeGetSet> nodes = new List<NodeGetSet>();

    public bool isHovered { get; set; }
    public bool isExpanded { get; set; }
    public Rect rect { get; set; }

    public static T Create<T>(Blackboard _blackboard) where T : BlackboardField {
        T newField = CreateInstance<T>();
        newField.Initialize(_blackboard);
        _blackboard.PreventDuplicateName(newField);

        AssetDatabase.AddObjectToAsset(newField, _blackboard);
        _blackboard.fields.Add(newField);

        EditorUtility.SetDirty(_blackboard.parentGraph);

        return newField;
    }

    public virtual string GetValueAsString() {
        return null;
    }

    protected virtual void Initialize(Blackboard _blackboard) {
        parentBlackboard = _blackboard;
        hideFlags = HideFlags.HideInHierarchy;
    }

    private void GetIsHovered() {
        Event e = Event.current;

        if (e.type == EventType.Repaint) {
            rect = GUILayoutUtility.GetLastRect();
            isHovered = rect.Contains(Event.current.mousePosition);
        }
    }

    public void Render(StyleSet _styleSet) {
        GUILayout.BeginVertical();
            RenderHead();
            RenderBody(_styleSet);
        GUILayout.EndVertical();
    }

    internal void RemoveNode(NodeGetSet _rightClickedNode) {
        nodes.Remove(_rightClickedNode);
    }

    private void RenderHead() {
        GUILayout.BeginHorizontal();
            if (GUILayout.Button(">", GUILayout.MaxWidth(20))) {
                isExpanded = !isExpanded;
                renameString = name;
            }

            GUILayout.Label(name);
            GetIsHovered();
            GUILayout.Label(dataType.ToString(), GUILayout.Width(40));

            //if (GUILayout.Button("X", GUILayout.MaxWidth(20))) {
            //    parentBlackboard.DeleteField(this);
            //}
        GUILayout.EndHorizontal();
    }
    
    private void RenderBody(StyleSet _styleSet) {
        GUILayout.BeginVertical(_styleSet.blackboardFieldBody);
            if (isExpanded) {
                RenderRename();
                RenderValue();
            }
        GUILayout.EndVertical();
    }

    private void RenderRename() {
        GUILayout.BeginHorizontal();
            GUILayout.Label("Name", GUILayout.Width(40));

            renameString = EditorGUILayout.TextField(renameString);
            
            if (GUILayout.Button("Rename", GUILayout.Width(60))) {
                name = renameString;
                parentBlackboard.PreventDuplicateName(this);

                foreach (NodeGetSet gs in nodes) {
                    gs.name = gs.name.Substring(0, 4) + name;
                }
            }
        GUILayout.EndHorizontal();
    }

    protected virtual void RenderValue() {
    }
}
