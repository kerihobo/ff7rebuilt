﻿using System;
using System.Collections.Generic;
using UnityEngine;

/*
 * Regan Music - Yesterday at 5:43 PM
 * Greetings all, sorry to lurk for months on end but i only dev on ff7 stuff when I've time.
 * I was hoping someone might know the answer to this. I'm trying to replicate FF7 battlefields
 * and I was wondering if anyone knew the size of the battlefield, and if or not that size was 
 * always the same between special/normal battle formations
 * More than anything I'm after a size ratio, I can sort the exact world-units later
 * if it was like "always roughly 2:1" then I guess I'm happy
 * I read on the wiki that monsters are placed via a number of pre-defined locations?
 * I'm making a monster placement tool so just wondering what rules FF7 already abides
 * 
 * Uprisen - Yesterday at 11:02 PM
 * if you use the tool Proud Clod you can go to the formations tab and see the enemies locations
 * in hex and view the location with the built in viewer. ff7 battle fields arent all the same 
 * dimensions but the "standard" enemy placement ranges should work on every field. ive been 
 * meaning to make a graph in GIMP to grid out the placement locations that PrC uses.
*/

[Serializable]
public class Formation : Data {
    public Formation() : base() {
        name = "New Formation";
    }

    /// <summary>
    /// Copy constructor.
    /// </summary>
    /// <param name="_source"></param>
    public Formation(Formation _source) : base(_source) {
        id          = _source.id;
        probability = _source.probability;
        placements  = _source.placements;
    }

    [Serializable]
    public class Placement {
        public SelectionData selectionEnemy = new SelectionData();
        public Vector2 initialPosition;
    }

    public int probability;
    [SerializeReference]
    public List<Placement> placements = new List<Placement>();

    [NonSerialized]
    public int selectedPlacementIndex = -1;
}
