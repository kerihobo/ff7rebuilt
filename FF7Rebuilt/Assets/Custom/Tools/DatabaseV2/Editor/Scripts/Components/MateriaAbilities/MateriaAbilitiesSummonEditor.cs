using FFVII.Database;
using InventoryTypes;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

public class MateriaAbilitiesSummonEditor : MateriaAbilitiesEditor {
    public MateriaAbilitiesSummonEditor(Materia _target, Database _database) : base(_target, _database) {
    }

    protected override string UXMLPath { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/MateriaAbilities/materia-abilities-summon.uxml";
    
    public override void BindAbilities() {
        DropdownField ddfCursorAction = this.Q<DropdownField>("drp-subtype");
        ddfCursorAction.choices = SummonSubtype.SUMMON_SUBTYPE_NAMES.ToList();
        ddfCursorAction.index = target.subtype;
        ddfCursorAction.RegisterValueChangedCallback(_e => {
            target.subtype = ddfCursorAction.index;

            RefreshSelectionDisplays();
            
            EditorUtility.SetDirty(database);
        });

        DropdownField ddf = this.Q<DropdownField>($"ddf-ability-0");
        ddf.Initialize(DBResources.GetAbilityList(AbilityType.Summon), target.selectionSpells[0], database);
        ddf.RegisterValueChangedCallback(_e => {
            target.selectionSpells[0].SetDataExternally(ddf.index);
            EditorUtility.SetDirty(database);
        });
    }

    public override void Refresh(Materia _target, VisualElement _ve) {
        target = _target;
        veParent = _ve;

        this.Q<DropdownField>("drp-subtype").index = target.subtype;

        RefreshSelectionDisplays();

        for (int i = 0; i < target.selectionSpells.Length; i++) {
            this.Q<DropdownField>($"ddf-ability-0").Initialize(DBResources.GetAbilityList(AbilityType.Summon), target.selectionSpells[0], database);
        }
    }
}
