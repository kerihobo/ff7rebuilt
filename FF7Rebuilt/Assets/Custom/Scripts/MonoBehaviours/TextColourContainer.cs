using UnityEngine;

[CreateAssetMenu(fileName = "TextColourContainer", menuName = "Kerihobo/Utilities/Text Colour Container")]
public class TextColourContainer : ScriptableObject {
    private static TextColourContainer Instance;

    public Color white;
    public Color grey;
    public Color yellow;
    public Color red;
    public Color magenta;
    public Color cyan;
    public Color green;

    public static TextColourContainer GetInstance() {
        if (!Instance) {
            Instance = Resources.Load<TextColourContainer>("ScriptableObjects/TextColourContainer");
        }

        return Instance;
    }
}
