﻿using System;
using UnityEngine;

[Serializable]
public class NodeSetInt : NodeSet {
    public NodeSetInt(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        minMaxThresholds = new Vector2(130, 190);
        size = new Vector2(minMaxThresholds.y, 74);

        AddPortIn<PortInt>("Value", Port.Capacity.SINGLE);
        AddPortOut<PortSetIntFunction>();
    }
    
    public enum Function {
        Equal
    ,   Increment
    ,   Decrement
    }

    public Function function;

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        function = (Function)portContainerOut.ports[1].GetValue();
        portContainerIn.ports[1].IsActive = function == Function.Equal;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        NodeSetInt newNode = new NodeSetInt(_nodeGraph, position + duplicationOffset, category, false);
        newNode.portContainerIn.ports[1].SetValue(portContainerIn.ports[1].GetValue());
        newNode.portContainerOut.ports[1].SetValue(portContainerOut.ports[1].GetValue());

        newNode.Initialize(field);

        return newNode;
    }

    public override void Run(NodeGraph _nodeGraph) {
        FieldInt targetField = (FieldInt)field;

        switch (function) {
            case Function.Equal:
                targetField.value = portContainerIn.ports[1].GetValue();
                break;
            case Function.Increment:
                targetField.value++;
                break;
            case Function.Decrement:
                targetField.value--;
                break;
        }
    }
}
