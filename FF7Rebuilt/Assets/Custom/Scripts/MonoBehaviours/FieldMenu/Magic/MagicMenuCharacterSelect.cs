using CharacterTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class MagicMenuCharacterSelect : MenuControllable {
    [SerializeField] private FingerCursorRight cursor;
    [SerializeField] private UICharacterInfoContainer characterInfoContainer;
    [SerializeField] private TextMeshProUGUI txtSpellName;
    [SerializeField] private RectTransform rctMPWarning;
    private int selectedIndex;
    private MagicMenuMagicList magicList;
    private ScreenFieldMagic screenFieldMagic;
    private Ability abilitySelected;
    private Vector2 sizeMPWarning;
    private bool isAll;

    private List<UICharacterInfo> AvailableCharacterInfos => characterInfoContainer.arrPlayerInfos.Where(x => x.player != null).ToList();

    public void Initialize(MagicMenuMagicList _magicList, ScreenFieldMagic _screenFieldMagic) {
        magicList = _magicList;
        screenFieldMagic = _screenFieldMagic;

        characterInfoContainer.Initialize();

        sizeMPWarning = rctMPWarning.sizeDelta;
        rctMPWarning.gameObject.SetActive(false);

        Close();

        RectTransform firstRectTransform = characterInfoContainer.arrPlayerInfos[0].rct;
        cursor.Initialize(firstRectTransform);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        cursor.Show();

        SetCursor(cursor);
    }

    public void SetCursor(FingerCursorRight _cursor) {
        _cursor.SetCursorPosition(characterInfoContainer.arrPlayerInfos[selectedIndex].rct);
    }

    public void Open(Ability _abilitySelected) {
        abilitySelected = _abilitySelected;
        txtSpellName.text = _abilitySelected.name;
        
        Show();
        characterInfoContainer.Refresh();
        
        isAll = false;
        cursor.SetTargetSingle(AvailableCharacterInfos.Select(x => x.rct).ToList()[selectedIndex]);

        SetControllable();
    }

    private void Hide() {
        gameObject.SetActive(false);
    }

    private void Show() {
        cursor.Show();
        gameObject.SetActive(true);
    }

    private void Close() {
        cursor.Hide();
        Hide();
        
        abilitySelected = null;
    }

    private void ToggleAll() {
        if (AvailableCharacterInfos.Count == 1) return;
        
        magicList.GetSelectedCharacter.dctAbilityAugmentations.TryGetValue(abilitySelected, out Player.AbilityAugmentation abilityAugmentation);

        if (abilityAugmentation == null) return;

        bool hasAll = abilityAugmentation.all > 0;
        if (!hasAll) return;

        isAll = !isAll;

        if (isAll) {
            cursor.SetTargetAll(AvailableCharacterInfos.Select(x => x.rct).ToList());
        } else {
            cursor.SetTargetSingle(AvailableCharacterInfos.Select(x => x.rct).ToList()[selectedIndex]);
        }
    }

    private void UseMagic() {
        Player playerSource = magicList.GetSelectedCharacter;
        UICharacterInfo selectedPlayerInfo = characterInfoContainer.arrPlayerInfos[selectedIndex];
        Player playerTarget = selectedPlayerInfo.player;

        if (playerTarget == null) return;

        Debug.Log($"{playerSource.name} uses {abilitySelected.name} on {selectedPlayerInfo.player.name}");

        // =========================
        // Use ability on character.
        // =========================
        //Collectible item = itemSelected.item;
        bool isUsed = false;

        Debug.Log($"{abilitySelected.name} > {playerTarget.name}");

        // Use item on character.
        TryUseAbility();

        if (isUsed) {
            Debug.Log($"{playerSource.mpCurrent} - {abilitySelected.mpCost}");
            playerSource.mpCurrent -= abilitySelected.mpCost;
            Debug.Log($"{playerSource.mpCurrent}");

            characterInfoContainer.Refresh();

            magicList.Refresh();
            screenFieldMagic.characterInfo.Refresh(playerSource);
        }

        void TryUseAbility() {
            if (abilitySelected.mpCost > playerSource.mpCurrent) {
                StartCoroutine(DisplayLimitMessage());

                return;
            }

            if (cursor.IsAll) {
                foreach (UICharacterInfo info in AvailableCharacterInfos) {
                    if (abilitySelected.TryUse(playerSource, info.player, isAll, false)) {
                        isUsed = true;
                    }
                }
            } else {
                isUsed = abilitySelected.TryUse(playerSource, playerTarget, false, false);
            }

            IEnumerator DisplayLimitMessage() {
                FieldMenu.Instance.menuControllable = null;

                rctMPWarning.gameObject.SetActive(true);
                rctMPWarning.sizeDelta = Vector2.zero;

                float t = 0;
                float duration = .1f;

                while (t < duration) {
                    rctMPWarning.sizeDelta = Vector2.Lerp(Vector2.zero, sizeMPWarning, t / duration);

                    t += Time.deltaTime;
                    yield return null;
                }

                rctMPWarning.sizeDelta = sizeMPWarning;
                t = 0;

                yield return new WaitForSeconds(.33f);

                SetControllable();

                while (t < duration) {
                    rctMPWarning.sizeDelta = Vector2.Lerp(sizeMPWarning, Vector2.zero, t / duration);

                    t += Time.deltaTime;
                    yield return null;
                }

                rctMPWarning.gameObject.SetActive(false);
            }
        }
    }

    #region Override methods.
    public override void GetInputConfirmPressed() {
        UseMagic();
    }

    public override void GetInputCancelPressed() {
        Close();
        magicList.ExitCharacterSelection();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (isAll) return;

        int v = -_v;
        selectedIndex = (selectedIndex + v) % characterInfoContainer.arrPlayerInfos.Length;

        if (selectedIndex < 0) {
            selectedIndex = characterInfoContainer.arrPlayerInfos.Length - 1;
        }

        SetCursor(cursor);
    }

    public override void GetInputHorizontal(int h, bool isHorizontalPressed) {
        if (isHorizontalPressed) {
            ToggleAll();
        }
    }

    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
