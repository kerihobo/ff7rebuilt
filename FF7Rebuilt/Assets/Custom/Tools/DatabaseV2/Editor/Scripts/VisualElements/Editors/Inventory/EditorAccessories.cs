using FFVII.Database;
using InventoryTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorAccessories : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Inventory/editor-accessories.uxml";
    protected override Database database { get; set; } = DBResources.GetAccessories;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetAccessories.accessoryList[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetAccessories.accessoryList;

    private (Button, ElementalAffinityManager.Affinity)[] arrPairElementButtons;
    private List<string> defenseTypeNames = new List<string>() { "Half", "Immune", "Absorb" };

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Accessory)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        if (target == null) return;

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<TextField>("txt-description").SetValue(((Accessory)target).description).RegisterValueChangedCallback(e => {
            ((Accessory)target).description = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<IntegerField>("int-sale-cost").SetValue(((Accessory)target).saleCost).RegisterValueChangedCallback(e => {
            ((Accessory)target).saleCost = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<EnumField>("enum-special-accessory-type").SetSpecialAccessoryType(((Accessory)target).specialAccessory).RegisterValueChangedCallback(e => {
            ((Accessory)target).specialAccessory = (SpecialAccessoryType)e.newValue;
            EditorUtility.SetDirty(database);
        });

        BindStatEnhancements();
        BindResistanceElements();
        BindResistanceStatuses();
        BindAutoStatuses();

        void BindStatEnhancements() {
            GroupBox statEnhancements = ve.Q<GroupBox>("grp-stat-enhancements");

            statEnhancements.Q<IntegerField>("int-strength").SetValue(((Accessory)target).equipEffects.primary.strength).RegisterValueChangedCallback(e => {
                ((Accessory)target).equipEffects.primary.strength = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statEnhancements.Q<IntegerField>("int-vitality").SetValue(((Accessory)target).equipEffects.primary.vitality).RegisterValueChangedCallback(e => {
                ((Accessory)target).equipEffects.primary.vitality = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statEnhancements.Q<IntegerField>("int-dexterity").SetValue(((Accessory)target).equipEffects.primary.dexterity).RegisterValueChangedCallback(e => {
                ((Accessory)target).equipEffects.primary.dexterity = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statEnhancements.Q<IntegerField>("int-magic").SetValue(((Accessory)target).equipEffects.primary.magic).RegisterValueChangedCallback(e => {
                ((Accessory)target).equipEffects.primary.magic = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statEnhancements.Q<IntegerField>("int-spirit").SetValue(((Accessory)target).equipEffects.primary.spirit).RegisterValueChangedCallback(e => {
                ((Accessory)target).equipEffects.primary.spirit = e.newValue;
                EditorUtility.SetDirty(database);
            });

            statEnhancements.Q<IntegerField>("int-luck").SetValue(((Accessory)target).equipEffects.primary.luck).RegisterValueChangedCallback(e => {
                ((Accessory)target).equipEffects.primary.luck = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindResistanceElements() {
            VisualElement resistanceElements = ve.Q<VisualElement>("ve-elemental-vulnerabilities");

            RestockButtonArray();

            DropdownField ddf = this.Q<DropdownField>("ddf-defense-type");
            ddf.RegisterValueChangedCallback(_e => {
                for (int i = 0; i < arrPairElementButtons.Length; i++) {
                    int j = i;
                    arrPairElementButtons[j].Item2.damageModifier = (ElementalAffinityManager.DamageModifier)Mathf.Clamp(ddf.index + 2, 2, 4);
                    arrPairElementButtons[j].Item1.SetElementalVulnerabilityClass(arrPairElementButtons[j].Item2, true);
                }

                EditorUtility.SetDirty(database);
            });

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                int j = i;
                arrPairElementButtons[j].Item1.RegisterCallback<ClickEvent>(e => {
                    arrPairElementButtons[j].Item1.ToggleElementalVulnerability(ref arrPairElementButtons[j].Item2);
                    EditorUtility.SetDirty(database);
                });
            }
        }

        void BindResistanceStatuses() {
            var statusResistances = ve.Q<VisualElement>("ve-status-resistances");

            statusResistances.Q<Button>("btn-death").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.death);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-sleep").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.sleep);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-poison").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.poison);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-sadness").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.sadness);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-fury").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.fury);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-confusion").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.confusion);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-silence").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.silence);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-haste").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.haste);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-slow").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.slow);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-stop").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.stop);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-frog").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.frog);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-small").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.small);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-slow-numb").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.slowNumb);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-petrify").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.petrify);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-regen").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.regen);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-barrier").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.barrier);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-mbarrier").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.mBarrier);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-reflect").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.reflect);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-shield").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.shield);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-death-sentence").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.deathSentence);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-berserk").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.berserk);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-paralyzed").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.paralyzed);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-darkness").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.darkness);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-death-force").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.deathForce);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-resist").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.resist);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-manipulate").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.manipulate);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-sense").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.sense);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-seizure").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.seizure);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-imprisoned").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.imprisoned);
                EditorUtility.SetDirty(database);
            });

            statusResistances.Q<Button>("btn-peerless").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).statusResistance.peerless);
                EditorUtility.SetDirty(database);
            });
        }

        void BindAutoStatuses() {
            var autoStatuses = ve.Q<VisualElement>("ve-auto-statuses");

            autoStatuses.Q<Button>("btn-auto-death-sentence").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).autoStatus.deathSentence);
                EditorUtility.SetDirty(database);
            });

            autoStatuses.Q<Button>("btn-auto-haste").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).autoStatus.haste);
                EditorUtility.SetDirty(database);
            });

            autoStatuses.Q<Button>("btn-auto-berserk").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).autoStatus.berserk);
                EditorUtility.SetDirty(database);
            });

            autoStatuses.Q<Button>("btn-auto-barrier").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).autoStatus.barrier);
                EditorUtility.SetDirty(database);
            });

            autoStatuses.Q<Button>("btn-auto-mbarrier").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).autoStatus.mBarrier);
                EditorUtility.SetDirty(database);
            });

            autoStatuses.Q<Button>("btn-auto-reflect").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).autoStatus.reflect);
                EditorUtility.SetDirty(database);
            });

            autoStatuses.Q<Button>("btn-auto-peerless").RegisterCallback<ClickEvent>(e => {
                ((Button)e.currentTarget).ToggleAfflictionBehaviour(ref ((Accessory)target).autoStatus.peerless);
                EditorUtility.SetDirty(database);
            });
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        Accessory accessory = (Accessory)target;

        ve.Q<TextField>("txt-name").value = target.name;
        ve.Q<TextField>("txt-description").value = accessory.description;
        ve.Q<IntegerField>("int-sale-cost").value = accessory.saleCost;
        ve.Q<EnumField>("enum-special-accessory-type").value = accessory.specialAccessory;

        RefreshStatEnhancements();
        RefreshElementalVulnerabilities();
        RefreshStatusResistances();
        RefreshAutoStatuses();

        void RefreshStatEnhancements() {
            GroupBox statEnhancements = ve.Q<GroupBox>("grp-stat-enhancements");
            
            statEnhancements.Q<IntegerField>("int-strength").value  = accessory.equipEffects.primary.strength;
            statEnhancements.Q<IntegerField>("int-vitality").value  = accessory.equipEffects.primary.vitality;
            statEnhancements.Q<IntegerField>("int-dexterity").value = accessory.equipEffects.primary.dexterity;
            statEnhancements.Q<IntegerField>("int-magic").value     = accessory.equipEffects.primary.magic;
            statEnhancements.Q<IntegerField>("int-spirit").value    = accessory.equipEffects.primary.spirit;
            statEnhancements.Q<IntegerField>("int-luck").value      = accessory.equipEffects.primary.luck;
        }

        void RefreshElementalVulnerabilities() {
            VisualElement veResistanceElements = ve.Q<VisualElement>("ve-elemental-vulnerabilities");
            ElementalAffinityManager elements = accessory.elements;

            RestockButtonArray();

            DropdownField ddf = veResistanceElements.Q<DropdownField>("ddf-defense-type");
            ddf.choices = defenseTypeNames;
            ddf.index = Mathf.Clamp((int)elements.fire.damageModifier - 2, 0, 2);
            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                arrPairElementButtons[i].Item2.damageModifier = (ElementalAffinityManager.DamageModifier)Mathf.Clamp(ddf.index + 2, 2, 4);
                arrPairElementButtons[i].Item1.SetElementalVulnerabilityClass(arrPairElementButtons[i].Item2, true);
            }
        }

        void RefreshStatusResistances() {
            var statusResistances = ve.Q<VisualElement>("ve-status-resistances");
            StatusEffectManager statusResistance = accessory.statusResistance;

            statusResistances.Q<Button>("btn-death")         .SetAfflictionBehaviourClass(statusResistance.death.isActive,         StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-sleep")         .SetAfflictionBehaviourClass(statusResistance.sleep.isActive,         StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-poison")        .SetAfflictionBehaviourClass(statusResistance.poison.isActive,        StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-sadness")       .SetAfflictionBehaviourClass(statusResistance.sadness.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-fury")          .SetAfflictionBehaviourClass(statusResistance.fury.isActive,          StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-confusion")     .SetAfflictionBehaviourClass(statusResistance.confusion.isActive,     StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-silence")       .SetAfflictionBehaviourClass(statusResistance.silence.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-haste")         .SetAfflictionBehaviourClass(statusResistance.haste.isActive,         StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-slow")          .SetAfflictionBehaviourClass(statusResistance.slow.isActive,          StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-stop")          .SetAfflictionBehaviourClass(statusResistance.stop.isActive,          StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-frog")          .SetAfflictionBehaviourClass(statusResistance.frog.isActive,          StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-small")         .SetAfflictionBehaviourClass(statusResistance.small.isActive,         StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-slow-numb")     .SetAfflictionBehaviourClass(statusResistance.slowNumb.isActive,      StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-petrify")       .SetAfflictionBehaviourClass(statusResistance.petrify.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-regen")         .SetAfflictionBehaviourClass(statusResistance.regen.isActive,         StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-barrier")       .SetAfflictionBehaviourClass(statusResistance.barrier.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-mbarrier")      .SetAfflictionBehaviourClass(statusResistance.mBarrier.isActive,      StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-reflect")       .SetAfflictionBehaviourClass(statusResistance.reflect.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-shield")        .SetAfflictionBehaviourClass(statusResistance.shield.isActive,        StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-death-sentence").SetAfflictionBehaviourClass(statusResistance.deathSentence.isActive, StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-berserk")       .SetAfflictionBehaviourClass(statusResistance.berserk.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-paralyzed")     .SetAfflictionBehaviourClass(statusResistance.paralyzed.isActive,     StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-darkness")      .SetAfflictionBehaviourClass(statusResistance.darkness.isActive,      StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-death-force")   .SetAfflictionBehaviourClass(statusResistance.deathForce.isActive,    StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-resist")        .SetAfflictionBehaviourClass(statusResistance.resist.isActive,        StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-manipulate")    .SetAfflictionBehaviourClass(statusResistance.manipulate.isActive,    StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-sense")         .SetAfflictionBehaviourClass(statusResistance.sense.isActive,         StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-seizure")       .SetAfflictionBehaviourClass(statusResistance.seizure.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-imprisoned")    .SetAfflictionBehaviourClass(statusResistance.imprisoned.isActive,    StatusEffectManager.StatusEffectAction.AFFLICT);
            statusResistances.Q<Button>("btn-peerless")      .SetAfflictionBehaviourClass(statusResistance.peerless.isActive,      StatusEffectManager.StatusEffectAction.AFFLICT);
        }

        void RefreshAutoStatuses() {
            var autoStatuses = ve.Q<VisualElement>("ve-auto-statuses");
            StatusEffectManager autoStatus = accessory.autoStatus;

            autoStatuses.Q<Button>("btn-auto-death-sentence").SetAfflictionBehaviourClass(autoStatus.deathSentence.isActive, StatusEffectManager.StatusEffectAction.AFFLICT);
            autoStatuses.Q<Button>("btn-auto-haste")         .SetAfflictionBehaviourClass(autoStatus.haste.isActive,         StatusEffectManager.StatusEffectAction.AFFLICT);
            autoStatuses.Q<Button>("btn-auto-berserk")       .SetAfflictionBehaviourClass(autoStatus.berserk.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            autoStatuses.Q<Button>("btn-auto-barrier")       .SetAfflictionBehaviourClass(autoStatus.barrier.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            autoStatuses.Q<Button>("btn-auto-mbarrier")      .SetAfflictionBehaviourClass(autoStatus.mBarrier.isActive,      StatusEffectManager.StatusEffectAction.AFFLICT);
            autoStatuses.Q<Button>("btn-auto-reflect")       .SetAfflictionBehaviourClass(autoStatus.reflect.isActive,       StatusEffectManager.StatusEffectAction.AFFLICT);
            autoStatuses.Q<Button>("btn-auto-peerless")      .SetAfflictionBehaviourClass(autoStatus.peerless.isActive,      StatusEffectManager.StatusEffectAction.AFFLICT);
        }
    }

    void RestockButtonArray() {
        VisualElement resistanceElements = ve.Q<VisualElement>("ve-elemental-vulnerabilities");
        List<Button> arrButtons = resistanceElements.Query<Button>().ToList();

        arrPairElementButtons = new (Button, ElementalAffinityManager.Affinity)[] {
                (arrButtons[0],  ((Accessory)target).elements.fire)
            ,   (arrButtons[1],  ((Accessory)target).elements.ice)
            ,   (arrButtons[2],  ((Accessory)target).elements.lightning)
            ,   (arrButtons[3],  ((Accessory)target).elements.earth)
            ,   (arrButtons[4],  ((Accessory)target).elements.poison)
            ,   (arrButtons[5],  ((Accessory)target).elements.gravity)
            ,   (arrButtons[6],  ((Accessory)target).elements.water)
            ,   (arrButtons[7],  ((Accessory)target).elements.wind)
            ,   (arrButtons[8],  ((Accessory)target).elements.holy)
            ,   (arrButtons[9],  ((Accessory)target).elements._10th)
            ,   (arrButtons[10], ((Accessory)target).elements.restorative)
            ,   (arrButtons[11], ((Accessory)target).elements.backAttack)
            ,   (arrButtons[12], ((Accessory)target).elements.punch)
            ,   (arrButtons[13], ((Accessory)target).elements.cut)
            ,   (arrButtons[14], ((Accessory)target).elements.hit)
            ,   (arrButtons[15], ((Accessory)target).elements.shoot)
            ,   (arrButtons[16], ((Accessory)target).elements.shout)
            ,   (arrButtons[17], ((Accessory)target).elements.death)
            ,   (arrButtons[18], ((Accessory)target).elements.sleep)
            ,   (arrButtons[19], ((Accessory)target).elements.confusion)
            };
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Accessory());

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new Accessory());

        base.Add();
    }
}
