﻿using InventoryTypes;
using System;
using System.Collections.Generic;
using UnityEngine;
using FFVII.Database;
using CharacterTypes;

[Serializable]
public class SelectionData {
    public string selectionName;
    public int index = -1;

    [SerializeField]
    protected List<string> names = new List<string>();
    // Names always have one more item than the list they're mirroring.
    // This is for the additional "-" option to allow "null" to be set.
    [SerializeField]
    protected int selectionDummy;

    public string GetCleanName => index < 0 ? "" : selectionName;

    public virtual string Reset<T>(List<T> _list) where T : Data {
        if (_list.IsNullOrEmpty()) {
            index = -1;
            selectionDummy = -1;
            names = null;

            return "NULL";
        }

        Data.ConvertDBListToNamesArray(_list, ref names);

        if (index >= names.Count) {
            index = names.Count - 1;
        }

        if (index < 0) {
            return "Empty";
        }

        return names[index];
    }

    protected bool IsListOutdated<T>(List<T> _list) where T : Data {
        bool isNameMisMatch = false;

        if (_list == null) {
            return true;
        }

        for (int i = 0; i < _list.Count; i++) {
            if (i >= names?.Count - 1 || _list[i]?.name != names?[i+1]) {
                Debug.Log("Reporting name mismatch.");
                isNameMisMatch = true;
                break;
            }
        }

        return
            isNameMisMatch
        ||  selectionDummy >= names?.Count 
        ||  names?.Count != _list.Count + 1
        ;
    }

    public void SetDataExternally(int _index) {
        selectionDummy = _index;
        index = selectionDummy - 1;

        selectionName = selectionDummy > 0 ? names[selectionDummy] : "NULL";
    }

    public void ForceNamesUpdate<T>(List <T> _list) where T : Data {
        Data.ConvertDBListToNamesArray(_list, ref names);
    }

    public Player GetSelectedPlayer(List<Player> lsSource = null) {
        if (lsSource == null) {
            lsSource = DBResources.GetPlayers.playerList;
        }

        return index >= 0 && lsSource.Count > index ? lsSource[index] : null;
    }

    public Enemy GetSelectedEnemy(List<Enemy> lsSource = null) {
        if (lsSource == null) {
            lsSource = DBResources.GetEnemies.enemyList;
        }

        return index >= 0 && lsSource.Count > index ? lsSource[index] : null;
    }

    public Item GetSelectedItem(List<Item> lsSource = null) {
        if (lsSource == null) {
            lsSource = DBResources.GetItems.itemsList;
        }

        return index >= 0 && lsSource.Count > index ? lsSource[index] : null;
    }

    public Armor GetSelectedArmor(List<Armor> lsSource = null) {
        if (lsSource == null) {
            lsSource = DBResources.GetArmor.armorList;
        }

        return index >= 0 && lsSource.Count > index ? lsSource[index] : null;
    }

    public Accessory GetSelectedAccessory(List<Accessory> lsSource = null) {
        if (lsSource == null) {
            lsSource = DBResources.GetAccessories.accessoryList;
        }

        return index >= 0 && lsSource.Count > index ? lsSource[index] : null;
    }

    public Ability GetSelectedAbility(AbilityType _type) {
        List<Ability> lsSource = DBResources.GetAbilities.GetSelectedAbilityList(_type);

        return index >= 0 && lsSource.Count > index ? lsSource[index] : null;
    }
}
