﻿using System.Collections.Generic;
using UnityEngine;

public class NodePrint : Node {
    public NodePrint(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        name = "Print";
        size = new Vector2(220, 75);

        AddPortIn<Port>();
        AddPortIn<PortFieldTextDynamic>();

        AddPortOut<Port>();
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(120, 220);
    }

    public override void Run(NodeGraph _nodeGraph) {
        dynamic dynamicValue = portContainerIn.ports[1].GetValue();
        Debug.Log($"{_nodeGraph} (Print): {dynamicValue}");

        List<Port> connectedPorts = portContainerOut.ports[0].connectedPorts;
        if (connectedPorts.Count > 0) {
            connectedPorts[0].parentNode.Run(_nodeGraph);
        }
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        dynamic newNode = new NodePrint(_nodeGraph, position + duplicationOffset, category, false);
        ((NodePrint)newNode).portContainerIn.ports[1].SetValue(portContainerIn.ports[1].GetValue());

        return newNode;
    }
}
