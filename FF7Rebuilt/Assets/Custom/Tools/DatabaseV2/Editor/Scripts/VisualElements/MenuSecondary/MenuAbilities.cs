using UnityEngine.UIElements;

public class MenuAbilities : MenuSecondary {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/MenuSecondary/menu-abilities.uxml";
    protected override string Name { get; set; } = "menu-abilities";

    public MenuAbilities() : base() {
        ve.Q<Button>("btn-commands").clickable.clickedWithEventInfo      += _e => { Show(_e, new EditorCommands()   , EWDatabaseEditor.ColumnWidths.ONE); };
        ve.Q<Button>("btn-magic").clickable.clickedWithEventInfo        += _e => { Show(_e, new EditorMagic()      , EWDatabaseEditor.ColumnWidths.TWO); };
        ve.Q<Button>("btn-summons").clickable.clickedWithEventInfo      += _e => { Show(_e, new EditorSummons()    , EWDatabaseEditor.ColumnWidths.TWO); };
        ve.Q<Button>("btn-enemy-skills").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorEnemySkills(), EWDatabaseEditor.ColumnWidths.TWO); };
        ve.Q<Button>("btn-limit-breaks").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorLimitBreaks(), EWDatabaseEditor.ColumnWidths.TWO); };
        ve.Q<Button>("btn-battle-items").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorBattleItems(), EWDatabaseEditor.ColumnWidths.TWO); };

        Show(ve.Q<Button>("btn-commands"), new EditorCommands(), EWDatabaseEditor.ColumnWidths.ONE);
    }
}
