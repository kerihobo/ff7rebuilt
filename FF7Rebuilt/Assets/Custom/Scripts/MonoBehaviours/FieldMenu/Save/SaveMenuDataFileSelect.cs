using System.Linq;
using TMPro;
using UnityEngine;

public class SaveMenuDataFileSelect : MenuControllable {
    private const int COLUMNS = 5;
    public FingerCursorRight cursor;

    [SerializeField] private SaveMenuSaveDataFileCheck saveDataFileCheck;
    [SerializeField] private RectTransform optionParent;
    private TextMeshProUGUI[] arrOptions;
    private ScreenFieldSave screenFieldSave;
    private int selectedIndex;
    private Color white;
    private Color grey;

    public void Initialize(ScreenFieldSave _screenFieldSave) {
        screenFieldSave = _screenFieldSave;

        arrOptions = new TextMeshProUGUI[optionParent.childCount];
        for (int i = 0; i < arrOptions.Length; i++) {
            arrOptions[i] = optionParent.GetChild(i).GetComponent<TextMeshProUGUI>();
        }

        cursor.Initialize(arrOptions[0].rectTransform);

        saveDataFileCheck.Initialize(screenFieldSave);

        white = TextColourContainer.GetInstance().white;
        grey = TextColourContainer.GetInstance().grey;

        Disable();
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void Open() {
        SetCursor();
        SetControllable();
        Show();
        Refresh();

        screenFieldSave.SetDescription("Select a save data file.");

        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            bool hasSaves = GameManager.Instance.saveGameSlots[i].saveGameFiles.Any(x => x != null);
            arrOptions[i].color = hasSaves ? white : grey;
        }
    }

    private void Close() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }

    private void Disable() {
        gameObject.SetActive(false);
    }

    private void Show() {
        gameObject.SetActive(true);
    }

#region Override methods.
    public override void GetInputPage(int _pageDirection, bool isPageXPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        int selectedIndexPrior = selectedIndex;
        int nextSelectedIndex = selectedIndex + _h;
        int lowEnd = selectedIndex - (selectedIndex % COLUMNS);
        int highEnd = lowEnd + (COLUMNS - 1);

        selectedIndex = nextSelectedIndex;

        bool tooLow = selectedIndex < lowEnd;
        bool tooHigh = selectedIndex > highEnd;
        bool isOutOfRange = tooLow || tooHigh;

        if (isOutOfRange) {
            selectedIndex = selectedIndexPrior;
        }

        SetCursor();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        selectedIndex += COLUMNS * -_v;

        int lowEnd = selectedIndex - (selectedIndex % COLUMNS);
        int difference = selectedIndex - lowEnd;
        int highEnd = lowEnd + 1;

        LoopWithinRow();
        SetCursor();

        void LoopWithinRow() {
            if (selectedIndex < 0) {
                selectedIndex = (arrOptions.Length - COLUMNS) + -difference;
            } else if (selectedIndex >= arrOptions.Length) {
                selectedIndex = difference;
            }
        }
    }

    public override void GetInputCancelPressed() {
        FieldMenu.Instance.StartCoroutine(FieldMenu.Instance.ReturnFromSelectedScreen());
    }

    public override void GetInputConfirmPressed() {
        Disable();
        saveDataFileCheck.Open(selectedIndex);
    }
#endregion
}
