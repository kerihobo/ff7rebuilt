﻿using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class PortInt : Port {
    public PortInt(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) : base(_parentNode, _portName, _direction, _capacity, _displayMode) {
        portType = NodeDataType.Int;
    }

    public int value;

    protected override void RenderField() {
        value = EditorGUILayout.IntField(value, GUILayout.MaxWidth(65));
    }

    public override dynamic GetValue() {
        if (direction == Direction.IN) {
            if (connectedPorts.Count > 0) {
                return connectedPorts[0].GetValue();
            }

            return value;
        }

        return parentNode.GetValue();
    }

    public override void SetValue(dynamic _value) {
        value = _value is int ? _value : 0;
    }
}
