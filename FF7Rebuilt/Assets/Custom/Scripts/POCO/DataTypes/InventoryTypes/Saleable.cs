﻿using System;

namespace InventoryTypes {
    [Serializable]
    public class Saleable : InventoryElement {
        public Saleable() : base() {
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Saleable(Saleable _source) : base(_source) {
            saleCost = _source.saleCost;
        }
        
        public int saleCost;
    }
}
