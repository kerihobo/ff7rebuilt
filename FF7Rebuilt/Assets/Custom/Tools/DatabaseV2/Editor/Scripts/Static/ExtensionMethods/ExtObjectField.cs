using Field;
using UnityEditor.UIElements;
using UnityEngine;

public static class ExtObjectField {
    public static ObjectField SetTransform(this ObjectField _self, Transform _value) {
        _self.value = _value;
        return _self;
    }

    public static ObjectField SetRectTransform(this ObjectField _self, RectTransform _value) {
        _self.value = _value;
        return _self;
    }

    public static ObjectField SetGameObject(this ObjectField _self, GameObject _value) {
        _self.value = _value;
        return _self;
    }

    public static ObjectField SetAnimator(this ObjectField _self, Animator _value) {
        _self.value = _value;
        return _self;
    }

    public static ObjectField SetPlayerControllerNavMesh(this ObjectField _self, PlayerControllerNavMesh _value) {
        _self.value = _value;
        return _self;
    }

    public static ObjectField SetSprite(this ObjectField _self, Sprite _value) {
        _self.value = _value;
        return _self;
    }

    public static ObjectField SetParticleSystem(this ObjectField _self, ParticleSystem _value) {
        _self.value = _value;
        return _self;
    }
}
