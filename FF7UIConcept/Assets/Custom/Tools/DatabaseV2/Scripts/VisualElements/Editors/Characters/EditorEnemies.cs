using UnityEngine.UIElements;

public class EditorEnemies : EditorElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Characters/editor-enemies.uxml";
    protected override string Name { get; set; } = "editor-enemies";

    public EditorEnemies() : base() {
    }
}
