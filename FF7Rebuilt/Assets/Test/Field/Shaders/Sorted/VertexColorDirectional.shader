// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Sorted/VertexColorDirectional" {
	Properties
	{
		_CharacterSize ("Character Size", Float) = 1.76
		// _BoxSize ("Box Size", Float) = 0.5
		// _HeightOffset ("Height Offset", Float) = 1.5
		_Power ("Power", Float) = 1.5
		_GlobalLight("Global Light", Color) = (0.3411765, 0.3294118, 0.3333333, 0)
		_Color1("Color 1", Color) = (0.3411765, 0.3294118, 0.3333333, 0)
		_Box1Position("Box 1 Position", Vector) = (0, 0, 0, 0)
		_Color2("Color 2", Color) = (0.3411765, 0.3294118, 0.3333333, 0)
		_Box2Position("Box 2 Position", Vector) = (0, 0, 0, 0)
		_Color3("Color 3", Color) = (0.3411765, 0.3294118, 0.3333333, 0)
		_Box3Position("Box 3 Position", Vector) = (0, 0, 0, 0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" "Queue"="Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Custom vertex:vert noambient noforwardadd
		#pragma target 3.0

		struct Input {
			float4 vertColor;
			float3 objPos;
			float3 worldPos;
		};

		struct SurfaceOutputCustom
		{
			fixed3 Albedo;
			fixed3 Emission;
			fixed3 Normal;
			fixed Alpha;
			fixed3 objPos;
		};
		
		float _CharacterSize;
		// float _BoxSize;
		// float _HeightOffset;
		float _Power;
		fixed4 _GlobalLight;
		fixed3 _Color1;
		fixed3 _Box1Position;
		fixed3 _Color2;
		fixed3 _Box2Position;
		fixed3 _Color3;
		fixed3 _Box3Position;

		float InverseLerp1(float a, float b, float t) {
			return (t - a) / (b - a);
		}

		half3 InverseLerp3(half3 a, half3 b, half3 t) {
			return (t - a) / (b - a);
		}

		half3 Remap(half3 In, float2 InMinMax, float2 OutMinMax) {
			return OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
		}

		half3 GetBoxPosition(half3 _boxPosition) {
			half3 boxPosition = _boxPosition.xzy/*  / 32768 */;
			boxPosition.y = -boxPosition.y;

			return boxPosition;
		}

		half3 GetBoxOffset(half3 _objPos, half3 _boxPosition, half3 _boxSize) {
			return (_objPos + _boxPosition) / _boxSize;
		}

		half3 GetGradient(half3 _objPos, half3 _boxPosition, half3 _boxSize) {
			// Multiply by 2 because otherwise the maximum white part of our gradient is outside our box.
			half3 gradient = (abs(_objPos + _boxPosition) * 2) / _boxSize;
			gradient = Remap(gradient, float2(0, 1), float2(-.5, 1));
			gradient = saturate(gradient);
			gradient = pow(gradient, _Power);
			return gradient;
		}

		half3 GetLight(half3 _boxPosAbs, half3 _boxPos, half3 _n, half3 _color) {
			float nx = dot(_n, mul(unity_ObjectToWorld, float3(1, 0, 0)));
			float ny = dot(_n, mul(unity_ObjectToWorld, float3(0, 1, 0)));
			float nz = dot(_n, mul(unity_ObjectToWorld, float3(0, 0, 1)));

			// Use this instead if you want DYNAMIC light!
			// float nx = dot(_n, float3(1, 0, 0));
			// float ny = dot(_n, float3(0, 1, 0));
			// float nz = dot(_n, float3(0, 0, 1));

			// Not sure this should be here.
			// float amount = .3;
			// nx = nx * amount + amount;
			// ny = ny * amount + amount;
			// nz = nz * amount + amount;

			float x = _boxPosAbs.x * nx * lerp(-1, 1, _boxPos.x > 0);
			float y = _boxPosAbs.y * ny * lerp(-1, 1, _boxPos.y > 0);
			float z = _boxPosAbs.z * nz * lerp(-1, 1, _boxPos.z > 0);

			return (x + y + z) * _color;
		}

		// Based on findings here:
		// https://forums.qhimm.com/index.php?topic=20540.0#:~:text=This%20is%20ridiculously%20cool%2C%20very%20promising..%20Hopefully%20it%20progresses%20so%20that%20it%20can%20be%20controlled%20per%20scene%2C%20somehow!
		// Uses a lightbox approach.
		float4 LightingCustom(SurfaceOutputCustom s, float3 lightDir, float atten) {
			// ==================================
			// Experimental lighting.
			// ==================================
			float n = dot(s.Normal, lightDir);
			float4 lightColor = _LightColor0;
			float intensity = length(lightColor.rgb);
			// float4 finalColor = float4(s.Albedo * lightColor.rgb * n * atten * intensity, 1);
			float4 finalColor = float4(s.Albedo * n * intensity, 1);

			// ==================================
			// Non-experimental code starts here.
			// ==================================
			float _BoxSize = _CharacterSize * 4;
			float _HeightOffset = _CharacterSize * .5;

			float4 col;
			col.rgb = s.Albedo;
			col.a = s.Alpha;

			half3 objPos = s.objPos;
			half3 heightOffset = half3(0, _HeightOffset, 0);
			objPos -= heightOffset;

			half3 box1Position = GetBoxPosition(_Box1Position);
			half3 box2Position = GetBoxPosition(_Box2Position);
			half3 box3Position = GetBoxPosition(_Box3Position);
			
			half3 box1Offset = GetBoxOffset(objPos, box1Position, _BoxSize);
			half3 box2Offset = GetBoxOffset(objPos, box2Position, _BoxSize);
			half3 box3Offset = GetBoxOffset(objPos, box3Position, _BoxSize);
			
			half3 box1Gradient = GetGradient(objPos, box1Position, _BoxSize);
			half3 box2Gradient = GetGradient(objPos, box2Position, _BoxSize);
			half3 box3Gradient = GetGradient(objPos, box3Position, _BoxSize);
			
			half3 light1 = saturate(GetLight(box1Gradient, box1Offset, s.Normal, _Color1));
			half3 light2 = saturate(GetLight(box2Gradient, box2Offset, s.Normal, _Color2));
			half3 light3 = saturate(GetLight(box3Gradient, box3Offset, s.Normal, _Color3));
			
			half3 h = max(light1, light2);
			h = max(h, light3);
			// half3 directionalLight = h;
			half3 directionalLight = light1 + light2 + light3;
			col.rgb = (directionalLight + _GlobalLight) * s.Albedo * 1.75;
			// col.rgb = light1 + _GlobalLight;
			// col.rgb = light2;
			// col.rgb = light3;
			
			// col.rgb = objPos.xyz;
			return max(col, finalColor);

			return col;
		}

		void vert(inout appdata_full v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = v.color;
			o.objPos = v.vertex;
		}

		void surf (Input IN, inout SurfaceOutputCustom o) {
			// o.Albedo = pow(IN.vertColor.rgb, 1/2.2);
			o.Albedo = IN.vertColor.rgb;
			o.objPos = IN.objPos;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
