﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConfigWindowColorAdjust : MenuControllable {
    [SerializeField] private FingerCursorRight cursor;
    [SerializeField] private RectTransform optionParent;
    [SerializeField] private ConfigMenuSliderPair sliderR;
    [SerializeField] private ConfigMenuSliderPair sliderG;
    [SerializeField] private ConfigMenuSliderPair sliderB;
    [SerializeField] private TextMeshProUGUI txtR;
    [SerializeField] private TextMeshProUGUI txtG;
    [SerializeField] private TextMeshProUGUI txtB;
    [SerializeField] private Image imgSample;
    private int selectedIndex;
    private ConfigWindowColorSelect configWindowColorSelect;
    private RectTransform[] arrOptions;
    private int selectedCorner;

    private Config config => GameManager.Instance.gameData.config;

    public void Initialize(ConfigWindowColorSelect _configWindowColorSelect) {
        configWindowColorSelect = _configWindowColorSelect;

        arrOptions = new RectTransform[optionParent.childCount];
        for (int i = 0; i < arrOptions.Length; i++) {
            arrOptions[i] = optionParent.GetChild(i).GetComponent<RectTransform>();
        }

        cursor.Initialize(arrOptions[0]);
        sliderR.Initialize();
        sliderG.Initialize();
        sliderB.Initialize();

        Disable();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex]);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    private void Disable() {
        gameObject.SetActive(false);
    }

    private void Close() {
        Disable();
        configWindowColorSelect.Open();
    }

    public void Open(int _selectedCorner) {
        selectedCorner = _selectedCorner;
        RefreshSample();

        gameObject.SetActive(true);

        ResetSelection();
        Refresh();

        cursor.Show();
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void Refresh() {
        RefreshR();
        RefreshG();
        RefreshB();
    }

    // Need to:
    // GET the window colours in config from the shader when Config initializes.
    // Update the shader as colours are adjusted via this interface.
    private void RefreshR() {
        sliderR.Refresh(config.windowColours[selectedCorner].r);
        txtR.text = $"<mspace=12>{(int)config.windowColours[selectedCorner].r:000}";
    }

    private void RefreshG() {
        sliderG.Refresh(config.windowColours[selectedCorner].g);
        txtG.text = $"<mspace=12>{(int)config.windowColours[selectedCorner].g:000}";
    }

    private void RefreshB() {
        sliderB.Refresh(config.windowColours[selectedCorner].b);
        txtB.text = $"<mspace=12>{(int)config.windowColours[selectedCorner].b:000}";
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputConfirmPressed() { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    
    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
    
    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        canDelayOnPress = true;

        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        canDelayOnPress = false;
        
        config.IncrementCornerChannel(selectedCorner, selectedIndex, _h);
        
        switch (selectedIndex) {
            case 0:
                RefreshR();
                break;
            case 1:
                RefreshG();
                break;
            case 2:
                RefreshB();
                break;
        }

        RefreshSample();
    }

    private void RefreshSample() {
        Color selectedColor = config.windowColours[selectedCorner];
        selectedColor.a = 1;

        imgSample.color = selectedColor;
    }

    public override void GetInputCancelPressed() {
        canDelayOnPress = true;
        Close();
    }
#endregion
}