﻿using UnityEngine;

public abstract class NodeSet : NodeGetSet {
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        name = "Set " + field?.name;
        size = new Vector2(140, 50);

        AddPortIn<Port>();
        AddPortOut<Port>();
    }

    public static NodeSet CreateFromField(NodeGraph _nodeGraph, Vector2 _offset, BlackboardField _field) {
        switch (_field.dataType) {
            case NodeDataType.Bool:
                return Create<NodeSetBool>(_nodeGraph, _offset, Category.BlackboardField);
            case NodeDataType.Int:
                return Create<NodeSetInt>(_nodeGraph, _offset, Category.BlackboardField);
        }

        return Create<NodeSetBool>(_nodeGraph, _offset);
    }
}
