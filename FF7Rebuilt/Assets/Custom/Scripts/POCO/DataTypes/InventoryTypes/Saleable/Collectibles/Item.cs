﻿using System;
using UnityEngine;

namespace InventoryTypes {
    [Serializable]
    public class Item : Collectible {
        public Item() : base() {
            name        = "New Item";
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Item(Item _source) : base(_source) {
            sourceType = _source.sourceType;
            intendedCharacter = _source.intendedCharacter;
        }

        // TODO: Add out-of-battle effect option.
        public ItemSourceType sourceType = ItemSourceType.NONE;
        [SerializeReference]
        public SelectionData intendedCharacter = new SelectionData();
    }
}