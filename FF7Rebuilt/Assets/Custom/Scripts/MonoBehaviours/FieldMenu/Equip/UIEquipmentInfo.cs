using InventoryTypes;
using TMPro;
using UnityEngine;

public class UIEquipmentInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public Collectible item { get; set; }

    private TextMeshProUGUI txtName;

    public void Initialize() {
        rct = GetComponent<RectTransform>();
        txtName = GetComponent<TextMeshProUGUI>();
    }

    public void Refresh(Collectible _collectible) {
        item = _collectible;

        if (item == null) {
            Clear();
            return;
        }

        txtName.text = item.name;
    }

    public void Clear() {
        txtName.text = "";
    }
}
