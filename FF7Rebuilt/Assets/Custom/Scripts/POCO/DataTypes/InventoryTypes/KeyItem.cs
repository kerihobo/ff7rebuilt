﻿using System;

namespace InventoryTypes {
    [Serializable]
    public class KeyItem : InventoryElement {
        public KeyItem() : base() {
            name = "New Key Item";
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public KeyItem(KeyItem _source) : base(_source) {
        }
    }
}