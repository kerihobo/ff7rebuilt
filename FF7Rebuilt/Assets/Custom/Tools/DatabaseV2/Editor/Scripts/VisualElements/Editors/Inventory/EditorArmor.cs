using FFVII.Database;
using InventoryTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorArmor : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Inventory/editor-armor.uxml";
    protected override Database database { get; set; } = DBResources.GetArmor;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetArmor.armorList[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetArmor.armorList;

    private (Button, ElementalAffinityManager.Affinity)[] arrPairElementButtons;
    private List<string> defenseTypeNames = new List<string>() { "Half", "Immune", "Absorb" };

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Armor)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        if (target == null) return;

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<TextField>("txt-description").SetValue(((Armor)target).description).RegisterValueChangedCallback(e => {
            ((Armor)target).description = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<IntegerField>("int-sale-cost").SetValue(((Armor)target).saleCost).RegisterValueChangedCallback(e => {
            ((Armor)target).saleCost = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<Toggle>("tgl-ziedrich").SetValue(((Armor)target).isZiedrich).RegisterValueChangedCallback(e => {
            ((Armor)target).isZiedrich = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<EnumField>("enum-gender").SetGender(((Armor)target).genderExclusivity).RegisterValueChangedCallback(e => {
            ((Armor)target).genderExclusivity = (Gender)e.newValue;
            EditorUtility.SetDirty(database);
        });

        BindPrimaryStats();
        BindDerivedStats();

        ve.Q<MateriaSlotEditor>().Initialize((Equipment)target, database);

        BindResistanceElements();

        void BindPrimaryStats() {
            ve.Q<IntegerField>("int-strength").SetValue(((Armor)target).equipEffects.primary.strength).RegisterValueChangedCallback(e => {
                ((Armor)target).equipEffects.primary.strength = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-magic").SetValue(((Armor)target).equipEffects.primary.magic).RegisterValueChangedCallback(e => {
                ((Armor)target).equipEffects.primary.magic = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-dexterity").SetValue(((Armor)target).equipEffects.primary.dexterity).RegisterValueChangedCallback(e => {
                ((Armor)target).equipEffects.primary.dexterity = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-luck").SetValue(((Armor)target).equipEffects.primary.luck).RegisterValueChangedCallback(e => {
                ((Armor)target).equipEffects.primary.luck = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindDerivedStats() {
            ve.Q<IntegerField>("int-defense").SetValue(((Armor)target).equipEffects.derived.defense).RegisterValueChangedCallback(e => {
                ((Armor)target).equipEffects.derived.defense = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-defense-percent").SetValue(((Armor)target).equipEffects.derived.defensePercent).RegisterValueChangedCallback(e => {
                ((Armor)target).equipEffects.derived.defensePercent = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-magic-defense").SetValue(((Armor)target).equipEffects.derived.magicDefense).RegisterValueChangedCallback(e => {
                ((Armor)target).equipEffects.derived.magicDefense = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-magic-defense-percent").SetValue(((Armor)target).equipEffects.derived.magicDefensePercent).RegisterValueChangedCallback(e => {
                ((Armor)target).equipEffects.derived.magicDefensePercent = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindResistanceElements() {
            VisualElement resistanceElements = ve.Q<VisualElement>("ve-elemental-vulnerabilities");
            
            RestockButtonArray();

            DropdownField ddf = this.Q<DropdownField>("ddf-defense-type");
            ddf.RegisterValueChangedCallback(_e => {
                for (int i = 0; i < arrPairElementButtons.Length; i++) {
                    int j = i;
                    arrPairElementButtons[j].Item2.damageModifier = (ElementalAffinityManager.DamageModifier)Mathf.Clamp(ddf.index + 2, 2, 4);
                    arrPairElementButtons[j].Item1.SetElementalVulnerabilityClass(arrPairElementButtons[j].Item2, true);
                }

                EditorUtility.SetDirty(database);
            });

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                int j = i;
                arrPairElementButtons[j].Item1.RegisterCallback<ClickEvent>(e => {
                    arrPairElementButtons[j].Item1.ToggleElementalVulnerability(ref arrPairElementButtons[j].Item2);
                    EditorUtility.SetDirty(database);
                });
            }
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        Armor armor = (Armor)target;

        ve.Q<TextField>("txt-name").value = target.name;
        ve.Q<TextField>("txt-description").value = armor.description;
        ve.Q<IntegerField>("int-sale-cost").value = armor.saleCost;
        ve.Q<Toggle>("tgl-ziedrich").value = armor.isZiedrich;
        ve.Q<EnumField>("enum-gender").value = armor.genderExclusivity;

        EquipEffect equipEffects = armor.equipEffects;

        RefreshPrimaryStats();
        RefreshDerivedStats();

        ve.Q<MateriaSlotEditor>().Refresh((Equipment)target);
        
        RefreshElementalVulnerabilities();

        void RefreshPrimaryStats() {
            ve.Q<IntegerField>("int-strength").value = equipEffects.primary.strength;
            ve.Q<IntegerField>("int-magic").value = equipEffects.primary.magic;
            ve.Q<IntegerField>("int-dexterity").value = equipEffects.primary.dexterity;
            ve.Q<IntegerField>("int-luck").value = equipEffects.primary.luck;
        }
        
        void RefreshDerivedStats() {
            ve.Q<IntegerField>("int-defense").value = equipEffects.derived.defense;
            ve.Q<IntegerField>("int-defense-percent").value = equipEffects.derived.defensePercent;
            ve.Q<IntegerField>("int-magic-defense").value = equipEffects.derived.magicDefense;
            ve.Q<IntegerField>("int-magic-defense-percent").value = equipEffects.derived.magicDefensePercent;
        }

        void RefreshElementalVulnerabilities() {
            VisualElement veResistanceElements = ve.Q<VisualElement>("ve-elemental-vulnerabilities");
            ElementalAffinityManager elements = armor.elements;

            RestockButtonArray();

            DropdownField ddf = veResistanceElements.Q<DropdownField>("ddf-defense-type");
            ddf.choices = defenseTypeNames;
            ddf.index = Mathf.Clamp((int)elements.fire.damageModifier - 2, 0, 2);
            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                arrPairElementButtons[i].Item2.damageModifier = (ElementalAffinityManager.DamageModifier)Mathf.Clamp(ddf.index + 2, 2, 4);
                arrPairElementButtons[i].Item1.SetElementalVulnerabilityClass(arrPairElementButtons[i].Item2, true);
            }
        }
    }

    void RestockButtonArray() {
        VisualElement resistanceElements = ve.Q<VisualElement>("ve-elemental-vulnerabilities");
        List<Button> arrButtons = resistanceElements.Query<Button>().ToList();

        arrPairElementButtons = new (Button, ElementalAffinityManager.Affinity)[] {
                (arrButtons[0],  ((Armor)target).elements.fire)
            ,   (arrButtons[1],  ((Armor)target).elements.ice)
            ,   (arrButtons[2],  ((Armor)target).elements.lightning)
            ,   (arrButtons[3],  ((Armor)target).elements.earth)
            ,   (arrButtons[4],  ((Armor)target).elements.poison)
            ,   (arrButtons[5],  ((Armor)target).elements.gravity)
            ,   (arrButtons[6],  ((Armor)target).elements.water)
            ,   (arrButtons[7],  ((Armor)target).elements.wind)
            ,   (arrButtons[8],  ((Armor)target).elements.holy)
            ,   (arrButtons[9],  ((Armor)target).elements._10th)
            ,   (arrButtons[10], ((Armor)target).elements.restorative)
            ,   (arrButtons[11], ((Armor)target).elements.backAttack)
            ,   (arrButtons[12], ((Armor)target).elements.punch)
            ,   (arrButtons[13], ((Armor)target).elements.cut)
            ,   (arrButtons[14], ((Armor)target).elements.hit)
            ,   (arrButtons[15], ((Armor)target).elements.shoot)
            ,   (arrButtons[16], ((Armor)target).elements.shout)
            ,   (arrButtons[17], ((Armor)target).elements.death)
            ,   (arrButtons[18], ((Armor)target).elements.sleep)
            ,   (arrButtons[19], ((Armor)target).elements.confusion)
            };
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Armor());

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new Armor());

        base.Add();
    }
}
