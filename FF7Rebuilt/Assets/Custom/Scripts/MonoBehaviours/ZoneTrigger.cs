using UnityEngine;

[ExecuteInEditMode]
public class ZoneTrigger : MonoBehaviour {
    [SerializeField] private Zone zone;

    private void OnTriggerEnter(Collider other) {
        StartCoroutine(zone.ChangeScene());
    }

    private void OnDrawGizmos() {
        Gizmos.color = new Color(1, 0, 0, .3f);
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawCube(Vector3.zero, Vector3.one);
    }
}
