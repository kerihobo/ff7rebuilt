﻿using UnityEngine;

public class NodeOr : Node {
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(90, 70);
        name = "Or";

        AddPortIn<PortBool>("A", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_PORT);
        AddPortIn<PortBool>("B", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_PORT);
        AddPortOut<PortBool>("Out", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        bool a = portContainerIn.ports[0].GetValue();
        bool b = portContainerIn.ports[1].GetValue();
        
        return a || b;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        return Create<NodeOr>(_nodeGraph, position + duplicationOffset, category, false);
    }
}
