using CharacterTypes;
using InventoryTypes;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EquipMenuItemList : MenuControllable {
    public enum ItemType {
        WEAPON
    ,   ARMOR
    ,   ACCESSORY
    }

    public FingerCursorRight cursor;
    
    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private UIEquipmentInfo additionalOption;
    [SerializeField] private RectTransform optionsParent;

    private RectTransform scrollBarFG;
    private int selectedIndex;
    private EquipMenuOptionList equipMenuOptionList;
    private UIEquipmentInfo[] arrOptions;

    // MAYBE REFACTOR
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 1;
    private Collectible[] arrViewItems = new Collectible[0];
    private ItemType itemType;

    private Collectible[] arrItems => GameManager.Instance.gameData.inventory.arrItems;
    private ScreenFieldEquip screenFieldEquip => equipMenuOptionList.screenFieldEquip;
    private Player GetSelectedCharacter => equipMenuOptionList.screenFieldEquip.GetSelectedCharacter;

    public void Initialize(EquipMenuOptionList _equipMenuOptionList) {
        equipMenuOptionList = _equipMenuOptionList;
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<UIEquipmentInfo>();
        foreach (UIEquipmentInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOption.Initialize();
        ClearAdditional();

        Disable();

        cursor.Initialize(arrOptions[0].rct);

        listSpace = vlgOptions.spacing;
        listHeight = arrOptions[0].rct.rect.height;
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = lowBound + i;
            Collectible itemInView = null;

            // Only show an item within the range of our viewItems.
            if (itemIndex < arrViewItems.Length) {
                itemInView = arrViewItems[itemIndex];
            }

            arrOptions[i].Refresh(itemInView);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - columns;
        UIEquipmentInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOption.rct.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        Collectible itemInView = null;

        // Only show an item within the range of our viewItems.
        if (anchoredIndex < arrViewItems.Length) {
            itemInView = arrViewItems[anchoredIndex];
        }

        additionalOption.Refresh(itemInView);
    }

    private void ClearAdditional() {
        additionalOption.Clear();
    }

    public void ResetSelection() {
        selectedIndex = 0;
        lowBound = 0;

        UpdateScrollBar();
        SetCursor();
    }

    public void SetCursor() {
        UIEquipmentInfo selectedItemInfo = arrOptions[selectedIndex];
        Collectible selectedItem = selectedItemInfo.item;

        cursor.SetCursorPosition(selectedItemInfo.rct);

        switch (itemType) {
            case ItemType.WEAPON:
                screenFieldEquip.RefreshInfoWeapon((Weapon)selectedItem);
                break;
            case ItemType.ARMOR:
                screenFieldEquip.RefreshInfoArmor((Armor)selectedItem);
                break;
            default:
                screenFieldEquip.RefreshInfoAccessory((Accessory)selectedItem);
                break;
        }
    }

    public void Open(ItemType _itemType, WeaponType _weaponType = WeaponType.GLOVE) {
        itemType = _itemType;
        lowBound = 0;

        PopulateItemList();

        if (arrViewItems.Length <= 0) {
            return;
        }

        Show();
        SetControllable();
        ResetSelection();
        FieldMenu.Instance.SetCooldownTraverse(0);

        void PopulateItemList() {
            switch (itemType) {
                case ItemType.WEAPON:
                    arrViewItems = arrItems.Where(x => x is Weapon && (x as Weapon).type == _weaponType).ToArray();
                    break;
                case ItemType.ARMOR:
                    arrViewItems = arrItems.Where(x => {
                        bool isArmor = x is Armor;
                        if (!isArmor) return false;

                        Armor armor = x as Armor;
                        bool isGenderMatch = armor.genderExclusivity == GetSelectedCharacter.gender;
                        bool isGenderNeutral = armor.genderExclusivity == Gender.None;
                        return isGenderMatch || isGenderNeutral;
                    }).ToArray();
                    
                    break;
                default:
                    arrViewItems = arrItems.Where(x => x is Accessory).ToArray();
                    break;
            }

            Refresh();
        }
    }

    private void Close() {
        Disable();
        equipMenuOptionList.ExitSubMenu();
    }

    private void Disable() {
        cursor.Hide();
        ClearAdditional();
        //Refresh();
        //UpdateScrollBar();
        Hide();
    }

    private void Show() {
        optionsParent.gameObject.SetActive(true);
        screenFieldEquip.ToggleStatInfoChangedText(true);
        SetScrollbarVisibility();
        SetScrollbarHeight();
        UpdateScrollBar();
        cursor.Show();

        void SetScrollbarVisibility() {
            bool isUsingScrollbar = arrViewItems.Length > arrOptions.Length;
            scrollBarBG.gameObject.SetActive(isUsingScrollbar);
        }

        void SetScrollbarHeight() {
            float scrollBarHeight = scrollBarBG.sizeDelta.y;
            scrollBarHeight *= arrOptions.Length / (float)arrViewItems.Length;
            scrollBarHeight = Mathf.RoundToInt(scrollBarHeight);
            scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarHeight);
        }
    }

    private void Hide() {
        optionsParent.gameObject.SetActive(false);
        scrollBarBG.gameObject.SetActive(false);
    }

    public override void GetInputConfirmPressed() {
        UIEquipmentInfo selectedItemInfo = arrOptions[selectedIndex];
        Collectible selectedItem = selectedItemInfo.item;
        int itemIndex = arrItems.ToList().IndexOf(selectedItem);
        Inventory inventory = GameManager.Instance.gameData.inventory;

        switch (itemType) {
            case ItemType.WEAPON:
                inventory.AddItem(GetSelectedCharacter.weapon, 1);
                GetSelectedCharacter.weapon = (Weapon)selectedItem;

                Materia[] materiaWeapon = GetSelectedCharacter.materiaWeapon;
                for (int i = GetSelectedCharacter.weapon.materiaSlotCount; i < materiaWeapon.Length; i++) {
                    GameManager.Instance.gameData.inventory.AddMateria(materiaWeapon[i]);
                    materiaWeapon[i] = null;
                }

                inventory.RemoveItem(selectedItem);
                break;
            case ItemType.ARMOR:
                inventory.AddItem(GetSelectedCharacter.armor, 1);
                GetSelectedCharacter.armor = (Armor)selectedItem;

                Materia[] materiaArmor = GetSelectedCharacter.materiaArmor;
                for (int i = GetSelectedCharacter.armor.materiaSlotCount; i < materiaArmor.Length; i++) {
                    GameManager.Instance.gameData.inventory.AddMateria(materiaArmor[i]);
                    materiaArmor[i] = null;
                }

                inventory.RemoveItem(selectedItem);
                break;
            case ItemType.ACCESSORY:
                inventory.AddItem(GetSelectedCharacter.accessory, 1);
                GetSelectedCharacter.accessory = (Accessory)selectedItem;
                inventory.RemoveItem(selectedItem);
                break;
            default:
                break;
        }

        GetSelectedCharacter.RecalculatePlayerStats();
        screenFieldEquip.RefreshPlayerInfo();

        screenFieldEquip.isChanged = true;

        Close();
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _v > 0) || (lowBound + selectedIndex == arrViewItems.Length - columns && _v < 0)) return;

        int v = -_v * columns;
        selectedIndex += v;

        // Clamp if you reach the end of our current items
        if (lowBound + selectedIndex >= arrViewItems.Length) {
            selectedIndex--;
        }

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
            lowBound -= lowBound % columns;
        }

        if (selectedIndex < 0) {
            selectedIndex += columns;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= columns;
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;
            ClearAdditional();
        }

        int v = _pageDirection * (10 * columns);
        int lowBoundMax = arrViewItems.Length - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    private void UpdateScrollBar() {
        float scroll = ((float)lowBound / columns) / (arrViewItems.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
}
