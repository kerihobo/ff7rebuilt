﻿using System;
using UnityEngine;

[Serializable]
public class PortBool : Port {
    public PortBool(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) : base(_parentNode, _portName, _direction, _capacity, _displayMode) {
        portType = NodeDataType.Bool;
    }

    public bool value;

    protected override void RenderField() {
        value = GUILayout.Toggle(value, "", GUILayout.Width(20));
    }

    public override dynamic GetValue() {
        if (direction == Direction.IN) {
            if (connectedPorts.Count > 0) {
                return connectedPorts[0].GetValue();
            }

            return value;
        }

        return parentNode.GetValue();
    }

    public override void SetValue(dynamic _value) {
        value = _value;
    }
}
