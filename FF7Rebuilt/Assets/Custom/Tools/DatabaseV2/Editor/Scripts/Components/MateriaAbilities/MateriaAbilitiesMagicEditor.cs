using FFVII.Database;
using InventoryTypes;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class MateriaAbilitiesMagicEditor : MateriaAbilitiesEditor {
    public MateriaAbilitiesMagicEditor(Materia _target, Database _database) : base(_target, _database) {
    }

    protected override string UXMLPath { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/MateriaAbilities/materia-abilities-magic.uxml";

    public override void BindAbilities() {
        DropdownField ddfCursorAction = this.Q<DropdownField>("drp-subtype");
        ddfCursorAction.choices = MagicSubtype.MAGIC_SUBTYPE_NAMES.ToList();
        ddfCursorAction.index = target.subtype;
        ddfCursorAction.RegisterValueChangedCallback(_e => {
            target.subtype = ddfCursorAction.index;

            RefreshSelectionDisplays();

            EditorUtility.SetDirty(database);
        });

        for (int i = 0; i < target.selectionSpells.Length; i++) {
            int j = i;
            DropdownField ddf = this.Q<DropdownField>($"ddf-ability-{j}");
            ddf.Initialize(DBResources.GetAbilityList(AbilityType.Magic), target.selectionSpells[j], database);
            ddf.RegisterValueChangedCallback(_e => {
                target.selectionSpells[j].SetDataExternally(ddf.index);
                EditorUtility.SetDirty(database);
            });
        }
    }

    public override void OnLevelsChanged() {
        VisualElement veMateriaAbilities = this.Q<VisualElement>("materia-abilities");
        VisualElement veDDFParent = veMateriaAbilities.Q<VisualElement>("ve-ddf");
        IEnumerable<VisualElement> ddfChildren = veDDFParent.Children();
        List<DropdownField> ddfs = ddfChildren.OfType<DropdownField>().ToList();
        ddfs.ForEach(x => x.style.display = DisplayStyle.None);

        for (int i = 0; i < target.levels; i++) {
            if (i - 1 < 0) continue;

            ddfs[i-1].style.display = DisplayStyle.Flex;
        }

        //List<DropdownField> ddfF = this.Query<VisualElement>("ve-ddf").Children<DropdownField>().ToList();
        //ddfF.ForEach(x => x.style.display = DisplayStyle.None);

        //for (int i = 0; i < target.levels; i++) {
        //    if (i - 1 < 0) continue;

        //    ddfF[i - 1].style.display = DisplayStyle.Flex;
        //}
    }

    public override void Refresh(Materia _target, VisualElement _ve) {
        target = _target;
        veParent = _ve;

        this.Q<DropdownField>("drp-subtype").index = target.subtype;

        RefreshSelectionDisplays();

        for (int i = 0; i < target.selectionSpells.Length; i++) {
            int j = i;
            this.Q<DropdownField>($"ddf-ability-{j}").Initialize(DBResources.GetAbilityList(AbilityType.Magic), target.selectionSpells[j], database);
        }

        OnLevelsChanged();
    }
}
