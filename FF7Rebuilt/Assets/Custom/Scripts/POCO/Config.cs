using System;
using UnityEngine;

[Serializable]
public partial class Config {
    [Serializable]
    public class MagicOrderArrangement {
        public MagicOrderArrangement(Ability.MagicOrder first, Ability.MagicOrder second, Ability.MagicOrder third) {
            order = new Ability.MagicOrder[] { first, second, third };
        }

        public Ability.MagicOrder[] order;
    }

    /// <summary>
    /// An array of 4 corners in this order: Top-Left, Top-Right, Bottom-Right, Bottom-Left.
    /// </summary>
    public Color32[] windowColours = new Color32[4];
    [Range(0, 100)]
    public int volumeMusic = 100;
    [Range(0, 100)]
    public int volumeSFX = 100;
    public ControllerType controller = ControllerType.CUSTOMIZE;
    public CursorType cursor = CursorType.INITIAL;
    public ATBType atb = ATBType.RECOMMENDED;
    [Range(0, 255)]
    public int speedBattle = 127;
    [Range(0, 255)]
    public int speedMessageBattle = 127;
    [Range(0, 255)]
    public int speedMessageField = 127;
    public CameraAngleType cameraAngle = CameraAngleType.AUTO;
    public int magicOrderIndex = 0;

    public static readonly MagicOrderArrangement[] MagicOrders = {
        new MagicOrderArrangement(Ability.MagicOrder.RESTORE , Ability.MagicOrder.ATTACK  , Ability.MagicOrder.INDIRECT)
    ,   new MagicOrderArrangement(Ability.MagicOrder.RESTORE , Ability.MagicOrder.INDIRECT, Ability.MagicOrder.ATTACK)
    ,   new MagicOrderArrangement(Ability.MagicOrder.ATTACK  , Ability.MagicOrder.INDIRECT, Ability.MagicOrder.RESTORE)
    ,   new MagicOrderArrangement(Ability.MagicOrder.ATTACK  , Ability.MagicOrder.RESTORE , Ability.MagicOrder.INDIRECT)
    ,   new MagicOrderArrangement(Ability.MagicOrder.INDIRECT, Ability.MagicOrder.RESTORE , Ability.MagicOrder.ATTACK)
    ,   new MagicOrderArrangement(Ability.MagicOrder.INDIRECT, Ability.MagicOrder.ATTACK  , Ability.MagicOrder.RESTORE)
    };

    [SerializeField] private Material materialWindowDefault;
    [SerializeField] private Material materialWindow;

    public Ability.MagicOrder[] GetMagicOrderArrangement => MagicOrders[magicOrderIndex].order;

    public void Initialize() {
        ResetWindowColour();
        GetWindowColours();
        SetWindowColours();
    }

    // Later, these should be taken from the save file, otherwise a default file.
    private void GetWindowColours() {
        windowColours[0] = materialWindow.GetColor("_ColorTL");
        windowColours[1] = materialWindow.GetColor("_ColorBL");
        windowColours[2] = materialWindow.GetColor("_ColorTR");
        windowColours[3] = materialWindow.GetColor("_ColorBR");
    }

    public void SetWindowColours() {
        materialWindow.SetColor("_ColorTL", windowColours[0]);
        materialWindow.SetColor("_ColorBL", windowColours[1]);
        materialWindow.SetColor("_ColorTR", windowColours[2]);
        materialWindow.SetColor("_ColorBR", windowColours[3]);
    }

    public void ResetWindowColour() {
        windowColours[0] = materialWindowDefault.GetColor("_ColorTL");
        windowColours[1] = materialWindowDefault.GetColor("_ColorBL");
        windowColours[2] = materialWindowDefault.GetColor("_ColorTR");
        windowColours[3] = materialWindowDefault.GetColor("_ColorBR");

        SetWindowColours();
    }

    public void IncrementCornerChannel(int _selectedCorner, int _selectedChannel, int _h) {
        FieldMenu.Instance.SetCooldownTraverse(.0125f);

        switch (_selectedChannel) {
            case 0:
                windowColours[_selectedCorner].r = (byte)Mathf.Clamp(windowColours[_selectedCorner].r + _h, 0, 255);
                break;
            case 1:
                windowColours[_selectedCorner].g = (byte)Mathf.Clamp(windowColours[_selectedCorner].g + _h, 0, 255);
                break;
            case 2:
                windowColours[_selectedCorner].b = (byte)Mathf.Clamp(windowColours[_selectedCorner].b + _h, 0, 255);
                //Debug.Log((int)windowColours[_selectedCorner].b);
                break;
        }

        switch (_selectedCorner) {
            case 0:
                materialWindow.SetColor("_ColorTL", windowColours[_selectedCorner]);
                break;
            case 1:
                materialWindow.SetColor("_ColorBL", windowColours[_selectedCorner]);
                break;
            case 2:
                materialWindow.SetColor("_ColorTR", windowColours[_selectedCorner]);
                break;
            case 3:
                materialWindow.SetColor("_ColorBR", windowColours[_selectedCorner]);
                break;
        }
    }

    public void IncrementVolumeMusic(int _h) {
        FieldMenu.Instance.SetCooldownTraverse(Time.deltaTime);
        volumeMusic = Mathf.Clamp(volumeMusic += _h, 0, 100);
    }

    public void IncrementVolumeSFX(int _h) {
        FieldMenu.Instance.SetCooldownTraverse(Time.deltaTime);
        volumeSFX = Mathf.Clamp(volumeSFX += _h, 0, 100);
    }

    public void IncrementCursorType(int _h) {
        int max = Enum.GetValues(typeof(CursorType)).Length - 1;
        cursor = (CursorType)Mathf.Clamp((int)cursor + _h, 0, max);
    }

    public void IncrementATBType(int _h) {
        int max = Enum.GetValues(typeof(ATBType)).Length - 1;
        atb = (ATBType)Mathf.Clamp((int)atb + _h, 0, max);
    }
    
    public void IncrementBattleSpeed(int _h) {
        FieldMenu.Instance.SetCooldownTraverse(.0125f);
        speedBattle = Mathf.Clamp(speedBattle += _h, 0, 255);
    }

    public void IncrementBattleMessage(int _h) {
        FieldMenu.Instance.SetCooldownTraverse(.0125f);
        speedMessageBattle = Mathf.Clamp(speedMessageBattle += _h, 0, 255);
    }

    public void IncrementFieldMessage(int _h) {
        FieldMenu.Instance.SetCooldownTraverse(.0125f);
        speedMessageField = Mathf.Clamp(speedMessageField += _h, 0, 255);
    }

    public void IncrementCameraAngle(int _h) {
        int max = Enum.GetValues(typeof(CameraAngleType)).Length - 1;
        cameraAngle = (CameraAngleType)Mathf.Clamp((int)cameraAngle + _h, 0, max);
    }
    
    public void IncrementMagicOrder(int _h) {
        magicOrderIndex += _h;
        magicOrderIndex = Mathf.Clamp(magicOrderIndex, 0, MagicOrders.Length - 1);
    }
}
