﻿using UnityEngine;

namespace InventoryTypes {
    public class Equipment : Collectible {        
        public Equipment() : base() {
            materiaGrowth = MateriaGrowthType.Normal;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Equipment(Equipment _source) : base(_source) {
            materiaSlotCount = _source.materiaSlotCount;
            links            = _source.links;
            materiaGrowth    = _source.materiaGrowth;
        }

        [Range(0, 8)]
        public int materiaSlotCount;
        public int links;
        public MateriaGrowthType materiaGrowth = MateriaGrowthType.Normal;
    }
}