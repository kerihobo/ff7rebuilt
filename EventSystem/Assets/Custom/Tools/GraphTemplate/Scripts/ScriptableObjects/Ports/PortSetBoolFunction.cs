﻿using UnityEditor;
using UnityEngine;

public class PortSetBoolFunction : Port {
    public NodeSetBool.Function value;

    protected override void RenderAsOutput(StyleSet _styleSet) {
        value = (NodeSetBool.Function)EditorGUILayout.EnumPopup(value, GUILayout.Width(70));
    }

    public override void SetValue(dynamic _value) {
        if (_value == null) {
            _value = NodeSetInt.Function.Equal;
        }

        value = (NodeSetBool.Function)_value;
    }

    public override dynamic GetValue() {
        return (int)value;
    }
}
