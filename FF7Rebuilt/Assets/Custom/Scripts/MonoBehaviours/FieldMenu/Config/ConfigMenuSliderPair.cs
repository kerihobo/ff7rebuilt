using UnityEngine;

public class ConfigMenuSliderPair : MonoBehaviour {
    [SerializeField] private int maxValue = 255;
    private RectTransform bg;
    private RectTransform fg;
    private float maxX;

    public void Initialize() {
        bg = GetComponent<RectTransform>();
        fg = bg.GetChild(0).GetComponent<RectTransform>();

        maxX = bg.sizeDelta.x - fg.sizeDelta.x;
    }

    public void Refresh(int _value) {
        fg.anchoredPosition = Vector2.right * maxX * (_value / (float)maxValue);
    }
}
