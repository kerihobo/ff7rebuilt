﻿public static class ElementSelection {
    public static readonly string[] ELEMENT_SELECTION_NAMES = {
        "0: None"
    ,   "1: Fire"
    ,   "2: Ice"
    ,   "3: Lightning"
    ,   "4: Earth"
    ,   "5: Poison"
    ,   "6: Gravity"
    ,   "7: Water"
    ,   "8: Wind"
    ,   "9: Holy"
    ,   "10: Restorative"
    ,   "11: Cut"
    ,   "12: Hit"
    ,   "13: Punch"
    ,   "14: Shoot"
    ,   "15: Shout"
    ,   "16: Hidden"
    };

    public static int GetIndex(int _indexIn) {
        switch (_indexIn) {
            case 0:  // None
            case 1:  // Fire
            case 2:  // Ice
            case 3:  // Lightning
            case 4:  // Earth
            case 5:  // Poison
            case 6:  // Gravity
            case 7:  // Water
            case 8:  // Wind
            case 9:  // Holy
                return _indexIn - 1;
            case 10: // Restorative
                return 10;
            case 11: // Cut
                return 13;
            case 12: // Hit
                return 14;
            case 13: // Punch
                return 12;
            case 14: // Shoot
                return 15;
            case 15: // Shout
                return 16;
            case 16: // Hidden
                return 9;
            default:
                return -1;
        }

        /* _indexIn needs to be mapped to one of the following values
         * to point to one of the elements listed in my ElementalAffinityManager.
         * ---
         * 0: fire
         * 1: ice
         * 2: lightning
         * 3: earth
         * 4: poison
         * 5: gravity
         * 6: water
         * 7: wind
         * 8: holy
         * 9: _10th
         * 10: restorative
         * 11: backAttack
         * 12: punch
         * 13: cut
         * 14: hit
         * 15: shoot
         * 16: shout
         * <NOTHING AFTER HERE IS INCLUDED. WE ONLY GO UP TO 16>
         * 17: death
         * 18: sleep
         * 19: confusion
        */
    }

    public static string GetElementLabel(int elementIndex) {
        switch (elementIndex) {
            case 1:  // Fire
            case 2:  // Ice
            case 3:  // Lightning
            case 4:  // Earth
            case 5:  // Poison
            case 6:  // Gravity
            case 7:  // Water
            case 8:  // Wind
            case 9:  // Holy
            case 10: // Restorative
            case 11: // Cut
            case 12: // Hit
            case 13: // Punch
            case 14: // Shoot
            case 15: // Shout
                string sName = ELEMENT_SELECTION_NAMES[elementIndex];
                const string splitText = ": ";
                int splitIndex = sName.IndexOf(splitText);

                return sName.Substring(splitIndex + splitText.Length);
            default:
                return string.Empty;
        }
    }
}
