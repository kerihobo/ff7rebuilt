using CharacterTypes;
using FFVII.Database;
using UnityEditor;
using UnityEngine.UIElements;

public class EnemyDropItemEditor : VisualElement {
    public new class UxmlFactory : UxmlFactory<EnemyDropItemEditor, UxmlTraits> { }

    public EnemyDropItemEditor() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        name = Name;
    }

    public Enemy target;
    public Database database;
    
    private const string UXMLPath = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/enemy-drop-item-editor.uxml";
    private const string Name = "enemy-drop-item-editor";
    
    private TemplateContainer tc;
    private int index;
    private SelectionCollectibleEditor slC;

    public EnemyDropItemEditor Initialize(Enemy _target, Database _database, int _index) {
        target = _target;
        database = _database;
        index = _index;

        BindUniqueControls();
        Refresh(target);

        return this;
    }

    private void BindUniqueControls() {
        slC = this.Q<SelectionCollectibleEditor>();
        slC.Initialize(target.dropItems[index].item, database);

        this.Q<Label>("lbl-index").text = $"#{index}";

        this.Q<IntegerField>("int-chance").SetValue(target.dropItems[index].chance).RegisterValueChangedCallback(e => {
            target.dropItems[index].chance = e.newValue;
            EditorUtility.SetDirty(database);
        });

        this.Q<Toggle>("tgl-steal").SetValue(target.dropItems[index].isSteal).RegisterValueChangedCallback(e => {
            target.dropItems[index].isSteal = e.newValue;
            EditorUtility.SetDirty(database);
        });
    }

    public void Refresh(Enemy _target) {
        target = _target;
        
        slC?.Rebuild(target.dropItems[index].item);

        this.Q<IntegerField>("int-chance").value = target.dropItems[index].chance;
        this.Q<Toggle>("tgl-steal").value = target.dropItems[index].isSteal;
    }
}
