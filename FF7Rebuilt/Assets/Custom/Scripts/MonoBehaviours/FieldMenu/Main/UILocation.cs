using TMPro;
using UnityEngine;

public class UILocation : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtLocation;

    private void OnEnable() {
        txtLocation.text = GameManager.Instance.gameData.location;
    }
}
