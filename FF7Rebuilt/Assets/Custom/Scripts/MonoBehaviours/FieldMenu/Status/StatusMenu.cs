using CharacterTypes;
using UnityEngine;

public abstract class StatusMenu : MonoBehaviour {
    public abstract void Initialize(ScreenFieldStatus _screenFieldStatus);
    public abstract void Show();
    public abstract void Hide();
    public abstract void Refresh(Player _player);
}
