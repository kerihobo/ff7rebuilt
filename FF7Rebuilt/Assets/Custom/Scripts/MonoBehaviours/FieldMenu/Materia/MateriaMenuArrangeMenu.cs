using CharacterTypes;
using TMPro;
using UnityEngine;

public class MateriaMenuArrangeMenu : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform optionsParent;
    private ScreenFieldMateria screenFieldMateria;
    private MateriaMenuMateriaList materiaList;
    private MateriaMenuOptionList optionList;
    private int selectedIndex;
    private TextMeshProUGUI[] arrOptions;
    
    private Player GetSelectedCharacter => screenFieldMateria.GetSelectedCharacter;
    private MateriaMenuSlotsMain slotsMain => screenFieldMateria.slotsMain;

    public void Initialize(MateriaMenuOptionList _optionList, MateriaMenuMateriaList _materiaList, ScreenFieldMateria _screenFieldMateria) {
        optionList = _optionList;
        materiaList = _materiaList;
        screenFieldMateria = _screenFieldMateria;
        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        cursor.Initialize(arrOptions[0].rectTransform);

        Disable();
    }
    
    public void Open() {
        Enable();
        cursor.SetFlickerState(false);
        SetControllable();
        SetCursor();
    }

    private void Close() {
        Disable();
        optionList.Open();
    }

    public void Disable() {
        gameObject.SetActive(false);
    }

    public void Enable() {
        gameObject.SetActive(true);
    }

    public void ResetSelection() {
        selectedIndex = 0;
        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

#region Override methods
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void GetInputPage(int pageDirection, bool isPageXPressed) {
        screenFieldMateria.CycleCharacter(pageDirection);
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputConfirmPressed() {
        switch (selectedIndex) {
            case 0: // Arrange
                GameManager.Instance.gameData.inventory.ArrangeMateria();
                materiaList.Refresh();
                break;
            case 1: // Exchange
                screenFieldMateria.exchange.Open();
                break;
            case 2: // Remove all
                for (int i = 0; i < GetSelectedCharacter.materiaWeapon.Length; i++) {
                    slotsMain.RemoveMateriaFromSlot(i, GetSelectedCharacter.materiaWeapon, slotsMain.arrOptionsWeapon);
                    slotsMain.RemoveMateriaFromSlot(i, GetSelectedCharacter.materiaArmor, slotsMain.arrOptionsArmor);
                }

                slotsMain.RefreshMateriaInfo();
                GetSelectedCharacter.RecalculatePlayerStats();
                screenFieldMateria.RefreshPlayerInfo();

                break;
            case 3: // Trash
                cursor.SetFlickerState(true);
                materiaList.Open(true);
                break;
        }
    }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }
#endregion
}
