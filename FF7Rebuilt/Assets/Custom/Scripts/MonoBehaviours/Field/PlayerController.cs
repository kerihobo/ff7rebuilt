﻿using UnityEngine;

namespace Field {
    public class PlayerController : MonoBehaviour {
        // Running is 4x faster than walking.
        private const float WALK_SPEED = 3;
        private const float RUN_SPEED = WALK_SPEED * 4;

        private Rigidbody rb;
        private Animator anim;
        private Vector3 currentSpeed;
        private bool isControllable = true;

        private void OnEnable() {
            EventManagerField.OnOpenFieldMenu  += Engage;
            EventManagerField.OnCloseFieldMenu += Disengage;
            EventManagerField.OnDisengage      += Disengage;
            EventManagerField.OnEngage         += Engage;
        }

        private void OnDisable() {
            EventManagerField.OnOpenFieldMenu  -= Disengage;
            EventManagerField.OnCloseFieldMenu -= Engage;
            EventManagerField.OnDisengage      += Disengage;
            EventManagerField.OnEngage         += Engage;
        }

        private void Awake() {
            rb = GetComponent<Rigidbody>();
            anim = GetComponent<Animator>();
        }

        private void Start() {
            //GameManager.Instance.currentlyControlledPlayerCharacter = this;
        }

        private void Update() {
            if (!isControllable) return;

            GetMenuRequest();

            bool isInput = Input.GetButton("Horizontal") || Input.GetButton("Vertical");
            if (!(isInput && isControllable)) {
                currentSpeed = Vector3.zero;
                anim.SetFloat("fSpeed", 0);
                return;
            }

            GetMoveInput();
        }

        private void FixedUpdate() {
            rb.linearVelocity = new Vector3(currentSpeed.x, rb.linearVelocity.y, currentSpeed.z);
        }

        private void Disengage() {
            isControllable = true;
        }

        private void Engage() {
            isControllable = false;
        }

        private void GetMoveInput() {
            bool isRunning = Input.GetKey(KeyCode.X);

            anim.SetFloat("fSpeed", isRunning ? 1 : .5f);

            Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            Vector3 direction = Camera.main.transform.forward * input.y;
            direction += Camera.main.transform.right * input.x;
            direction = Vector3.Normalize(direction);

            float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;

            currentSpeed = direction * (isRunning ? RUN_SPEED : WALK_SPEED);
            transform.localEulerAngles = new Vector3(0, angle, 0);
        }

        private void GetMenuRequest() {
            bool isMenuRequest = Input.GetKeyDown(KeyCode.V);

            if (isMenuRequest) {
                EventManagerField.OpenFieldMenu();
            }
        }
    }
}
