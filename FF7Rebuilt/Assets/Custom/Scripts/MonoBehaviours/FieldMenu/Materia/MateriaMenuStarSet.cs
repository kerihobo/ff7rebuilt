using InventoryTypes;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuStarSet : MonoBehaviour {
    private ItemIconContainer.StarIconPair selectedStarIconPair;
    private Image[] arrStarsIcons;

    public void Initialize() {
        arrStarsIcons = GetComponentsInChildren<Image>();
    }

    public void Refresh(Materia _materia) {
        ClearAllStars();

        if (_materia == null) return;

        selectedStarIconPair = SetSelectedStarIconPair(_materia);

        for (int i = 0; i < _materia.levels; i++) {
            arrStarsIcons[i].enabled = true;
            
            bool isThresholdReached = _materia.ap >= _materia.apThresholds[i];
            arrStarsIcons[i].sprite = isThresholdReached ? selectedStarIconPair.sprStarActive : selectedStarIconPair.sprStarInactive;
        }

        void ClearAllStars() {
            for (int i = 0; i < arrStarsIcons.Length; i++) {
                arrStarsIcons[i].enabled = false;
                arrStarsIcons[i].sprite = null;
            }
        }
    }

    private ItemIconContainer.StarIconPair SetSelectedStarIconPair(Materia _materia) => _materia.type switch {
        MateriaType.Command     => ItemIconContainer.GetInstance().starsCommand,
        MateriaType.Independent => ItemIconContainer.GetInstance().starsIndependent,
        MateriaType.Magic       => ItemIconContainer.GetInstance().starsMagic,
        MateriaType.Summon      => ItemIconContainer.GetInstance().starsSummon,
        _                       => ItemIconContainer.GetInstance().starsSupport
    };
}
