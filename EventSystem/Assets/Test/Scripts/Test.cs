﻿using UnityEngine;

public class Test : MonoBehaviour {
    public NodeGraph nodeGraph;
    
    private void Awake() {
        nodeGraph.Run();
    }

    private void Update() {
        if (Input.GetButtonDown("Submit")) {
            nodeGraph.Run();
        }
    }
}
