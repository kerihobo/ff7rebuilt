using System;
using UnityEngine;

namespace InventoryTypes {
    [Serializable]
    public class Materia : Saleable {
        public Materia() : base() {
            name = "New Materia";

            for (int i = 0; i < selectionSpells.Length; i++) {
                selectionSpells[i] = new SelectionAbility(AbilityType.Magic);
            }
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="_source"></param>
        public Materia(Materia _source) : base(_source) {
            sellValueMaster     = _source.sellValueMaster;
            ap                  = _source.ap;
            levels              = _source.levels;
            apThresholds        = _source.apThresholds;
            isIgnoreFirstLevel  = _source.isIgnoreFirstLevel;
            elementIndex        = _source.elementIndex;
            type                = _source.type;
            subtype             = _source.subtype;
            boostValues         = _source.boostValues;
            independentManager  = _source.independentManager;
            supportManager      = _source.supportManager;
            selectionSpells     = _source.selectionSpells;
            equipEffects        = _source.equipEffects;
            elements            = _source.elements;
            statusEffectManager = _source.statusEffectManager;

            if (IsEnemySkill) {
                CheatManager.CheatUnlockRandomEnemySkills(this);
            }
        }

        public int sellValueMaster;
        [Range(0, 16777215)]
        public int ap;
        [Range(1, 5)]
        public int levels;
        public int command;
        public int[] apThresholds = new int[5];
        public bool isIgnoreFirstLevel;
        //public bool isEnemySkill;
        //public bool isMaster;
        //public bool isInferElement;
        public MateriaType type;
        /// <summary>
        /// Support:<br/>
        /// 0:  None 1: All<br/>
        /// 2:  Counter<br/>
        /// 3:  Magic Counter<br/>
        /// 4:  Sneak Attack<br/>
        /// 5:  Final Attack<br/>
        /// 6:  MP Turbo<br/>
        /// 7:  MP Absorb<br/>
        /// 8:  HP Absorb<br/>
        /// 9:  Added Cut<br/>
        /// 10: Steal As Well<br/>
        /// 11: Elemental<br/>
        /// 12: Added Effect<br/>
        /// 13: Quadra Magic<br/>
        /// <br/>
        /// Independent:<br/>
        /// 0:  None<br/>
        /// 1:  HP&lt;-&gt;MP<br/>
        /// 2:  Underwater<br/>
        /// 3:  Cover<br/>
        /// 4:  Counter Attack<br/>
        /// 5:  Mega All<br/>
        /// 6:  Long Range<br/>
        /// 7:  Pre-Emptive<br/>
        /// 8:  Chocobo Lure<br/>
        /// 9:  Enemy Lure<br/>
        /// 10: Enemy Away<br/>
        /// 11: Gil Plus<br/>
        /// 12: EXP Plus<br/>
        /// 13: Luck Plus<br/>
        /// 14: Magic Plus<br/>
        /// 15: Speed Plus<br/>
        /// 16: HP Plus<br/>
        /// 17: MP Plus
        /// </summary>
        public int subtype;
        public int elementIndex = 16;
        public int[] boostValues = new int[5];
        [SerializeReference] public IndependentManager independentManager   = new IndependentManager();
        [SerializeReference] public SupportManager supportManager           = new SupportManager();
        [SerializeReference] public EnemySkillManager enemySkillManager     = new EnemySkillManager();
        [SerializeReference] public SelectionAbility[] selectionSpells      = new SelectionAbility[4];
        [SerializeReference] public EquipEffect equipEffects                = new EquipEffect();
        [SerializeReference] public ElementalAffinityManager elements       = new ElementalAffinityManager(true);
        [SerializeReference] public StatusEffectManager statusEffectManager = new StatusEffectManager();

        /* TODO: Link Effects
        * Draw a compatability chart in excel and identify which materia is compatible with which.
        * I think I will have a field of compatible materias in the support database.
        * I think spells can have an animation override field in the event of All materia.
        * Added Effect and Elemental can just use my element and effect GUIs.
        */
        // TODO: Independent effects

        //    "0: Add a command"
        //,   "1: Replace Attack command"
        //,   "2: Enemy Skill"
        //,   "3: W-Magic"
        //,   "4: W-Summon"
        //,   "5: W-Item"
        //,   "6: Master Command"

        public bool IsAddACommand          => type == MateriaType.Command && subtype == 0;
        public bool IsReplaceAttackCommand => type == MateriaType.Command && subtype == 1;
        public bool IsEnemySkill => type == MateriaType.Command && subtype == 2;
        public bool IsWMagic     => type == MateriaType.Command && subtype == 3;
        public bool IsWSummon    => type == MateriaType.Command && subtype == 4;
        public bool IsWItem      => type == MateriaType.Command && subtype == 5;
        public bool IsMaster => 
            ((type == MateriaType.Magic || type == MateriaType.Summon) && subtype == 1)
        ||  (type == MateriaType.Command && subtype == 6)
        ;

        public void SetType(MateriaType _type) {
            type = _type;
            AbilityType spellType = AbilityType.Magic;

            foreach (SelectionAbility ss in selectionSpells) {
                switch (_type) {
                    case MateriaType.Magic:
                        spellType = AbilityType.Magic;
                        break;
                    case MateriaType.Summon:
                        spellType = AbilityType.Summon;
                        break;
                    default:
                        break;
                }

                ss.type = spellType;
            }
        }

        public int GetLevel() {
            int level = 0;
            for (int i = 0; i < levels; i++) {
                bool isThresholdReached = ap >= apThresholds[i];

                if (isThresholdReached) {
                    level++;
                } else {
                    break;
                }
            }

            return level - 1;
        }

        public int GetTNL() {
            int lv = GetLevel();

            if (lv >= levels - 1) {
                return 0;
            } else {
                return apThresholds[lv + 1] - ap;
            }
        }

        public int GetBonusValue() {
            int level = GetLevel();

            //Debug.Log($"{name}: lv({level}), boost({boostValues[level]})");
            return boostValues[level];
        }
    }
}