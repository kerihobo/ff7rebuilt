using UnityEngine.UIElements;

public static class ExtIntegerField {
    public static IntegerField SetValue(this IntegerField _self, int _value) {
        _self.value = _value;
        return _self;
    }
}