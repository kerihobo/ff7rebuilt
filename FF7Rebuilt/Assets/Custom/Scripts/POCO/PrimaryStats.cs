using System;
using UnityEngine;

[Serializable]
public class PrimaryStats {
    public PrimaryStats() {
    }

    public PrimaryStats(PrimaryStats _source) {
        strength = _source.strength;
        vitality = _source.vitality;
        magic = _source.magic;
        spirit = _source.spirit;
        dexterity = _source.dexterity;
        luck = _source.luck;
    }

    [Range(0, 255)] public int strength;
    [Range(0, 255)] public int vitality;
    [Range(0, 255)] public int magic;
    [Range(0, 255)] public int spirit;
    [Range(0, 255)] public int dexterity;
    [Range(0, 255)] public int luck;

    public static PrimaryStats operator +(PrimaryStats a, PrimaryStats b) {
        PrimaryStats temp = new PrimaryStats();

        if (b == null) return a;

        temp.strength = a.strength + b.strength;
        temp.vitality  = a.vitality + b.vitality;
        temp.magic     = a.magic + b.magic;
        temp.spirit    = a.spirit + b.spirit;
        temp.dexterity = a.dexterity + b.dexterity;
        temp.luck      = a.luck + b.luck;

        return temp;
    }

    public void Log() {
        Debug.Log($"Str({strength}), Vit({vitality}), Mag({magic}), Spr({spirit}), Dex({dexterity}), Lck({luck})");
    }

    public void Clamp() {
        strength  = Mathf.Clamp(strength , 0, 255);   
        vitality  = Mathf.Clamp(vitality , 0, 255);   
        magic     = Mathf.Clamp(magic    , 0, 255);
        spirit    = Mathf.Clamp(spirit   , 0, 255);
        dexterity = Mathf.Clamp(dexterity, 0, 255);   
        luck      = Mathf.Clamp(luck     , 0, 255);
    }
}


