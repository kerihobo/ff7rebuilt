using CharacterTypes;
using FFVII.Database;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class EnemyLsStandardAbilities : VisualElement {
    public new class UxmlFactory : UxmlFactory<EnemyLsStandardAbilities, UxmlTraits> { }

    public EnemyLsStandardAbilities() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path).Instantiate();
        Add(tc);

        name = Name;
    }
    
    private const string Path = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/enemy-ls-standard-abilities.uxml";
    private const string Name = "enemy-ls-standard-abilities";

    private TemplateContainer tc;
    private Database database = DBResources.GetEnemies;
    private Enemy target;
    private ListView listView;

    public EnemyLsStandardAbilities Initialize(Enemy _target, Database _database) {
        target = _target;
        database = _database;

        Refresh(target);

        return this;
    }

    public void Add() {
        target.standardAbilities.Add(new SelectionAbility(AbilityType.Magic));

        EditorUtility.SetDirty(database);
        AssetDatabase.SaveAssets();

        listView.RefreshItems();
    }

    public void Refresh(Enemy _target) {
        target = _target;

        if (listView != null) {
            Remove(listView);
        }

        listView = new ListView();
        listView.AddToClassList("scr-abilities");

        listView.fixedItemHeight = 24;
        listView.itemsSource = target.standardAbilities;
        listView.makeItem = MakeItem;
        listView.bindItem = BindItem;
        listView.selectionType = SelectionType.None;

        Add(listView);

        VisualElement MakeItem() => AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/enemy-lsitem-standard-ability.uxml").Instantiate();

        void BindItem(VisualElement _ve, int _i) {
            int j = _i;
            SelectionAbility sa = target.standardAbilities[j];

            _ve.Q<EnumField>("enum-category").SetAbilityType(sa.type).RegisterValueChangedCallback(e => {
                sa.type = (AbilityType)e.newValue;
                EditorUtility.SetDirty(database);

                DropdownField ddf = _ve.Q<DropdownField>("drp-selection");
                ddf.Initialize(DBResources.GetAbilityList(sa.type), sa, database);
            });

            DropdownField ddf = _ve.Q<DropdownField>("drp-selection");
            ddf.Initialize(DBResources.GetAbilityList(sa.type), sa, database);
            ddf.RegisterValueChangedCallback(_e => {
                sa.SetDataExternally(ddf.index);
                EditorUtility.SetDirty(database);
            });

            _ve.Q<Button>("btn-remove").clicked += Remove;

            void Remove() {
                target.standardAbilities.Remove(sa);
                EditorUtility.SetDirty(database);
                AssetDatabase.SaveAssets();

                listView.RefreshItems();
            }
        }
    }
}
