﻿using System;

[Serializable]
public class SelectionBattleItem : SelectionData {
    public AbilityType type = AbilityType.BattleItem;
}