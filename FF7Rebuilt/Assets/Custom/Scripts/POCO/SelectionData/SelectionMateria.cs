﻿using System;
using System.Collections.Generic;
using InventoryTypes;
using FFVII.Database;

[Serializable]
public class SelectionMateria : SelectionData {
    public MateriaType type = MateriaType.Magic;

    public override string Reset<T>(List<T> _list) {
        return base.Reset(DBResources.GetMateriaList(type));
    }

    public Materia Nullify() {
        index = -1;

        selectionName = "Empty";

        return null;
    }

    public Materia GetSelectedMateria() {
        if (DBResources.GetMateriaList(type) != null && DBResources.GetMateriaList(type).Count > index && index > -1) {
            return DBResources.GetMateriaList(type)[index];
        }

        return null;
    }

    public bool IsValidMateriaSelection() => GetSelectedMateria() != null;

}
