using InventoryTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIMateriaInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public Materia materia { get; set; }

    private CanvasGroup cnvGrp;

    private Image imgIcon;
    private TextMeshProUGUI txtName;

    public void Initialize() {
        rct = GetComponent<RectTransform>();
        cnvGrp = GetComponent<CanvasGroup>();
        txtName = GetComponent<TextMeshProUGUI>();
        imgIcon = transform.GetChild(0).GetComponent<Image>();
    }

    public void Refresh(Materia _materia) {
        materia = _materia;
        cnvGrp.alpha = materia != null ? 1 : 0;

        if (materia == null) {
            Clear();
            return;
        }
        
        imgIcon.sprite = ItemIconContainer.GetInstance().GetIconByMateria(_materia);
        imgIcon.SetNativeSize();
        txtName.text = materia.name;
    }

    public void Clear() {
        txtName.text = "";
        imgIcon.sprite = null;
        cnvGrp.alpha = 0;
    }
}
