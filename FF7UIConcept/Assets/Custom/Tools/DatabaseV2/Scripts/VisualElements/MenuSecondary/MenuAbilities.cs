using UnityEngine.UIElements;

public class MenuAbilities : MenuSecondary {
    public new class UxmlFactory : UxmlFactory<MenuAbilities, UxmlTraits> {
    }

    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/MenuSecondary/menu-abilities.uxml";
    protected override string Name { get; set; } = "menu-abilities";

    public MenuAbilities() : base() {
        ve.Q<Button>("btn-magic").clickable.clickedWithEventInfo        += _e => { Show(_e, new EditorMagic()      , EWTest.ColumnWidths.TWO); };
        ve.Q<Button>("btn-summons").clickable.clickedWithEventInfo      += _e => { Show(_e, new EditorSummons()    , EWTest.ColumnWidths.TWO); };
        ve.Q<Button>("btn-enemy-skills").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorEnemySkills(), EWTest.ColumnWidths.TWO); };
        ve.Q<Button>("btn-limit-breaks").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorLimitBreaks(), EWTest.ColumnWidths.TWO); };
        ve.Q<Button>("btn-battle-items").clickable.clickedWithEventInfo += _e => { Show(_e, new EditorBattleItems(), EWTest.ColumnWidths.TWO); };

        Show(ve.Q<Button>("btn-magic"), new EditorMagic(), EWTest.ColumnWidths.TWO);
    }
}
