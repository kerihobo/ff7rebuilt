﻿using System;

[Serializable]
public class SelectionAbility : SelectionData {
    public SelectionAbility(AbilityType _type) {
        type = _type;
    }

    public AbilityType type = AbilityType.Magic;
}