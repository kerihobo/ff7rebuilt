public static class EventManagerField {
    public delegate void Method();

    public static event Method OnOpenFieldMenu;
    public static void OpenFieldMenu() => OnOpenFieldMenu?.Invoke();

    public static event Method OnCloseFieldMenu;
    public static void CloseFieldMenu() => OnCloseFieldMenu?.Invoke();

    public static event Method OnEngage;
    public static void Engage() => OnEngage?.Invoke();

    public static event Method OnDisengage;
    public static void Disengage() => OnDisengage?.Invoke();
}
