using InventoryTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuStatInfo : MonoBehaviour {
    [SerializeField] private Image imgIcon;
    [SerializeField] private TextMeshProUGUI txtName;
    [SerializeField] private TextMeshProUGUI txtElement;
    [SerializeField] private TextMeshProUGUI txtAp;
    [SerializeField] private TextMeshProUGUI txtMaster;
    [SerializeField] private TextMeshProUGUI txtTnl;
    [SerializeField] private MateriaMenuStarSet starSet;
    [SerializeField] private MateriaMenuEquipEffectManager equipEffectManager;
    [SerializeField] private MateriaMenuStatInfoAbilityList abilityList;
    [SerializeField] private MateriaMenuEnemySkillInfo enemySkillInfo;
    [SerializeField] private CanvasGroup cnvGroupAll;
    [SerializeField] private CanvasGroup cnvGroupMain;

    public void Initialize() {
        starSet.Initialize();
        equipEffectManager.Initialize();
        enemySkillInfo.Initialize();
        abilityList.Initialize();
    }

    public void Refresh(Materia _materia) {
        if (_materia == null) {
            cnvGroupAll.alpha = 0;

            return;
        }

        enemySkillInfo.SetVisibility(false);
        cnvGroupAll.alpha = 1;

        imgIcon.sprite = ItemIconContainer.GetInstance().GetIconByMateria(_materia);
        txtName.text = _materia.name;
        
        if (_materia.IsEnemySkill) {
            cnvGroupMain.alpha = 0;
            enemySkillInfo.Refresh(_materia);

            return;
        }

        bool isMasterMateria = _materia.IsMaster;
        cnvGroupMain.alpha = isMasterMateria ? 0 : 1;
        if (isMasterMateria) return;

        txtElement.text = ElementSelection.GetElementLabel(_materia.elementIndex);
        txtTnl.text = _materia.GetTNL().ToString();
        txtAp.text = _materia.ap.ToString();
        starSet.Refresh(_materia);
        abilityList.Refresh(_materia);
        equipEffectManager.Refresh(_materia);
        ToggleMasterText(_materia);

        void ToggleMasterText(Materia _materia) {
            bool isMastered = _materia.ap >= _materia.apThresholds[_materia.levels - 1];
            txtAp.gameObject.SetActive(!isMastered);
            txtMaster.gameObject.SetActive(isMastered);
        }
    }
}
