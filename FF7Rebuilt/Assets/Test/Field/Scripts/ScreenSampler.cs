using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class ScreenSampler : MonoBehaviour {
    public Rect sampleRect = new Rect(80, 240, 320, 240); // Sampling rectangle on the screen, dynamically updated
    public float zoomFactor = 2.0f; // Zoom factor for the sampled texture
    public RawImage displayImage; // UI RawImage to display the sampled texture

    private const int RESOLUTION_MULTIPLIER = 2; // Multiplier for full resolution RenderTexture
    private RenderTexture fullResRenderTexture;
    private RenderTexture sampledRenderTexture;
    private Material blitMaterial;

    void Start() {
        InitializeRenderTextures();
    }

    void Update() {
        // Dynamically calculate sampleRect size based on screen size and zoom level
        float sampleWidth = Screen.width / zoomFactor;
        float sampleHeight = Screen.height / zoomFactor;

        // Update the sampled RenderTexture size if the zoom level changes
        if (sampledRenderTexture != null) {
            //int width = (int)(sampleRect.width * RESOLUTION_MULTIPLIER);          // I'M NOT SURE IF IT'S SAFE TO USE ZOOMFACTOR IN PLACE OF RESOLUTION_MULTIPLIER!!!
            //int height = (int)(sampleRect.height * RESOLUTION_MULTIPLIER);
            int width = (int)(sampleRect.width * zoomFactor);
            int height = (int)(sampleRect.height * zoomFactor);
            if (sampledRenderTexture.width != width || sampledRenderTexture.height != height) {
                UpdateSampledRenderTexture();
            }
        }
    }

    void InitializeRenderTextures() {
        // Create a full resolution RenderTexture with increased fidelity
        fullResRenderTexture = new RenderTexture(Screen.width * RESOLUTION_MULTIPLIER, Screen.height * RESOLUTION_MULTIPLIER, 0, RenderTextureFormat.RGB565);
        fullResRenderTexture.filterMode = FilterMode.Bilinear;
        fullResRenderTexture.Create();

        // Assign the full resolution RenderTexture to this camera
        Camera.main.targetTexture = fullResRenderTexture;

        // Create a material with a shader to handle the blit operation
        blitMaterial = new Material(Shader.Find("Hidden/BlitSample"));

        // Create a RenderTexture for the sampled area
        UpdateSampledRenderTexture();

        // Set the sampled RenderTexture to the RawImage
        //displayImage.texture = sampledRenderTexture;
    }

    void UpdateSampledRenderTexture() {
        if (sampledRenderTexture != null) {
            sampledRenderTexture.Release();
        }

        // Create a RenderTexture for the sampled area with zoom applied
        //int width = (int)(sampleRect.width * RESOLUTION_MULTIPLIER);          // I'M NOT SURE IF IT'S SAFE TO USE ZOOMFACTOR IN PLACE OF RESOLUTION_MULTIPLIER!!!
        //int height = (int)(sampleRect.height * RESOLUTION_MULTIPLIER);
        int width = (int)(sampleRect.width * zoomFactor);
        int height = (int)(sampleRect.height * zoomFactor);

        sampledRenderTexture = new RenderTexture(width, height, 0, RenderTextureFormat.RGB565);
        sampledRenderTexture.filterMode = FilterMode.Bilinear;
        sampledRenderTexture.Create();
        displayImage.texture = sampledRenderTexture;
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest) {
        // Check if all required components are initialized
        if (fullResRenderTexture == null || sampledRenderTexture == null || blitMaterial == null) {
            Debug.LogError("RenderTextures or material not initialized properly");
            return;
        }

        Vector4 sampleRectNormalized = new Vector4(
            (sampleRect.x * RESOLUTION_MULTIPLIER) / fullResRenderTexture.width,
            (sampleRect.y * RESOLUTION_MULTIPLIER) / fullResRenderTexture.height,
            (sampleRect.width * RESOLUTION_MULTIPLIER) / fullResRenderTexture.width,
            (sampleRect.height * RESOLUTION_MULTIPLIER) / fullResRenderTexture.height
        );

        Vector4 zoomParams = new Vector4(
            1.0f / zoomFactor, // ScaleX
            1.0f / zoomFactor, // ScaleY
            0.0f, // Not used
            0.0f  // Not used
        );

        blitMaterial.SetVector("_SampleRect", sampleRectNormalized);
        blitMaterial.SetVector("_ZoomParams", zoomParams);

        // Apply the custom shader to sample the region of interest
        Graphics.Blit(fullResRenderTexture, sampledRenderTexture, blitMaterial);

        // Blit the source RenderTexture to the destination RenderTexture (e.g., post-processing)
        Graphics.Blit(src, dest);
    }

    void OnDestroy() {
        System.Action<Object> destroyAction = Application.isPlaying ? Destroy : DestroyImmediate;

        // Clean up resources
        if (fullResRenderTexture != null) {
            if (Camera.main) {
                Camera.main.targetTexture = null;
            }

            fullResRenderTexture.Release();
            destroyAction(fullResRenderTexture);
        }
        if (sampledRenderTexture != null) {
            sampledRenderTexture.Release();
            destroyAction(sampledRenderTexture);
        }
        if (blitMaterial != null) {
            destroyAction(blitMaterial);
        }
    }

    public void ForceUpdate(Rect _sampleRect, float _zoomFactor) {
        sampleRect = _sampleRect;
        zoomFactor = _zoomFactor;

#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
    }

    public void RaycastToWorld(Vector2 _mousePosition) {
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;
        float renderTextureWidth = fullResRenderTexture.width;
        float renderTextureHeight = fullResRenderTexture.height;

        _mousePosition.y = screenHeight - _mousePosition.y;

        Vector2 normalizedMousePosition = new Vector2(
            _mousePosition.x / screenWidth,
            _mousePosition.y / screenHeight
        );

        _mousePosition = sampleRect.position + (sampleRect.size * normalizedMousePosition);

        Vector2 scale = new Vector2(
            renderTextureWidth / screenWidth,
            renderTextureHeight / screenHeight
        );

        Vector2 scaledMousePosition = new Vector2(
            _mousePosition.x * scale.x,
            _mousePosition.y * scale.y
        );

        Ray ray = Camera.main.ScreenPointToRay(scaledMousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("WalkMesh"))) {
            Debug.DrawLine(ray.origin, hit.point, Color.green, 2);
        } else {
            Debug.DrawRay(ray.origin, ray.direction * 1000f, Color.red, 2);
        }
    }
}