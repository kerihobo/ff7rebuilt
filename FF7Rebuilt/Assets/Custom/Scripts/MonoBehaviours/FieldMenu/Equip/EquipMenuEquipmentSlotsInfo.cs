using InventoryTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EquipMenuEquipmentSlotsInfo : MonoBehaviour {
    [SerializeField] private RectTransform growthTextParent;
    [SerializeField] private RectTransform slotsParent;
    [SerializeField] private GameObject infoContainer;
    [SerializeField] private Sprite sprSlotNoGrowth;
    [SerializeField] private Sprite sprSlotNormal;
    [SerializeField] private Image[] arrImgLinks;
    private Image[] arrImgSlots;
    private TextMeshProUGUI[] arrTxtGrowth;

    public void Initialize() {
        arrTxtGrowth = growthTextParent.GetComponentsInChildren<TextMeshProUGUI>(true);

        arrImgSlots = new Image[slotsParent.childCount];
        foreach (Transform t in slotsParent) {
            arrImgSlots[t.GetSiblingIndex()] = t.GetComponent<Image>();
        }

        Clear();
        ToggleDisplay(false);
    }

    public void Clear() {
        HideAllGrowthTexts();
        HideAllSlots();

        void HideAllGrowthTexts() {
            foreach (TextMeshProUGUI txt in arrTxtGrowth) {
                txt.gameObject.SetActive(false);
            }
        }

        void HideAllSlots() {
            foreach (Image img in arrImgSlots) {
                img.enabled = false;
            }

            foreach (Image img in arrImgLinks) {
                img.enabled = false;
            }
        }
    }


    public void ToggleDisplay(bool _isActive) {
        infoContainer.SetActive(_isActive);
    }

    public void Refresh(Equipment _target) {
        Clear();
        RefreshSlots();
        RefreshLinks();
        
        void RefreshSlots() {
            for (int i = 0; i < _target.materiaSlotCount; i++) {
                arrImgSlots[i].enabled = true;
             
                bool isGrowthNone = _target.materiaGrowth == MateriaGrowthType.None;
                bool isGrowthNothing = _target.materiaGrowth == MateriaGrowthType.Nothing;
                arrImgSlots[i].sprite = isGrowthNone || isGrowthNothing ? sprSlotNoGrowth : sprSlotNormal;
            }
        }

        void RefreshLinks() {
            for (int i = 0; i < _target.links; i++) {
                arrImgLinks[i].enabled = true;
            }
        }
    }
}
