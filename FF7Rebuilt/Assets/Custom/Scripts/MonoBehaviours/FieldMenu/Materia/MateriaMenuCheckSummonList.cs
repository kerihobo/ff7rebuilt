using CharacterTypes;
using FFVII.Database;
using InventoryTypes;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuCheckSummonList : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private MateriaMenuCheckSummonInfo additionalOption;
    [SerializeField] private RectTransform optionsParent;

    private RectTransform scrollBarFG;
    private int selectedIndex;
    private MateriaMenuCheck check;
    private MateriaMenuCheckCommand commandWindow;
    private MateriaMenuCheckSummonInfo[] arrOptions;

    // MAYBE REFACTOR
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 1;
    private Materia[] arrViewItems = new Materia[0];
    //private Materia[] arrItems;

    private Player GetSelectedCharacter => check.GetSelectedCharacter;

    public void Initialize(MateriaMenuCheck _check, MateriaMenuCheckCommand _commandWindow) {
        check = _check;
        commandWindow = _commandWindow;
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        // It's just "all the summon materia". Can be populated during Initialize.
        //arrViewItems = DBResources.GetMateriaList(MateriaType.Summon).Where(x => !x.IsMaster)
        arrViewItems = GameManager.Instance.gameData.inventory.lsAllMateria
            .Where(x => x != null && x.type == MateriaType.Summon && !x.IsMaster)
            .OrderByDescending(x => x.id)
            .ToArray()
        ;
        //arrItems = GameManager.Instance.gameData.inventory.arrMateria.Where(x => x.type == MateriaType.Summon).ToArray();

        arrOptions = optionsParent.GetComponentsInChildren<MateriaMenuCheckSummonInfo>(true);
        foreach (MateriaMenuCheckSummonInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOption.Initialize();
        ClearAdditional();

        Disable();

        cursor.Initialize(arrOptions[0].rct);

        listSpace = vlgOptions.spacing;
        listHeight = arrOptions[0].rct.rect.height;
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = lowBound + i;
            Materia itemInView = null;

            // Only show an item within the range of our viewItems.
            if (itemIndex < arrViewItems.Length) {
                itemInView = arrViewItems[itemIndex];
            }

            arrOptions[i].Refresh(GetSelectedCharacter, itemInView);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - columns;
        MateriaMenuCheckSummonInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOption.rct.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        Materia itemInView = null;

        // Only show an item within the range of our viewItems.
        if (anchoredIndex < arrViewItems.Length) {
            itemInView = arrViewItems[anchoredIndex];
        }

        additionalOption.Refresh(GetSelectedCharacter, itemInView);
    }

    private void ClearAdditional() {
        additionalOption.Clear();
    }

    public void ResetSelection() {
        selectedIndex = 0;
        lowBound = 0;

        UpdateScrollBar();
        SetCursor();
    }

    public void SetCursor() {
        MateriaMenuCheckSummonInfo selectedItemInfo = arrOptions[selectedIndex];

        cursor.SetCursorPosition(selectedItemInfo.rct);

        bool isMasterSummon = GetSelectedCharacter.Materia
            .Any(x => x.type == MateriaType.Summon && x.IsMaster)
        ;

        Materia materiaReference;
        if (isMasterSummon) {
            materiaReference = selectedItemInfo.materiaReference;
        } else {
            materiaReference = selectedItemInfo.GetEquippedMateria(GetSelectedCharacter, selectedItemInfo.materiaReference);
        }

        check.SetDescription(materiaReference?.description);
        check.SetInfoSummon(materiaReference, isMasterSummon);
    }

    public void Open() {
        lowBound = 0;

        //if (arrViewItems.Length <= 0) {
        //    return;
        //}

        Show();
        SetControllable();
        Refresh();
        ResetSelection();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    public void Disable() {
        cursor.Hide();
        ClearAdditional();

        gameObject.SetActive(false);
    }

    public void Close() {
        Disable();
        commandWindow.ExitSubMenu();
        check.CloseInfoWindow();

        //foreach (MateriaMenuCheckSummonInfo info in arrOptions) {
        //    info.foundMateria = null;
        //}
    }

    private void Show() {
        gameObject.SetActive(true);
        SetScrollbarHeight();
        UpdateScrollBar();
        cursor.Show();

        void SetScrollbarHeight() {
            float scrollBarHeight = scrollBarBG.sizeDelta.y;
            scrollBarHeight *= arrOptions.Length / (float)arrViewItems.Length;
            scrollBarHeight = Mathf.RoundToInt(scrollBarHeight);
            scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarHeight);
        }
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        float scroll = ((float)lowBound / columns) / (arrViewItems.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

#region Override methods.
    public override void GetInputConfirmPressed() { }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _v > 0) || (lowBound + selectedIndex == arrViewItems.Length - columns && _v < 0)) return;

        int v = -_v * columns;
        selectedIndex += v;

        // Clamp if you reach the end of our current items
        if (lowBound + selectedIndex >= arrViewItems.Length) {
            selectedIndex--;
        }

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
            lowBound -= lowBound % columns;
        }

        if (selectedIndex < 0) {
            selectedIndex += columns;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= columns;
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;
            ClearAdditional();
        }

        int v = _pageDirection * (3 * columns);
        int lowBoundMax = arrViewItems.Length - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
