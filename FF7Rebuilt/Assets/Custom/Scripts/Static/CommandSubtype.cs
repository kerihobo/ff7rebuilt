﻿public static class CommandSubtype {
    public static readonly string[] COMMAND_SUBTYPE_NAMES = {
        "0: Add a command"
    ,   "1: Replace Attack command"
    ,   "2: Enemy Skill"
    ,   "3: W-Magic"
    ,   "4: W-Summon"
    ,   "5: W-Item"
    ,   "6: Master Command"
    };
}
