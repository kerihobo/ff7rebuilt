﻿public static class CursorAction {
    public static readonly string[] CURSOR_ACTION_NAMES = {
        "0: None"
    ,   "1: Perform command using target data"
    ,   "2: Magic menu"
    ,   "3: Summon menu"
    ,   "4: Item menu"
    ,   "5: Enemy Skill menu"
    ,   "6: Throw menu"
    ,   "7: Limit menu"
    ,   "8: Enable target selection via cursor"
    ,   "9: W-Magic menu"
    ,   "10: W-Summon menu"
    ,   "11: W-Item menu"
    ,   "12: Coin menu"
    };
}
