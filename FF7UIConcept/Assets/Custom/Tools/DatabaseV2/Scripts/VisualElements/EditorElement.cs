using UnityEditor;
using UnityEngine.UIElements;

/// <summary>
/// Intended as an abstract type.
/// </summary>
public class EditorElement : VisualElement {
    public new class UxmlFactory : UxmlFactory<MenuInventory, UxmlTraits> {
    }

    protected virtual string Path { get; set; } = null;
    protected virtual string Name { get; set; } = null;

    protected VisualTreeAsset vta;
    protected VisualElement ve;

    public EditorElement() {
        AddToClassList("editor");

        vta = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path);
        ve = vta.Instantiate();
        name = Name;
        Add(ve);
    }
}
