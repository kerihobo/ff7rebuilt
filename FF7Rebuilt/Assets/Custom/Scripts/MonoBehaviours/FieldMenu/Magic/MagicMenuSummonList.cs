using CharacterTypes;
using InventoryTypes;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MagicMenuSummonList : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private RectTransform optionsParent;
    private MagicMenuOptions magicMenuOptions;
    private ScreenFieldMagic screenFieldMagic;
    private RectTransform scrollBarFG;
    private int selectedIndex;
    private MagicMenuSummonInfo[] arrOptions;

    [SerializeField] private RectTransform additionalOptionParent;
    private MagicMenuSummonInfo[] additionalOptions;
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 2;
    private Materia[] arrViewItems = new Materia[0];

    private Player GetSelectedCharacter => screenFieldMagic.GetSelectedCharacter;

    public void Initialize(ScreenFieldMagic _screenFieldMagic, MagicMenuOptions _magicMenuOptions) {
        magicMenuOptions = _magicMenuOptions;
        screenFieldMagic = _screenFieldMagic;
        
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();

        GridLayoutGroup glgOptions = GetComponentInChildren<GridLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(glgOptions.GetComponent<RectTransform>());

        // It's just "all the summon materia". Can be populated during Initialize.
        arrViewItems = GameManager.Instance.gameData.inventory.lsAllMateria
            .Where(x => x != null && x.type == MateriaType.Summon && !x.IsMaster)
            .OrderByDescending(x => x.id)
            .ToArray()
        ;

        arrOptions = optionsParent.GetComponentsInChildren<MagicMenuSummonInfo>(true);
        foreach (MagicMenuSummonInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOptions = additionalOptionParent.GetComponentsInChildren<MagicMenuSummonInfo>(true);
        foreach (MagicMenuSummonInfo info in additionalOptions) {
            info.Initialize();
        }

        ClearAdditional();

        Disable();

        cursor.Initialize(arrOptions[0].rct);

        listSpace = glgOptions.spacing.y;
        listHeight = arrOptions[0].rct.rect.height;
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = lowBound + i;
            Materia itemInView = null;

            // Only show an item within the range of our viewItems.
            if (itemIndex < arrViewItems.Length) {
                itemInView = arrViewItems[itemIndex];
            }

            arrOptions[i].Refresh(GetSelectedCharacter, itemInView);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - columns;
        MagicMenuSummonInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOptionParent.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        for (int i = 0; i < additionalOptions.Length; i++) {
            Materia materia = arrViewItems[anchoredIndex + i];
            additionalOptions[i].Refresh(GetSelectedCharacter, materia);
        }
    }

    private void ClearAdditional() {
        foreach (var info in additionalOptions) {
            info.Clear();
        }
    }

    public void ResetSelection() {
        selectedIndex = 0;
        lowBound = 0;

        UpdateScrollBar();
        SetCursor();
    }

    public void SetCursor() {
        MagicMenuSummonInfo selectedItemInfo = arrOptions[selectedIndex];

        cursor.SetCursorPosition(selectedItemInfo.rct);

        bool isMasterSummon = GetSelectedCharacter.Materia
            .Any(x => x.type == MateriaType.Summon && x.IsMaster)
        ;

        Materia materiaReference;
        if (isMasterSummon) {
            materiaReference = selectedItemInfo.materiaReference;
        } else {
            materiaReference = selectedItemInfo.GetEquippedMateria(GetSelectedCharacter, selectedItemInfo.materiaReference);
        }

        screenFieldMagic.SetDescription(materiaReference?.description);
        screenFieldMagic.SetInfoSummon(materiaReference, isMasterSummon);
    }

    public void Open() {
        lowBound = 0;

        //if (arrViewItems.Length <= 0) {
        //    return;
        //}

        Show();
        SetControllable();
        Refresh();
        SetCursor();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    public void Disable() {
        cursor.Hide();
        ClearAdditional();

        gameObject.SetActive(false);
    }

    public void Close() {
        Disable();
        screenFieldMagic.OpenOptions();
    }

    private void Show() {
        gameObject.SetActive(true);
        SetScrollbarHeight();
        UpdateScrollBar();
        cursor.Show();

        void SetScrollbarHeight() {
            float scrollBarHeight = scrollBarBG.sizeDelta.y;
            scrollBarHeight *= arrOptions.Length / (float)arrViewItems.Length;
            scrollBarHeight = Mathf.RoundToInt(scrollBarHeight);
            scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarHeight);
        }
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        if (arrViewItems == null || arrViewItems.Length == 0) return;

        float scroll = ((float)lowBound / columns) / (arrViewItems.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

#region Override methods.
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputConfirmPressed() { }

    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex <= columns - 1 && _v > 0) || (lowBound + selectedIndex >= arrViewItems.Length - columns && _v < 0)) return;

        int v = -_v * columns;
        selectedIndex += v;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
            lowBound -= lowBound % columns;
        }

        if (selectedIndex < 0) {
            selectedIndex += columns;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= columns;
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _h < 0) || (lowBound + selectedIndex == arrViewItems.Length - 1 && _h > 0)) return;

        selectedIndex += _h;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += _h * columns;
            lowBound -= lowBound % columns;

            if (isTooHigh) {
                selectedIndex -= columns;
            } else if (isTooLow) {
                selectedIndex += columns;
            }
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrViewItems.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        SetCursor();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(_h));
        }
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;

            ClearAdditional();
        }

        int v = _pageDirection * (3 * columns);
        int lowBoundMax = arrViewItems.Length - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
