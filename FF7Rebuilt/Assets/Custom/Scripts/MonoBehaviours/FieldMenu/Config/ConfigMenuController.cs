﻿using UnityEngine;

public class ConfigMenuController : MenuControllable {
    [SerializeField] private FingerCursorRight cursor;
    [SerializeField] private RectTransform optionParent;
    private int selectedIndex;
    private RectTransform[] arrOptions;
    private ScreenFieldConfig screenFieldConfig;
    private float lifetime;
    private bool isSwitch;

    public void Initialize(ScreenFieldConfig _screenFieldConfig) {
        screenFieldConfig = _screenFieldConfig;

        arrOptions = new RectTransform[optionParent.childCount];
        for (int i = 0; i < arrOptions.Length; i++) {
            arrOptions[i] = optionParent.GetChild(i).GetComponent<RectTransform>();
        }

        Disable();

        cursor.Initialize(arrOptions[0]);
    }

    private void Update() {
        lifetime += Time.deltaTime;

        if (lifetime > 2) {
            lifetime = 0;

            SetDescription();
        }
    }

    private void SetDescription() {
        isSwitch = !isSwitch;
        string description = isSwitch ? "Press [CANCEL] to end." : "Press [OK] to configure a key.";
        screenFieldConfig.SetDescription(description);
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex]);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    public void Disable() {
        gameObject.SetActive(false);
    }

    private void Close() {
        Disable();
        screenFieldConfig.Open();
    }

    public void Open() {
        gameObject.SetActive(true);

        lifetime = 0;
        isSwitch = false;

        SetDescription();

        ResetSelection();

        cursor.Show();
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputConfirmPressed() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    
    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }

    public override void GetInputCancelPressed() {
        screenFieldConfig.OpenMain();
    }
#endregion
}