﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PortContainer {
    [SerializeReference]
    public List<Port> ports = new List<Port>();
    public Port.Direction direction;

    public PortContainer(Port.Direction _direction) {
        direction = _direction;
    }

    public void Add<T>(Node _parentNode, string _name, Port.Direction _direction, Port.Capacity _capacity, Port.DisplayMode _displayMode) where T : Port {
        ports.Add((T)Activator.CreateInstance(typeof(T), _parentNode, _name, _direction, _capacity, _displayMode));
    }

    public void Render(StyleSet _styleSet, Node _parentNode) {
        GUIStyle style = _styleSet.portContainerIn;
        if (Event.current.type == EventType.Repaint) {
            style = direction == Port.Direction.IN ? _styleSet.portContainerIn : _styleSet.portContainerOut;
        }

        Color initialBGColour = GUI.backgroundColor;

        if (_parentNode.isSelected) {
            GUI.backgroundColor = new Color(220 / 255f, 255 / 255f, 255 / 255f);
        }

        GUILayout.BeginVertical(style);
            GUI.backgroundColor = initialBGColour;
        
            for (int i = 0; i < ports.Count; i++) {
                ports[i].Render(_styleSet);
                GUILayout.Space(5);
            }
        GUILayout.EndVertical();
    }

    public void DisconnectPorts() {
        foreach (Port p in ports) {
            p.Disconnect();
        }
    }
}
