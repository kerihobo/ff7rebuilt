using UnityEditor;
using UnityEngine.UIElements;

public class MenuPrimary : VisualElement {
    public new class UxmlFactory : UxmlFactory<MenuPrimary, UxmlTraits> {
    }

    private VisualTreeAsset vta;
    private VisualElement ve;

    public MenuPrimary() {
        vta = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom/Tools/DatabaseV2/DOM/UXML/menu-primary.uxml");
        ve = vta.Instantiate();
        name = "menu-primary";
        Add(ve);

        EWTest.Instance.rootVisualElement.Add(this);

        ve.Q<Button>("btn-characters").clickable.clickedWithEventInfo += _e => Show(_e, new MenuCharacters());
        ve.Q<Button>("btn-inventory").clickable.clickedWithEventInfo  += _e => Show(_e, new MenuInventory());
        ve.Q<Button>("btn-abilities").clickable.clickedWithEventInfo  += _e => Show(_e, new MenuAbilities());
        ve.Q<Button>("btn-areas").clickable.clickedWithEventInfo      += _e => Show(_e, new MenuAreas());

        Show(ve.Q<Button>("btn-characters"), new MenuCharacters());
    }

    private void Show(EventBase _e, MenuSecondary _menu) {
        Show((VisualElement)_e.currentTarget, _menu);
    }

    protected void Show(VisualElement _ve, MenuSecondary _menu) {
        SetSelected(_ve);
        EWTest.Instance.ShowSecondary(_menu);
    }

    public void SetSelected(VisualElement _ve) {
        ve.Query<Button>().ForEach(x => x.RemoveFromClassList("selected-primary"));
        _ve.AddToClassList("selected-primary");
    }
}
