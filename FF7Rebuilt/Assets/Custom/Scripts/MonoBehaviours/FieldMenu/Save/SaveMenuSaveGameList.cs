using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SaveMenuSaveGameList : MenuControllable {
    public FingerCursorRight cursor;

    private const float SCROLL_TIME = 8 / 60f;

    [SerializeField] private RectTransform optionsParent;
    [SerializeField] private TextMeshProUGUI txtIndex;
    [SerializeField] private UISaveGameInfo additionalOption;
    [SerializeField] private SaveMenuConfirmationPrompt confirmationPrompt;
    private ScreenFieldSave screenFieldSave;
    private int selectedIndex;
    private UISaveGameInfo[] arrOptions;
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int selectedSlot;

    private GameData[] arrSaveGameFiles => GameManager.Instance.saveGameSlots[selectedSlot].saveGameFiles;

    public void Initialize(ScreenFieldSave _screenFieldSave) {
        screenFieldSave = _screenFieldSave;
        screenFieldSave.saveGameList = this;

        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<UISaveGameInfo>();
        foreach (UISaveGameInfo option in arrOptions) {
            option.Initialize();
        }
        additionalOption.Initialize();

        confirmationPrompt.Initialize(screenFieldSave, this);

        Close();

        cursor.Initialize(arrOptions[0].rct);

        listSpace = vlgOptions.spacing;
        listHeight = arrOptions[0].rct.rect.height;
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        // TODO: Get save-game from data-file
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = lowBound + i;
            GameData selectedItem = arrSaveGameFiles[itemIndex];
            arrOptions[i].Refresh(selectedItem);
        }
    }

    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - 1;
        UISaveGameInfo anchoredInfo = arrOptions[^1];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOption.rct.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        GameData selectedItem = arrSaveGameFiles[anchoredIndex];
        additionalOption.Refresh(selectedItem);
    }

    private void ClearAdditional() {
        additionalOption.Clear();
    }

    public void ResetSelection() {
        selectedIndex = 0;
        lowBound = 0;

        SetCursor();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rct);

        SetIndexText();
        Refresh();
    }

    private void SetIndexText() {
        txtIndex.text = $"{(lowBound + selectedIndex + 1):00}";
    }

    public void Open() {
        Show();
        SetControllable();

        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    public void Open(int _selectedSlot) {
        //arrSaveGameFiles[0] = GameManager.Instance.gameData;
        selectedSlot = _selectedSlot;
        ResetSelection();
        
        Open();
    }

    private void Close() {
        Disable();
        screenFieldSave.OpenDataFileSelect();
        
        ClearAdditional();
    }

    public void Disable() {
        gameObject.SetActive(false);
    }

    public void Show() {
        SetCursor();
        screenFieldSave.SetDescription("Select a save game.");
        SetIndexText();
        gameObject.SetActive(true);
        cursor.SetFlickerState(false);
    }

    private IEnumerator AnimatedScroll(int _v) {
        FieldMenu.Instance.SetCooldownTraverse(SCROLL_TIME);
        FieldMenu.Instance.SetCooldownNavigate(SCROLL_TIME);
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = SCROLL_TIME;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;

        ClearAdditional();
        Refresh();
        SetCursor();

        corAnimatedScroll = null;
    }

#region Override Methods
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputConfirm() { }
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }

    public override void GetInputConfirmPressed() {
        cursor.SetFlickerState(true);
        confirmationPrompt.Open(selectedSlot, lowBound + selectedIndex);
    }
    
    public override void GetInputCancelPressed() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _v > 0) || (lowBound + selectedIndex == arrSaveGameFiles.Length - 1 && _v < 0)) return;
        //Debug.Log(LowBound + SelectedIndex + " : " + arrItems.Length);

        int v = -_v;
        selectedIndex += v;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
        }

        if (selectedIndex < 0) {
            selectedIndex += 1;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= 1;
        }

        lowBound = Mathf.Clamp(lowBound, 0, arrSaveGameFiles.Length - arrOptions.Length);

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
        }
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;
            ClearAdditional();
        }

        int v = _pageDirection * 3;
        int lowBoundMax = arrSaveGameFiles.Length - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);

        Refresh();
        SetCursor();
    }
    
    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }
#endregion
}
