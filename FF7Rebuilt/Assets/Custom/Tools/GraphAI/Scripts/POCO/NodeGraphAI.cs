﻿using System;
using CharacterTypes;
using FFVII.Database;
using UnityEditor;
using UnityEngine;

[Serializable]
public class NodeGraphAI : NodeGraph {
    [SerializeReference, HideInInspector]
    public Enemy enemy;

    public NodeGraphAI(Enemy _enemy) : base() {
        enemy = _enemy;
        enemy.aiGraph = this;
    }

    protected override void InitializeBlackboard() {
        blackboard = new BlackboardAI(this);
    }

    protected override void InitializeSearchWindow() {
        searchWindow = new SearchWindowAI(this);
    }

    public override void Save() {
        //EditorUtility.SetDirty(enemy);
        EditorUtility.SetDirty(DBResources.GetEnemies);
        AssetDatabase.SaveAssets();
    }
}
