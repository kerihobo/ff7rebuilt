﻿using System.Collections.Generic;
using UnityEngine;

public class NodeIf : Node {
    public bool value = false;

    private bool copyA;

    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(160, 75);
        name = "If";

        AddPortIn<Port>();
        AddPortIn<PortBool>("Predicate");
        AddPortOut<Port>("True");
        AddPortOut<Port>("False");

        portContainerIn.ports[1].SetValue(copyA);
    }

    public override void Run(NodeGraph _nodeGraph) {
        List<Port> predicatePorts = portContainerIn.ports[1].connectedPorts;
        bool predicate = predicatePorts.Count > 0 ? predicatePorts[0].GetValue() : value;

        if (predicate) {
            portContainerOut.ports[0].connectedPorts[0].parentNode.Run(_nodeGraph);
        } else {
            portContainerOut.ports[1].connectedPorts[0].parentNode.Run(_nodeGraph);
        }
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeIf>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeIf)newNode).copyA = portContainerIn.ports[1].GetValue();

        return newNode;
    }
}
