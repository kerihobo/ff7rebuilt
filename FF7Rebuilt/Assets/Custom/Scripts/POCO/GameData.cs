﻿using FFVII.Database;
using System;
using UnityEngine;

[Serializable]
public class GameData {
    public float enemyLureValue = 16 / 16f;
    [Range(0, 32)]
    public int chocoboLureRating = 0;

    [Range(0, 359999)]
    public int playTime;
    [Range(0, 4294967295)]
    public int gil;
    public string location = "Platform";
    [Range(0, 65535)]
    public int battles;
    [Range(0, 65535)]
    public int escapes;
    [Range(0, 10000)]
    public int gp;
    [Range(0, 65000)]
    public int bp;
    [Range(0, 100)]
    public int snowBoardScore;
    public int submarineScore;
    public int speedSquareScore;
    public int motorCycleScore;
    public PHS phs = new PHS();
    public Inventory inventory = new Inventory();
    public Config config = new Config();

    public bool IsMateriaUnlocked => true;
    public bool IsPHSUnlocked => true;
    
    /// <summary>
    /// Only to be used for New Games. It is meant to populate starting data.
    /// </summary>
    public void Initialize() {
        inventory.Initialize();
        phs.Initialize();
        config.Initialize();
        UpdateLocation();
        
        gil = DBResources.GetInitial.gil;
    }

    public void UpdateLocation() {
        location = UnityEngine.Object.FindFirstObjectByType<SceneInfo>().locationName;
    }

    // This whole method is possibly garbage. Keeping it for the sharp reminder on the last 2 lines.
    //public void SetEnemyLureValue() {
    //    int enemyLure = 0;
    //    int enemyAway = 0;
    //    foreach (CharacterTypes.Player p in phs.arrCurrent) {
    //        foreach (Materia m in p.materiaWeapon) {
    //            // TODO: Should be 7 or 14 for Enemy Lure.
    //            // TODO: Should be -7 or -14 for Enemy Away.
    //            if (m.subtype == 9) {
    //                enemyLure += m.GetBonusValue();
    //            }

    //            if (m.subtype == 10) {
    //                enemyAway += m.GetBonusValue();
    //            }
    //        }

    //        foreach (Materia m in p.materiaArmor) {
    //            if (m.subtype == 9) {
    //                enemyLure += m.GetBonusValue();
    //            }

    //            if (m.subtype == 10) {
    //                enemyAway += m.GetBonusValue();
    //            }
    //        }
    //    }

    //    enemyLureValue = (16 / 16f) + (enemyLure / 16f) - (enemyAway / 16f);
    //    enemyLureValue = Mathf.Clamp(enemyLureValue, (2 / 16f), (32 / 16f));
    //}

    // This also might be garbage. Keeping it for the useful 2 lines at the end.
    //public void SetChocoboLureValue() {
    //    int chocoboLure = 0;
    //    foreach (CharacterTypes.Player p in phs.arrCurrent) {
    //        foreach (SelectionMateria sm in p.defaultMateriaWeapon) {
    //            if (sm.GetSelectedMateria().independentManager.type == IndependentManager.Type.ChocoboLure) {
    //                chocoboLure += sm.GetSelectedMateria().GetBonusValue();
    //            }
    //        }
    //    }

    //    chocoboLureRating = (int)(chocoboLure / 32f);
    //    chocoboLureRating = (int)Mathf.Clamp(enemyLureValue, 0, 1.0f);
    //}
}
