﻿using UnityEngine;

public class NodeClamp : Node {
    private dynamic copyA;
    private dynamic copyB;
    private dynamic copyC;

    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(165, 95);
        name = "Clamp";

        AddPortIn<PortInt>("In", Port.Capacity.SINGLE);
        AddPortIn<PortInt>("Min", Port.Capacity.SINGLE);
        AddPortIn<PortInt>("Max", Port.Capacity.SINGLE);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);

        portContainerIn.ports[0].SetValue(copyA);
        portContainerIn.ports[1].SetValue(copyB);
        portContainerIn.ports[2].SetValue(copyC);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(110, 175);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();
        int c = portContainerIn.ports[2].GetValue();

        return Mathf.Clamp(a, b, c);
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeClamp>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeClamp)newNode).copyA = portContainerIn.ports[0].GetValue();
        ((NodeClamp)newNode).copyB = portContainerIn.ports[1].GetValue();
        ((NodeClamp)newNode).copyC = portContainerIn.ports[1].GetValue();

        return newNode;
    }
}