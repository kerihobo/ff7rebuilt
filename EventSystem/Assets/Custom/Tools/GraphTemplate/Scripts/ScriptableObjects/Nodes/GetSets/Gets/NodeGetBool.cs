﻿using UnityEngine;

public class NodeGetBool : NodeGet {
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        minMaxThresholds = new Vector2(140, 140);
        size = new Vector2(minMaxThresholds.y, 50);

        AddPortIn<PortFieldLabel>();
        AddPortOut<PortBool>("Value", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        bool value = ((FieldBool)field).value;
        
        return value;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeGetBool>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeGetBool)newNode).field = field;

        field.nodes.Add((NodeGetBool)newNode);

        return newNode;
    }
}
