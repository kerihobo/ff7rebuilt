﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NodeGraph {
    public NodeGraph() {
        nodeEntry = new NodeEntry(this, Vector2.zero);
        InitializeBlackboard();
        InitializeSearchWindow();
    }

    [SerializeReference]
    public List<Node> nodes = new List<Node>();
    [SerializeReference, HideInInspector]
    public NodeEntry nodeEntry;
    [SerializeReference, HideInInspector]
    public Blackboard blackboard;
    [SerializeReference, HideInInspector]
    public SearchWindow searchWindow;
    public bool isShowGrid = true;

    public Vector2 nodeSpawnPosition { get; set; }

    protected virtual void InitializeBlackboard() {
        blackboard = new Blackboard(this);
    }

    protected virtual void InitializeSearchWindow() {
        searchWindow = new SearchWindow(this);
    }

    public void Render(Event _e, StyleSet _styleSet, ref Vector2 _pan, Rect _windowRect) {
        foreach (Node node in nodes) {
            node.RenderEdges();
        }
        
        foreach (Node node in nodes) {
            node.RenderNode(_pan, _styleSet);
        }

        GUILayout.BeginHorizontal();
            blackboard.Render(_e, _styleSet, _pan);

            GUILayout.BeginVertical(_styleSet.buttonContainer);
                if (GUILayout.Button("Save")) {
                    Save();
                }

                if (GUILayout.Button((isShowGrid ? "Hide " : "Show ") + "Grid")) {
                    isShowGrid = !isShowGrid;
                }

                if (GUILayout.Button("Reset View")) {
                    _pan = _windowRect.size / 3;
                }
            GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        searchWindow?.Render(_e, _styleSet);
    }

    public void DeleteNodes(List<Node> _selectedNodes) {
        for (int i = 0; i < _selectedNodes.Count; i++) {
            if (_selectedNodes[i].GetType() == typeof(NodeEntry)) continue;

            DeleteNode(_selectedNodes[i]);
        }
    }

    public void DeleteNode(Node _targetNode) {
        if (_targetNode == null) return;

        _targetNode.DisconnectPorts();

        nodes.Remove(_targetNode);

        // Do a little extra cleanup if we're deleting a Get/Set node.
        if (_targetNode.GetType().IsSubclassOf(typeof(NodeGetSet))) {
            NodeGetSet getset = (NodeGetSet)_targetNode;
            getset?.field.RemoveNode(getset);
        }

        Save();
    }

    public virtual void Save() {
        //EditorUtility.SetDirty(this);
        //AssetDatabase.SaveAssets();
    }

    public void Run() {
        nodeEntry.Run(this);
    }
}