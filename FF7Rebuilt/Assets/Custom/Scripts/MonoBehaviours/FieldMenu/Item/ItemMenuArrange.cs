using UnityEngine;
using System.Linq;
using InventoryTypes;
using TMPro;
using UnityEngine.UI;

public class ItemMenuArrange : MenuControllable {
    public enum MethodID {
        Custom
    ,   Field
    ,   Battle
    ,   Throw
    ,   Type
    ,   Name
    ,   Most
    ,   Least
    };

    public FingerCursorRight cursor;

    public RectTransform rct { get; private set; }

    [SerializeField] private RectTransform optionsParent;
    /// <summary>
    /// Custom: 0<br/>
    /// Field: 1<br/>
    /// Battle: 2<br/>
    /// Throw: 3<br/>
    /// Type: 4<br/>
    /// Name: 5<br/>
    /// Most: 6<br/>
    /// Least: 7<br/>
    /// </summary>
    private TextMeshProUGUI[] arrOptions;
    private int selectedIndex;
    private ItemMenuOptionList itemMenuOptionList;

    private Inventory GetInventory() => GameManager.Instance.gameData.inventory;
    
    public void Initialize(ItemMenuOptionList _itemMenuOptionList) {
        itemMenuOptionList = _itemMenuOptionList;
        
        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<TextMeshProUGUI>();

        Disable();

        cursor.Initialize(arrOptions[0].rectTransform);
    }

    public void Open() {
        gameObject.SetActive(true);
        cursor.Show();
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);
    }

    private void Sort(MethodID type) {
        switch (type) {
            case MethodID.Custom:
                OpenItemListToCustomize();
                break;
            case MethodID.Field:
                GetInventory().arrItems = GetInventory().arrItems.OrderBy(x => x == null).ThenBy(x => x?.fieldApplication == ItemFieldApplication.NONE).ToArray();
                break;
            case MethodID.Battle:
                GetInventory().arrItems = GetInventory().arrItems.OrderBy(x => x == null).ThenBy(x => !x?.isBattleItem).ToArray();
                break;
            case MethodID.Throw:
                GetInventory().arrItems = GetInventory().arrItems.OrderBy(x => x == null).ThenBy(x => !(x is Weapon)).ThenBy(x => !(x as Weapon)?.isThrowable).ThenByDescending(x => x?.id).ToArray();
                break;
            case MethodID.Type:
                GetInventory().arrItems = GetInventory().arrItems.OrderBy(x => x == null).ThenBy(x => x?.id).ToArray();
                break;
            case MethodID.Name:
                GetInventory().arrItems = GetInventory().arrItems.OrderBy(x => x == null).ThenBy(x => x?.name).ToArray();
                break;
            case MethodID.Most:
                GetInventory().arrItems = GetInventory().arrItems.OrderBy(x => x == null).ThenByDescending(x => x?.quantity).ToArray();
                break;
            case MethodID.Least:
                GetInventory().arrItems = GetInventory().arrItems.OrderBy(x => x == null).ThenBy(x => x?.quantity).ToArray();
                break;
            default:
                break;
        }

        if (type != MethodID.Custom) {
            // Close the arrange menu.
            itemMenuOptionList.itemMenuUse.itemList.Refresh();
            Close();
        }

        LogSortResult(type);

        void LogSortResult(MethodID type) {
            string s = type.ToString() + "\n";
            for (int i = 0; i < GetInventory().arrItems.Length; i++) {
                Collectible collectible = GetInventory().arrItems[i];
                if (collectible != null) {
                    s += collectible.id + ": " + collectible.name + " (" + collectible.quantity + ")" + "\n";

                } else {
                    s += "-\n";

                }
            }

            Debug.Log(s);
        }

        void OpenItemListToCustomize() {
            Close();
            itemMenuOptionList.cursor.SetFlickerState(true);
            itemMenuOptionList.itemMenuUse.itemList.OpenViaArrange();
        }
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void ResetSelection() {
        selectedIndex = 0;

        SetCursor();
    }

    private void Close() {
        Disable();
        itemMenuOptionList.ExitSubMenu();
    }

    private void Disable() {
        cursor.Hide();
        gameObject.SetActive(false);
    }

    #region Override methods.
    public override void GetInputConfirm() { }
    public override void GetInputCancelPressed() { }
    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputSquarePressed() { }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputConfirmPressed() {
        Sort((MethodID)selectedIndex);
    }

    public override void GetInputCancel() {
        Close();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        int v = -_v;
        selectedIndex = (selectedIndex + v) % arrOptions.Length;

        if (selectedIndex < 0) {
            selectedIndex = arrOptions.Length - 1;
        }

        SetCursor();
    }
#endregion




    // =====================================================================================
    // I tried implementing this but couldn't understand how it's supposed to work.
    // =====================================================================================

    // Mostly taken from: https://github.com/p3k22/FF7-Csharp/blob/main/FF7%20Arrange%20Inventory.cs#L170

    // player.inventory.items = Your inventory list with at minimum the following fields:
    // SortTag, Quantity, DatabaseID, Name
    // StaticExtensions.ReturnAllItemsAndEquipmentNames should be an array of every item name in order It/Wp/Ar/Ac


    //private List<string> nameOrderList;

    //private MethodID methodID;

    //public void Sort(int maxSlotCount, MethodID methodID) {
    //    this.methodID = methodID;

    //    nameOrderList = GetInventory().arrItems.Select(x => x != null ? x.name : "").ToList();
    //    //nameOrderList = StaticExtensions.ReturnAllItemsAndEquipmentNames.ToList();
    //    nameOrderList.Sort();

    //    int[] lastTopSlot = new int[64];
    //    int[] lastBottomSlot = new int[64];
    //    lastTopSlot[0] = 0;
    //    lastBottomSlot[0] = maxSlotCount - 1;

    //    int iterator = 1;

    //    // Loop 1
    //    while (1 != 0) {
    //        if (iterator-- <= 0) {
    //            break;
    //        }

    //        int topSlot = lastTopSlot[iterator];
    //        int topSlotPlusOne = topSlot + 1;
    //        int bottomSlot = lastBottomSlot[iterator];
    //        maxSlotCount = bottomSlot;

    //        // Loop 2
    //        while (1 != 0) {
    //            if (topSlotPlusOne >= bottomSlot) {
    //                break;
    //            }

    //            // Loop 3
    //            while (topSlotPlusOne < bottomSlot) {
    //                if (RequiresSorting(topSlot, topSlotPlusOne)) {
    //                    break;
    //                }

    //                topSlotPlusOne++;
    //            }

    //            // Loop 4
    //            while (1 != 0) {
    //                if (topSlotPlusOne > bottomSlot) {
    //                    break;
    //                }

    //                if (RequiresSorting(bottomSlot, topSlot)) {
    //                    break;
    //                }

    //                bottomSlot--;
    //            }

    //            if (topSlotPlusOne < bottomSlot) {
    //                SwapSlots(topSlotPlusOne, bottomSlot);

    //                topSlotPlusOne++;
    //                bottomSlot--;
    //            }
    //        }

    //        // Restart Loop
    //        if (RequiresSorting(bottomSlot, topSlot)) {
    //            SwapSlots(bottomSlot, topSlot);
    //        }

    //        if (bottomSlot > topSlot) {
    //            bottomSlot--;
    //        }

    //        if (bottomSlot > topSlot && maxSlotCount > topSlotPlusOne
    //            && bottomSlot - topSlot < maxSlotCount - topSlotPlusOne) {
    //            SwapIndex(ref bottomSlot, ref maxSlotCount);
    //            SwapIndex(ref topSlot, ref topSlotPlusOne);
    //        }

    //        if (bottomSlot > topSlot) {
    //            lastTopSlot[iterator] = topSlot;
    //            lastBottomSlot[iterator++] = bottomSlot;
    //        }

    //        if (maxSlotCount > topSlotPlusOne) {
    //            lastTopSlot[iterator] = topSlotPlusOne;
    //            lastBottomSlot[iterator++] = maxSlotCount;
    //        }

    //        if (iterator >= 64) {
    //            break;
    //        }
    //    }
    //}



    //private bool RequiresSorting(int _a, int _b) {
    //    switch (methodID) {
    //        case MethodID.Field:
    //        case MethodID.Battle:
    //        case MethodID.Throw:
    //            return SortByTag(_a, _b, (int)methodID) > 0;
    //        case MethodID.Type:
    //            return SortByType(_a, _b) > 0;
    //        case MethodID.Name:
    //            return SortByName(_a, _b) > 0;
    //        case MethodID.Most:
    //            return SortByMost(_a, _b) > 0;
    //        case MethodID.Least:
    //            return SortByLeast(_a, _b) > 0;
    //    }

    //    return false;
    //}

    //private int SortByTag(int _a, int _b, int tagID) {
    //    ItemSortTag tagToMatch = (ItemSortTag)tagID;
    //    Collectible[] inventory = GetInventory().arrItems;
    //    Debug.Log(_a);
    //    Debug.Log(_b);
    //    int s1Count = inventory[_a].GetSortTag().HasFlag(tagToMatch) ? 2 - inventory[_a].quantity : 2000;
    //    int s2Count = inventory[_b].GetSortTag().HasFlag(tagToMatch) ? 2 - inventory[_b].quantity : 2000;
    //    int sign = s2Count - s1Count;

    //    return sign < 0 ? -1 : sign > 0 ? 1 : 0;
    //}

    //private int SortByType(int _a, int _b) {
    //    //var inventory = player.inventory.items;
    //    Collectible[] inventory = GetInventory().arrItems;
    //    int s1Count = string.IsNullOrEmpty(inventory[_a].name) ? 2000 : inventory[_a].id;
    //    int s2Count = string.IsNullOrEmpty(inventory[_b].name) ? 2000 : inventory[_b].id;
    //    int sign = s2Count - s1Count;

    //    return sign < 0 ? -1 : sign > 0 ? 1 : 0;
    //}


    //private int SortByName(int _a, int _b) {
    //    //var inventory = player.inventory.items;
    //    Collectible[] inventory = GetInventory().arrItems;
    //    int s1Count = string.IsNullOrEmpty(inventory[_a].name) ? 2000 : nameOrderList.IndexOf(inventory[_a].name);
    //    int s2Count = string.IsNullOrEmpty(inventory[_b].name) ? 2000 : nameOrderList.IndexOf(inventory[_b].name);
    //    int sign = s2Count - s1Count;

    //    return sign < 0 ? -1 : sign > 0 ? 1 : 0;
    //}

    //private int SortByMost(int _a, int _b) {
    //    //var inventory = player.inventory.items;
    //    Collectible[] inventory = GetInventory().arrItems;
    //    int s1Count = string.IsNullOrEmpty(inventory[_a].name) ? 0 : inventory[_a].quantity;
    //    int s2Count = string.IsNullOrEmpty(inventory[_b].name) ? 0 : inventory[_b].quantity;
    //    int sign = s1Count - s2Count;

    //    return sign < 0 ? -1 : sign > 0 ? 1 : 0;
    //}

    //private int SortByLeast(int _a, int _b) {
    //    //var inventory = player.inventory.items;
    //    Collectible[] inventory = GetInventory().arrItems;
    //    int s1Count = string.IsNullOrEmpty(inventory[_a].name) ? 2000 : inventory[_a].quantity;
    //    int s2Count = string.IsNullOrEmpty(inventory[_b].name) ? 2000 : inventory[_b].quantity;
    //    int sign = s2Count - s1Count;

    //    return sign < 0 ? -1 : sign > 0 ? 1 : 0;
    //}

    //private void SwapSlots(int _a, int _b) {
    //    Collectible[] inventory = GetInventory().arrItems;
    //    //Collectible item1 = inventory[_a];
    //    //Collectible item2 = inventory[_b];

    //    //inventory[_b].SetAsCopy(item1);
    //    //inventory[_a].SetAsCopy(item2);

    //    (inventory[_a], inventory[_b]) = (inventory[_b], inventory[_a]);
    //}

    //private void SwapIndex(ref int slot1Index, ref int slot2Index) {
    //    int t = slot1Index;
    //    slot1Index = slot2Index;
    //    slot2Index = t;
    //}
}
