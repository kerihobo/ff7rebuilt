﻿using UnityEngine;

public class Test : MonoBehaviour {
    public SelectionData enemy;

    private void Awake() {
        enemy.GetSelectedEnemy().aiGraph.Run();
    }

    private void Update() {
        if (Input.GetButtonDown("Submit")) {
            enemy.GetSelectedEnemy().aiGraph.Run();
        }
    }
}