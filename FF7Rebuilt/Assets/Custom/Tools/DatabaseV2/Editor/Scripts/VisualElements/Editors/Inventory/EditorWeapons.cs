using FFVII.Database;
using InventoryTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorWeapons : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Inventory/editor-weapons.uxml";
    protected override Database database { get; set; } = DBResources.GetWeapons;
    protected override Action<VisualElement, int> bindItem { get; set; } = (e, i) => e.Q<Label>().text = DBResources.GetWeaponsList(WeaponType.SWORD)[i].name;
    protected override IList itemSource { get; set; } = DBResources.GetWeaponsList(WeaponType.SWORD);

    private WeaponType selectedWeaponType;

    public EditorWeapons() : base() {
        selectedWeaponType = WeaponType.SWORD;

        SetSelected(ve.Q<Button>("btn-type-select-sword"));
        OnSelectionChange(DBResources.GetWeaponsList(WeaponType.SWORD));
        //Refresh();
        //GetNewTarget(((List<Weapon>)itemSource).FirstOrDefault());
    }

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Weapon)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        BindTypeSelectionButtons();

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<TextField>("txt-description").SetValue(((Weapon)target).description).RegisterValueChangedCallback(e => {
            ((Weapon)target).description = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<IntegerField>("int-sale-cost").SetValue(((Weapon)target).saleCost).RegisterValueChangedCallback(e => {
            ((Weapon)target).saleCost = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<ObjectField>("t-weapon-prefab").SetTransform(((Weapon)target).prefabWeapon).RegisterValueChangedCallback(e => {
            ((Weapon)target).prefabWeapon = (Transform)e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<ObjectField>("ps-effect-prefab").SetGameObject(((Weapon)target).prefabEffectOverride).RegisterValueChangedCallback(e => {
            ((Weapon)target).prefabEffectOverride = (GameObject)e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<Toggle>("tgl-ranged").SetValue(((Weapon)target).isRanged).RegisterValueChangedCallback(e => {
            ((Weapon)target).isRanged = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<Toggle>("tgl-throwable").SetValue(((Weapon)target).isThrowable).RegisterValueChangedCallback(e => {
            ((Weapon)target).isThrowable = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<IntegerField>("int-critical-percent").SetValue(((Weapon)target).criticalPercent).RegisterValueChangedCallback(e => {
            ((Weapon)target).criticalPercent = e.newValue;
            EditorUtility.SetDirty(database);
        });

        BindPrimaryStats();
        BindDerivedStats();

        ve.Q<MateriaSlotEditor>().Initialize((Equipment)target, database);

        BindAttackElements();

        void BindTypeSelectionButtons() {
            ve.Q<Button>("btn-type-select-glove").RegisterCallback<ClickEvent>((e)      => ChangeCategory(WeaponType.GLOVE, e));
            ve.Q<Button>("btn-type-select-gun").RegisterCallback<ClickEvent>((e)        => ChangeCategory(WeaponType.GUN, e));
            ve.Q<Button>("btn-type-select-gun-arm").RegisterCallback<ClickEvent>((e)    => ChangeCategory(WeaponType.GUN_ARM, e));
            ve.Q<Button>("btn-type-select-headdress").RegisterCallback<ClickEvent>((e)  => ChangeCategory(WeaponType.HEADDRESS, e));
            ve.Q<Button>("btn-type-select-megaphone").RegisterCallback<ClickEvent>((e)  => ChangeCategory(WeaponType.MEGAPHONE, e));
            ve.Q<Button>("btn-type-select-shuriken").RegisterCallback<ClickEvent>((e)   => ChangeCategory(WeaponType.SHURIKEN, e));
            ve.Q<Button>("btn-type-select-spear").RegisterCallback<ClickEvent>((e)      => ChangeCategory(WeaponType.SPEAR, e));
            ve.Q<Button>("btn-type-select-staff").RegisterCallback<ClickEvent>((e)      => ChangeCategory(WeaponType.STAFF, e));
            ve.Q<Button>("btn-type-select-sword").RegisterCallback<ClickEvent>((e)      => ChangeCategory(WeaponType.SWORD, e));
            ve.Q<Button>("btn-type-select-long-sword").RegisterCallback<ClickEvent>((e) => ChangeCategory(WeaponType.LONG_SWORD, e));

            void ChangeCategory(WeaponType _weaponType, ClickEvent e) {
                selectedWeaponType = _weaponType;

                RenewList();
                SetSelected((Button)e.currentTarget);
                OnSelectionChange(DBResources.GetWeaponsList(_weaponType));

                Refresh();

                void RenewList() {
                    itemSource = DBResources.GetWeaponsList(_weaponType);

                    bindItem = (e, i) => e.Q<Label>().text = DBResources.GetWeaponsList(_weaponType)[i].name;

                    listView.bindItem = null;
                    listView.itemsSource = itemSource;
                    listView.bindItem = bindItem;
                    listView.SetSelection(0);
                }
            }
        }

        void BindPrimaryStats() {
            ve.Q<IntegerField>("int-vitality").SetValue(((Weapon)target).equipEffects.primary.vitality).RegisterValueChangedCallback(e => {
                ((Weapon)target).equipEffects.primary.vitality = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-spirit").SetValue(((Weapon)target).equipEffects.primary.spirit).RegisterValueChangedCallback(e => {
                ((Weapon)target).equipEffects.primary.spirit = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-dexterity").SetValue(((Weapon)target).equipEffects.primary.dexterity).RegisterValueChangedCallback(e => {
                ((Weapon)target).equipEffects.primary.dexterity = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-magic").SetValue(((Weapon)target).equipEffects.primary.magic).RegisterValueChangedCallback(e => {
                ((Weapon)target).equipEffects.primary.magic = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindDerivedStats() {
            ve.Q<IntegerField>("int-attack").SetValue(((Weapon)target).equipEffects.derived.attack).RegisterValueChangedCallback(e => {
                ((Weapon)target).equipEffects.derived.attack = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-attack-percent").SetValue(((Weapon)target).equipEffects.derived.attackPercent).RegisterValueChangedCallback(e => {
                ((Weapon)target).equipEffects.derived.attackPercent = e.newValue;
                EditorUtility.SetDirty(database);
            });

            // TODO: Check that this derived stat is correct.
            //ve.Q<IntegerField>("int-magic").SetValue(((Weapon)target).equipEffects.derived.magicAttack).RegisterValueChangedCallback(e => {
            //    ((Weapon)target).equipEffects.derived.magicAttack = e.newValue;
            //    EditorUtility.SetDirty(database);
            //});
        }
        
        void BindAttackElements() {
            VisualElement attackElements = ve.Q<VisualElement>("ve-attack-elements");

            attackElements.Q<Button>("btn-10th").SetBoolClass(((Weapon)target).elements._10th.isActive).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Weapon)target).elements._10th.isActive);
                EditorUtility.SetDirty(database);
            });

            attackElements.Q<Button>("btn-punch").SetBoolClass(((Weapon)target).elements.punch.isActive).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Weapon)target).elements.punch.isActive);
                EditorUtility.SetDirty(database);
            });

            attackElements.Q<Button>("btn-cut").SetBoolClass(((Weapon)target).elements.cut.isActive).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Weapon)target).elements.cut.isActive);
                EditorUtility.SetDirty(database);
            });

            attackElements.Q<Button>("btn-hit").SetBoolClass(((Weapon)target).elements.hit.isActive).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Weapon)target).elements.hit.isActive);
                EditorUtility.SetDirty(database);
            });

            attackElements.Q<Button>("btn-shoot").SetBoolClass(((Weapon)target).elements.shoot.isActive).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Weapon)target).elements.shoot.isActive);
                EditorUtility.SetDirty(database);
            });

            attackElements.Q<Button>("btn-shout").SetBoolClass(((Weapon)target).elements.shout.isActive).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Weapon)target).elements.shout.isActive);
                EditorUtility.SetDirty(database);
            });
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = target.name;
        ve.Q<TextField>("txt-description").value = ((Weapon)target).description;
        ve.Q<IntegerField>("int-sale-cost").value = ((Weapon)target).saleCost;
        ve.Q<ObjectField>("t-weapon-prefab").value = ((Weapon)target).prefabWeapon;
        ve.Q<ObjectField>("ps-effect-prefab").value = ((Weapon)target).prefabEffectOverride;
        ve.Q<Toggle>("tgl-ranged").value = ((Weapon)target).isRanged;
        ve.Q<Toggle>("tgl-throwable").value = ((Weapon)target).isThrowable;
        ve.Q<IntegerField>("int-critical-percent").value = ((Weapon)target).criticalPercent;

        EquipEffect equipEffects = ((Weapon)target).equipEffects;

        RefreshPrimaryStats();
        RefreshDerivedStats();
        
        ve.Q<MateriaSlotEditor>().Refresh((Equipment)target);
        
        RefreshAttackElements();

        void RefreshPrimaryStats() {
            Debug.Log(equipEffects);
            Debug.Log(equipEffects.primary);
            ve.Q<IntegerField>("int-vitality").value = equipEffects.primary.vitality;
            ve.Q<IntegerField>("int-spirit").value = equipEffects.primary.spirit;
            ve.Q<IntegerField>("int-dexterity").value = equipEffects.primary.dexterity;
            ve.Q<IntegerField>("int-magic").value = equipEffects.primary.magic;
        }

        void RefreshDerivedStats() {
            ve.Q<IntegerField>("int-attack").value = equipEffects.derived.attack;
            ve.Q<IntegerField>("int-attack-percent").value = equipEffects.derived.attackPercent;
        }

        void RefreshAttackElements() {
            var attackElements = ve.Q<VisualElement>("ve-attack-elements");

            attackElements.Q<Button>("btn-10th")       .SetBoolClass(((Weapon)target).elements._10th.isActive);
            attackElements.Q<Button>("btn-punch")      .SetBoolClass(((Weapon)target).elements.punch.isActive);
            attackElements.Q<Button>("btn-cut")        .SetBoolClass(((Weapon)target).elements.cut.isActive);
            attackElements.Q<Button>("btn-hit")        .SetBoolClass(((Weapon)target).elements.hit.isActive);
            attackElements.Q<Button>("btn-shoot")      .SetBoolClass(((Weapon)target).elements.shoot.isActive);
            attackElements.Q<Button>("btn-shout")      .SetBoolClass(((Weapon)target).elements.shout.isActive);
        }
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Weapon());

        base.Insert();
    }

    protected override void Add() {
        Weapon newWeapon = new Weapon();
        newWeapon.type = selectedWeaponType;

        itemSource.Add(newWeapon);

        base.Add();
    }

    void SetSelected(VisualElement _ve) {
        VisualElement typeList = ve.Q<VisualElement>("type-list");
        typeList.Query<Button>().ForEach(x => x.RemoveFromClassList("selected"));
        _ve.AddToClassList("selected");
    }
}
