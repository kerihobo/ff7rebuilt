using CharacterTypes;
using TMPro;
using UnityEngine;

public class MateriaMenuExchangeOptionList : MenuControllable {
    public TextMeshProUGUI[] arrOptions { get; set; }

    [SerializeField] private TextMeshProUGUI txtName;
    private MateriaMenuExchange exchange;
    private MateriaMenuExchangeCharacterContainer characterContainer;
    private int selectedIndex;
    private Player assignedPlayer;
    
    private FingerCursorRight cursor => exchange.cursorOptions;
    private FingerCursorRight cursorFlicker => exchange.cursorSlotsFlicker;

    public void Initialize(MateriaMenuExchange _exchange, MateriaMenuExchangeCharacterContainer _characterContainer) {
        exchange = _exchange;
        characterContainer = _characterContainer;
        
        arrOptions = GetComponentsInChildren<TextMeshProUGUI>();
    }

    public void SetCursor() {
        cursor.SetCursorPosition(arrOptions[selectedIndex].rectTransform);
    }

    public void Open() {
        SetControllable();
        SetCursor();
        cursor.Show();
        exchange.RefreshMateriaInfo(null);
    }

    public void Disable() {
        cursor.Hide();
    }

    public void SetSelectedIndex(int optionsSelectedIndex) {
        selectedIndex = optionsSelectedIndex;
    }

    // This is not necessarily what happens.
    // At least it should ONLY happen this way when browsing to slot.
    // Probably need a new name for it, not Close, maybe GoToSlots();
    private void Close() {
        Disable();
        characterContainer.slotsExchange.SetEquipmentSelected(selectedIndex - 1);
        characterContainer.slotsExchange.Open();
    }

    // Actually this should never happen. We only expect slot0 to be the initial selection
    // on a freshly loaded game/save.
    public void ResetSelection() {
        selectedIndex = 0;
    }

    public void SetPlayer(Player _player) {
        assignedPlayer = _player;
        txtName.text = assignedPlayer.name;
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputConfirm() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputPage(int pageDirection, bool isPageXPressed) {}
    
    public override void GetInputConfirmPressed() {
        bool isExchangePlayer = exchange.playerA != null;
        bool isExchangeEquipment = exchange.arrMateriaA != null;

        if (isExchangePlayer) {
            exchange.Exchange2Players(assignedPlayer);
            return;
        } else if (isExchangeEquipment) {
            switch (selectedIndex) {
                case 1: // Weapon
                    exchange.Exchange2Equipment(assignedPlayer, assignedPlayer.weapon, assignedPlayer.materiaWeapon);
                    break;
                case 2: // Armor
                    exchange.Exchange2Equipment(assignedPlayer, assignedPlayer.armor, assignedPlayer.materiaArmor);
                    break;
            }

            return;
        }

        Debug.Log("HERE?");
        switch (selectedIndex) {
            case 0: // Character
                exchange.playerA = assignedPlayer;
                break;
            case 1: // Weapon
                exchange.arrMateriaA = assignedPlayer.materiaWeapon;
                exchange.equipmentA = assignedPlayer.weapon;
                break;
            case 2: // Armor
                exchange.arrMateriaA = assignedPlayer.materiaArmor;
                exchange.equipmentA = assignedPlayer.armor;
                break;
        }

        exchange.SetCursorFlicker(cursorFlicker, arrOptions[selectedIndex].rectTransform);
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (FieldMenu.Instance.coolDownTraverse > 0) return;

        bool isPressUp = _v > 0;
        bool isPressDown = _v < 0;
        bool hasRoomAbove = exchange.lowBound + exchange.selectedIndex > 0;
        bool hasRoomBelow = exchange.lowBound + exchange.selectedIndex < exchange.arrViewableCharacters.Length - 1;
        bool isLowest = selectedIndex == GetLowestAllowedValue();
        bool isHighest = selectedIndex == GetHighestAllowedValue();
        bool isScrollUp = isLowest && isPressUp && hasRoomAbove;
        bool isScrollDown = isHighest && isPressDown && hasRoomBelow;
        if (isScrollUp || isScrollDown) {
            // Need to write method exchange.NavigateCharactersOptions(_v);
            exchange.NavigateCharactersOptions(_v);

            return;
        }

        selectedIndex -= _v;
        selectedIndex = Mathf.Clamp(selectedIndex, GetLowestAllowedValue(), GetHighestAllowedValue());

        SetCursor();

        int GetHighestAllowedValue() {
            bool isExchangePlayer = exchange.playerA != null;
            return isExchangePlayer ? 0 : arrOptions.Length - 1;
        }

        int GetLowestAllowedValue() {
            bool isExchangeEquipment = exchange.arrMateriaA != null;
            return isExchangeEquipment ? 1 : 0;
        }
    }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        bool isRight = _h > 0;
        bool isEquipmentHovered = selectedIndex > 0;
        bool isMidExchange = exchange.arrMateriaA != null;

        if (isRight && isEquipmentHovered && !isMidExchange) {
            Close();
        }
    }

    public override void GetInputCancelPressed() {
        bool hasPlayer = exchange.playerA != null;
        bool hasEquipment = exchange.arrMateriaA != null;
        
        if (hasEquipment || hasPlayer) {
            exchange.CloseCurrentExchange();
        } else {
            exchange.Close();
        }
    }
#endregion
}
