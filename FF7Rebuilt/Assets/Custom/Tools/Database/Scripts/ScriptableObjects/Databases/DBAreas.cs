﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Areas", menuName = "Kerihobo/Database/Areas")]
    public class DBAreas : Database {
        public List<Area> field;
        public List<Area> world;

        public AreaType selectedMenuType { get; private set; }

        public List<Area> GetSelectedAreaList(AreaType _type) => _type switch {
            AreaType.Field => field,
            AreaType.World => world,
            _ => null
        };
    }
}
