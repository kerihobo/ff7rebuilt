﻿using CharacterTypes;
using FFVII.Database;
using InventoryTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SupportManager {
    public static void RecalculateSupportEffects(Player _player) {
        List<ElementalAffinityManager.Affinity> elementAttack = _player.elementAttack.GetAsList();
        List<ElementalAffinityManager.Affinity> elementHalve = _player.elementHalve.GetAsList();
        List<ElementalAffinityManager.Affinity> elementInvalid = _player.elementInvalid.GetAsList();
        List<ElementalAffinityManager.Affinity> elementAbsorb = _player.elementAbsorb.GetAsList();

        DBAbilities.CommandList cmd = DBResources.GetAbilities.commandList;
        List<Ability> counterCommands = new List<Ability> {
            cmd.cmdSteal
        ,   cmd.cmdSense
        ,   cmd.cmdThrow
        ,   cmd.cmdMorph
        ,   cmd.cmdDeathblow
        ,   cmd.cmdManipulate
        ,   cmd.cmdMime
        };
        List<Ability> invalidCommands = new List<Ability>() {
            cmd.cmdLimit
        ,   cmd.cmdMagic
        ,   cmd.cmdWMagic
        ,   cmd.cmdSummon
        ,   cmd.cmdWSummon
        ,   cmd.cmdEnemySkill
        };
        List<Ability> masterCommands = cmd.GetCommandsAsList.Except(invalidCommands).ToList();

        ProcessSupportPairs(_player.weapon, _player.materiaWeapon);
        ProcessSupportPairs(_player.armor, _player.materiaArmor);

        void ProcessSupportPairs(Equipment _equipment, Materia[] _materiaInEquipment) {
            if (_equipment == null) return;

            bool isWeapon = _equipment is Weapon;
            int linkedSlots = _equipment.links * 2;
            HashSet<int> skipSubtypes = new HashSet<int> { 2, 3, 4, 5 };

            for (int i = 0; i < linkedSlots; i++) {
                Materia supportMateria = _materiaInEquipment[i];
                if (supportMateria == null
                ||  supportMateria.type != MateriaType.Support
                ||  skipSubtypes.Contains(supportMateria.subtype))
                    continue;

                int increment = (i % 2 == 0) ? 1 : -1;

                Materia pairedMateria = _materiaInEquipment[i + increment];
                if (pairedMateria == null) continue;

                bool isMagicMateria = pairedMateria.type == MateriaType.Magic;
                bool isSummonMateria = pairedMateria.type == MateriaType.Summon;
                bool isMagicOrSummonMateria = isMagicMateria || isSummonMateria;
                bool isMaster = pairedMateria.IsMaster;
                int level = pairedMateria.GetLevel();
                int bonusValue = supportMateria.GetBonusValue();
                // Maybe cache the bonus value too.

                switch (supportMateria.subtype) {
                    case 1:  // All
                        RecalculateAll(_player, bonusValue, pairedMateria, isMagicMateria, isMaster);
                        break;
                    case 6:  // MP Turbo
                        RecalculateMPTurbo(_player, bonusValue, pairedMateria, isMagicOrSummonMateria);
                        break;
                    case 7:  // MP Absorb
                    case 8:  // HP Absorb
                    case 9:  // Added Cut
                    case 10: // Steal as well
                        RecalculatePostAttack(_player, counterCommands, masterCommands, supportMateria, pairedMateria, isMagicOrSummonMateria, isMaster, level);
                        break;
                    case 11:  // Elemental
                        RecalculateElemental(elementAttack, elementHalve, elementInvalid, elementAbsorb, isWeapon, bonusValue, pairedMateria);
                        break;
                    case 12:  // Added Effect
                        RecalculateAddedEffect(_player, isWeapon, pairedMateria);
                        break;
                    case 13:  // Quadra Magic
                        RecalculateQuadraMagic(_player, bonusValue, pairedMateria, isMagicOrSummonMateria, isMaster, level);
                        break;
                }
            }
        }
    }

    private static void RecalculateQuadraMagic(
        Player _player
    ,   int bonusValue
    ,   Materia pairedMateria
    ,   bool isMagicOrSummonMateria
    ,   bool isMaster
    ,   int level
    ) {
        if (!isMagicOrSummonMateria) return;
        List<Ability> abilities = null;

        if (pairedMateria.type == MateriaType.Summon) {
            if (isMaster) {
                abilities = _player.lsAvailableSummons;
            } else {
                Ability summon = pairedMateria.selectionSpells[0].GetSelectedAbility(AbilityType.Summon);
                abilities = new List<Ability> { summon };
            }
        } else {
            if (isMaster) {
                abilities = _player.lsAvailableMagic;
            } else {
                abilities = new List<Ability>(level + 1);
                for (int j = 0; j <= level; j++) {
                    abilities.Add(pairedMateria.selectionSpells[j].GetSelectedAbility(AbilityType.Magic));
                }
            }
        }

        if (abilities != null) {
            foreach (Ability ability in abilities) {
                if (!ability.isPreventQuadraMagic
                && _player.dctAbilityAugmentations.TryGetValue(ability, out Player.AbilityAugmentation augmentation)) {
                    augmentation.quadraMagic += bonusValue;
                }
            }
        }
    }

    private static void RecalculateAddedEffect(Player _player, bool _isWeapon, Materia _pairedMateria
    ) {
        if (_isWeapon) {
            _player.statusAttack += _pairedMateria.statusEffectManager;
        } else {
            _player.statusDefense += _pairedMateria.statusEffectManager;
        }

        // Don't know why this wouldn't work...
        //StatusEffectManager targetManager = _isWeapon ? _player.statusAttack : _player.statusDefense;
        //targetManager += _pairedMateria.statusEffectManager;
    }

    private static void RecalculateElemental(
        List<ElementalAffinityManager.Affinity> elementAttack
    ,   List<ElementalAffinityManager.Affinity> elementHalve
    ,   List<ElementalAffinityManager.Affinity> elementInvalid
    ,   List<ElementalAffinityManager.Affinity> elementAbsorb
    ,   bool isWeapon
    ,   int bonusValue
    ,   Materia pairedMateria
    ) {
        int pairedElementIndex = ElementSelection.GetIndex(pairedMateria.elementIndex);
        if (pairedElementIndex == -1) return;

        if (isWeapon) {
            elementAttack[pairedElementIndex].isActive = true;
        } else {
            List<ElementalAffinityManager.Affinity> lsElementalAffinities;
            switch (bonusValue) {
                case 1:
                    lsElementalAffinities = elementInvalid;
                    break;
                case 2:
                    lsElementalAffinities = elementAbsorb;
                    break;
                default:
                    lsElementalAffinities = elementHalve;
                    break;
            }

            ElementalAffinityManager.Affinity affinity = lsElementalAffinities[pairedElementIndex];
            affinity.isActive = true;
        }
    }

    private static void RecalculatePostAttack(
        Player _player
    ,   List<Ability> counterCommands
    ,   List<Ability> masterCommands
    ,   Materia supportMateria
    ,   Materia pairedMateria
    ,   bool isMagicOrSummonMateria
    ,   bool isMaster
    ,   int level
    ) {
        if (!IsPairedMateriaValid()) return;

        HashSet<Ability> abilities = GetAvailableMateriaAbilities();

        Dictionary<int, Action<Player.AbilityAugmentation>> augmentationActions = new Dictionary<int, Action<Player.AbilityAugmentation>> {
            { 7 , aug => aug.mpAbsorb    = true }
        ,   { 8 , aug => aug.hpAbsorb    = true }
        ,   { 9 , aug => aug.addedCut    = true }
        ,   { 10, aug => aug.stealAsWell = true }
        };

        foreach (Ability a in abilities) {
            if (_player.dctAbilityAugmentations.TryGetValue(a, out Player.AbilityAugmentation augmentation)) {
                augmentationActions[supportMateria.subtype](augmentation);
            }
        }

        bool IsPairedMateriaValid() {
            if (isMagicOrSummonMateria) return true;

            if (pairedMateria.type != MateriaType.Command) return false;
            if (isMaster) return true;

            if (pairedMateria.selectionSpells
                .Select(x => x.GetSelectedAbility(AbilityType.Command))
                .Intersect(counterCommands).Any()) {
                return true;
            }

            return false;
        }

        HashSet<Ability> GetAvailableMateriaAbilities() {
            HashSet<Ability> abilities = new HashSet<Ability>();

            switch (pairedMateria.type) {
                case MateriaType.Command:
                    if (isMaster) {
                        abilities.UnionWith(masterCommands);
                    } else {
                        for (int j = 0; j < pairedMateria.levels - 1; j++) {
                            Ability cmd = pairedMateria.selectionSpells[j].GetSelectedAbility(AbilityType.Command);
                            if (_player.arrAvailableCommands.Contains(cmd)) {
                                abilities.Add(cmd);
                            }
                        }
                    }
                    break;

                case MateriaType.Magic:
                    if (isMaster) {
                        abilities.UnionWith(DBResources.GetAbilityList(AbilityType.Magic));
                    } else {
                        for (int j = 0; j <= level; j++) {
                            Ability magic = pairedMateria.selectionSpells[j].GetSelectedAbility(AbilityType.Magic);
                            abilities.Add(magic);
                        }
                    }
                    break;

                case MateriaType.Summon:
                    if (isMaster) {
                        abilities.UnionWith(DBResources.GetAbilityList(AbilityType.Summon));
                    } else {
                        Ability summon = pairedMateria.selectionSpells[0].GetSelectedAbility(AbilityType.Summon);
                        abilities.Add(summon);
                    }
                    break;
            }

            return abilities;
        }
    }

    private static void RecalculateMPTurbo(
        Player _player
    ,   int bonusValue
    ,   Materia pairedMateria
    ,   bool isMagicOrSummonMateria
    ) {
        if (!isMagicOrSummonMateria) return;

        IEnumerable<Ability> abilities = pairedMateria.type switch {
            MateriaType.Summon =>
                    pairedMateria.IsMaster
                ?   _player.lsAvailableSummons
                :   new Ability[] { pairedMateria.selectionSpells[0].GetSelectedAbility(AbilityType.Summon) }
        ,   _ => pairedMateria.IsMaster ? _player.lsAvailableMagic : GetMagicAbilities(pairedMateria),
        };

        foreach (Ability ability in abilities) {
            if (!ability.isPreventQuadraMagic && _player.dctAbilityAugmentations.TryGetValue(ability, out Player.AbilityAugmentation augmentation)) {
                augmentation.mpTurbo = Mathf.Clamp(augmentation.mpTurbo + bonusValue, 0, 50);
                //Debug.Log($"ability({ability.name}), mpTurbo({augmentation.mpTurbo})");
            }
        }
    }

    private static void RecalculateAll(
        Player _player
    ,   int bonusValue
    ,   Materia pairedMateria
    ,   bool isMagicMateria
    ,   bool isMaster
    ) {
        if (!isMagicMateria) return;

        IEnumerable<Ability> abilities =
            isMaster
        ?   _player.lsAvailableMagic
        :   GetMagicAbilities(pairedMateria);

        UpdateAbilityAugmentations(abilities, bonusValue);

        void UpdateAbilityAugmentations(IEnumerable<Ability> abilities, int allValue) {
            foreach (Ability ability in abilities) {
                if (_player.dctAbilityAugmentations.TryGetValue(ability, out Player.AbilityAugmentation augmentation)) {
                    augmentation.all += allValue;
                }
            }
        }
    }

    private static IEnumerable<Ability> GetMagicAbilities(Materia materia) {
        int level = materia.GetLevel();
        List<Ability> abilities = new List<Ability>(level + 1);

        for (int i = 0; i <= level; i++) {
            abilities.Add(materia.selectionSpells[i].GetSelectedAbility(AbilityType.Magic));
        }

        return abilities.Where(x => x != null).ToList();
    }

    public static void RecalculateCounterAttacks(Player _player) {
        List<(Materia, Materia)> allMateriaPairs = new();
        DBAbilities.CommandList cmd = DBResources.GetAbilities.commandList;
        List<Ability> counterCommands = new List<Ability> {
            cmd.cmdSteal
        ,   cmd.cmdSense
        ,   cmd.cmdThrow
        ,   cmd.cmdMorph
        ,   cmd.cmdDeathblow
        ,   cmd.cmdManipulate
        ,   cmd.cmdMime
        };

        GetCounterPair(_player.weapon, _player.materiaWeapon);
        GetCounterPair(_player.armor, _player.materiaArmor);

        _player.counterMateriaPairs = allMateriaPairs;
        //Debug.Log("COUNTER: " + counterMateriaPairs.Count);

        void GetCounterPair(Equipment _equipment, Materia[] _arrMateria) {
            if (_equipment == null) return;

            int linkedSlots = _equipment.links * 2;
            int materiaSlotCount = _equipment.materiaSlotCount;

            for (int i = 0; i < materiaSlotCount && allMateriaPairs.Count < 8; i++) {
                Materia counterMateria = _arrMateria[i];

                if (counterMateria == null) continue;
                        
                if (counterMateria.type == MateriaType.Independent
                &&  counterMateria.subtype == 4) { // Counter Attack
                    allMateriaPairs.Add((counterMateria, null));

                    continue;
                }

                if (i >= linkedSlots || counterMateria.type != MateriaType.Support) continue;

                Func<Materia, bool> filter;
                Materia pairedMateria = null;

                switch (counterMateria.subtype) {
                    case 2: // Counter
                        filter = FilterForCounter;
                        break;
                    case 3: // Magic Counter
                        filter = FilterForMagicCounter;
                        break;
                    case 4: // Sneak Attack
                        filter = FilterAll;
                        break;
                    case 5: // Final Attack
                        filter = FilterAll;
                        break;
                    default:
                        continue;
                }

                int increment = (i % 2 == 0) ? 1 : -1;

                pairedMateria = _arrMateria[i + increment];
                if (pairedMateria == null) continue;

                if (filter(counterMateria)) {
                    allMateriaPairs.Add((counterMateria, pairedMateria));
                }

                bool FilterForCounter(Materia _materia) {
                    bool isCommand = pairedMateria.type == MateriaType.Command;

                    if (!isCommand) return false;

                    if (pairedMateria.IsMaster) {
                        return true;
                    }

                    IEnumerable<Ability> abilities = pairedMateria.selectionSpells
                        .Select(x => x.GetSelectedAbility(AbilityType.Command)
                    );

                    if (abilities.Intersect(counterCommands).Any()) {
                        return true;
                    }

                    return false;
                }

                bool FilterForMagicCounter(Materia _materia) {
                    bool isMagicMateria = pairedMateria.type == MateriaType.Magic;
                    bool isSummonMateria = pairedMateria.type == MateriaType.Summon;

                    return isMagicMateria || isSummonMateria;
                }

                bool FilterAll(Materia _materia) {
                    bool isMagicOrSummonMateria = pairedMateria.type == MateriaType.Magic || pairedMateria.type == MateriaType.Summon;

                    if (isMagicOrSummonMateria) return true;
                            
                    if(pairedMateria.type != MateriaType.Command) return false;
                    if (pairedMateria.IsMaster) return true;

                    if (pairedMateria.selectionSpells
                        .Select(x => x.GetSelectedAbility(AbilityType.Command))
                        .Intersect(counterCommands).Any()) {
                        return true;
                    }

                    return false;
                }
            }
        }
    }
}
