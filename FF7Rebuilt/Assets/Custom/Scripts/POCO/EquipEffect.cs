using System;

[Serializable]
public class EquipEffect {
    public PrimaryStats primary = new PrimaryStats();
    public DerivedStats derived = new DerivedStats();
    public int maxHPPercent;
    public int maxMPPercent;

    //public List<Materia> counterAttackMateria { get; set; } = new List<Materia>();
    public int coverChance { get; set; }
    public int gilDropped { get; set; }
    public int encounterRate { get; set; }
    public int chocoboEncounterRate { get; set; }
    public int preEmptiveChance { get; set; }
    public int expEarned { get; set; }
    public int megaAll { get; set; }
    public bool isUnderWater { get; set; }
    public bool isHPSwitchMP { get; set; }

    public EquipEffect() {
    }

    public EquipEffect(EquipEffect _source) {
        primary = new PrimaryStats(_source.primary);
        derived = new DerivedStats(_source.derived);
        maxHPPercent         = _source.maxHPPercent;
        maxMPPercent         = _source.maxMPPercent;
        coverChance          = _source.coverChance;
        //counterAttackMateria = _source.counterAttackMateria;
        gilDropped           = _source.gilDropped;
        encounterRate        = _source.encounterRate;
        chocoboEncounterRate = _source.chocoboEncounterRate;
        preEmptiveChance     = _source.preEmptiveChance;
        expEarned            = _source.expEarned;
        isUnderWater         = _source.isUnderWater;
        isHPSwitchMP         = _source.isHPSwitchMP;
        megaAll            = _source.megaAll;
    }

    public static EquipEffect operator +(EquipEffect a, EquipEffect b) {
        EquipEffect temp = new EquipEffect();

        if (b == null) return a;

        temp.primary              = a.primary              + b.primary;
        temp.derived              = a.derived              + b.derived;
        temp.maxHPPercent         = a.maxHPPercent         + b.maxHPPercent;
        temp.maxMPPercent         = a.maxMPPercent         + b.maxMPPercent;
        temp.coverChance          = a.coverChance          + b.coverChance;
        temp.gilDropped           = a.gilDropped           + b.gilDropped;
        temp.encounterRate        = a.encounterRate        + b.encounterRate;
        temp.chocoboEncounterRate = a.chocoboEncounterRate + b.chocoboEncounterRate;
        temp.preEmptiveChance     = a.preEmptiveChance     + b.preEmptiveChance;
        temp.expEarned            = a.expEarned            + b.expEarned;
        //temp.counterAttackMateria = a.counterAttackMateria.Concat(b.counterAttackMateria).ToList();
        temp.isUnderWater = a.isUnderWater || b.isUnderWater;
        temp.isHPSwitchMP = a.isHPSwitchMP || b.isHPSwitchMP;
        temp.megaAll      = a.megaAll      + b.megaAll;

        return temp;
    }

    public void Clamp() {
        primary.Clamp();
        derived.Clamp();
    }
}
