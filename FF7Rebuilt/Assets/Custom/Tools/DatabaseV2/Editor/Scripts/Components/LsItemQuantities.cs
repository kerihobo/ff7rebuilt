using FFVII.Database;
using UnityEditor;
using UnityEngine.UIElements;

public class LsItemQuantities : VisualElement {
    public new class UxmlFactory : UxmlFactory<LsItemQuantities, UxmlTraits> { }

    public LsItemQuantities() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path).Instantiate();
        Add(tc);

        name = Name;
    }
    
    private const string Path = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/ls-item-quantities.uxml";
    private const string Name = "ls-item-quantities";

    private TemplateContainer tc;
    private Database database = DBResources.GetInitial;
    private DBInitial target;
    private ListView listView;

    public LsItemQuantities Initialize(DBInitial _target) {
        target = _target;
        database = _target;

        Refresh(target);

        return this;
    }

    public void Add() {
        target.startingItems.Add(new SelectionCollectible());

        EditorUtility.SetDirty(database);
        AssetDatabase.SaveAssets();

        listView.RefreshItems();
    }

    public void Refresh(DBInitial _target) {
        target = _target;

        if (listView != null) {
            Remove(listView);
        }

        listView = new ListView();
        listView.AddToClassList("scr-item-quantities");

        listView.fixedItemHeight = 24;
        listView.itemsSource = target.startingItems;
        listView.makeItem = MakeItem;
        listView.bindItem = BindItem;
        listView.selectionType = SelectionType.None;

        Add(listView);

        VisualElement MakeItem() => AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/lsitem-selection-collectible-quantity.uxml").Instantiate();

        void BindItem(VisualElement _ve, int _i) {
            int j = _i;

            SelectionCollectible sc = target.startingItems[j];

            _ve.Q<SelectionCollectibleQuantityEditor>().Initialize(target.startingItems[j], database);
            _ve.Q<Button>("btn-remove").clicked += Remove;

            void Remove() {
                target.startingItems.Remove(sc);
                EditorUtility.SetDirty(database);
                AssetDatabase.SaveAssets();

                listView.RefreshItems();
            }
        }
    }

}
