using CharacterTypes;
using InventoryTypes;
using System.Linq;
using TMPro;
using UnityEngine;

public class MateriaMenuCheckEnemySkillInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public Ability abilityReference { get; set; }

    private TextMeshProUGUI txtName;

    public void Initialize() {
        txtName = GetComponent<TextMeshProUGUI>();
        rct = GetComponent<RectTransform>();
    }

    public void Refresh(Player _player, Ability[] _arrViewItems, int _abilityIndex) {
        bool foundEnemySkill = GetSkillUnlocked(_player, _abilityIndex);
        
        if (foundEnemySkill) {
            abilityReference = _arrViewItems[_abilityIndex];
            txtName.text = abilityReference.name;
        } else {
            Clear();
        }
    }

    //public Materia GetEquippedMateria(Player _player, Materia _materia) {
    //    if (_materia == null) return null;

    //    return _player.materiaWeapon
    //        .Concat(_player.materiaArmor)
    //        .Where(x => x != null)
    //        .FirstOrDefault(x => x.name == _materia.name)
    //    ;
    //}

    public bool GetSkillUnlocked(Player _player, int _abilityIndex) {
        // TODO: Confirm which EnemySkill materia is considered when multiple are equipped.
        Materia materia = _player.Materia.FirstOrDefault(x => x.IsEnemySkill);

        if (materia == null) return false;

        bool[] skillsUnlocked = materia.enemySkillManager.skillsUnlocked;

        return skillsUnlocked[_abilityIndex];
    }

    public void Clear() {
        abilityReference = null;
        txtName.text = "";
    }
}
