﻿using UnityEngine;
using UnityEngine.AI;

namespace Field {
    public class PlayerControllerNavMesh : MonoBehaviour {
        public static bool IsControllable = true;

        public SelectionData selectionPlayer;

        public NavMeshAgent agent { get; set; }
        
        // Running is 4x faster than walking.
        private const float WALK_SPEED = 1.66f;
        private const float RUN_SPEED = WALK_SPEED * 4;

        private Animator anim;
        private Collider col;
        private Vector3 currentSpeed;
        private int layerMaskCharacter;
        private int layerMaskWalkMesh;
        private Vector3 lastPosition;
        private bool isRunning;

        private void OnEnable() {
            EventManagerField.OnOpenFieldMenu  += Engage;
            EventManagerField.OnCloseFieldMenu += Disengage;
            EventManagerField.OnDisengage      += Disengage;
            EventManagerField.OnEngage         += Engage;
        }

        private void OnDisable() {
            EventManagerField.OnOpenFieldMenu  -= Disengage;
            EventManagerField.OnCloseFieldMenu -= Engage;
            EventManagerField.OnDisengage      -= Disengage;
            EventManagerField.OnEngage         -= Engage;
        }

        private void Awake() {
            anim = GetComponent<Animator>();
            col = GetComponent<Collider>();
            agent = GetComponent<NavMeshAgent>();
            agent.updateRotation = false;

            layerMaskCharacter = LayerMask.GetMask("Character");
            layerMaskWalkMesh = LayerMask.GetMask("WalkMesh");
        }

        private void Start() {
            GameManager.Instance.currentlyControlledPlayerCharacter = this;
        }

        private void Update() {
            if (!IsControllable) return;

            GetMenuRequest();

            isRunning = FieldInputManager.Instance.x;
            bool isInput = Input.GetButton("Horizontal") || Input.GetButton("Vertical");
            // Instead of carving with NavMeshObstacle, maybe I could raycast from the player to destination?
            // Means all characters will need Colliders but so what?
            if (!(isInput && IsControllable)) {
                StandStill();
                return;
            }

            GetMoveInput();
        }

        private bool IsDestinationOccupied(Vector3 direction) {
            col.enabled = false;
            
            RaycastHit hit;
            if (Physics.Raycast(transform.position, direction, out hit, agent.radius, layerMaskCharacter)) {
                Debug.Log("Character detected: " + hit.collider.name);

                col.enabled = true;
                return true;
            }

            col.enabled = true;
            return false;
        }

        private void StandStill() {
            currentSpeed = Vector3.zero;
            anim.SetFloat("fSpeed", 0);
            agent.SetDestination(transform.position);
        }

        private void Disengage() {
            IsControllable = true;
        }

        private void Engage() {
            IsControllable = false;
            StandStill();
        }

        private void GetMoveInput() {

            Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
            Vector3 relativeInput = SceneInfo.Instance.GetDirection(input);
            relativeInput.y = transform.position.y;
            
            Vector3 destination = transform.position + (relativeInput * agent.radius);
            Vector3 direction = destination - transform.position;

            Vector3 rayOrigin = destination + Vector3.up;
            Ray ray = new Ray(rayOrigin, Vector3.down);

            bool isRaycastHit = Physics.Raycast(ray, out RaycastHit raycastHit, 3, layerMaskWalkMesh);
            if (isRaycastHit) {
                bool isNavMeshSamplePosition = NavMesh.SamplePosition(raycastHit.point, out NavMeshHit hit, .5f, NavMesh.AllAreas);
                if (hit.hit && !IsDestinationOccupied(direction)) {
                    agent.SetDestination(destination);
                } else {
                    StandStill();
                }
            } else {
                StandStill();
            }

            float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;

            // Experiment to keep cloud rotating right when the agent makes movements I didn't predict when I wrote this controller.
            Vector3 position = transform.position;
            Vector2 moveDelta = (new Vector2(position.x, position.z) - new Vector2(lastPosition.x, lastPosition.z)).normalized;
            if (moveDelta.sqrMagnitude > .1f) {
                angle = Mathf.Atan2(moveDelta.x, moveDelta.y) * Mathf.Rad2Deg;
            }

            transform.localEulerAngles = new Vector3(0, angle, 0);

            agent.speed = isRunning ? RUN_SPEED : WALK_SPEED;

            if (agent.velocity.sqrMagnitude > .1f) {
                anim.SetFloat("fSpeed", isRunning ? 1 : .5f);
            } else {
                anim.SetFloat("fSpeed", 0);
            }

            lastPosition = transform.position;
        }

        private void GetMenuRequest() {
            bool isMenuRequest = Input.GetKeyDown(KeyCode.V);

            if (isMenuRequest) {
                EventManagerField.OpenFieldMenu();
            }
        }
    }
}
