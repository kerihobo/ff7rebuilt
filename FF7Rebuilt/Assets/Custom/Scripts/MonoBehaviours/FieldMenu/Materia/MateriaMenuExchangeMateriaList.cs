using InventoryTypes;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MateriaMenuExchangeMateriaList : MenuControllable {
    public FingerCursorRight cursor;

    [SerializeField] private RectTransform scrollBarBG;
    [SerializeField] private UIMateriaInfo additionalOption;
    [SerializeField] private RectTransform optionsParent;

    private RectTransform scrollBarFG;
    private int selectedIndex;
    private MateriaMenuExchange exchange;
    private UIMateriaInfo[] arrOptions;

    // MAYBE REFACTOR
    private int lowBound = 0;
    private Vector2 initialListHierarchyPosition;
    private float listSpace;
    private float listHeight;
    private Coroutine corAnimatedScroll;
    private int columns = 1;

    private Materia[] arrMateria  => GameManager.Instance.gameData.inventory.arrMateria;
    private Materia[] arrMateriaA  => exchange.arrMateriaA;
    private int slotIndexA => exchange.slotIndexA;
    //private MateriaMenuExchangeSlots slots => exchange.;

    public void Initialize(MateriaMenuExchange _exchange) {
        exchange = _exchange;
        
        Initialize();
    }

    private void Initialize() {
        // MAYBE REFACTOR
        scrollBarFG = scrollBarBG.GetChild(0).GetComponent<RectTransform>();
        scrollBarFG.sizeDelta = new Vector2(scrollBarFG.sizeDelta.x, scrollBarBG.sizeDelta.y * (10f / 200));

        VerticalLayoutGroup vlgOptions = GetComponentInChildren<VerticalLayoutGroup>(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate(vlgOptions.GetComponent<RectTransform>());

        arrOptions = optionsParent.GetComponentsInChildren<UIMateriaInfo>();
        foreach (UIMateriaInfo option in arrOptions) {
            option.Initialize();
        }

        additionalOption.Initialize();
        ClearAdditional();

        Disable();

        cursor.Initialize(arrOptions[0].rct);

        // MAYBE REFACTOR
        listSpace = vlgOptions.spacing;
        listHeight = arrOptions[0].rct.rect.height;
        //listHierarchy = arrOptions[0].rct.parent.GetComponent<RectTransform>();
        initialListHierarchyPosition = optionsParent.anchoredPosition;
    }

    public void Refresh() {
        for (int i = 0; i < arrOptions.Length; i++) {
            int itemIndex = lowBound + i;
            Materia selectedMateria = arrMateria[itemIndex];
            arrOptions[i].Refresh(selectedMateria);
        }
    }

    // MAYBE REFACTOR
    private void RefreshAdditional(float _v) {
        Vector3 dir = Vector3.down;
        int anchoredIndex = lowBound + arrOptions.Length - columns;
        UIMateriaInfo anchoredInfo = arrOptions[^columns];

        if (_v < 0) {
            dir = Vector3.up;
            anchoredInfo = arrOptions[0];
            anchoredIndex = lowBound;
        }

        additionalOption.rct.position = anchoredInfo.rct.position + (dir * (listSpace + listHeight));

        Materia selectedMateria = arrMateria[anchoredIndex];
        additionalOption.Refresh(selectedMateria);
    }

    private void ClearAdditional() {
        additionalOption.Clear();
    }

    public void ResetSelection() {
        selectedIndex = 0;

        lowBound = 0;

        UpdateScrollBar();
        SetCursor();
    }

    public void SetCursor() {
        UIMateriaInfo selectedItemInfo = arrOptions[selectedIndex];

        cursor.SetCursorPosition(selectedItemInfo.rct);

        Materia selectedMateria = arrOptions[selectedIndex].materia;
        exchange.RefreshMateriaInfo(selectedMateria);
    }

    public void Open() {
        Debug.Log("Opened item list to browse");
        Show();
        cursor.Show();
        //ResetSelection();
        SetControllable();
        FieldMenu.Instance.SetCooldownTraverse(0);

        SetCursor();
        exchange.RefreshMateriaInfo(arrOptions[selectedIndex].materia);
    }

    private void Disable() {
        cursor.Hide();

        ClearAdditional();
        UpdateScrollBar();
        Refresh();
    }

    public void Show() {
        UpdateScrollBar();
        Refresh();
    }

    public void ExitCharacterSelection() {
        cursor.SetFlickerState(false);
        SetControllable();
    }

    private void EquipMateria() {
        int iMateria = lowBound + selectedIndex;

        (arrMateria[iMateria], arrMateriaA[slotIndexA]) = (arrMateriaA[slotIndexA], arrMateria[iMateria]);

        Close();
        exchange.CloseCurrentExchange();
    }

    private void Close() {
        Disable();
        exchange.ReturnFromMateriaList();
    }

    private IEnumerator AnimatedScroll(int _v) {
        _v = Mathf.Clamp(_v, -1, 1);

        float t = 0;
        float duration = FieldMenu.COOLDOWN_SCROLL;
        Vector2 start = initialListHierarchyPosition;
        Vector2 dir = new Vector2(0, _v);
        Vector2 end = start + (dir * (listSpace + listHeight));

        while (t < duration) {
            Vector2 newPosition = Vector2.Lerp(start, end, t / duration);
            optionsParent.anchoredPosition = newPosition;

            RefreshAdditional(_v);

            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        optionsParent.anchoredPosition = start;
        
        ClearAdditional();
        Refresh();
        SetCursor();
        exchange.RefreshMateriaInfo(arrOptions[selectedIndex].materia);

        corAnimatedScroll = null;
    }

    private void UpdateScrollBar() {
        float scroll = ((float)lowBound / columns) / (arrMateria.Length / columns);
        scroll = (int)(scrollBarBG.rect.height * scroll);
        //Debug.Log($"progress({scroll:f3}), bgHeight({scrollBarBG.rect.height:f3}), potentialLoc({scroll})");
        scrollBarFG.anchoredPosition = new Vector2(0, -scroll);
    }

#region Override methods.
    public override void GetInputCancel() { }
    public override void GetInputSquarePressed() { }
    public override void GetInputTrianglePressed() { }
    public override void GetInputConfirm() { }

    public override void GetInputHorizontal(int _h, bool isHorizontalPressed) {
        if (_h < 0) {
            Close();
        }
    }

    public override void GetInputConfirmPressed() {
        EquipMateria();
    }

    public override void GetInputCancelPressed() {
        Close();
        exchange.CloseCurrentExchange();
    }

    public override void GetInputVertical(int _v, bool isVerticalPressed) {
        if (corAnimatedScroll != null || (lowBound + selectedIndex == 0 && _v > 0) || (lowBound + selectedIndex == arrMateria.Length - columns && _v < 0)) return;
        //Debug.Log(LowBound + SelectedIndex + " : " + arrItems.Length);

        int v = -_v * columns;
        selectedIndex += v;

        bool isTooLow = selectedIndex < 0;
        bool isTooHigh = selectedIndex > arrOptions.Length - 1;
        bool isOutOfRange = isTooLow || isTooHigh;

        if (isOutOfRange) {
            lowBound += v;
            lowBound -= lowBound % columns;
        }

        if (selectedIndex < 0) {
            selectedIndex += columns;
        } else if (selectedIndex > arrOptions.Length - 1) {
            selectedIndex -= columns;
        }
        //SelectedIndex = Mathf.Clamp(SelectedIndex, 0, arrOptions.Length - 1);

        lowBound = Mathf.Clamp(lowBound, 0, arrMateria.Length - arrOptions.Length);
        lowBound -= lowBound % columns;

        UpdateScrollBar();

        if (isOutOfRange) {
            corAnimatedScroll = StartCoroutine(AnimatedScroll(v));
        } else {
            SetCursor();
            exchange.RefreshMateriaInfo(arrOptions[selectedIndex].materia);
        }
    }

    public override void SetControllable() {
        FieldMenu.Instance.menuControllable = this;
    }

    public override void GetInputPage(int _pageDirection, bool isPageXPressed) {
        if (corAnimatedScroll != null) {
            StopCoroutine(corAnimatedScroll);
            corAnimatedScroll = null;
            optionsParent.anchoredPosition = initialListHierarchyPosition;
            ClearAdditional();
        }

        int v = _pageDirection * (10 * columns);
        int lowBoundMax = arrMateria.Length - arrOptions.Length;
        lowBound += v;
        lowBound = Mathf.Clamp(lowBound, 0, lowBoundMax);
        lowBound -= lowBound % columns;

        UpdateScrollBar();
        Refresh();
        SetCursor();
        exchange.RefreshMateriaInfo(arrOptions[selectedIndex].materia);
    }
#endregion
}
