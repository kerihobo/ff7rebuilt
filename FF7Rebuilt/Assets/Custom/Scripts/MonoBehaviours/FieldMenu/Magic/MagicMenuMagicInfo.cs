using CharacterTypes;
using TMPro;
using UnityEngine;

public class MagicMenuMagicInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public Ability abilityReference { get; set; }

    private TextMeshProUGUI txtName;
    private Color white;
    private Color grey;

    public bool isUsableInField => abilityReference?.restoreType == (int)RestoreType.HP;

    public void Initialize() {
        txtName = GetComponent<TextMeshProUGUI>();
        rct = GetComponent<RectTransform>();

        white = TextColourContainer.GetInstance().white;
        grey = TextColourContainer.GetInstance().grey;
    }

    public void Refresh(Player _player, Ability _ability) {
        if (_ability == null) {
            Clear();
            return;
        }

        // So I looked into this. FF7 does not grey spells that're unusable due to MP cost.
        //bool isUsableByPlayer = _player.mpCurrent >= _ability.mpCost;

        abilityReference = _ability;
        txtName.text = abilityReference.name;
        txtName.color = isUsableInField /*&& isUsableByPlayer*/ ? white : grey;
    }

    public Ability GetSpellAvailable() {
        return abilityReference;
    }

    public void Clear() {
        abilityReference = null;
        txtName.text = "";
    }
}
