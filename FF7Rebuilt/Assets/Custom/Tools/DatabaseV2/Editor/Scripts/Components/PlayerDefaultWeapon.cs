using CharacterTypes;
using FFVII.Database;
using UnityEditor;
using UnityEngine.UIElements;

public class PlayerDefaultWeapon : VisualElement {
    public new class UxmlFactory : UxmlFactory<PlayerDefaultWeapon, UxmlTraits> { }

    public PlayerDefaultWeapon() {
        tc = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXMLPath).Instantiate();
        Add(tc);

        name = Name;
    }
    
    public Player target;
    public Database database;
    private const string UXMLPath = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/player-default-weapon.uxml";
    private const string Name = "player-default-weapon";
    private TemplateContainer tc;
    private int selectedMateriaIndex = -1;

    public PlayerDefaultWeapon Initialize(Player _target, Database _database) {
        target = _target;
        database = _database;

        BindUniqueControls();
        Refresh(target, database);

        return this;
    }

    public void BindUniqueControls() {
        DropdownField ddfWeaponSelect = this.Q<DropdownField>("drp-weapon");
        ddfWeaponSelect.Initialize(DBResources.GetWeaponsList(target.selectionStartingWeapon.type), target.selectionStartingWeapon, database);
        ddfWeaponSelect.RegisterValueChangedCallback(_e => {
            target.selectionStartingWeapon.SetDataExternally(ddfWeaponSelect.index);
            EditorUtility.SetDirty(database);

            RefreshButtons();

            selectedMateriaIndex = -1;
            
            RefreshGrpMateriaSelect();
        });

        this.Q<Button>("btn-clear-materia").RegisterCallback<ClickEvent>(e => {
            foreach (SelectionMateria sm in target.defaultMateriaWeapon) {
                sm.Nullify();
            }
            RefreshButtons();

            EditorUtility.SetDirty(database);
        });

        BindButtons();

        this.Q<EnumField>("enum-materia-type")./*SetMateriaType(target.defaultMateriaWeapon[selectedMateriaIndex].type).*/RegisterValueChangedCallback(e => {
            target.defaultMateriaWeapon[selectedMateriaIndex].type = (MateriaType)e.newValue;
            EditorUtility.SetDirty(database);

            RefreshGrpMateriaSelect();
        });

        DropdownField ddfMateriaSelect = this.Q<DropdownField>("drp-materia");
        //this.Q<DropdownField>("drp-materia").Initialize(
        //    DBResources.GetMateriaList(target.defaultMateriaWeapon[0].type)
        //,   ref target.defaultMateriaWeapon[0].index
        //,   database
        //);
        ddfMateriaSelect.RegisterValueChangedCallback(_e => {
            if (selectedMateriaIndex > -1) {
                target.defaultMateriaWeapon[selectedMateriaIndex].SetDataExternally(ddfMateriaSelect.index);
                EditorUtility.SetDirty(database);
            }
            
            RefreshButtons();
        });

        void BindButtons() {
            for (int i = 0; i < 8; i++) {
                int j = i;
                this.Q<Button>($"btn-materia-{j}").RegisterCallback<ClickEvent>(e => {
                    selectedMateriaIndex = j;
                    RefreshGrpMateriaSelect();
                });
            }
        }
    }

    public void Refresh(Player _target, Database _database) {
        target = _target;
        database = _database;
        selectedMateriaIndex = -1;

        this.Q<DropdownField>("drp-weapon").Initialize(DBResources.GetWeaponsList(target.selectionStartingWeapon.type), target.selectionStartingWeapon, database);

        RefreshGrpMateriaSelect();
    }

    private void RefreshGrpMateriaSelect() {
        this.Q<GroupBox>("grp-materia-select").style.display = selectedMateriaIndex > -1 ? DisplayStyle.Flex : DisplayStyle.None;

        if (selectedMateriaIndex > -1) {
            this.Q<EnumField>("enum-materia-type").value = target.defaultMateriaWeapon[selectedMateriaIndex].type;
        }
        
        RefreshDrpMateria();
    }

    private void RefreshButtons() {
        for (int i = 0; i < 8; i++) {
            Button btn = this.Q<Button>($"btn-materia-{i}");
            ClearMateriaClass(btn);

            string materiaClass = AddMateriaClass(i);
            if (target.defaultMateriaWeapon[i].index > -1) {
                btn.AddToClassList(materiaClass);
                btn.text = target.defaultMateriaWeapon[i].GetSelectedMateria().name;
            }

            btn.style.display = i < target.selectionStartingWeapon.GetSelectedWeapon()?.materiaSlotCount ? DisplayStyle.Flex : DisplayStyle.None;
        }

        void ClearMateriaClass(Button _btn) {
            _btn.RemoveFromClassList("magic");
            _btn.RemoveFromClassList("summon");
            _btn.RemoveFromClassList("command");
            _btn.RemoveFromClassList("independent");
            _btn.RemoveFromClassList("support");

            _btn.text = "Empty";
        }

        string AddMateriaClass(int _index) => (target.defaultMateriaWeapon[_index].type) switch {
            MateriaType.Magic => "magic",
            MateriaType.Summon => "summon",
            MateriaType.Command => "command",
            MateriaType.Independent => "independent",
            MateriaType.Support => "support",
            _ => null
        };
    }
    
    private void RefreshDrpMateria() {
        if (selectedMateriaIndex > -1) {
            this.Q<DropdownField>("drp-materia").Initialize(DBResources.GetMateriaList(
                target.defaultMateriaWeapon[selectedMateriaIndex].type)
            ,   target.defaultMateriaWeapon[selectedMateriaIndex], database
            );
        }

        RefreshButtons();
    }
}