using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FieldMenu : MonoBehaviour {
    public const float COOLDOWN_MAIN = 12 / 60f;
    public const float COOLDOWN_SCROLL = 4 / 60f;
    
    public static FieldMenu Instance;

    public FieldMenuOptionList pnlFieldOptions;

    public CanvasScaler canvasScaler { get; set; }
    public MenuControllable menuControllable { get; set; }
    /// <summary>
    /// Used to manufacture a delay in cursor movement. Set whenever:<br/>
    /// - Any button is pressed (long delay)<br/>
    /// - The cursor moves within a list (short delay)<br/>
    /// - A menu is first opened (possibly not, a menu might just be inaccessible until deemed fully open)<br/>
    /// - A selected option is invalid (long delay)<br/>
    /// - During unique, custom events. (E.g. using the item Catastrophe on a valid or invalid subject)
    /// </summary>
    public float coolDownTraverse { get; set; }
    /// <summary>
    /// Used to manufacture a delay in navigation (opening/closing menus).
    /// </summary>
    private float coolDownNavigate { get; set; }
    public Action onClosed { get; set; }
    
    [SerializeField] private PromptSaveCrystal promptSaveCrystal;
    [Header("Black Fade Layers")]
    [SerializeField] private CanvasGroup cnvGBlackBG;
    [SerializeField] private CanvasGroup cnvGBlackMG;
    [SerializeField] private CanvasGroup cnvGBlackFG;
    [Header("Screens")]
    [SerializeField] private ScreenFieldMain    fieldMain;
    [SerializeField] private ScreenFieldItem    item;
    [SerializeField] private ScreenFieldMagic   magic;
    [SerializeField] private ScreenFieldMateria materia;
    [SerializeField] private ScreenFieldEquip   equip;
    [SerializeField] private ScreenFieldStatus  status;
    [SerializeField] private ScreenFieldLimit   limit;
    [SerializeField] private ScreenFieldConfig  config;
    [SerializeField] private ScreenFieldPHS     phs;
    [SerializeField] private ScreenFieldSave    save;
    [SerializeField] private ScreenFieldQuit    quit;
    private ScreenField currentScreen;
    private int h;
    private int v;
    private int pageDirection;

    public int selectedCharacterIndex {
        get { return fieldMain.characterSelect.selectedIndex;
        } set {
            fieldMain.characterSelect.selectedIndex = value;
        }
    }

    public ScreenField[] Screens => new ScreenField[] {
        fieldMain
    ,   item
    ,   magic
    ,   materia
    ,   equip
    ,   status
    ,   limit
    ,   config
    ,   phs
    ,   save
    ,   quit
    };

    private void OnEnable() {
        EventManagerField.OnOpenFieldMenu += OnOpenMenu;
    }

    private void OnDisable() {
        EventManagerField.OnOpenFieldMenu -= OnOpenMenu;
    }

    private void OnOpenMenu() {
        StopAllCoroutines();
        StartCoroutine(Open());
    }

    private void Awake() {
        Instance = this;

        canvasScaler = GetComponent<CanvasScaler>();
    }

    private void Start() {
        Initialize();
        ForceClose();

        void Initialize() {
            pnlFieldOptions.Initialize(this, fieldMain);

            foreach (ScreenField sf in Screens) {
                sf.Initialize(this);
            }

            promptSaveCrystal.Initialize();
        }

        void ForceClose() {
            cnvGBlackBG.gameObject.SetActive(false);
            cnvGBlackBG.alpha = 0;

            HideAllScreens();

            menuControllable = null;
        }
    }

    private void Update() {
        ProcessInput();
    }

    private void ProcessInput() {
        // Scroll quickly up and down a list or...
        // Change which character we're viewing (equip/materia).
        // Constantly scrolls or switches without delay.
        // Can engage immediately if already held when a menu is opened.
        bool isPageUpPressed   = Input.GetKeyDown(KeyCode.PageUp);
        bool isPageUp          = Input.GetKey(KeyCode.PageUp);
        bool isPageDownPressed = Input.GetKeyDown(KeyCode.PageDown);
        bool isPageDown        = Input.GetKey(KeyCode.PageDown);
        bool isTrianglePressed = Input.GetKeyDown(KeyCode.V);
        bool isSquarePressed   = Input.GetKeyDown(KeyCode.Z);

        if (isPageUp || isPageDown) {
            pageDirection = isPageDown ? 1 : -1;
        } else {
            pageDirection = 0;
        }

        if (isPageUpPressed) {
            pageDirection = -1;
        } else if (isPageDownPressed) {
            pageDirection = 1;
        }

        bool isUp = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W);
        bool isLeft  = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
        bool isHorizontalInput = Input.GetButton("Horizontal");
        bool isHorizontalPressed = Input.GetButtonDown("Horizontal");
        bool isHorizontalReleased = Input.GetButtonUp("Horizontal");
        bool isVerticalInput = Input.GetButton("Vertical");
        bool isVerticalPressed = Input.GetButtonDown("Vertical");
        bool isVerticalReleased = Input.GetButtonUp("Vertical");

        // Added this to improve usability across the board but have not tested it much at all.
        if (IsAnyKeyUp()) {
            SetCooldownTraverse(0);
        }

        if (isVerticalInput) {
            v = isUp ? 1 : -1;
        } else {
            v = 0;
        }

        // We should prioritize a direction if both are pressed.
        // Only allow zero when neither are pressed.
        if (isHorizontalInput) {
            h = isLeft ? -1 : 1;
        } else {
            h = 0;
        }

        bool isConfirmPressed = Input.GetKeyDown(KeyCode.C);
        bool isConfirm = Input.GetKey(KeyCode.C);
        bool isCancelPressed  = Input.GetKeyDown(KeyCode.X);
        bool isCancel  = Input.GetKey(KeyCode.X);
        // The order here is important.
        // Within a single frame, confirmation is always given priority...
        // ... then cancel, then vertical input, then horizontal input.
        bool isCooledTraverse = coolDownTraverse <= 0;
        bool isCooledNavigate = coolDownNavigate <= 0;

        if (menuControllable) {
            bool hasPageDirection = !Mathf.Approximately(pageDirection, 0);
            bool isPageXPressed = isPageDownPressed || isPageUpPressed;

            if (isCooledNavigate && isSquarePressed) {
                menuControllable.GetInputSquarePressed();
            } else if (isCooledNavigate && isTrianglePressed) {
                menuControllable.GetInputTrianglePressed();
            } else if (isCooledNavigate && isConfirmPressed) {
                menuControllable.GetInputConfirmPressed();
            } else if (isCooledNavigate && isConfirm) {
                menuControllable.GetInputConfirm();
            } else if (isCooledNavigate && isCancelPressed) {
                menuControllable.GetInputCancelPressed();
            } else if (isCooledNavigate && isCancel) {
                menuControllable.GetInputCancel();
            } else if (isCooledTraverse && (hasPageDirection || isPageXPressed)) {
                SetCooldownTraverse(COOLDOWN_SCROLL);
                menuControllable.GetInputPage(pageDirection, isPageXPressed);
            } else if (isCooledTraverse && (isVerticalInput || isVerticalPressed)) {
                SetCooldownTraverse(COOLDOWN_SCROLL);
                menuControllable.GetInputVertical(v, isVerticalPressed);
            } else if (isCooledTraverse && (isHorizontalInput || isHorizontalPressed)) {
                SetCooldownTraverse(COOLDOWN_SCROLL);
                menuControllable.GetInputHorizontal(h, isHorizontalPressed);
            }
        }

        coolDownTraverse -= Time.deltaTime;
        coolDownNavigate -= Time.deltaTime;

        // Not sure about this.
        // FF7 seems to delay scrolling if any key is pushed down or released.
        // Seems ok for now but needs testing.
        bool canDelayOnPress = !menuControllable || menuControllable.canDelayOnPress;
        if (canDelayOnPress && IsAnyKeyDown()/* || IsAnyKeyUp()*/) {
            SetCooldownTraverse(COOLDOWN_MAIN);
        }

        bool IsAnyKeyUp() {
            bool isPageUpReleased = Input.GetKeyUp(KeyCode.PageUp);
            bool isPageDownReleased = Input.GetKeyUp(KeyCode.PageDown);
            bool isTriangleReleased = Input.GetKeyUp(KeyCode.V);
            bool isOReleased = Input.GetKeyUp(KeyCode.C);
            bool isXReleased = Input.GetKeyUp(KeyCode.X);

            return
                isPageUpReleased
            ||  isPageDownReleased
            ||  isTriangleReleased
            ||  isOReleased
            ||  isXReleased
            ||  isHorizontalReleased
            ||  isVerticalReleased
            ;
        }

        bool IsAnyKeyDown() {
            bool isPageUpPressed   = Input.GetKeyDown(KeyCode.PageUp);
            bool isPageDownPressed = Input.GetKeyDown(KeyCode.PageDown);
            bool isTrianglePressed = Input.GetKeyDown(KeyCode.V);
            bool isOPressed        = Input.GetKeyDown(KeyCode.C);
            bool isXPressed        = Input.GetKeyDown(KeyCode.X);
            

            return
                isPageUpPressed
            ||  isPageDownPressed
            ||  isTrianglePressed
            ||  isOPressed
            ||  isXPressed
            ||  isHorizontalPressed
            ||  isVerticalPressed
            ;
        }
    }

    public void OpenSelectedScreenQuick(int _optionIndex) {
        currentScreen = null;
        menuControllable = null;

        // Skip Field
        int screenIndex = _optionIndex + 1;
        // Skip Order
        if (_optionIndex > 5) {
            screenIndex--;
        }

        ScreenField selectedScreen = Screens[screenIndex];
        selectedScreen.SetDefaultDisplay();

        pnlFieldOptions.ShowOnlySelected(_optionIndex);

        FieldOut();
        StartCoroutine(NextScreenIn());

        //currentScreen.enabled = true;

        void FieldOut() {
            cnvGBlackMG.gameObject.SetActive(true);
            pnlFieldOptions.cursor.Hide();

            cnvGBlackMG.alpha = 1;
        }

        IEnumerator NextScreenIn() {
            selectedScreen.gameObject.SetActive(true);
            yield return StartCoroutine(selectedScreen.Open());

            currentScreen = selectedScreen;
        }
    }

    public void ReturnFromSelectedScreenQuick() {
        ScreenFieldMain scrFieldMain = (ScreenFieldMain)fieldMain;
        scrFieldMain.characterSelect.Close();
        ScreenField selectedScreen = currentScreen;

        currentScreen = null;
        menuControllable = null;

        //yield return StartCoroutine(selectedScreen.Close());

        scrFieldMain.characterSelect.characterInfoContainer.Refresh();

        FieldIn();

        pnlFieldOptions.ShowAll();
        pnlFieldOptions.cursor.Show();

        currentScreen = fieldMain;
        currentScreen.enabled = true;

        pnlFieldOptions.SetControllable();
        pnlFieldOptions.SetCursor();

        void FieldIn() {
            selectedScreen.gameObject.SetActive(false);

            cnvGBlackFG.gameObject.SetActive(false);

            cnvGBlackMG.alpha = 0;
            cnvGBlackMG.gameObject.SetActive(false);
        }
    }

    public IEnumerator OpenSelectedScreen(int _optionIndex) {
        currentScreen = null;
        menuControllable = null;

        // Skip Field
        int screenIndex = _optionIndex + 1;
        // Skip Order
        if (_optionIndex > 5) {
            screenIndex--;
        }

        ScreenField selectedScreen = Screens[screenIndex];
        selectedScreen.SetDefaultDisplay();

        pnlFieldOptions.ShowOnlySelected(_optionIndex);

        yield return StartCoroutine(FieldOut());
        yield return StartCoroutine(NextScreenIn());

        //currentScreen.enabled = true;

        IEnumerator FieldOut() {
            pnlFieldOptions.ProcessScreenTransition(0f);

            cnvGBlackMG.gameObject.SetActive(true);
            pnlFieldOptions.cursor.Hide();

            float t = 0;
            float duration = .2f;

            while (t < duration) {
                cnvGBlackMG.alpha = t / duration;
                pnlFieldOptions.ProcessScreenTransition(t / duration);

                t += Time.deltaTime;
                yield return null;
            }

            cnvGBlackMG.alpha = 1;
            pnlFieldOptions.ProcessScreenTransition(1);
        }

        IEnumerator NextScreenIn() {
            StartCoroutine(FadeFromBlackFG());
            
            selectedScreen.gameObject.SetActive(true);
            yield return StartCoroutine(selectedScreen.Open());

            //selectedScreen.SetInitialCursor();
            //rctCursor.gameObject.SetActive(true);

            currentScreen = selectedScreen;
        }
    }

    public IEnumerator SwitchScreens(int _i) {
        ScreenField nextScreen = Screens[_i];
        ScreenField selectedScreen = currentScreen;

        yield return StartCoroutine(CloseFieldScreen());
        yield return StartCoroutine(FadeToBlack());

        selectedScreen.gameObject.SetActive(false);
        selectedScreen.SetDefaultDisplay();
        pnlFieldOptions.SwapTextSelected(_i - 1);
        pnlFieldOptions.ShowOnlySelected(_i - 1);

        StartCoroutine(FadeFromBlackFG());
        nextScreen.gameObject.SetActive(true);
        yield return StartCoroutine(nextScreen.Open());

        currentScreen = nextScreen;
    }

    public IEnumerator ReturnFromSelectedScreen() {
        ScreenFieldMain scrFieldMain = fieldMain;
        scrFieldMain.characterSelect.Close();
        ScreenField selectedScreen = currentScreen;

        yield return StartCoroutine(CloseFieldScreen());
        yield return StartCoroutine(FadeToBlack());

        scrFieldMain.characterSelect.characterInfoContainer.Refresh();

        yield return StartCoroutine(FieldIn());

        pnlFieldOptions.ShowAll();
        pnlFieldOptions.cursor.Show();
        
        currentScreen = fieldMain;
        currentScreen.enabled = true;
        
        pnlFieldOptions.SetControllable();
        pnlFieldOptions.SetCursor();

        IEnumerator FieldIn() {
            selectedScreen.gameObject.SetActive(false);

            yield return StartCoroutine(FadeFromBlackFG());

            IEnumerator FadeFromBlackFG() {
                pnlFieldOptions.ProcessScreenTransition(1f);
                cnvGBlackFG.gameObject.SetActive(false);
                cnvGBlackMG.gameObject.SetActive(true);

                float t = 0;
                float duration = .2f;

                while (t < duration) {
                    float progress = 1 - (t / duration);
                    pnlFieldOptions.ProcessScreenTransition(progress);
                    cnvGBlackMG.alpha = progress;

                    t += Time.deltaTime;
                    yield return null;
                }

                pnlFieldOptions.ProcessScreenTransition(0);
                cnvGBlackMG.alpha = 0;
                cnvGBlackMG.gameObject.SetActive(false);
            }
        }
    }

    IEnumerator FadeToBlack() {
        cnvGBlackFG.gameObject.SetActive(true);

        float t = 0;
        float duration = .2f;

        while (t < duration) {
            cnvGBlackFG.alpha = t / duration;

            t += Time.deltaTime;
            yield return null;
        }

        cnvGBlackFG.alpha = 1;
    }

    IEnumerator FadeFromBlackBG() {
        float maxDelta = 3;

        while (Mathf.Abs(cnvGBlackBG.alpha - 1) > .1f) {
            cnvGBlackBG.alpha = Mathf.MoveTowards(cnvGBlackBG.alpha, 0, maxDelta * Time.deltaTime);

            yield return null;
        }

        cnvGBlackBG.alpha = 0;
        cnvGBlackBG.gameObject.SetActive(false);
    }

    IEnumerator FadeFromBlackFG() {
        cnvGBlackFG.gameObject.SetActive(true);

        float t = 0;
        float duration = .2f;

        while (t < duration) {
            cnvGBlackFG.alpha = 1 - (t / duration);

            t += Time.deltaTime;
            yield return null;
        }

        cnvGBlackFG.alpha = 0;
    }

    public IEnumerator Open() {
        yield return StartCoroutine(FadeToBlackBG());
        yield return StartCoroutine(ShowFieldScreen());

        IEnumerator FadeToBlackBG() {
            cnvGBlackBG.gameObject.SetActive(true);

            float maxDelta = 3;

            while (Mathf.Abs(cnvGBlackBG.alpha - 1) > .1f) {
                cnvGBlackBG.alpha = Mathf.MoveTowards(cnvGBlackBG.alpha, 1, maxDelta * Time.deltaTime);
                
                yield return null;
            }

            cnvGBlackBG.alpha = 1;
        }

        IEnumerator ShowFieldScreen() {
            HideAllScreens();

            fieldMain.gameObject.SetActive(true);
            yield return StartCoroutine(fieldMain.Open());

            //rctCursor.gameObject.SetActive(true);

            currentScreen = fieldMain;
        }
    }

    public IEnumerator Close() {
        yield return StartCoroutine(CloseFieldScreen());

        HideAllScreens();

        StartCoroutine(FadeFromBlackBG());

        EventManagerField.CloseFieldMenu();
        RunOnCloseEvent();

    }

    public IEnumerator QuickCLose() {
        menuControllable = null;

        yield return StartCoroutine(FadeToBlack());
        //yield return StartCoroutine(Close());
        HideAllScreens();

        pnlFieldOptions.ResetDisplay();

        cnvGBlackMG.alpha = 0;
        cnvGBlackFG.alpha = 0;

        //StartCoroutine(FadeFromBlackFG());

        StartCoroutine(FadeFromBlackBG());

        EventManagerField.CloseFieldMenu();
        RunOnCloseEvent();
    }

    void RunOnCloseEvent() {
        onClosed?.Invoke();
        onClosed = null;
    }
    
    private IEnumerator CloseFieldScreen() {
        ScreenField screenToClose = currentScreen;
        currentScreen = null;
        menuControllable = null;

        yield return StartCoroutine(screenToClose.Close());
    }

    private void HideAllScreens() {
        foreach (ScreenField sf in Screens) {
            sf.gameObject.SetActive(false);
            //sf.enabled = false;
        }

        pnlFieldOptions.gameObject.SetActive(false);
    }

    public void SetCooldownNavigate(float _cooldown) {
        coolDownNavigate = _cooldown;
    }

    public void SetCooldownTraverse(float _cooldown) {
        coolDownTraverse = _cooldown;
    }
}
