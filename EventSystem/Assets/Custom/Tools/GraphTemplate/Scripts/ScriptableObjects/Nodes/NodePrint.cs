﻿using System.Collections.Generic;
using UnityEngine;

public class NodePrint : Node {
    private dynamic copyA;
    
    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        name = "Print";
        size = new Vector2(220, 75);

        AddPortIn<Port>();
        AddPortIn<PortFieldTextDynamic>();

        AddPortOut<Port>();

        portContainerIn.ports[1].SetValue(copyA);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(120, 220);
    }

    public override void Run(NodeGraph _nodeGraph) {
        dynamic dynamicValue = portContainerIn.ports[1].GetValue();
        Debug.Log($"{_nodeGraph.name} (Print): {dynamicValue}");

        List<Port> connectedPorts = portContainerOut.ports[0].connectedPorts;
        if (connectedPorts.Count > 0) {
            connectedPorts[0].parentNode.Run(_nodeGraph);
        }
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodePrint>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodePrint)newNode).copyA = portContainerIn.ports[1].GetValue();

        return newNode;
    }
}
