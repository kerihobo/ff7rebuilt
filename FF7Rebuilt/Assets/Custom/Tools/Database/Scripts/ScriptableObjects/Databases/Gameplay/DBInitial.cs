﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Initial", menuName = "Kerihobo/Database/Gameplay/Initial")]
    public class DBInitial : Database {
        public int gil;
        [SerializeReference]
        public SelectionData[] currentPlayers = new SelectionData[3] {
            new SelectionData()
        ,   new SelectionData()
        ,   new SelectionData()
        };
        [SerializeReference]
        public SelectionData[] reservePlayers = new SelectionData[9] {
            new SelectionData()
        ,   new SelectionData()
        ,   new SelectionData()
        ,   new SelectionData()
        ,   new SelectionData()
        ,   new SelectionData()
        ,   new SelectionData()
        ,   new SelectionData()
        ,   new SelectionData()
        };
        [SerializeReference]
        public List<SelectionCollectible> startingItems = new List<SelectionCollectible>();
    }
}