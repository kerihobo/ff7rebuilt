﻿using System;
using UnityEngine;

[Serializable]
public class NodeAbsolute : Node {
    public NodeAbsolute(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(90, 50);
        name = "Absolute";

        AddPortIn<PortInt>("In", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_PORT);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();

        return Mathf.Abs(a);
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        return new NodeAbsolute(_nodeGraph, position + duplicationOffset, category, false);
    }
}