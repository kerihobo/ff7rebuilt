﻿using UnityEngine;

public class NodeMax : Node {
    private dynamic copyA;
    private dynamic copyB;

    protected override void OnBecamePersistent() {
        base.OnBecamePersistent();

        size = new Vector2(90, 75);
        name = "Max";

        AddPortIn<PortInt>("A");
        AddPortIn<PortInt>("B");
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);

        portContainerIn.ports[0].SetValue(copyA);
        portContainerIn.ports[1].SetValue(copyB);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(90, 160);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();

        return Mathf.Max(a, b);
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = Create<NodeMax>(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeMax)newNode).copyA = portContainerIn.ports[0].GetValue();
        ((NodeMax)newNode).copyB = portContainerIn.ports[1].GetValue();

        return newNode;
    }
}