using UnityEditor;
using UnityEngine.UIElements;
using UnityEngine;

public class EWTest : EditorWindow {
    public enum ColumnWidths {
        ONE = LIST_WIDTH + SECTION_WIDTH
    ,   TWO = LIST_WIDTH + SECTION_WIDTH + SECTION_MARGIN_WIDTH + SECTION_WIDTH
    }
    
    private const int SECTION_WIDTH = 450;
    private const int SECTION_MARGIN_WIDTH = 5;
    private const int LIST_WIDTH = 150;
    private const int MENU_BAR_HEIGHT = 17;
    public const int HEIGHT = 600 + (MENU_BAR_HEIGHT * 2);

    public static EWTest Instance;
    
    private VisualElement root;
    private MenuPrimary veMenuPrimary;
    private MenuSecondary veMenuSecondary;
    private VisualElement veEditor;

    [MenuItem("Tools/Test")]
    public static void OpenWindow() {
        Instance = GetWindow<EWTest>();
        Instance.titleContent = new GUIContent("Test");
    }

    public void CreateGUI() {
        Instance = this;

        root = rootVisualElement;
        root.styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Custom/Tools/DatabaseV2/DOM/USS/main.uss"));

        Debug.Log("DarkMode: " + EditorGUIUtility.isProSkin);

        veMenuPrimary = new MenuPrimary();
    }

    private static void SetWindowSize(int _width) {
        Instance.minSize = new Vector2(_width, HEIGHT);
        Instance.maxSize = new Vector2(_width, HEIGHT);
    }

    public void ShowSecondary(MenuSecondary _ve) {
        Debug.Log(_ve.name);

        if (veMenuSecondary != null) {
            root.Remove(veMenuSecondary);
        }

        veMenuSecondary = _ve;
        root.Insert(1, veMenuSecondary);
    }

    public void ShowEditor(VisualElement _ve, ColumnWidths _width) {
        Debug.Log(_ve.name);

        SetWindowSize((int)_width);

        if (veEditor != null) {
            root.Remove(veEditor);
        }

        veEditor = _ve;
        root.Insert(2, veEditor);
    }
}
