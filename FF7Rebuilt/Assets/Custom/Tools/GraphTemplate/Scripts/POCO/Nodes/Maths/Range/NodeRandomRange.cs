﻿using System;
using UnityEngine;

[Serializable]
public class NodeRandomRange : Node {
    public NodeRandomRange(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(90, 75);
        name = "Random Range";

        AddPortIn<PortInt>("A", Port.Capacity.SINGLE);
        AddPortIn<PortInt>("B", Port.Capacity.SINGLE);
        AddPortOut<PortInt>("Out", Port.Capacity.MULTIPLE);
    }

    public override void RenderNode(Vector2 _pan, StyleSet _styleSet) {
        base.RenderNode(_pan, _styleSet);

        ScaleForConnectedInputs(90, 160);
    }

    public override dynamic GetValue() {
        int a = portContainerIn.ports[0].GetValue();
        int b = portContainerIn.ports[1].GetValue();
        int r = UnityEngine.Random.Range(a, b + 1);
        
        return r;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        Node newNode = new NodeRandomRange(_nodeGraph, position + duplicationOffset, category, false);
        ((NodeRandomRange)newNode).portContainerIn.ports[0].SetValue(portContainerIn.ports[0].GetValue());
        ((NodeRandomRange)newNode).portContainerIn.ports[1].SetValue(portContainerIn.ports[1].GetValue());
        
        return newNode;
    }
}