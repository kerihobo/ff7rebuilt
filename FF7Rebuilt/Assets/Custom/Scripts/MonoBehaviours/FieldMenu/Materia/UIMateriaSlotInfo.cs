using InventoryTypes;
using UnityEngine;
using UnityEngine.UI;

public class UIMateriaSlotInfo : MonoBehaviour {
    public RectTransform rct { get; set; }
    public Materia materia { get; set; }

    private CanvasGroup cnvGrp;

    private Image imgIcon;

    public void Initialize() {
        rct = GetComponent<RectTransform>();
        cnvGrp = GetComponent<CanvasGroup>();
        imgIcon = GetComponent<Image>();
    }

    public void Refresh(Materia _materia) {
        materia = _materia;
        cnvGrp.alpha = materia != null ? 1 : 0;

        if (materia == null) {
            Clear();
            return;
        }
        
        imgIcon.sprite = ItemIconContainer.GetInstance().GetIconByMateria(_materia);
        //imgIcon.SetNativeSize();
    }

    public void Clear() {
        cnvGrp.alpha = 0;
        imgIcon.sprite = null;
        materia = null;
    }
}
