using UnityEditor;
using UnityEngine.UIElements;

/// <summary>
/// Intended as an abstract type.
/// </summary>
public class MenuSecondary : VisualElement {
    public new class UxmlFactory : UxmlFactory<MenuAbilities, UxmlTraits> {
    }
    
    protected virtual string Path { get; set; } = null;
    protected virtual string Name { get; set; } = null;
    
    protected VisualTreeAsset vta;
    protected VisualElement ve;

    public MenuSecondary() {
        vta = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path);
        ve = vta.Instantiate();
        name = Name;
        Add(ve);

        EWTest.Instance.ShowSecondary(this);
    }

    protected void Show(EventBase _e, VisualElement _menu, EWTest.ColumnWidths _width = EWTest.ColumnWidths.ONE) {
        Show((VisualElement)_e.currentTarget, _menu, _width);
    }

    protected void Show(VisualElement _ve, VisualElement _menu, EWTest.ColumnWidths _width = EWTest.ColumnWidths.ONE) {
        SetSelected(_ve);
        EWTest.Instance.ShowEditor(_menu, _width);
    }

    protected void SetSelected(VisualElement _ve) {
        ve.Query<Button>().ForEach(x => x.RemoveFromClassList("selected-secondary"));
        _ve.AddToClassList("selected-secondary");
    }
}
