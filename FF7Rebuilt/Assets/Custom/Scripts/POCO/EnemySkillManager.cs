﻿using System;

[Serializable]
public class EnemySkillManager {
    public const int MAX_INDEX = 24;

    public bool[] skillsUnlocked = new bool[MAX_INDEX];
}