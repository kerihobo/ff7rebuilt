﻿using UnityEngine;

public class PortFieldLabel : Port {
    public string value;

    public override void Initialize(Node _parentNode, string _portName, Direction _direction, Capacity _capacity, DisplayMode _displayMode) {
        base.Initialize(_parentNode, _portName, _direction, _capacity, _displayMode);

        portType = NodeDataType.Bool;
    }

    protected override void RenderAsInput(StyleSet _styleSet) {
        GUILayout.Label(((NodeGetSet)parentNode).field.GetValueAsString().ToString());
    }

    public override void SetValue(dynamic _value) {
        value = _value;
    }
}
