using CharacterTypes;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class StatusMenu2 : StatusMenu {
    [SerializeField] private RectTransform pnlAttack;
    [SerializeField] private RectTransform pnlDefend;
    private ScreenFieldStatus screenFieldStatus;
    private Color white;
    private Color grey;
    private List<TextMeshProUGUI> lsTxtAttack;
    private List<TextMeshProUGUI> lsTxtDefend;

    public override void Initialize(ScreenFieldStatus _screenFieldStatus) {
        screenFieldStatus = _screenFieldStatus;

        white = TextColourContainer.GetInstance().white;
        grey = TextColourContainer.GetInstance().grey;

        lsTxtAttack = pnlAttack.GetComponentsInChildren<TextMeshProUGUI>(true).ToList();
        lsTxtDefend = pnlDefend.GetComponentsInChildren<TextMeshProUGUI>(true).ToList();
    }

    public override void Refresh(Player _player) {
        StatusEffectManager statusAttack = _player.statusAttack;

        lsTxtAttack[0] .color = GetActiveColor(statusAttack.death);
        lsTxtAttack[1] .color = statusAttack.isNearDeath ? white : grey;
        lsTxtAttack[2] .color = GetActiveColor(statusAttack.sleep);
        lsTxtAttack[3] .color = GetActiveColor(statusAttack.poison);
        lsTxtAttack[4] .color = GetActiveColor(statusAttack.sadness);
        lsTxtAttack[5] .color = GetActiveColor(statusAttack.fury);
        lsTxtAttack[6] .color = GetActiveColor(statusAttack.confusion);
        lsTxtAttack[7] .color = GetActiveColor(statusAttack.silence);
        lsTxtAttack[8] .color = GetActiveColor(statusAttack.haste);
        lsTxtAttack[9] .color = GetActiveColor(statusAttack.slow);
        lsTxtAttack[10].color = GetActiveColor(statusAttack.stop);
        lsTxtAttack[11].color = GetActiveColor(statusAttack.frog);
        lsTxtAttack[12].color = GetActiveColor(statusAttack.small);
        lsTxtAttack[13].color = GetActiveColor(statusAttack.slowNumb);
        lsTxtAttack[14].color = GetActiveColor(statusAttack.petrify);
        lsTxtAttack[15].color = GetActiveColor(statusAttack.regen);
        lsTxtAttack[16].color = GetActiveColor(statusAttack.barrier);
        lsTxtAttack[17].color = GetActiveColor(statusAttack.mBarrier);
        lsTxtAttack[18].color = GetActiveColor(statusAttack.reflect);
        lsTxtAttack[19].color = GetActiveColor(statusAttack.shield);
        lsTxtAttack[20].color = GetActiveColor(statusAttack.deathSentence);
        lsTxtAttack[21].color = GetActiveColor(statusAttack.manipulate);
        lsTxtAttack[22].color = GetActiveColor(statusAttack.berserk);
        lsTxtAttack[23].color = GetActiveColor(statusAttack.peerless);
        lsTxtAttack[24].color = GetActiveColor(statusAttack.paralyzed);
        lsTxtAttack[25].color = GetActiveColor(statusAttack.darkness);

        StatusEffectManager statusDefend = _player.statusDefense;

        lsTxtDefend[0] .color = GetActiveColor(statusDefend.death);
        lsTxtDefend[1] .color = statusDefend.isNearDeath ? white : grey;
        lsTxtDefend[2] .color = GetActiveColor(statusDefend.sleep);
        lsTxtDefend[3] .color = GetActiveColor(statusDefend.poison);
        lsTxtDefend[4] .color = GetActiveColor(statusDefend.sadness);
        lsTxtDefend[5] .color = GetActiveColor(statusDefend.fury);
        lsTxtDefend[6] .color = GetActiveColor(statusDefend.confusion);
        lsTxtDefend[7] .color = GetActiveColor(statusDefend.silence);
        lsTxtDefend[8] .color = GetActiveColor(statusDefend.haste);
        lsTxtDefend[9] .color = GetActiveColor(statusDefend.slow);
        lsTxtDefend[10].color = GetActiveColor(statusDefend.stop);
        lsTxtDefend[11].color = GetActiveColor(statusDefend.frog);
        lsTxtDefend[12].color = GetActiveColor(statusDefend.small);
        lsTxtDefend[13].color = GetActiveColor(statusDefend.slowNumb);
        lsTxtDefend[14].color = GetActiveColor(statusDefend.petrify);
        lsTxtDefend[15].color = GetActiveColor(statusDefend.regen);
        lsTxtDefend[16].color = GetActiveColor(statusDefend.barrier);
        lsTxtDefend[17].color = GetActiveColor(statusDefend.mBarrier);
        lsTxtDefend[18].color = GetActiveColor(statusDefend.reflect);
        lsTxtDefend[19].color = GetActiveColor(statusDefend.shield);
        lsTxtDefend[20].color = GetActiveColor(statusDefend.deathSentence);
        lsTxtDefend[21].color = GetActiveColor(statusDefend.manipulate);
        lsTxtDefend[22].color = GetActiveColor(statusDefend.berserk);
        lsTxtDefend[23].color = GetActiveColor(statusDefend.peerless);
        lsTxtDefend[24].color = GetActiveColor(statusDefend.paralyzed);
        lsTxtDefend[25].color = GetActiveColor(statusDefend.darkness);

        Color GetActiveColor(StatusEffectManager.StatusEffect _statusEffect) {
            return _statusEffect.isActive ? white : grey;
        }
    }

    public override void Show() {
        gameObject.SetActive(true);
    }

    public override void Hide() {
        gameObject.SetActive(false);
    }
}
