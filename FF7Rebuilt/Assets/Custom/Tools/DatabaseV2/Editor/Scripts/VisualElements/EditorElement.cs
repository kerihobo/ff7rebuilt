﻿using FFVII.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public abstract class EditorElement : VisualElement {
    protected virtual bool IsListCustomizable { get; set; } = true;
    protected virtual string Path { get; set; } = null;
    protected virtual Database database { get; set; } = null;
    protected virtual Action<VisualElement, int> bindItem { get; set; } = null;
    protected virtual IList itemSource { get; set; } = null;

    protected VisualElement ve;
    protected Data target;
    protected ListView listView;
    protected int hoveredIndex;

    public EditorElement() {
        AddToClassList("editor");

        ve = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(Path).Instantiate();
        Add(ve);

        name = "data-editor";
        
        SetupControls();
    }

    protected void SetupControls() {
        if (itemSource != null) {
            SetupListView();
        }
        
        BindUniqueControls();
    }

    protected virtual void SetupListView() {
        listView = ve.Q<ListView>("content-list");
        listView.fixedItemHeight = 16;
        listView.selectionChanged += OnSelectionChange;
        listView.itemsSource = itemSource;
        listView.makeItem = MakeItem;
        listView.bindItem = bindItem;
        listView.SetSelection(0);

        Button btnAdd = ve.Q<Button>("btn-add-bottom");
        if (btnAdd != null) {
            btnAdd.clicked += Add;
        }
    }

    protected virtual void GetNewTarget<T>(T _data) where T : Data {
        target = _data;
        Refresh();

        if (target == null) return;

        Debug.Log($"<b><color=#0ff>onSelectionChange:</color></b> {target.name}");
    }

    protected virtual VisualElement MakeItem() {
        VisualElement ve = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom/Tools/DatabaseV2/DOM/UXML/Components/list-item.uxml").Instantiate();

        ve.Q<VisualElement>("ve-btns").style.display = DisplayStyle.None;

        if (!IsListCustomizable) return ve;
        
        ve.RegisterCallback<MouseEnterEvent>(e => {
            ve.Q<VisualElement>("ve-btns").style.display = DisplayStyle.Flex;
            hoveredIndex = ((VisualElement)e.currentTarget).parent.IndexOf((VisualElement)e.currentTarget);
        });

        ve.RegisterCallback<MouseLeaveEvent>(e => { ve.Q<VisualElement>("ve-btns").style.display = DisplayStyle.None; });

        ve.Q<Button>("btn-add").clicked += Insert;
        ve.Q<Button>("btn-remove").clicked += Remove;

        return ve;
    }

    /// <summary>
    /// Override in derived classes to add an item to the the itemSource. Finish by calling this base method.
    /// </summary>
    protected virtual void Insert() {
        EditorUtility.SetDirty(database);
        AssetDatabase.SaveAssets();

        listView.RefreshItems();
        listView.SetSelection(hoveredIndex + 1);
    }

    /// <summary>
    /// Override in derived classes to add an item to the the itemSource. Finish by calling this base method.
    /// </summary>
    protected virtual void Add() {
        EditorUtility.SetDirty(database);
        AssetDatabase.SaveAssets();

        listView.RefreshItems();
        listView.SetSelection(itemSource.Count - 1);
    }

    private void Remove() {
        itemSource.RemoveAt(hoveredIndex);
        EditorUtility.SetDirty(database);
        AssetDatabase.SaveAssets();

        listView.RefreshItems();
        listView.SetSelection(hoveredIndex - 1);
    }

    protected virtual void OnSelectionChange(IEnumerable<object> _o) { }
    protected virtual void BindUniqueControls() { }
    protected virtual void Refresh() { }
}
