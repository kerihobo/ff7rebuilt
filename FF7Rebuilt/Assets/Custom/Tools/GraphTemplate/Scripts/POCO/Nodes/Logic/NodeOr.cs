﻿using System;
using UnityEngine;

[Serializable]
public class NodeOr : Node {
    public NodeOr(NodeGraph _nodeGraph, Vector2 _offset, Category _category = Category.None, bool _isSave = true) : base(_nodeGraph, _offset, _category, _isSave) {
        size = new Vector2(90, 70);
        name = "Or";

        AddPortIn<PortBool>("A", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_PORT);
        AddPortIn<PortBool>("B", Port.Capacity.SINGLE, Port.DisplayMode.SHOW_PORT);
        AddPortOut<PortBool>("Out", Port.Capacity.MULTIPLE);
    }

    public override dynamic GetValue() {
        bool a = portContainerIn.ports[0].GetValue();
        bool b = portContainerIn.ports[1].GetValue();
        
        return a || b;
    }

    public override Node Duplicate(NodeGraph _nodeGraph) {
        return new NodeOr(_nodeGraph, position + duplicationOffset, category, false);
    }
}
