using FFVII.Database;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public abstract class EditorAbilities : EditorElement {
    protected override string Path { get; set; } = "Assets/Custom/Tools/DatabaseV2/DOM/UXML/EditorPanels/Abilities/editor-abilities.uxml";
    protected override Database database { get; set; } = DBResources.GetAbilities;

    private (Button, ElementalAffinityManager.Affinity)[] arrPairElementButtons;
    private (Button, StatusEffectManager.StatusEffect)[] arrPairStatusResistanceButtons;

    protected override void OnSelectionChange(IEnumerable<object> _o) {
        GetNewTarget((Ability)_o.ToList().FirstOrDefault());
    }

    protected override void BindUniqueControls() {
        base.BindUniqueControls();

        if (target == null) return;

        ve.Q<TextField>("txt-name").SetValue(target.name).RegisterValueChangedCallback(e => {
            target.name = e.newValue;
            EditorUtility.SetDirty(database);
            listView.RefreshItems();
        });

        ve.Q<TextField>("txt-description").SetValue(((Ability)target).description).RegisterValueChangedCallback(e => {
            ((Ability)target).description = e.newValue;
            EditorUtility.SetDirty(database);
        });

        ve.Q<ObjectField>("go-prefab-effect").SetGameObject(((Ability)target).prefabEffect).RegisterValueChangedCallback(e => {
            ((Ability)target).prefabEffect = (GameObject)e.newValue;
            EditorUtility.SetDirty(database);
        });

        Toggle tglPreventQuadraMagic = ve.Q<Toggle>("tgl-is-prevent-quadra-magic");
        if (tglPreventQuadraMagic != null) {
            tglPreventQuadraMagic.SetValue(((Ability)target).isPreventQuadraMagic).RegisterValueChangedCallback(e => {
                ((Ability)target).isPreventQuadraMagic = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        DropdownField ddfSpecialEffect = ve.Q<DropdownField>("drp-special-effect");
        ddfSpecialEffect.choices = SpecialEffectManager.SPECIAL_EFFECT_NAMES.ToList();
        ddfSpecialEffect.index = ((Ability)target).specialEffect;
        ddfSpecialEffect.RegisterValueChangedCallback(_e => {
            ((Ability)target).specialEffect = ddfSpecialEffect.index;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfDamageType = ve.Q<DropdownField>("drp-damage-type");
        ddfDamageType.choices = DamageType.DAMAGE_TYPE_NAMES.ToList();
        ddfDamageType.index = ((Ability)target).accuracyCalculation;
        ddfDamageType.RegisterValueChangedCallback(_e => {
            ((Ability)target).accuracyCalculation = ddfDamageType.index;
            EditorUtility.SetDirty(database);
        });

        DropdownField ddfDamageFormula = ve.Q<DropdownField>("drp-damage-formula");
        ddfDamageFormula.choices = DamageFormula.DAMAGE_FORMULA_NAMES.ToList();
        ddfDamageFormula.index = ((Ability)target).damageFormula;
        ddfDamageFormula.RegisterValueChangedCallback(_e => {
            ((Ability)target).damageFormula = ddfDamageFormula.index;
            EditorUtility.SetDirty(database);
        });

        ve.Q<EnumField>("enum-restore-type").SetRestoreType((RestoreType)((Ability)target).restoreType).RegisterValueChangedCallback(e => {
            ((Ability)target).restoreType = (int)(RestoreType)e.newValue;
            EditorUtility.SetDirty(database);
        });

        BindSpellStats();
        BindTargetOptions();
        BindExecutionOptions();
        BindAttackElements();
        BindAfflictions();

        void BindSpellStats() {
            ve.Q<IntegerField>("int-power").SetValue(((Ability)target).power).RegisterValueChangedCallback(e => {
                ((Ability)target).power = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-attack-percent").SetValue(((Ability)target).attackPercent).RegisterValueChangedCallback(e => {
                ((Ability)target).attackPercent = e.newValue;
                EditorUtility.SetDirty(database);
            });

            ve.Q<IntegerField>("int-mp-cost").SetValue(((Ability)target).mpCost).RegisterValueChangedCallback(e => {
                ((Ability)target).mpCost = e.newValue;
                EditorUtility.SetDirty(database);
            });
        }

        void BindTargetOptions() {
            VisualElement targetOptions = ve.Q<VisualElement>("ve-target-options");

            targetOptions.Q<Button>("btn-enable-selection").SetBoolClass(((Ability)target).targetOptions.isEnableSelection).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isEnableSelection);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-start-on-enemies").SetBoolClass(((Ability)target).targetOptions.isStartOnEnemies).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isStartOnEnemies);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-multiple-by-default").SetBoolClass(((Ability)target).targetOptions.isMultipleTargetsByDefault).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isMultipleTargetsByDefault);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-toggle-single-multiple-targets").SetBoolClass(((Ability)target).targetOptions.isToggleSingleMultipleTargets).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isToggleSingleMultipleTargets);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-one-row-only").SetBoolClass(((Ability)target).targetOptions.isOneRowOnly).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isOneRowOnly);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-short-range").SetBoolClass(((Ability)target).targetOptions.isShortRange).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isShortRange);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-all-rows").SetBoolClass(((Ability)target).targetOptions.isAllRows).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isAllRows);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-random-target").SetBoolClass(((Ability)target).targetOptions.isRandomTarget).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isRandomTarget);
                EditorUtility.SetDirty(database);
            });

            targetOptions.Q<Button>("btn-fallen").SetBoolClass(((Ability)target).targetOptions.isFallen).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).targetOptions.isFallen);
                EditorUtility.SetDirty(database);
            });
        }

        void BindExecutionOptions() {
            VisualElement executionOptions = ve.Q<VisualElement>("ve-execution-options");

            executionOptions.Q<Button>("btn-affects-mp").SetBoolClass(((Ability)target).executionProperties.isAffectsMP).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isAffectsMP);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-drains-some-damage").SetBoolClass(((Ability)target).executionProperties.isDrainsSomeDamage).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isDrainsSomeDamage);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-ignores-status-defense").SetBoolClass(((Ability)target).executionProperties.isIgnoreStatusDefense).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isIgnoreStatusDefense);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-ignores-defense").SetBoolClass(((Ability)target).executionProperties.isIgnoreDefense).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isIgnoreDefense);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-drains-hp-and-mp").SetBoolClass(((Ability)target).executionProperties.isDrainHPAndMP).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isDrainHPAndMP);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-miss-if-alive").SetBoolClass(((Ability)target).executionProperties.isMissIfAlive).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isMissIfAlive);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-no-retarget-if-dead").SetBoolClass(((Ability)target).executionProperties.isNoRetargetIfDead).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isNoRetargetIfDead);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-affected-by-darkness").SetBoolClass(((Ability)target).executionProperties.isAffectedByDarkness).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isAffectedByDarkness);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-reflectable").SetBoolClass(((Ability)target).executionProperties.isReflectable).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isReflectable);
                EditorUtility.SetDirty(database);
            });

            executionOptions.Q<Button>("btn-always-critical-hit").SetBoolClass(((Ability)target).executionProperties.isAlwaysCriticalHit).RegisterCallback<ClickEvent>(e => {
                ((VisualElement)e.currentTarget).FlipBool(ref ((Ability)target).executionProperties.isAlwaysCriticalHit);
                EditorUtility.SetDirty(database);
            });
        }

        void BindAttackElements() {
            VisualElement elementalAffinities = ve.Q<VisualElement>("ve-elemental-affinities");

            RestockButtonArrayElements();

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                int j = i;

                arrPairElementButtons[j].Item1.SetBoolClass(arrPairElementButtons[j].Item2.isActive).RegisterCallback<ClickEvent>(e => {
                    ((VisualElement)e.currentTarget).FlipBool(ref arrPairElementButtons[j].Item2.isActive);
                    EditorUtility.SetDirty(database);
                });
            }
        }

        void BindAfflictions() {
            VisualElement statusEffects = ve.Q<VisualElement>("ve-afflictions");

            ve.Q<EnumField>("enum-status-effect-action").SetStatusEffectAction(((Ability)target).statusEffectManager.statusEffectAction).RegisterValueChangedCallback(e => {
                ((Ability)target).statusEffectManager.statusEffectAction = (StatusEffectManager.StatusEffectAction)e.newValue;
               
                RefreshStatusResistances();
                EditorUtility.SetDirty(database);
            });

            IntegerField intFChance = statusEffects.Q<IntegerField>("int-chance");
            intFChance.SetValue(((Ability)target).statusEffectManager.chance).RegisterValueChangedCallback(e => {
                if (e.newValue < 0) intFChance.value = 0;
                if (e.newValue > 64) intFChance.value = 64;
                
                ((Ability)target).statusEffectManager.chance = intFChance.value;
                EditorUtility.SetDirty(database);

                statusEffects.Q<Label>("lbl-chance").text = $"{(int)((intFChance.value / 64f) * 256)}%";
            });

            RestockButtonArrayStatusResistance();

            for (int i = 0; i < arrPairStatusResistanceButtons.Length; i++) {
                int j = i;
                arrPairStatusResistanceButtons[j].Item1.RegisterCallback<ClickEvent>(e => {
                    arrPairStatusResistanceButtons[j].Item1.ToggleAfflictionBehaviour(ref arrPairStatusResistanceButtons[j].Item2, ((Ability)target).statusEffectManager.statusEffectAction);
                    EditorUtility.SetDirty(database);
                });
            }
        }
    }

    protected override void Refresh() {
        base.Refresh();

        if (target == null) return;

        ve.Q<TextField>("txt-name").value = target.name;
        ve.Q<TextField>("txt-description").value = ((Ability)target).description;
        ve.Q<ObjectField>("go-prefab-effect").value = ((Ability)target).prefabEffect;
        ve.Q<DropdownField>("drp-special-effect").index = ((Ability)target).specialEffect;
        ve.Q<DropdownField>("drp-damage-type").index = ((Ability)target).accuracyCalculation;
        ve.Q<DropdownField>("drp-damage-formula").index = ((Ability)target).damageFormula;
        ve.Q<EnumField>("enum-restore-type").value = (RestoreType)((Ability)target).restoreType;

        Toggle tglPreventQuadraMagic = ve.Q<Toggle>("tgl-is-prevent-quadra-magic");
        if (tglPreventQuadraMagic != null) {
            tglPreventQuadraMagic.value = ((Ability)target).isPreventQuadraMagic;
        }

        RefreshTargetOptions();
        RefreshExecutionOptions();
        RefreshSpellStats();
        RefreshAttackElements();
        RefreshStatusResistances();

        void RefreshTargetOptions() {
            VisualElement targetOptions = ve.Q<VisualElement>("ve-target-options");

            targetOptions.Q<Button>("btn-enable-selection").SetBoolClass(((Ability)target).targetOptions.isEnableSelection);
            targetOptions.Q<Button>("btn-start-on-enemies").SetBoolClass(((Ability)target).targetOptions.isStartOnEnemies);
            targetOptions.Q<Button>("btn-multiple-by-default").SetBoolClass(((Ability)target).targetOptions.isMultipleTargetsByDefault);
            targetOptions.Q<Button>("btn-toggle-single-multiple-targets").SetBoolClass(((Ability)target).targetOptions.isToggleSingleMultipleTargets);
            targetOptions.Q<Button>("btn-one-row-only").SetBoolClass(((Ability)target).targetOptions.isOneRowOnly);
            targetOptions.Q<Button>("btn-short-range").SetBoolClass(((Ability)target).targetOptions.isShortRange);
            targetOptions.Q<Button>("btn-all-rows").SetBoolClass(((Ability)target).targetOptions.isAllRows);
            targetOptions.Q<Button>("btn-random-target").SetBoolClass(((Ability)target).targetOptions.isRandomTarget);
            targetOptions.Q<Button>("btn-fallen").SetBoolClass(((Ability)target).targetOptions.isFallen);
        }

        void RefreshExecutionOptions() {
            VisualElement executionOptions = ve.Q<VisualElement>("ve-execution-options");

            executionOptions.Q<Button>("btn-affects-mp").SetBoolClass(((Ability)target).executionProperties.isAffectsMP);
            executionOptions.Q<Button>("btn-drains-some-damage").SetBoolClass(((Ability)target).executionProperties.isDrainsSomeDamage);
            executionOptions.Q<Button>("btn-ignores-status-defense").SetBoolClass(((Ability)target).executionProperties.isIgnoreStatusDefense);
            executionOptions.Q<Button>("btn-ignores-defense").SetBoolClass(((Ability)target).executionProperties.isIgnoreDefense);
            executionOptions.Q<Button>("btn-drains-hp-and-mp").SetBoolClass(((Ability)target).executionProperties.isDrainHPAndMP);
            executionOptions.Q<Button>("btn-miss-if-alive").SetBoolClass(((Ability)target).executionProperties.isMissIfAlive);
            executionOptions.Q<Button>("btn-no-retarget-if-dead").SetBoolClass(((Ability)target).executionProperties.isNoRetargetIfDead);
            executionOptions.Q<Button>("btn-affected-by-darkness").SetBoolClass(((Ability)target).executionProperties.isAffectedByDarkness);
            executionOptions.Q<Button>("btn-reflectable").SetBoolClass(((Ability)target).executionProperties.isReflectable);
            executionOptions.Q<Button>("btn-always-critical-hit").SetBoolClass(((Ability)target).executionProperties.isAlwaysCriticalHit);
        }

        void RefreshSpellStats() {
            VisualElement spellStats = ve.Q<VisualElement>("ve-spell-stats");

            spellStats.Q<IntegerField>("int-power").value = ((Ability)target).power;
            spellStats.Q<IntegerField>("int-attack-percent").value = ((Ability)target).attackPercent;
            spellStats.Q<IntegerField>("int-mp-cost").value = ((Ability)target).mpCost;
        }

        void RefreshAttackElements() {
            var elementalAffinities = ve.Q<VisualElement>("ve-elemental-affinities");

            RestockButtonArrayElements();

            for (int i = 0; i < arrPairElementButtons.Length; i++) {
                arrPairElementButtons[i].Item1.SetBoolClass(arrPairElementButtons[i].Item2.isActive);
            }
        }
    }
    
    private void RefreshStatusResistances() {
        VisualElement statusEffects = ve.Q<VisualElement>("ve-afflictions");

        ve.Q<EnumField>("enum-status-effect-action").value = ((Ability)target).statusEffectManager.statusEffectAction;

        IntegerField intFChance = statusEffects.Q<IntegerField>("int-chance");
        intFChance.value = ((Ability)target).statusEffectManager.chance;
        statusEffects.Q<Label>("lbl-chance").text = $"{(int)((intFChance.value / 64f) * 256)}%";

        RestockButtonArrayStatusResistance();

        for (int i = 0; i < arrPairStatusResistanceButtons.Length; i++) {
            arrPairStatusResistanceButtons[i].Item1.SetAfflictionBehaviourClass(arrPairStatusResistanceButtons[i].Item2.isActive, ((Ability)target).statusEffectManager.statusEffectAction);
        }
    }

    void RestockButtonArrayElements() {
        GroupBox elementalAffinities = ve.Q<GroupBox>("grp-elemental-affinities");
        List<Button> lsButtons = elementalAffinities.Query<Button>().ToList();

        arrPairElementButtons = new (Button, ElementalAffinityManager.Affinity)[] {
                (lsButtons[0],  ((Ability)target).elements.fire)
            ,   (lsButtons[1],  ((Ability)target).elements.ice)
            ,   (lsButtons[2],  ((Ability)target).elements.lightning)
            ,   (lsButtons[3],  ((Ability)target).elements.earth)
            ,   (lsButtons[4],  ((Ability)target).elements.poison)
            ,   (lsButtons[5],  ((Ability)target).elements.gravity)
            ,   (lsButtons[6],  ((Ability)target).elements.water)
            ,   (lsButtons[7],  ((Ability)target).elements.wind)
            ,   (lsButtons[8],  ((Ability)target).elements.holy)
            ,   (lsButtons[9],  ((Ability)target).elements._10th)
            ,   (lsButtons[10], ((Ability)target).elements.restorative)
            ,   (lsButtons[11], ((Ability)target).elements.punch)
            ,   (lsButtons[12], ((Ability)target).elements.cut)
            ,   (lsButtons[13], ((Ability)target).elements.hit)
            ,   (lsButtons[14], ((Ability)target).elements.shoot)
            ,   (lsButtons[15], ((Ability)target).elements.shout)
            ,   (lsButtons[16], ((Ability)target).elements.death)
            ,   (lsButtons[17], ((Ability)target).elements.sleep)
            ,   (lsButtons[18], ((Ability)target).elements.confusion)
            };
    }

    void RestockButtonArrayStatusResistance() {
        GroupBox statusEffects = ve.Q<GroupBox>("grp-afflictions");
        List<Button> lsButtons = statusEffects.Query<Button>().ToList();

        arrPairStatusResistanceButtons = new (Button, StatusEffectManager.StatusEffect)[] {
                (lsButtons[0],  ((Ability)target).statusEffectManager.death)
            ,   (lsButtons[1],  ((Ability)target).statusEffectManager.sleep)
            ,   (lsButtons[2],  ((Ability)target).statusEffectManager.poison)
            ,   (lsButtons[3],  ((Ability)target).statusEffectManager.sadness)
            ,   (lsButtons[4],  ((Ability)target).statusEffectManager.fury)
            ,   (lsButtons[5],  ((Ability)target).statusEffectManager.confusion)
            ,   (lsButtons[6],  ((Ability)target).statusEffectManager.silence)
            ,   (lsButtons[7],  ((Ability)target).statusEffectManager.haste)
            ,   (lsButtons[8],  ((Ability)target).statusEffectManager.slow)
            ,   (lsButtons[9],  ((Ability)target).statusEffectManager.stop)
            ,   (lsButtons[10], ((Ability)target).statusEffectManager.frog)
            ,   (lsButtons[11], ((Ability)target).statusEffectManager.small)
            ,   (lsButtons[12], ((Ability)target).statusEffectManager.slowNumb)
            ,   (lsButtons[13], ((Ability)target).statusEffectManager.petrify)
            ,   (lsButtons[14], ((Ability)target).statusEffectManager.regen)
            ,   (lsButtons[15], ((Ability)target).statusEffectManager.barrier)
            ,   (lsButtons[16], ((Ability)target).statusEffectManager.mBarrier)
            ,   (lsButtons[17], ((Ability)target).statusEffectManager.reflect)
            ,   (lsButtons[18], ((Ability)target).statusEffectManager.shield)
            ,   (lsButtons[19], ((Ability)target).statusEffectManager.deathSentence)
            ,   (lsButtons[20], ((Ability)target).statusEffectManager.berserk)
            ,   (lsButtons[21], ((Ability)target).statusEffectManager.paralyzed)
            ,   (lsButtons[22], ((Ability)target).statusEffectManager.darkness)
            ,   (lsButtons[23], ((Ability)target).statusEffectManager.deathForce)
            ,   (lsButtons[24], ((Ability)target).statusEffectManager.resist)
            ,   (lsButtons[25], ((Ability)target).statusEffectManager.manipulate)
            ,   (lsButtons[26], ((Ability)target).statusEffectManager.sense)
            ,   (lsButtons[27], ((Ability)target).statusEffectManager.seizure)
            ,   (lsButtons[28], ((Ability)target).statusEffectManager.imprisoned)
            ,   (lsButtons[29], ((Ability)target).statusEffectManager.peerless)
            };
    }

    protected override void Insert() {
        itemSource.Insert(hoveredIndex + 1, new Ability());

        base.Insert();
    }

    protected override void Add() {
        itemSource.Add(new Ability());

        base.Add();
    }
}
