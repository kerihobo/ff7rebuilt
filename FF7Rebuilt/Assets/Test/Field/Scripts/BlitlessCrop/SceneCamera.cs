using UnityEngine;

/// <summary>
/// This is an experiment that I'm shelving for the time being.
/// Its aim is to crop the camera's frustrum to be able to pan and zoom without resorting to Blit.
/// I'm having trouble centering the player on the screen with it though so it's currently not reliable.
/// And for different aspect ratios or resolutions I am a bit lost for how to handle it.
/// </summary>
[ExecuteAlways]
public class SceneCamera : MonoBehaviour {
    private static readonly Vector2 InitialSensorSize = new Vector2(36, 24);
    
    [SerializeField] private float zoom = 1f;
    [SerializeField] private Transform target;
    [SerializeField] private Vector2 offset;
    [SerializeField] private bool isFollow;
    [Header("Debug")]
    [SerializeField] private bool isPreview;
    private Camera mainCam;
    private Vector2 screenSize;

    void Start() {
        mainCam = GetComponent<Camera>();
    }

    void Update() {
        screenSize = new Vector2(Screen.width, Screen.height);
        mainCam.sensorSize = InitialSensorSize;
        mainCam.lensShift = Vector2.zero;
        
        Vector3 point = (screenSize / 2) + offset;

        bool shouldFollow = isFollow && target;
        bool shouldPreview = !Application.isPlaying && isPreview;
        if ((shouldPreview || Application.isPlaying) && shouldFollow) {
            point = mainCam.WorldToScreenPoint(target.position);
        }
        
        zoom = Mathf.Max(zoom, .1f);

        float v = mainCam.aspect / (4 / 3f);
        Debug.Log(v);
        Debug.Log(mainCam.aspect);
        Debug.Log(640/480f);
        
        Vector2 lensShift = ((point / screenSize) - (Vector2.one / 2)) * zoom;
        Debug.Log($"point({point}), lensShift({lensShift})");

        if (Application.isPlaying || shouldPreview) {
            mainCam.sensorSize = InitialSensorSize / zoom;
            mainCam.lensShift = lensShift;
        }
    }

    private void OnGUI() {
        Vector2 boxSize = new Vector2(100, 100);

        Vector2 screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);

        Rect boxRect = new Rect(screenCenter.x - boxSize.x / 2,
                                screenCenter.y - boxSize.y / 2,
                                boxSize.x, boxSize.y);

        GUI.Box(boxRect, "");
    }
}








//using UnityEngine;

//public class CameraControl : MonoBehaviour {
//    public float zoomFactor = 1f;
//    private Camera cam;
//    public Vector2 offset;

//    private static readonly Vector2 DefaultSensorSize = new Vector2(36f, 24f);

//    void Start() {
//        cam = GetComponent<Camera>();
//    }

//    void Update() {
//        UpdateCamera();
//    }

//    void UpdateCamera() {
//        Vector2 newSensorSize = new Vector2(DefaultSensorSize.x / zoomFactor, DefaultSensorSize.y / zoomFactor);
//        float aspectRatio = cam.aspect;
//        newSensorSize.y = newSensorSize.x / aspectRatio;

//        cam.sensorSize = newSensorSize;

//        // Calculate lens shift
//        cam.lensShift = new Vector2(
//            (newSensorSize.x - DefaultSensorSize.x) / (2 * newSensorSize.x) + ((offset.x / Screen.width) * zoomFactor), // Horizontal shift
//            (newSensorSize.y - DefaultSensorSize.y) / (2 * newSensorSize.y) + ((offset.y / Screen.height) * zoomFactor) // Vertical shift
//        );
//    }
//}



//using UnityEngine;

//public class CameraControl : MonoBehaviour {
//    public float zoomFactor = 1f; // Zoom factor
//    public Vector2 offset;
//    private Camera cam;

//    // Default sensor size constant (full-frame sensor size, typically 36mm x 24mm)
//    private static readonly Vector2 DefaultSensorSize = new Vector2(36f, 24f);

//    void Start() {
//        cam = GetComponent<Camera>();
//    }

//    void Update() {
//        UpdateCamera();
//    }

//    void UpdateCamera() {
//        // Calculate the new sensor size based on zoomFactor
//        Vector2 newSensorSize = new Vector2(DefaultSensorSize.x / zoomFactor, DefaultSensorSize.y / zoomFactor);
//        cam.sensorSize = newSensorSize;

//        // Calculate the lens shift to keep the bottom-left corner anchored
//        cam.lensShift = new Vector2(
//            (newSensorSize.x - DefaultSensorSize.x) / (2 * newSensorSize.x) + ((offset.x / Screen.width) * zoomFactor), // Horizontal shift to the right
//            (newSensorSize.y - DefaultSensorSize.y) / (2 * newSensorSize.y)  // Vertical shift up
//        );
//    }
//}

