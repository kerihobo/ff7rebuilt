using CharacterTypes;
using TMPro;
using UnityEngine;

public class StatusMenu0 : StatusMenu {
    [SerializeField] private MateriaMenuSlotsMain materiaSlots;
    [SerializeField] private MateriaMenuCheckCommand commandWindow;
    [SerializeField] private TextMeshProUGUI txtAccessoryName;
    [Header("Primary")]
    [SerializeField] private TextMeshProUGUI txtStrength;
    [SerializeField] private TextMeshProUGUI txtDexterity;
    [SerializeField] private TextMeshProUGUI txtVitality;
    [SerializeField] private TextMeshProUGUI txtMagic;
    [SerializeField] private TextMeshProUGUI txtSpirit;
    [SerializeField] private TextMeshProUGUI txtLuck;
    [Header("Derived")]
    [SerializeField] private TextMeshProUGUI txtAttack;
    [SerializeField] private TextMeshProUGUI txtAttackPercent;
    [SerializeField] private TextMeshProUGUI txtDefense;
    [SerializeField] private TextMeshProUGUI txtDefensePercent;
    [SerializeField] private TextMeshProUGUI txtMagicAttack;
    [SerializeField] private TextMeshProUGUI txtMagicDefense;
    [SerializeField] private TextMeshProUGUI txtMagicDefensePercent;
    private ScreenFieldStatus screenFieldStatus;

    public override void Initialize(ScreenFieldStatus _screenFieldStatus) {
        screenFieldStatus = _screenFieldStatus;

        materiaSlots.Initialize(null);
        commandWindow.Initialize(null);
    }

    public override void Refresh(Player _player) {
        materiaSlots.RefreshEquipment(_player);
        materiaSlots.RefreshMateria(_player);
        commandWindow.Refresh(_player);

        txtAccessoryName.text = _player.accessory?.name;

        txtStrength.text            = _player.totalPrimary.strength.ToString();
        txtDexterity.text           = _player.totalPrimary.dexterity.ToString();
        txtVitality.text            = _player.totalPrimary.vitality.ToString();
        txtMagic.text               = _player.totalPrimary.magic.ToString();
        txtSpirit.text              = _player.totalPrimary.spirit.ToString();
        txtLuck.text                = _player.totalPrimary.luck.ToString();
        txtAttack.text              = _player.Attack.ToString();
        txtAttackPercent.text       = _player.AttackPercent.ToString();
        txtDefense.text             = _player.Defense.ToString();
        txtDefensePercent.text      = _player.MagicDefensePercent.ToString();
        txtMagicAttack.text         = _player.MagicAttack.ToString();
        txtMagicDefense.text        = _player.MagicDefense.ToString();
        txtMagicDefensePercent.text = _player.MagicDefensePercent.ToString();
    }

    public override void Show() {
        gameObject.SetActive(true);
    }

    public override void Hide() {
        gameObject.SetActive(false);
    }
}
