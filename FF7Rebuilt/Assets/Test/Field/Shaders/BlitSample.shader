Shader "Hidden/BlitSample" {
    Properties {
        _SampleRect ("Sample Rect", Vector) = (0,0,1,1)
        _ZoomParams ("Zoom Parameters", Vector) = (1,1,0,0)
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    SubShader {
        Tags { "RenderType" = "Opaque" }
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
            };

            uniform sampler2D _MainTex;
            uniform float4 _SampleRect;
            uniform float4 _ZoomParams;

            v2f vert (appdata_t v) {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            half4 frag (v2f i) : SV_Target {
                // Apply zoom and sampling
                // float2 uv = (i.uv - _SampleRect.xy) * _ZoomParams.xy + _SampleRect.xy;
                float2 uv = (i.uv * _ZoomParams.xy) + _SampleRect.xy;
                return tex2D(_MainTex, uv);
            }
            ENDCG
        }
    }
    Fallback "Diffuse"
}
