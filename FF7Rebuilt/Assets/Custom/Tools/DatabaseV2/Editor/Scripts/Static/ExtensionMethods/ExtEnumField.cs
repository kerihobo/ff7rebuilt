using UnityEngine.UIElements;

public static class ExtEnumField {
    public static EnumField SetMateriaType(this EnumField _self, MateriaType _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetMateriaGrowthType(this EnumField _self, MateriaGrowthType _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetGender(this EnumField _self, Gender _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetWeaponType(this EnumField _self, WeaponType _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetSpecialAccessoryType(this EnumField _self, SpecialAccessoryType _value) {
        _self.value = _value;
        return _self;
    }

    //public static EnumField SetIndependentEffectType(this EnumField _self, IndependentManager.Type _value) {
    //    _self.value = _value;
    //    return _self;
    //}

    //public static EnumField SetSupportEffectType(this EnumField _self, SupportManager.Type _value) {
    //    _self.value = _value;
    //    return _self;
    //}

    //public static EnumField SetCommandIndex(this EnumField _self, CommandIndex _value) {
    //    _self.value = _value;
    //    return _self;
    //}

    public static EnumField SetCollectibleType(this EnumField _self, SelectionCollectible.CollectibleType _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetRestoreType(this EnumField _self, RestoreType _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetStatusEffectAction(this EnumField _self, StatusEffectManager.StatusEffectAction _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetMagicOrder(this EnumField _self, Ability.MagicOrder _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetAbilityType(this EnumField _self, AbilityType _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetItemFieldApplication(this EnumField _self, ItemFieldApplication _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetItemSourceType(this EnumField _self, ItemSourceType _value) {
        _self.value = _value;
        return _self;
    }

    public static EnumField SetElementalDefense(this EnumField _self, ElementalAffinityManager.DamageModifier _value) {
        _self.value = _value;
        return _self;
    }
}
