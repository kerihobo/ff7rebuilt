﻿using UnityEngine;
using System;
using System.Collections.Generic;
using InventoryTypes;

namespace FFVII.Database {
    [Serializable]
    [CreateAssetMenu(fileName = "New Key Items", menuName = "Kerihobo/Database/Inventory/Key Items")]
    public class DBKeyItems : Database {
        public List<KeyItem> keyItemsList = new List<KeyItem>();
    }
}