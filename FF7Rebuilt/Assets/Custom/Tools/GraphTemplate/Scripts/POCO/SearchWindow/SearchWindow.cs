﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class SearchWindow {
    public SearchWindow(NodeGraph _selectedNodeGraph) {
        selectedNodeGraph = _selectedNodeGraph;
        Populate();
    }

    [SerializeReference]
    public NodeGraph selectedNodeGraph;

    public bool isOpen { get; set; }
    public string currentSearchPath { get; set; }

    protected Dictionary<string, SearchEntry> dictSearchEntries { get; set; }
    
    private string searchQuery = "";
    private string[] matchingEntries;
    private List<SearchEntryButton> searchEntryButtons;
    private Rect rect;
    private Vector2 pan;
    private Vector2 listScroll;

    public Dictionary<string, SearchEntry> GetSearchEntries() {
        if (dictSearchEntries == null) {
            Populate();
        }

        return dictSearchEntries;
    }

    protected virtual void Populate() {
        dictSearchEntries = new Dictionary<string, SearchEntry>() {
            { "Print"       , new SearchEntry("Print"                   , () => AddNode<NodePrint>(Node.Category.None)       )},
            { "Bool"        , new SearchEntry("Input/Bool"              , () => AddNode<NodeBool>(Node.Category.Input)       )},
            { "Int"         , new SearchEntry("Input/Int"               , () => AddNode<NodeInt>(Node.Category.Input)        )},
            { "And"         , new SearchEntry("Logic/And"               , () => AddNode<NodeAnd>(Node.Category.Logic)        )},
            { "Comparison"  , new SearchEntry("Logic/Comparison"        , () => AddNode<NodeComparison>(Node.Category.Logic) )},
            { "If"          , new SearchEntry("Logic/If"                , () => AddNode<NodeIf>(Node.Category.Logic)         )},
            { "Not"         , new SearchEntry("Logic/Not"               , () => AddNode<NodeNot>(Node.Category.Logic)        )},
            { "Or"          , new SearchEntry("Logic/Or"                , () => AddNode<NodeOr>(Node.Category.Logic)         )},
            { "Absolute"    , new SearchEntry("Maths/Advanced/Absolute" , () => AddNode<NodeAbsolute>(Node.Category.Maths)   )},
            { "Modulo"      , new SearchEntry("Maths/Advanced/Modulo"   , () => AddNode<NodeModulo>(Node.Category.Maths)     )},
            { "Add"         , new SearchEntry("Maths/Basic/Add"         , () => AddNode<NodeAdd>(Node.Category.Maths)        )},
            { "Divide"      , new SearchEntry("Maths/Basic/Divide"      , () => AddNode<NodeDivide>(Node.Category.Maths)     )},
            { "Multiply"    , new SearchEntry("Maths/Basic/Multiply"    , () => AddNode<NodeMultiply>(Node.Category.Maths)   )},
            { "Subtract"    , new SearchEntry("Maths/Basic/Subtract"    , () => AddNode<NodeSubtract>(Node.Category.Maths)   )},
            { "Clamp"       , new SearchEntry("Maths/Range/Clamp"       , () => AddNode<NodeClamp>(Node.Category.Maths)      )},
            { "Max"         , new SearchEntry("Maths/Range/Max"         , () => AddNode<NodeMax>(Node.Category.Maths)        )},
            { "Min"         , new SearchEntry("Maths/Range/Min"         , () => AddNode<NodeMin>(Node.Category.Maths)        )},
            { "Random Range", new SearchEntry("Maths/Range/Random Range", () => AddNode<NodeRandomRange>(Node.Category.Maths))}
        };
    }

    protected void AddNode<T>(Node.Category _category) where T : Node {
        Activator.CreateInstance(typeof(T), selectedNodeGraph, rect.position - pan, _category, true);
    }

    public void Open(Vector2 _position, Vector2 _pan) {
        rect = new Rect(_position, new Vector2(200, 250));
        pan = _pan;
        currentSearchPath = "";
        searchEntryButtons = new List<SearchEntryButton>();

        List<string> namesInCurrentPath = new List<string>();
        foreach (KeyValuePair<string, SearchEntry> kvp in GetSearchEntries()) {
            string buttonTitle = kvp.Value.path.Split('/')[0];
            if (!namesInCurrentPath.Contains(buttonTitle)) {
                namesInCurrentPath.Add(buttonTitle);
                searchEntryButtons.Add(new SearchEntryButton(this, buttonTitle));
            }
        }

        isOpen = true;
    }

    public void Close() {
        isOpen = false;
    }

    public void Render(Event _e, StyleSet _styleSet) {
        if (!isOpen) return;

        if (searchQuery == null) searchQuery = "";

        GUILayout.BeginArea(rect, _styleSet.searchWindowBG);
            GUILayout.Label("Create Node", _styleSet.searchTitle);

            GUILayout.BeginHorizontal();
                GUILayout.Box("", _styleSet.searchIcon, GUILayout.Width(20));
                EditorGUI.BeginChangeCheck();
                    searchQuery = GUILayout.TextField(searchQuery);
                if (EditorGUI.EndChangeCheck()) {
                    if (searchQuery.Length > 0) {
                        matchingEntries = GetSearchEntries().Keys.Where(x => x.ToLower().Contains(searchQuery.ToLower())).ToArray();
                    }
                }
            GUILayout.EndHorizontal();

            GUILayout.BeginVertical(_styleSet.searchWindowListFrame);
                GUILayout.BeginVertical(_styleSet.searchWindowList);
                    listScroll = GUILayout.BeginScrollView(listScroll);
                        if (searchQuery.Length == 0) {
                            RenderTreeView(_styleSet);
                        } else {
                            foreach (string s in matchingEntries) {
                                GUILayout.BeginHorizontal(_styleSet.searchEntry, GUILayout.Height(20));
                                    if (GUILayout.Button(s, _styleSet.searchEntryButton, GUILayout.Height(20))) {
                                        GetSearchEntries()[s].action();
                                        Close();
                                    }
                                GUILayout.EndHorizontal();
                            }
                        }
                    GUILayout.EndScrollView();
                GUILayout.EndVertical();
            GUILayout.EndVertical();
        GUILayout.EndArea();

        if (_e.type == EventType.MouseDown && _e.button == 0 || _e.button == 2) {
            if (!rect.Contains(_e.mousePosition)) {
                Close();
            }
        }

        // HACK: This could cause problems later.
        GUIUtility.ExitGUI();
    }

    private void RenderTreeView(StyleSet _styleSet) {
        List<SearchEntryButton> newSearchEntryButtons = null;

        if (currentSearchPath.Length > 0 && GUILayout.Button("<- " + currentSearchPath, _styleSet.searchBackButton, GUILayout.Height(20))) {
            string[] elements = currentSearchPath.Split('/');
            string targetSearchPath = "";
            for (int i = 0; i < elements.Length - 1; i++) {
                Debug.Log(elements[i]);

                if (i > 0) {
                    targetSearchPath += "/";
                }
                targetSearchPath += elements[i];
            }

            currentSearchPath = targetSearchPath;
            searchEntryButtons = new List<SearchEntryButton>(GetNewChoiceList());
        }

        foreach (SearchEntryButton btn in searchEntryButtons) {
            newSearchEntryButtons = btn.Render(_styleSet);
            if (newSearchEntryButtons != null) {
                break;
            }
        }

        if (newSearchEntryButtons != null) {
            searchEntryButtons = new List<SearchEntryButton>(newSearchEntryButtons);
        }
    }

    public List<SearchEntryButton> GetNewChoiceList() {
        KeyValuePair<string, SearchEntry>[] categories = GetSearchEntries().Where(x => x.Value.path.ToLower().Contains(currentSearchPath.ToLower())).ToArray();
        List<string> uniqueEntries = new List<string>();
        foreach (KeyValuePair<string, SearchEntry> kvp in categories) {
            string item = kvp.Value.path.Substring(currentSearchPath.Length > 0 ? kvp.Value.path.IndexOf(currentSearchPath) + currentSearchPath.Length + 1 : 0);
            if (item.Contains('/')) {
                item = item.Split('/')[0];
            }

            if (!uniqueEntries.Contains(item)) {
                Debug.Log(item);
                uniqueEntries.Add(item);
            }
        }

        List<SearchEntryButton> newSearchEntryButtons = new List<SearchEntryButton>();
        foreach (string s in uniqueEntries) {
            newSearchEntryButtons.Add(new SearchEntryButton(this, s));
        }

        Debug.Log(newSearchEntryButtons.Count);
        return newSearchEntryButtons;
    }
}
