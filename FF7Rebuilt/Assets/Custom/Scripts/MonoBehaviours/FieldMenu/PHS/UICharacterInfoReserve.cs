using CharacterTypes;
using UnityEngine;
using UnityEngine.UI;

public class UICharacterInfoReserve : MonoBehaviour {
    public Player player { get; set; }
    public Image imgAvatar { get; set; }
    
    private CanvasGroup cnvGrp;

    public void Initialize() {
        imgAvatar = GetComponent<Image>();
        cnvGrp = GetComponent<CanvasGroup>();
    }

    public void Refresh(Player _player) {
        player = _player;
        cnvGrp.alpha = player != null ? 1 : 0;

        if (player == null) {
            Clear();
            return;
        }
        
        imgAvatar.sprite = _player.avatar;
    }

    private void Clear() {
        imgAvatar.sprite = null;
    }
}
